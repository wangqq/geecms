from insurerManagement.models import Insurer, InsurerHistory
from insurerManagement.forms import InsurerForm
from employeeManagement.models import AuthUser

from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.response import Response
from static.common.constant import Constant
from rest_framework.authentication import SessionAuthentication
from utils.auth import ExpiringTokenAuthentication
from rest_framework.permissions import IsAuthenticated

from django.db import transaction, IntegrityError
from geeCMS.settings import proDb


# Create your views here.


@api_view(['GET'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def index(request):
    if request.GET.get('name'):
        items = Insurer.objects.using(proDb).filter(name__icontains=request.GET.get('name'))
    else:
        items = Insurer.objects.using(proDb).all()

    result = []
    for item in items:
        try:
            auth_user = AuthUser.objects.get(id=item.created_by_id)
            username = auth_user.username,
        except AuthUser.DoesNotExist:
            username = ''
        app_result = {
            'name': item.name,
            'insurance_num': item.insurance_num,
            'username': username,
            'id': item.id
        }
        result.append(app_result)

    return Response(result)


@api_view(['POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def create(request):
    form = InsurerForm(request.data)
    if request.method == 'POST':
        if form.is_valid():
            user_id = request.user.id
            obj = Insurer(
                name=form.cleaned_data.get('name'),
                created_by_id=user_id,
                updated_by_id=user_id
            )
            obj.save(using=proDb)
            return Response({'code': Constant.SUCCESS, 'message': Constant.SUCCESS_MESSAGE})
        else:
            return Response({'code': Constant.INVAILD, 'message': form.errors})


@api_view(['GET', 'POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def update(request, id):
    obj = Insurer.objects.using(proDb).get(id=id)
    his_insurer = Insurer.objects.using(proDb).get(id=id)
    user_id = request.user.id
    if request.method == 'POST':
        form = InsurerForm(request.data, instance=obj)
        if form.is_valid():
            try:
                with transaction.atomic(using=proDb):
                    # 历史数据
                    his_obj = InsurerHistory(
                        fid=his_insurer.id,
                        name=his_insurer.name,
                        insurance_num=his_insurer.insurance_num,
                        created_by_id=his_insurer.created_by_id,
                        updated_by_id=his_insurer.updated_by_id,
                        created_at=his_insurer.created_at,
                        updated_at=his_insurer.updated_at,
                        action=Constant.ACTION_UPDATE,
                        his_created_by=int(user_id)
                    )
                    his_obj.save(using=proDb)
                    # 更新
                    obj.save(using=proDb, update_fields=['name', 'updated_by_id', 'updated_at'])
                    return Response({'code': Constant.SUCCESS, 'message': Constant.SUCCESS_MESSAGE})
            except IntegrityError:
                return Response({'code': Constant.FAIL, 'message': Constant.FAIL_MESSAGE})
        else:
            return Response({'code': Constant.INVAILD, 'message': form.errors})

    result = {
        'name': obj.name
    }
    return Response(result)
