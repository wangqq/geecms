from django import forms
from django.contrib.auth import (
    password_validation,
)
from insurerManagement.models import Insurer
from django.contrib.admin import widgets
from django.core.exceptions import ValidationError
from geeCMS.settings import proDb


class InsurerForm(forms.ModelForm):
    name = forms.CharField(
        label=u"名称",
        widget=forms.TextInput()
    )

    class Meta:
        model = Insurer
        fields = ['name']

    def clean_name(self):
        cleaned_data = super(InsurerForm, self).clean()
        name = cleaned_data.get('name')
        exist_check = Insurer.objects.using(proDb).filter(name = name).count()
        if exist_check > 0:
            self._errors['name'] = self.error_class(["【" + name + "】,已经存在。"])
        return name

