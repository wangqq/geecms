from django.apps import AppConfig


class InsurermanagementConfig(AppConfig):
    name = 'insurerManagement'
