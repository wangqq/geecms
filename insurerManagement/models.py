from django.db import models
from employeeManagement.models import AuthUser
from geeCMS.settings import proDb


# Create your models here.


class Insurer(models.Model):
    name = models.CharField(max_length=128)
    insurance_num = models.IntegerField(default=0)
    created_by_id = models.IntegerField()
    updated_by_id = models.IntegerField()
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        app_label = proDb
        db_table = "insurer"


class InsurerHistory(models.Model):
    fid = models.IntegerField()
    name = models.CharField(max_length=128)
    insurance_num = models.IntegerField(default=0)
    created_by_id = models.IntegerField()
    updated_by_id = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    action = models.CharField(max_length=50)
    his_created_by = models.IntegerField()
    his_created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        app_label = proDb
        db_table = "insurer_his"
