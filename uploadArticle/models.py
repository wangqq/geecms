from django.db import models

# Create your models here.


# 文章表
class Article(models.Model):
    media_id = models.CharField(max_length=255)
    type_id = models.IntegerField()
    content = models.TextField()
    content_wechat = models.TextField()
    introduction = models.TextField()
    thumbnail = models.CharField(max_length=200)
    thumbnail_media_id = models.CharField(max_length=100)
    title = models.CharField(max_length=255)
    browse_num = models.IntegerField()
    share_num = models.IntegerField()
    message_num = models.IntegerField()
    creator_id = models.IntegerField()
    status = models.IntegerField()
    release_time = models.DateTimeField()
    upload_wechat_time = models.DateTimeField()
    reason = models.TextField()
    createtime = models.DateTimeField(auto_now_add=True)
    updatetime = models.DateTimeField(auto_now=True)
    updated_by = models.IntegerField()

    class Meta:
        db_table = 'z_article'


# 文章历史表
class ArticleHis(models.Model):
    media_id = models.CharField(max_length=255)
    fid = models.IntegerField()
    type_id = models.IntegerField()
    content = models.TextField()
    content_wechat = models.TextField()
    introduction = models.TextField()
    thumbnail = models.CharField(max_length=200)
    thumbnail_media_id = models.CharField(max_length=100)
    title = models.CharField(max_length=255)
    browse_num = models.IntegerField()
    share_num = models.IntegerField()
    message_num = models.IntegerField()
    creator_id = models.IntegerField()
    status = models.IntegerField()
    release_time = models.DateTimeField()
    upload_wechat_time = models.DateTimeField()
    reason = models.TextField()
    createtime = models.DateTimeField()
    updatetime = models.DateTimeField()
    updated_by = models.IntegerField()
    action = models.CharField(max_length=50)
    his_created_by = models.IntegerField()
    his_created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
            db_table = 'z_article_his'


# 文章状态表
class ArticleStatus(models.Model):
    id = models.IntegerField(primary_key=True)
    description = models.CharField(max_length=100)
    is_active = models.IntegerField()

    class Meta:
        db_table = 'z_article_status'


# 文章类型表
class ArticleType(models.Model):
    id = models.IntegerField(primary_key=True)
    description = models.CharField(max_length=100)
    display_order = models.IntegerField()
    is_active = models.IntegerField()

    class Meta:
        db_table = 'z_article_type'


# 公众号token表
class WxAccessToken(models.Model):
    appid = models.CharField(max_length=255)
    access_token = models.TextField()
    expires_in = models.IntegerField()
    last_time = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'wx_access_token'


# 图片推送微信记录表
class ArticleImgWechat(models.Model):
    article_id = models.IntegerField()
    src = models.CharField(max_length=255)
    wechat_url = models.CharField(max_length=255)

    class Meta:
        db_table = 'z_article_img_wechat'


# 图片推送微信记录表
class ArticleCode(models.Model):
    user_id = models.IntegerField()
    code = models.CharField(max_length=100)
    create_time = models.DateTimeField(auto_now_add=True)
    is_active = models.IntegerField()

    class Meta:
        db_table = 'z_article_code'


# 图片推送微信记录表
class ArticleResultReason(models.Model):
    user_id = models.IntegerField()
    reason = models.TextField()
    create_time = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'z_article_recall_reason'



