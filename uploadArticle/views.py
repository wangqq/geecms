from uploadArticle.models import Article, ArticleType, ArticleStatus, ArticleImgWechat, ArticleCode, ArticleResultReason
from employeeManagement.models import AuthUser
import re, datetime, time, requests, json

from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.response import Response
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from utils.auth import ExpiringTokenAuthentication

from django.conf import settings
from django.db import transaction, IntegrityError

from static.common.cousterFunction import list_fetch_key, dict_fetchall, get_access_token, upload_img_wechat, \
    upload_img_text_wechat, modify_img_text_wechat, get_wechat_user, mass_message, send_code_wechat, gen_password, get_str_time, upload_img_text_message_wechat, mass_message_tag
from static.common.constant import *
from static.common.HistoryData import fun_z_article_his
from static.common.log import Logger
from django.views.decorators.csrf import csrf_exempt

# 图片的路径
ARTICLE_IMG_PATH = '/var/www/html/article/article_admin/upload/attach/'
#ARTICLE_IMG_PATH = '/var/www/html/wx_app/article_admin/upload/attach/'

# Create your views here.
@api_view(['GET'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def index(request):
    # 文章类型
    articleResult = ArticleType.objects.filter(is_active=Constant.STATUS_ACTIVE).values_list('id', 'description').order_by('display_order')
    articleTypeList = list_fetch_key(articleResult)
    articleTypeDict = dict_fetchall(articleResult)
    # 文章显示的状态
    articleStatusList = list_fetch_key(ArticleStatus.objects.filter(is_active=Constant.STATUS_ACTIVE).values_list('id', 'description'))
    result = []
    for row_status in articleStatusList:
        items = Article.objects.order_by('-id').filter(status=row_status['id']).all()

        if request.GET.get('timeStart'):
            items = items.filter(release_time__gte=(request.GET.get('timeStart')) + ' 00:00:00')
        if request.GET.get('timeEnd'):
            items = items.filter(release_time__lte=(request.GET.get('timeEnd')) + ' 23:59:59')
        if request.GET.get('type'):
            items = items.filter(type_id=request.GET.get('type'))
        # 未发布
        v_result = []
        for item in items:
            if item.release_time:
                release_time = datetime.datetime.strftime(item.release_time, '%Y/%m/%d')
            else:
                release_time = '--'
            v_result.append({
                'id': item.id,
                'thumbnail': item.thumbnail,
                'title': item.title,
                'status_id': row_status['id'],
                'status': row_status['name'],
                'type': articleTypeDict[item.type_id],
                'release_time': release_time
            })

        result.append({
            'status_id': row_status['id'],
            'status': row_status['name'],
            'data': v_result
        })

    return Response({'result': result, 'articleTypeList': articleTypeList,  'articleStatusList': articleStatusList})


# 状态撤回
@api_view(['POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def update_status(request):
    user_id = request.user.id
    id = int(request.data.get('id'))
    reason = request.data.get('reason')
    # 文章查询
    try:
        with transaction.atomic():
            article = Article.objects.get(id=id)
            # 判断当前文章是否处于审核中
            if article.status == Constant.ARTICLE_STATUS_IN_REVIEW:
                # 历史记录
                return_bind_his = fun_z_article_his(article, Constant.ACTION_UPDATE, user_id)
                if return_bind_his == Constant.FAIL:
                    raise IntegrityError
                # 状态改为创作中
                article.reason = reason
                article.status = Constant.ARTICLE_STATUS_CREATING
                article.updated_by = user_id
                # 保存
                article.save()
                # 存储
                reason_model = ArticleResultReason(
                    user_id=user_id,
                    reason=reason
                )
                reason_model.save()
                return Response({'code': Constant.REQUEST_SUCCESS_CODE, 'message': Constant.REQUEST_SUCCESS_MSG})
            else:
                return Response({'code': Constant.DATA_INVAILD_CODE, 'message': '文章状态有误'})
    except Article.DoesNotExist:
        return Response({'code': Constant.SQL_DATA_NOT_EXIST_CODE, 'message': Constant.SQL_DATA_NOT_EXIST_MSG})
    except IntegrityError as err:
        # 输出错误日志
        log_msg = "update_status执行失败：" + str(err)
        Logger('article', level='error').logger.error(log_msg)
        return Response({'code': Constant.REQUEST_FAIL_CODE, 'message': Constant.REQUEST_FAIL_MSG})


# 预览接口
@api_view(['POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def article_preview(request):
    user_id = request.user.id
    id = int(request.data.get('id'))
    wechat_id = request.data.get('wechat_id')
    # 获取media_id
    media_id_result = get_wechat_media_id(user_id, id)
    if media_id_result['code'] == Constant.REQUEST_FAIL_CODE:
        return Response({'code': Constant.REQUEST_FAIL_CODE, 'message': media_id_result['errmsg']})

    media_id = media_id_result['media_id']

    # 获得ACCESS_TOKEN
    access_token_result = get_access_token(settings.APP_ID, settings.APP_SECRET)
    if access_token_result['code'] == Constant.REQUEST_FAIL_CODE:
        return Response({'code': Constant.REQUEST_FAIL_CODE, 'message': access_token_result['errmsg']})

    access_token = access_token_result['access_token']
    param = {
        'access_token': access_token
    }
    # 预览接口
    preview_url = "https://api.weixin.qq.com/cgi-bin/message/mass/preview"
    data = {
           "towxname": wechat_id,
           "mpnews": {
             "media_id": media_id
            },
           "msgtype": "mpnews"
        }

    r = requests.post(url=preview_url, params=param, data=json.dumps(data))
    result = r.json()
    if result['errcode'] == 0:
        return Response({'code': Constant.REQUEST_SUCCESS_CODE, 'message': result['errmsg']})
    else:
        return Response({'code': Constant.REQUEST_FAIL_CODE, 'message': result['errmsg']})


# 推送验证码
@api_view(['GET'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def article_send_code(request):
    user_id = request.user.id
    # 获取用户openid
    auth_user = AuthUser.objects.get(id=user_id)
    openid = auth_user.openid
    # 获得验证码
    code = gen_password(Constant.ARTICLE_CODE_LENGTH)
    # 发送验证码
    result = send_code_wechat(openid, auth_user.username, code)

    if result['code'] == Constant.REQUEST_SUCCESS_CODE:
        # 存储
        code_model = ArticleCode(
            user_id=user_id,
            code=code,
            is_active=Constant.STATUS_ACTIVE
        )
        code_model.save()

        return Response({'code': Constant.REQUEST_SUCCESS_CODE, 'message': Constant.REQUEST_SUCCESS_MSG})
    else:
        return Response({'code': Constant.REQUEST_FAIL_CODE, 'message': result['errmsg']})


# 发布接口
@api_view(['POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def article_release(request):
    user_id = request.user.id
    id = int(request.data.get('id'))
    # 验证码
    code = request.data.get('code')
    # 验证验证码是否有效
    try:
        codeCheck = ArticleCode.objects.get(user_id=user_id, code=code, is_active=Constant.STATUS_ACTIVE)
        # 判断时长是否有效
        surplus_time = time.time() - get_str_time(str(codeCheck.create_time))
        if surplus_time > Constant.ARTICLE_CODE_VALID_TIME:
            return Response({'code': Constant.REQUEST_FAIL_CODE, 'message': '验证码无效'})
        else:
            # 获取media_id
            media_id_result = get_wechat_media_id(user_id, id)
            if media_id_result['code'] == Constant.REQUEST_FAIL_CODE:
                return Response({'code': Constant.REQUEST_FAIL_CODE, 'message': media_id_result['errmsg']})

            media_id = media_id_result['media_id']
            result_mass = mass_message_tag(media_id)
            # 群发成功时,更新发布时间
            if result_mass['code'] == Constant.REQUEST_SUCCESS_CODE:
                article = Article.objects.get(id=id)  # 历史记录
                return_bind_his = fun_z_article_his(article, Constant.ACTION_UPDATE, user_id)
                if return_bind_his == Constant.FAIL:
                    raise IntegrityError
                # 状态改为创作中
                article.status = Constant.ARTICLE_STATUS_RELEASE
                article.release_time = datetime.datetime.now()
                article.updated_by = user_id
                # 保存
                article.save()
                # 修改code的状态
                codeCheck.is_active = Constant.STATUS_UNACTIVE
                codeCheck.save()
                return Response({'code': Constant.REQUEST_SUCCESS_CODE, 'message': Constant.REQUEST_SUCCESS_MSG})
            else:
                return Response({'code': Constant.REQUEST_FAIL_CODE, 'message': result_mass['errmsg']})
            # 获取公众号用户列表
            # openid_list_result = get_wechat_user()
            # 获取成功时，群发
            # if openid_list_result['code'] == Constant.REQUEST_SUCCESS_CODE:
            #     openid_list = openid_list_result['result']
            #     openid = openid_list['data']['openid']
            #     # openid = ['oIm-P1PrCmQpGRmc1n3T8BYfM9F8' , 'oIm-P1KlX7KorOlKJNzGFYtC3s1o'
            #     result_mass = mass_message(openid, media_id)
            #     # 群发成功时,更新发布时间
            #     if result_mass['code'] == Constant.REQUEST_SUCCESS_CODE:
            #         article = Article.objects.get(id=id) # 历史记录
            #         return_bind_his = fun_z_article_his(article, Constant.ACTION_UPDATE, user_id)
            #         if return_bind_his == Constant.FAIL:
            #             raise IntegrityError
            #         # 状态改为创作中
            #         article.status = Constant.ARTICLE_STATUS_RELEASE
            #         article.release_time = datetime.datetime.now()
            #         article.updated_by = user_id
            #         # 保存
            #         article.save()
            #         # 修改code的状态
            #         codeCheck.is_active = Constant.STATUS_UNACTIVE
            #         codeCheck.save()
            #         return Response({'code': Constant.REQUEST_SUCCESS_CODE, 'message': Constant.REQUEST_SUCCESS_MSG})
            #     else:
            #         return Response({'code': Constant.REQUEST_FAIL_CODE, 'message': result_mass['errmsg']})
            # else:
            #     # 获取失败时，返回信息
            #     return Response({'code': Constant.REQUEST_FAIL_CODE, 'message': openid_list_result['errmsg']})
    except ArticleCode.DoesNotExist:
        return Response({'code': Constant.REQUEST_FAIL_CODE, 'message': '验证码无效'})


# 文章的内部图片替换
def get_wechat_media_id(user_id, id):
    errmsg = ''
    try:
        article = Article.objects.get(id=id)
        # 判断文章的状态
        if article.status == Constant.ARTICLE_STATUS_IN_REVIEW:
            # 要替换的字符串
            content_wechat = article.content
            # 查询文章中的图片
            img_list = re.findall(r'="upload/attach/(.*?)"', article.content, re.M | re.S)
            # 有图片，先上传图片
            if img_list:
                # 替换图片
                for row_img in img_list:
                    # 判断图片是否已经推送过
                    try:
                        # 已经推送过不在处理
                        img_wechat = ArticleImgWechat.objects.get(src=row_img)
                        wechat_url = img_wechat.wechat_url
                    except ArticleImgWechat.DoesNotExist:
                        # 文件存储的绝对路径
                        v_path = ARTICLE_IMG_PATH + row_img
                        # 推送微信
                        wechat_url_result = upload_img_wechat(v_path)
                        if wechat_url_result['code'] != Constant.REQUEST_SUCCESS_CODE:
                            errmsg = wechat_url_result['errmsg']
                            return {'code': Constant.REQUEST_FAIL_CODE, 'errmsg': errmsg}
                        wechat_url = wechat_url_result['url']
                        # 存储到表里
                        wechat_img_model = ArticleImgWechat(
                           article_id=id,
                           src=row_img,
                           wechat_url=wechat_url
                        )
                        wechat_img_model.save()

                    # 替换微信里的
                    replace_old = 'upload/attach/' + row_img
                    replace_new = wechat_url
                    content_wechat = content_wechat.replace(replace_old, replace_new)
            else:
                # 无图片，直接上传消息
                pass
        else:
            return {'code': Constant.REQUEST_FAIL_CODE, 'errmsg': '文章状态有误'}

        with transaction.atomic():
                # 文章保存
                return_bind_his = fun_z_article_his(article, Constant.ACTION_UPDATE, user_id)
                if return_bind_his == Constant.FAIL:
                    raise IntegrityError
                article.content_wechat = content_wechat
                article.updated_by = user_id
                article.save()

                # 判断文章是否已经发布, 调用文章修改接口
                if article.media_id:
                    result_modify = modify_img_text_wechat(article)
                    if result_modify['code'] == Constant.REQUEST_FAIL_CODE:
                        # 错误时，返回
                        return {'code': Constant.REQUEST_FAIL_CODE, 'errmsg': result_modify['errmsg']}
                else:
                    # 发布图文消息
                    result_media_result = upload_img_text_wechat(article)
                    # 获取失败时，抛出异常
                    if result_media_result['code'] == Constant.REQUEST_FAIL_CODE:
                        errmsg = result_media_result['errmsg']
                        raise IntegrityError

                    result_media = result_media_result['result']
                    return_bind_his = fun_z_article_his(article, Constant.ACTION_UPDATE, user_id)
                    if return_bind_his == Constant.FAIL:
                        raise IntegrityError
                    article.media_id = result_media['media_id']
                    article.upload_wechat_time = datetime.datetime.now()
                    article.updated_by = user_id
                    article.save()

                # 返回media_id
                return {'code': Constant.REQUEST_SUCCESS_CODE, 'media_id': article.media_id}
    except Article.DoesNotExist:
        return Response({'code': Constant.REQUEST_FAIL_CODE, 'errmsg': Constant.SQL_DATA_NOT_EXIST_MSG})
    except IntegrityError as err:
        if not errmsg:
            errmsg = Constant.REQUEST_FAIL_MSG
        # 输出错误日志
        log_msg = "article_img_replace图片推送微信执行失败：" + str(err)
        Logger('article.log', level='error').logger.error(log_msg)
        return {'code': Constant.REQUEST_FAIL_CODE, 'errmsg': errmsg}



