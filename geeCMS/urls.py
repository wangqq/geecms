"""geeCMS URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/dev/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from customerManagement import views
from employeeManagement import views as auth_views
from cityManagement import views as city_views
from insuranceManagement import views as insurance_views
from insuranceManagement import views_score as score_views
from insuranceManagement import views_bind as bind_views
from insurerManagement import views as insurer_views
from profileManagement import views as profile_views
from dataProcessing import views as data_views
from uploadArticle import views as article_views

# 登录、退出、个人设置
urlpatterns = [
    url(r'^api/login$', profile_views.login),
    url(r'^api/logout$', profile_views.logout),
    url(r'^api/profileManagement/password_change$', profile_views.password_change),
    url(r'^api/profileManagement/home', profile_views.home),
]

# 城市管理url
urlpatterns += [
    url(r'^api/city$', city_views.index),
    url(r'^api/city/create$', city_views.create),
    url(r'^api/city/update/(?P<id>\d+)$', city_views.update),
    url(r'^api/city/social/(?P<id>\d+)$', city_views.index_social),
    url(r'^api/city/social/create$', city_views.create_social),
    url(r'^api/city/social/delete/(?P<id>\d+)$', city_views.delete_social),
    url(r'^api/city/social/info/(?P<id>\d+)$', city_views.social_info),
    url(r'^api/city/update_match/(?P<id>\d+)$', city_views.update_career_social_match),
]

# 系统用户管理
urlpatterns += [
    url(r'^api/auth/user/add/$', auth_views.create),
    url(r'^api/auth/user/(?P<id>\d+)/change/$', auth_views.change),
    url(r'^api/auth/user/', auth_views.index),
]

# 财务系统使用的用户团队添加
urlpatterns += [
    url(r'^api/customerManagement/user/add/$', views.create),
    url(r'^api/customerManagement/user/(?P<id>\d+)/change/$', views.update),
    url(r'^api/customerManagement/user/$', views.index),
    url(r'^api/customerManagement/user/(?P<id>\d+)/select_product/$', views.select_product),
]

# 保险公司管理
urlpatterns += [
    url(r'^api/insurerManagement/$', insurer_views.index),
    url(r'^api/insurerManagement/create', insurer_views.create),
    url(r'^api/insurerManagement/(?P<id>\d+)/update/$', insurer_views.update),
]

# 保险产品管理
urlpatterns += [
    url(r'^api/insuranceManagement$', insurance_views.index),
    url(r'^api/insuranceManagement/create', insurance_views.create),
    url(r'^api/insuranceManagement/(?P<id>\d+)/update/(?P<type>\d+)$', insurance_views.update),
    url(r'^api/insuranceManagement/upload_file$', insurance_views.upload_ratio_file),
    url(r'^api/insuranceManagement/ratio$', insurance_views.ratio),
    url(r'^api/insuranceManagement/(?P<id>\d+)/(?P<schedule_id>\d+)/(?P<user_id>\d+)/ratio_edit$', insurance_views.ratio_edit),
    url(r'^api/insuranceManagement/(?P<id>\d+)/(?P<schedule_id>\d+)/(?P<user_id>\d+)/ratio_edit2$', insurance_views.ratio_edit2),
    url(r'^api/insuranceManagement/schedule_edit', insurance_views.schedule_edit),
    url(r'^api/insuranceManagement/(?P<id>\d+)/(?P<schedule_id>\d+)/download_excel/(?P<type>\d+)$', insurance_views.download_excel),
    url(r'^api/insuranceManagement/schedule_delete$', insurance_views.schedule_delete),
    url(r'^api/insuranceManagement/product_delete$', insurance_views.product_delete),
    url(r'^api/insuranceManagement/deductible_delete$', insurance_views.deductible_delete),
    url(r'^api/insuranceManagement/comp_ratio_delete$', insurance_views.comp_ratio_delete),
]

# 产品评分
urlpatterns += [
    url(r'^api/insuranceManagement/product/score$', score_views.pro_score_init),
    url(r'^api/insuranceManagement/product/score/save$', score_views.pro_score_save),
]

# 产品绑定
urlpatterns += [
    url(r'^api/insuranceManagement/(?P<id>\d+)/bind$', bind_views.bind_index),
    url(r'^api/insuranceManagement/(?P<main_id>\d+)/(?P<sch_id>\d+)/bind/schedule$', bind_views.get_sch_relation),
    url(r'^api/insuranceManagement/bind/create$', bind_views.add_bind),
    url(r'^api/insuranceManagement/exclude/optional/right$', bind_views.get_sch_exclude_right),
    url(r'^api/insuranceManagement/exclude/optional/left$', bind_views.get_sch_exclude_left),
    url(r'^api/insuranceManagement/(?P<main_id>\d+)/exclude$', bind_views.set_sch_exclude),
    url(r'^api/insuranceManagement/bind/delete/(?P<id>\d+)$', bind_views.delete_bind),
    url(r'^api/insuranceManagement/bind/schedule/delete/(?P<id>\d+)$', bind_views.delete_sch_bind),
]

# 数据处理中心
urlpatterns += [
    url(r'^data/userUseInfo$', data_views.user_use_info),
    url(r'^data/overview/lively/day$', data_views.overview_active_day),
    url(r'^data/overview/lively/week$', data_views.overview_active_week),
    url(r'^data/overview/lively/month$', data_views.overview_active_month),
    url(r'^data/overview/login_visit/day$', data_views.overview_login_visit_day),
    url(r'^data/overview/login_visit/month$', data_views.overview_login_visit_month),
    url(r'^data/overview/register/day$', data_views.overview_register_day),
    url(r'^data/overview/register/month$', data_views.overview_register_month),
    url(r'^data/overview/course$', data_views.overview_level),
    url(r'^data/overview/video$', data_views.overview_video),
    url(r'^data/overview_video/level/one$', data_views.overview_video_level_one),
    url(r'^data/overview_video/level/(?P<level>\d+)$', data_views.overview_video_level),
    url(r'^data/user/video/(?P<id>\d+)$', data_views.video_user),
    url(r'^data/user/page/(?P<id>\d+)$', data_views.page_stay_time),
    url(r'^data/overview/channel$', data_views.overview_channel),
    url(r'^data/register/user', data_views.register_user),
]

# 文章
urlpatterns += [
    url(r'^article/index$', article_views.index),
    url(r'^article/recall$', article_views.update_status),
    url(r'^article/preview$', article_views.article_preview),
    url(r'^article/send_code$', article_views.article_send_code),
    url(r'^article/release', article_views.article_release),
]
