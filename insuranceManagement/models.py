from django.db import models
from insurerManagement.models import Insurer
from geeCMS.settings import proDb

# Create your models here.

PAYMENT_METHOD = ((1, '给付'), (2, '补偿'))
AMOUNT_TYPE = ((1, '固定'), (2, '不固定'))
CATEGORY_TYPE = ((0, '人寿险'), (1, '意外险'), (2, '医疗险'), (3, '重疾险'))

# 险种：医疗险（住院,手术,生育,重疾,门急诊,体检,疫苗,齿科,眼科,海外重疾,孕中险,意外医疗,恶性肿瘤）

CATEGORY = {
            17: '住院',
            18: '手术',
            19: '生育',
            20: '重疾',
            21: '门急诊',
            22: '体检',
            23: '疫苗',
            24: '齿科',
            25: '眼科',
            26: '海外重疾',
            27: '孕中险',
            28: '意外医疗',
            29: '恶性肿瘤'
            }

GUARANTEE_PERIOD = []
for i in range(1, 151):
    GUARANTEE_PERIOD.append((str(i), str(i)+'年'))


GUARANTEE_PERIOD.append(('960', '保至60岁'))
GUARANTEE_PERIOD.append(('965', '保至65岁'))
GUARANTEE_PERIOD.append(('966', '保至66岁'))
GUARANTEE_PERIOD.append(('970', '保至70岁'))
GUARANTEE_PERIOD.append(('975', '保至75岁'))
GUARANTEE_PERIOD.append(('977', '保至77岁'))
GUARANTEE_PERIOD.append(('980', '保至80岁'))
GUARANTEE_PERIOD.append(('985', '保至85岁'))
GUARANTEE_PERIOD.append(('988', '保至88岁'))
GUARANTEE_PERIOD.append(('9999', '终身'))


PAYMENT_PERIOD = []
for i in range(1, 151):
    PAYMENT_PERIOD.append((str(i), str(i)+'年'))
PAYMENT_PERIOD.append(('960', '至60岁'))
PAYMENT_PERIOD.append(('965', '至65岁'))
PAYMENT_PERIOD.append(('970', '至70岁'))

PAYMENT_PERIOD.append(('9999', '终身'))


# 保险产品抽象类
class InsProAbs(models.Model):
    name = models.CharField(max_length=30)
    category_type = models.IntegerField()
    category = models.CharField(max_length=30)
    payment_method = models.CharField(max_length=50)
    # insurer_id = models.IntegerField()
    insurer = models.ForeignKey(Insurer)
    info = models.TextField(blank=True)
    score = models.DecimalField(max_digits=7, decimal_places=2)
    policy_pattern = models.IntegerField()
    guarantee_pattern = models.IntegerField()
    is_active = models.IntegerField()
    file_url = models.CharField(max_length=200)
    notice_file_url = models.CharField(max_length=200)
    clause_file_url = models.CharField(max_length=200)
    leaflets_file_url = models.CharField(max_length=200)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    covered_area = models.IntegerField(blank=True)
    payment_period = models.CharField(max_length=100,blank=True)
    age_insured_start = models.IntegerField(blank=True)
    age_insured_end = models.IntegerField(blank=True)
    no_examination_amount = models.IntegerField(blank=True)
    min_premium = models.IntegerField(blank=True)
    has_schedule = models.IntegerField(default=0)
    has_attach = models.IntegerField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.IntegerField()
    updated_by = models.IntegerField()
    is_composite = models.IntegerField()
    category_type_comp = models.IntegerField()
    category_comp = models.IntegerField()

    class Meta:
        abstract = True
        ordering = ['is_active', '-score', '-id']


# 主险产品表
class InsProduct(InsProAbs):
    class Meta:
        app_label = proDb
        db_table = 'ins_product'


# 附加险产品表
class InsProAtt(InsProAbs):
    class Meta:
        app_label = proDb
        db_table = 'ins_pro_att'


# 主险产品历史表
class InsProHisAbs(models.Model):
    fid = models.IntegerField()
    name = models.CharField(max_length=30)
    category = models.CharField(max_length=30)
    category_type = models.IntegerField()
    payment_method = models.CharField(max_length=50)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    created_by = models.IntegerField()
    updated_by = models.IntegerField()
    info = models.TextField(blank=True)
    insurer = models.ForeignKey(Insurer)
    has_schedule = models.IntegerField(default=0)
    has_attach = models.IntegerField(default=0)
    score = models.DecimalField(max_digits=7, decimal_places=2)
    policy_pattern = models.IntegerField()
    guarantee_pattern = models.IntegerField()
    is_active = models.IntegerField()
    file_url = models.CharField(max_length=200)
    notice_file_url = models.CharField(max_length=200)
    clause_file_url = models.CharField(max_length=200)
    leaflets_file_url = models.CharField(max_length=200)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    covered_area = models.IntegerField(blank=True)
    payment_period = models.CharField(max_length=100, blank=True)
    age_insured_start = models.IntegerField(blank=True)
    age_insured_end = models.IntegerField(blank=True)
    no_examination_amount = models.IntegerField(blank=True)
    min_premium = models.IntegerField(blank=True)
    is_composite = models.IntegerField()
    category_type_comp = models.IntegerField()
    category_comp = models.IntegerField()
    action = models.CharField(max_length=50)
    his_created_by = models.IntegerField()
    his_created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True


# 主险产品
class InsProductHistory(InsProHisAbs):
    class Meta:
        app_label = proDb
        db_table = 'ins_product_his'


# 附加险产品
class InsProAttHistory(InsProHisAbs):
    class Meta:
        app_label = proDb
        db_table = 'ins_pro_att_his'


class InsProSchAbs(models.Model):
    ins_product_id = models.IntegerField()
    amount_type = models.IntegerField()
    amount_min = models.IntegerField(default=0)
    amount_max = models.IntegerField(default=0)
    name = models.CharField(max_length=30)
    is_default = models.IntegerField(blank=True)
    has_deductible = models.IntegerField(blank=True)
    has_compensation_ratio = models.IntegerField(blank=True)
    status = models.IntegerField()

    class Meta:
        abstract = True


# 主险产品
class InsProductSchedule(InsProSchAbs):
    class Meta:
        app_label = proDb
        db_table = 'ins_product_schedule'


# 附加险产品
class InsProSchAtt(InsProSchAbs):
    class Meta:
        app_label = proDb
        db_table = 'ins_pro_sch_att'


class InsProSchHisAbs(models.Model):
    fid = models.IntegerField()
    ins_product_id = models.IntegerField()
    status = models.IntegerField()
    amount_type = models.IntegerField()
    amount_min = models.IntegerField(default=0)
    amount_max = models.IntegerField(default=0)
    name = models.CharField(max_length=30)
    is_default = models.IntegerField()
    has_deductible = models.IntegerField(blank=True)
    has_compensation_ratio = models.IntegerField(blank=True)
    action = models.CharField(max_length=50)
    his_created_by = models.IntegerField()
    his_created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True


# 主险产品
class InsProductScheduleHistory(InsProSchHisAbs):
    class Meta:
        app_label = proDb
        db_table = 'ins_product_schedule_his'


# 附加险产品
class InsProSchAttHistory(InsProSchHisAbs):
    class Meta:
        app_label = proDb
        db_table = 'ins_pro_sch_att_his'


class InsProRatioAbs(models.Model):
    ins_product_id = models.IntegerField()
    ins_product_schedule_id = models.IntegerField()
    ratio = models.DecimalField(decimal_places=5,max_digits=15)
    gender = models.IntegerField()
    has_social_security = models.IntegerField(blank=True)
    age = models.IntegerField()
    career_type = models.IntegerField(blank=True)
    guarantee_period = models.IntegerField()
    payment_period = models.IntegerField()
    status = models.IntegerField()

    class Meta:
        abstract = True


# 主险产品
class InsProductRatio(InsProRatioAbs):
    class Meta:
        app_label = proDb
        db_table = 'ins_product_ratio'


# 附加险产品
class InsProRatioAtt(InsProRatioAbs):
    class Meta:
        app_label = proDb
        db_table = 'ins_pro_ratio_att'


class InsProRatioHisAbs(models.Model):
    fid = models.IntegerField()
    ins_product_id = models.IntegerField()
    ins_product_schedule_id = models.IntegerField()
    ratio = models.DecimalField(decimal_places=5,max_digits=15)
    gender = models.IntegerField()
    has_social_security = models.IntegerField()
    age = models.IntegerField()
    career_type = models.IntegerField()
    guarantee_period = models.IntegerField()
    payment_period = models.IntegerField()
    status = models.IntegerField()
    action = models.CharField(max_length=50)
    his_created_by = models.IntegerField()
    his_created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True


# 主险产品
class InsProductRatioHistory(InsProRatioHisAbs):
    class Meta:
        app_label = proDb
        db_table = 'ins_product_ratio_his'


# 附加险产品
class InsProRatioAttHistory(InsProRatioHisAbs):
    class Meta:
        app_label = proDb
        db_table = 'ins_pro_ratio_att_his'


class InsProConAbs(models.Model):
    ins_product_id = models.IntegerField()
    ins_product_schedule_id = models.IntegerField()
    gender = models.IntegerField()
    social_guarantee = models.CharField(max_length=255, blank=True)
    career_type = models.CharField(max_length=255, blank=True)
    payment_period = models.CharField(max_length=255)
    guarantee_period = models.CharField(max_length=255)
    age_min = models.IntegerField()
    age_max = models.IntegerField()
    status = models.IntegerField()

    class Meta:
        abstract = True
        ordering = ['-id']


# 主险产品
class InsProductRatioCondition(InsProConAbs):
    class Meta:
        app_label = proDb
        db_table = 'ins_product_ratio_condition'


# 附加险产品
class InsProRatioConAtt(InsProConAbs):
    class Meta:
        app_label = proDb
        db_table = 'ins_pro_ratio_con_att'


class InsProConHisAbs(models.Model):
    fid = models.IntegerField()
    ins_product_id = models.IntegerField()
    ins_product_schedule_id = models.IntegerField()
    gender = models.CharField(max_length=255)
    social_guarantee = models.CharField(max_length=255, blank=True)
    age_min = models.IntegerField()
    age_max = models.IntegerField()
    career_type = models.CharField(max_length=255, blank=True)
    payment_period = models.CharField(max_length=255)
    guarantee_period = models.CharField(max_length=255)
    status = models.IntegerField()
    action = models.CharField(max_length=50)
    his_created_by = models.IntegerField()
    his_created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True


# 主险产品
class InsProductRatioConditionHistory(InsProConHisAbs):
    class Meta:
        app_label = proDb
        db_table = 'ins_product_ratio_condition_his'


# 附加险产品
class InsProRatioConAttHistory(InsProConHisAbs):
    class Meta:
        app_label = proDb
        db_table = 'ins_pro_ratio_con_att_his'


class Convert:
    def get_gender_integer(self,txt=''):
        if txt == '男':
            return 1
        elif txt == '女':
            return 2
        else:
            return 0

    def get_gender_txt(self,num=0):
        num = int(num)
        if num == 1:
            return '男'
        elif num == 2:
            return '女'
        else:
            return ''

    def get_social_integer(self, txt=''):
        if txt == '有':
            return 1
        elif txt == '无':
            return 0
        else:
            return 0


class InsTypeDs(models.Model):
    id = models.IntegerField(primary_key=True)
    description = models.CharField(max_length=100)
    benefit_list = models.TextField()
    is_active = models.IntegerField()
    cache_fresh = models.DateTimeField(auto_now=True)

    class Meta:
        app_label = proDb
        db_table = 'ins_type_ds'


class InsTypeInsurance(models.Model):
    id = models.IntegerField(primary_key=True)
    description = models.CharField(max_length=100)
    is_active = models.IntegerField()
    is_count = models.IntegerField()
    category_type = models.IntegerField()

    class Meta:
        app_label = proDb
        db_table = 'ins_type_insurance'


class InsTypeReturn(models.Model):
    id = models.IntegerField(primary_key=True)
    description = models.CharField(max_length=100)
    is_active = models.IntegerField()

    class Meta:
        app_label = proDb
        db_table = 'ins_type_return'


class InsDisplayList(models.Model):
    type_id = models.IntegerField(primary_key=True)
    ins_id = models.IntegerField(primary_key=True)
    ins_description = models.CharField(max_length=100)
    return_description = models.CharField(max_length=100)

    class Meta:
        app_label = proDb
        db_table = 'ins_display_list'


class InsProductCity(models.Model):
    product_id = models.IntegerField()
    bsc_city_id = models.IntegerField()

    class Meta:
        app_label = proDb
        db_table = 'ins_product_city'
        ordering = ['bsc_city_id']

