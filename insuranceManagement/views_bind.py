from insuranceManagement.models import InsProAtt, InsTypeDs, InsProductSchedule,\
    InsProSchAtt, InsProduct
from insuranceManagement.models_composite import InsCompositePro, InsProductBind, InsScheduleBind, InsAmountBind, \
    InsProductBindExclude, InsProductBindSchedule
from insuranceManagement.models_score import InsProductScore, InsProductScoreAtt

from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.response import Response
from rest_framework.authentication import SessionAuthentication
from utils.auth import ExpiringTokenAuthentication
from rest_framework.permissions import IsAuthenticated
from static.common.log import Logger

from static.common.cousterFunction import get_list_str, dict_fetchall, ins_pro_change_redis, set_composite_info, get_list, get_val
from static.common.HistoryData import *
from django.db import transaction, IntegrityError, connection
from django.db.models import Q, Count
from geeCMS.param import SERVER_HOST, FILE_FOLDER

from geeCMS.settings import proDb

import time, json

# Create your views here.


# 绑定列表
@api_view(['GET'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def bind_index(request, id):
    items = InsProductBind.objects.using(proDb).filter(main_id=id, status=Constant.STATUS_ACTIVE)

    # 主险的计划列表
    main_sch_list = InsProductSchedule.objects.using(proDb).filter(ins_product_id=id, status=Constant.STATUS_ACTIVE).values('id', 'name')

    result = []

    for item in items:
        # 相应组合产品取得
        try:
            comp_pro = InsCompositePro.objects.using(proDb).get(main_id=id, comp_pro_id=item.attach_id, status=Constant.STATUS_ACTIVE)
            score = comp_pro.score
            category_type = comp_pro.category_type
            is_composite = 1
        except InsCompositePro.DoesNotExist:
            score = None
            category_type = None
            is_composite = 0

        # 计划的关联取得
        sch_relations_list = []
        amount_relation = {}
        if item.relation_type == Constant.RELATION_TYPE_SCH:
            sch_relations_main = InsScheduleBind.objects.using(proDb).filter(main_id=item.main_id, attach_id=item.attach_id, bind_id=item.id, status=Constant.STATUS_ACTIVE).values('main_sch_id').annotate(dcount=Count('id')).order_by('main_sch_id')
            for item_m in sch_relations_main:
                sch_relations = InsScheduleBind.objects.using(proDb).filter(bind_id=item.id, main_sch_id=item_m['main_sch_id'], status=Constant.STATUS_ACTIVE).order_by('attach_sch_id').values()
                v_sch_relations_list = []
                # 是否必绑取得
                sch_must_bind = InsProductBindSchedule.objects.using(proDb).filter(main_id=item.main_id, main_sch_id=item_m['main_sch_id'], attach_id=item.attach_id).count()
                if sch_must_bind > 0:
                    sch_must_bind = 1
                for item_r in sch_relations:
                    v_sch_relations = {
                        'id': item_r['id'],
                        'attach_sch_id': item_r['attach_sch_id'],
                        'is_modifiable': item_r['is_modifiable'],
                        'chain_type': item_r['chain_type'],
                        'is_relation': item_r['is_relation'],
                        'relation_ratio': item_r['ratio']
                    }
                    v_sch_relations_list.append(v_sch_relations)
                sch_relations_list.append({
                    'main_sch_id': item_m['main_sch_id'],
                    'attach_sch_list': v_sch_relations_list,
                    'sch_must_bind': sch_must_bind
                })
        elif item.relation_type == Constant.RELATION_TYPE_AMOUNT:
            try:
                amount_relation_obj = InsAmountBind.objects.using(proDb).get(main_id=item.main_id, attach_id=item.attach_id, bind_id=item.id, status=Constant.STATUS_ACTIVE)
                amount_relation = {
                    'chain_type': amount_relation_obj.chain_type,
                    'ratio': amount_relation_obj.ratio
                }
            except InsAmountBind.DoesNotExist:
                pass
        # 附加险的计划列表
        attach_sch_list = InsProSchAtt.objects.using(proDb).filter(ins_product_id=item.attach_id, status=Constant.STATUS_ACTIVE).values('id', 'name')
        app_result = {
            'att_id': item.attach_id,
            'bind_type': item.bind_type,
            'relation_type': item.relation_type,
            'bind_id': item.id,
            'score': score,
            'is_composite': is_composite,
            'category_type': category_type,
            'main_sch_list': main_sch_list,
            'attach_sch_list': attach_sch_list,
            'is_relation_payment': item.is_relation_payment,
            'is_relation_gua': item.is_relation_gua,
            'is_relation_social': item.is_relation_social,
            'is_relation_career': item.is_relation_career,
            'sch_relations': sch_relations_list,
            'amount_relation': amount_relation
        }
        result.append(app_result)

    # 附加险列表
    pro_att_list = dict_fetchall(InsProAtt.objects.using(proDb).values_list('id', 'name').filter(is_active=Constant.STATUS_ACTIVE))

    # 绑定状态列表
    bind_list = Constant.BIND_DICT
    # 险种类型
    insurance_type = InsTypeDs.objects.using(proDb).filter(is_active=Constant.STATUS_ACTIVE).values_list('id', 'description')

    return Response({'pro_att_list': pro_att_list,   'bind_list': bind_list, 'insurance_type': insurance_type, 'result': result})


# 计划关联列表
@api_view(['GET'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def get_sch_relation(request, main_id, sch_id):
    # 主险的计划列表
    main_sch_list = InsProductSchedule.objects.using(proDb).filter(ins_product_id=main_id, status=Constant.STATUS_ACTIVE).values('id', 'name')
    # 附加险的计划列表
    attach_sch_list = InsProSchAtt.objects.using(proDb).filter(ins_product_id=sch_id, status=Constant.STATUS_ACTIVE).values('id', 'name')
    # 默认的关联设定
    main_sch_id_list = []
    attach_sch_id_list = []
    # id数组赋值
    for item_m in main_sch_list:
        main_sch_id_list.append(item_m['id'])

    for item_s in attach_sch_list:
        attach_sch_id_list.append(item_s['id'])

    # 主险的计划个数
    main_sch_len = len(main_sch_id_list)
    # 附加险的计划个数
    att_sch_len = len(attach_sch_id_list)
    # 关联的初期数组
    default_relation_list = []
    for i in range(0, main_sch_len):
        # 附加险的关联id,假设附加险计划个数小于主险的，多出的主险计划默认关联附加险数组中的首个计划
        if (i + 1) > att_sch_len:
            v_att_id = attach_sch_id_list[0]
        else:
            v_att_id = attach_sch_id_list[i]
        v_default_relation_list = {'main_sch_id': main_sch_id_list[i], 'attach_sch_id': v_att_id}
        # 在返回的字典中追加元素
        default_relation_list.append(v_default_relation_list)\

    return Response({'main_sch_list': main_sch_list, 'attach_sch_list': attach_sch_list, 'default_relation_list': default_relation_list})


# 排他关联列表
@api_view(['POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def get_sch_exclude_left(request):
    main_id = int(request.data.get('main_id'))
    sch_id = int(request.data.get('sch_id'))
    # 绑定附加险列表
    att_id_dict = get_list(InsProductBind.objects.using(proDb).filter(main_id=main_id, status=Constant.STATUS_ACTIVE).values_list('attach_id'))
    if len(att_id_dict) <= 1:
        return Response({'code': Constant.REQUEST_FAIL_CODE, 'message': '绑定的附加险个数未超过1个时，排他设定无效'})

    att_id_set = set(att_id_dict)
    # 附加险的列表
    attach_sch_dict = dict_fetchall(InsProAtt.objects.using(proDb).filter(is_active=Constant.STATUS_ACTIVE).values_list('id', 'name'))

    # 可选列表
    exclude_list = []
    for row_att_id in att_id_dict:
        # 可选列表
        exclude_dict = InsProductBindExclude.objects.using(proDb).filter(main_id=main_id, main_sch_id=sch_id).filter(Q(attach_id=row_att_id) | Q(attach_id_exc=row_att_id)).values(
            'attach_id', 'attach_id_exc')
        exclude_att = [row_att_id]
        for row_exc in exclude_dict:
            if row_exc['attach_id'] != row_att_id:
                exclude_att.append(row_exc['attach_id'])
            elif row_exc['attach_id_exc'] != row_att_id:
                exclude_att.append(row_exc['attach_id_exc'])

        exclude_set = set(exclude_att)
        # 没有可选择的附加险时，后续不执行
        if len(exclude_att) == len(att_id_dict):
            continue
        else:
            att_list = list(att_id_set.difference(exclude_set))

            if att_list:
                exclude_list.append({
                    'id': row_att_id,
                    'name': attach_sch_dict[row_att_id]
                })

    return Response({'exclude_list': exclude_list})


# 排他关联列表
@api_view(['POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def get_sch_exclude_right(request):
    main_id = int(request.data.get('main_id'))
    sch_id = int(request.data.get('sch_id'))
    att_id = int(request.data.get('att_id'))
    # 绑定附加险列表
    att_id_dict = get_list(InsProductBind.objects.using(proDb).filter(main_id=main_id, status=Constant.STATUS_ACTIVE).values_list('attach_id'))
    if len(att_id_dict) <= 1:
        return Response({'code': Constant.REQUEST_FAIL_CODE, 'message': '绑定的附加险个数未超过1个时，排他设定无效'})

    att_id_set = set(att_id_dict)
    # 附加险的计划列表
    attach_sch_dict = dict_fetchall(InsProAtt.objects.using(proDb).filter(is_active=Constant.STATUS_ACTIVE).values_list('id', 'name'))

    # 可选列表
    exclude_dict = InsProductBindExclude.objects.using(proDb).filter(main_id=main_id, main_sch_id=sch_id).filter(Q(attach_id=att_id) | Q(attach_id_exc=att_id)).values(
            'attach_id', 'attach_id_exc')

    exclude_att = [att_id]
    for row_exc in exclude_dict:
        if row_exc['attach_id'] != att_id:
            exclude_att.append(row_exc['attach_id'])
        elif row_exc['attach_id_exc'] != att_id:
            exclude_att.append(row_exc['attach_id_exc'])

    exclude_set = set(exclude_att)
    exclude_list = []
    # 没有可选择的附加险时，后续不执行
    if len(exclude_att) < len(att_id_dict):
        att_list = list(att_id_set.difference(exclude_set))

        for row_diff_att in att_list:
            exclude_list.append({
                'id': row_diff_att,
                'name': attach_sch_dict[row_diff_att]
            })

    return Response({'exclude_list': exclude_list})


# 排他设定
@api_view(['GET', 'POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def set_sch_exclude(request, main_id):
    # 附加险的列表
    attach_sch_dict = dict_fetchall(InsProAtt.objects.using(proDb).filter(is_active=Constant.STATUS_ACTIVE).values_list('id', 'name'))
    # 附加险个数不足2个时，返回异常设定
    if len(attach_sch_dict) <= 1:
        return Response({'code': Constant.REQUEST_FAIL_CODE, 'message': '绑定的附加险个数未超过1个时，排他设定无效'})
    # 主险的计划列表
    main_sch_list = InsProductSchedule.objects.using(proDb).filter(ins_product_id=main_id, status=Constant.STATUS_ACTIVE).values('id', 'name')
    # 计划的排他取得额
    exclude_list = []
    for row_main_sch in main_sch_list:
        exclude_att = InsProductBindExclude.objects.using(proDb).filter(main_id=main_id, main_sch_id=row_main_sch['id']).order_by(
            'attach_id', 'attach_id_exc').values('attach_id', 'attach_id_exc')
        v_exclude = []
        for row_exc_att in exclude_att:
            v_exclude.append({
                'att_id': row_exc_att['attach_id'],
                'att_name': attach_sch_dict[row_exc_att['attach_id']],
                'exc_att_id': row_exc_att['attach_id_exc'],
                'exc_att_name': attach_sch_dict[row_exc_att['attach_id_exc']]
            })
        exclude_list.append({
            'main_sch_id': row_main_sch['id'],
            'main_sch_name': row_main_sch['name'],
            'exclude_arr': v_exclude
        })

    if request.method == 'POST':
        try:
            with transaction.atomic(using=proDb):
                att_exclude_json = request.data.get('att_exclude_json')
                # 删除排他
                InsProductBindExclude.objects.using(proDb).filter(main_id=main_id).delete()
                # 返回信息
                msg = ''
                # 排他数组保存
                for row_exc_m in att_exclude_json:
                    for row_exc in row_exc_m['exclude_arr']:
                        # 验证是否已经验证
                        exclude_exist = InsProductBindExclude.objects.using(proDb).filter(main_id=main_id, main_sch_id=row_exc_m['main_sch_id']).filter(Q(attach_id=row_exc['att_id'], attach_id_exc=row_exc['att_exclude_id']) | Q(attach_id=row_exc['att_exclude_id'], attach_id_exc=row_exc['att_id'])).count()
                        if not exclude_exist:
                            # 两个产品是不是都是必绑
                            bind_must_sch_att = InsProductBindSchedule.objects.using(proDb).filter(main_id=main_id, main_sch_id=row_exc_m['main_sch_id'], attach_id=row_exc['att_id']).count()
                            bind_must_pro_att = InsProductBind.objects.using(proDb).filter(main_id=main_id, bind_type=Constant.BIND_TYPE_MUST, attach_id=row_exc['att_id']).count()
                            if bind_must_sch_att or bind_must_pro_att:
                                must_att = True
                            else:
                                must_att = False

                            bind_must_sch_exc = InsProductBindSchedule.objects.using(proDb).filter(main_id=main_id, main_sch_id=row_exc_m['main_sch_id'] , attach_id=row_exc['att_exclude_id']).count()
                            bind_must_pro_exc = InsProductBind.objects.using(proDb).filter(main_id=main_id, bind_type=Constant.BIND_TYPE_MUST, attach_id=row_exc['att_exclude_id']).count()
                            if bind_must_sch_exc or bind_must_pro_exc:
                                must_exc = True
                            else:
                                must_exc = False

                            if must_att and must_exc:
                                msg = '附加险【' + attach_sch_dict[row_exc['att_id']] + '】和附加险【' + attach_sch_dict[row_exc['att_exclude_id']] + '】都是必绑产品，不可以设置互斥；   '
                            else:
                                att_exclude_model = InsProductBindExclude(
                                    main_id=main_id,
                                    main_sch_id=row_exc_m['main_sch_id'],
                                    attach_id=row_exc['att_id'],
                                    attach_id_exc=row_exc['att_exclude_id']
                                )
                                att_exclude_model.save(using=proDb)

                # 缓存刷新时间
                ins_pro = InsProduct.objects.using(proDb).get(id=main_id)
                ins_pro_change_redis(ins_pro.category_type)

                if not msg:
                    msg = Constant.REQUEST_SUCCESS_MSG

                return Response({'code': Constant.REQUEST_SUCCESS_CODE, 'message': msg})
        except IntegrityError as err:
            return Response({'code': Constant.REQUEST_FAIL_CODE, 'message': Constant.REQUEST_FAIL_MSG})



    # 绑定附加险列表
    att_id_dict = get_list(InsProductBind.objects.using(proDb).filter(main_id=main_id, status=Constant.STATUS_ACTIVE).values_list('attach_id'))
    if len(att_id_dict) <= 1:
        return Response({'code': Constant.REQUEST_FAIL_CODE, 'message': '绑定的附加险个数未超过1个时，排他设定无效'})

    att_id_set = set(att_id_dict)
    # 附加险的计划列表
    att_list = []
    for row_a in att_id_dict:
        att_name = get_val(InsProAtt.objects.using(proDb).filter(id=row_a).values_list('name'))
        att_list.append({
            'id': row_a,
            'name': att_name
        })

    return Response({'exclude_list': exclude_list, 'att_list': att_list})


# 添加绑定
@api_view(['POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def add_bind(request):
    user_id = int(request.user.id)
    try:
        with transaction.atomic(using=proDb):
            # 参数验证
            request_data = request.data
            # 必须值列表
            required_list = ['main_id', 'att_id', 'bind_type', 'is_relation_payment', 'is_relation_gua', 'is_relation_social', 'is_relation_career', 'relation_type']
            for row_req in required_list:
                if row_req not in request_data:
                    return Response({'code': Constant.ERR_PARAM_CODE, 'message': Constant.ERR_PARAM_CODE_MESSAGE})

            main_id = request.data.get('main_id')
            bind_id = request.data.get('bind_id')
            att_id = request.data.get('att_id')
            bind_type = int(request.data.get('bind_type'))
            # 附加险的计划列表
            attach_sch_dict = dict_fetchall(InsProAtt.objects.using(proDb).filter(is_active=Constant.STATUS_ACTIVE).values_list('id', 'name'))
            # 验证是否和必绑产品设置了排他
            if bind_type == Constant.BIND_TYPE_MUST:
                exc_list = InsProductBindExclude.objects.using(proDb).filter(main_id=main_id, attach_id=att_id).filter(Q(attach_id=att_id) | Q(attach_id_exc=att_id)).values(
                'attach_id', 'attach_id_exc')
                for row_exc in exc_list:
                    # 判定是否有必绑
                    if row_exc['attach_id'] != att_id:
                        v_att_id = row_exc['attach_id']
                    else:
                        v_att_id = row_exc['attach_id_exc']

                    must_bind_pro = InsProductBind.objects.using(proDb).filter(main_id=main_id, bind_type=Constant.BIND_TYPE_MUST, attach_id=v_att_id).count()
                    must_bind_sch = InsProductBindSchedule.objects.using(proDb).filter(main_id=main_id,attach_id=v_att_id).count()

                    if must_bind_pro or must_bind_sch:
                        return Response( {'code': Constant.ERR_PARAM_CODE, 'message': '和必绑产品【' + attach_sch_dict[row_exc['attach_id']] + '】互斥，该产品不可设置为必绑。'})

            is_relation_payment = int(request.data.get('is_relation_payment'))
            is_relation_gua = int(request.data.get('is_relation_gua'))
            is_relation_social = int(request.data.get('is_relation_social'))
            is_relation_career = int(request.data.get('is_relation_career'))
            relation_type = int(request.data.get('relation_type'))
            schedule_relation_json = request.data.get('schedule_relation_json')
            amount_relation_json = request.data.get('amount_relation_json')
            if request.data.get('att_bind_schedule_arr'):
                att_bind_schedule_arr = request.data.get('att_bind_schedule_arr')
            else:
                att_bind_schedule_arr = []

            # 是否和其他必绑产品互斥
            for row_bind_sch in att_bind_schedule_arr:
                exc_list = InsProductBindExclude.objects.using(proDb).filter(main_id=main_id, main_sch_id=row_bind_sch, attach_id=att_id).filter(
                    Q(attach_id=att_id) | Q(attach_id_exc=att_id)).values(
                    'attach_id', 'attach_id_exc')
                for row_exc in exc_list:
                    # 判定是否有必绑
                    if row_exc['attach_id'] != att_id:
                        v_att_id = row_exc['attach_id']
                    else:
                        v_att_id = row_exc['attach_id_exc']

                    must_bind_pro = InsProductBind.objects.using(proDb).filter(main_id=main_id,
                                                                  bind_type=Constant.BIND_TYPE_MUST,
                                                                  attach_id=v_att_id).count()
                    must_bind_sch = InsProductBindSchedule.objects.using(proDb).filter(main_id=main_id, main_sch_id=row_bind_sch,
                                                                          attach_id=v_att_id).count()

                    if must_bind_pro or must_bind_sch:
                        return Response({'code': Constant.ERR_PARAM_CODE, 'message': '和必绑产品【' + attach_sch_dict[
                            row_exc['attach_id']] + '】互斥，该产品不可设置为必绑。'})

            # 判定传值是否正确
            if relation_type == Constant.RELATION_TYPE_SCH:
                if 'schedule_relation_json' not in request_data:
                    return Response({'code': Constant.ERR_PARAM_CODE, 'message': Constant.ERR_PARAM_CODE_MESSAGE})

                if not schedule_relation_json:
                    return Response({'code': Constant.ERR_EXIST_CODE, 'message': "计划绑定时，计划json非空"})
            elif relation_type == Constant.RELATION_TYPE_AMOUNT:
                if 'amount_relation_json' not in request_data:
                    return Response({'code': Constant.ERR_PARAM_CODE, 'message': Constant.ERR_PARAM_CODE_MESSAGE})

                if not amount_relation_json:
                    return Response({'code': Constant.ERR_EXIST_CODE, 'message': "保额绑定时，保额json非空"})

            # 绑定的产品已经被其他产品绑定
            pro_bind_len = InsProductBind.objects.using(proDb).filter(~Q(id=bind_id)).filter(main_id=main_id, attach_id=att_id, status=Constant.STATUS_ACTIVE).count()
            if pro_bind_len:
                return Response({'code': Constant.ERR_EXIST_CODE, 'message': "该产品已经被绑定"})

            # 主险的计划列表
            main_sch_list = dict_fetchall(InsProductSchedule.objects.using(proDb).filter(ins_product_id=main_id, status=Constant.STATUS_ACTIVE).values_list('id', 'name'))
            # 附加险的计划列表
            attach_sch_list = dict_fetchall(InsProSchAtt.objects.using(proDb).filter(ins_product_id=att_id, status=Constant.STATUS_ACTIVE).values_list('id', 'name'))

            # 必绑时验证绑定是否有效
            rel_main_list = []
            rel_att_list = []

            # 主附险计划字典
            main_att_sch_dict = {}
            # 计划绑定时，验证计划是否合适
            if relation_type == Constant.RELATION_TYPE_SCH:
                for rel_check_item in schedule_relation_json:
                    # 计划是否存在
                    main_sch_exist = InsProductSchedule.objects.using(proDb).filter(id=rel_check_item['main_sch_id'], ins_product_id=main_id, status=Constant.STATUS_ACTIVE).count()
                    if main_sch_exist == 0:
                        return Response({'code': Constant.REQUEST_FAIL_CODE, 'message': "主险计划有误"})
                    att_sch_exist = InsProSchAtt.objects.using(proDb).filter(id=rel_check_item['attach_sch_id'], ins_product_id=att_id, status=Constant.STATUS_ACTIVE).count()
                    if att_sch_exist == 0:
                        return Response({'code': Constant.REQUEST_FAIL_CODE, 'message': "附加险计划有误"})
                    # 必绑时验证是否为一一对应
                    rel_main_list.append(rel_check_item['main_sch_id'])
                    rel_att_list.append(rel_check_item['attach_sch_id'])
                    # 字典中添加，先判定key值是否已经存在
                    if rel_check_item['main_sch_id'] in main_att_sch_dict:
                        # 附加险列表计划添加
                        main_att_sch_dict[rel_check_item['main_sch_id']].append(rel_check_item['attach_sch_id'])
                    else:
                        main_att_sch_dict[rel_check_item['main_sch_id']] = [rel_check_item['attach_sch_id']]
                # 列表转为集合
                rel_main_set = set(rel_main_list)
                # 错误判定，1：主险计划重复
                if len(main_sch_list) != len(rel_main_set):
                    return Response({'code': Constant.REQUEST_FAIL_CODE, 'message': "主险的计划选择有误"})

                # 绑定关系循环判定
                for main_sch_item, att_sch_item_list in main_att_sch_dict.items():
                    err_msg = "主险计划：" + main_sch_list[main_sch_item]
                    # 添加的附加险的计划个数，不可多余附加险本身的计划个数
                    if len(attach_sch_list) < len(att_sch_item_list):
                        err_msg += "绑定的附加险个数大于附加险本身的计划个数"
                        return Response({'code': Constant.REQUEST_FAIL_CODE, 'message': err_msg})

                    # 添加的附加险的计划重复
                    att_sch_item_set = set(att_sch_item_list)
                    if len(att_sch_item_list) > len(att_sch_item_set):
                        err_msg += "绑定的附加险计划重复"
                        return Response({'code': Constant.REQUEST_FAIL_CODE, 'message': err_msg})

                    # 必绑时，附加险的个数只能为1
                    if bind_type == Constant.BIND_TYPE_MUST:
                        if len(att_sch_item_list) != 1:
                            return Response({'code': Constant.REQUEST_FAIL_CODE, 'message': "必绑时，计划必须一一对应"})

            # 绑定的附加险已经存在
            try:
                already_bind_all = InsProductBind.objects.using(proDb).filter(main_id=main_id, attach_id=att_id).values()
                if len(already_bind_all) > 1:
                    # 保留一条，其他删除
                    del_key = 0
                    for item_a in already_bind_all:
                        if del_key > 0:
                            InsProductBindSchedule.objects.using(proDb).filter(bind_id=item_a['id']).delete()
                            InsAmountBind.objects.using(proDb).filter(bind_id=item_a['id']).delete()
                            InsProductBind.objects.using(proDb).filter(id=item_a['id']).delete()
                        del_key = del_key + 1

                already_bind = InsProductBind.objects.using(proDb).get(main_id=main_id, attach_id=att_id)
                if bind_id and already_bind.id != bind_id:
                    # 删除当前的bindID，启动之前的
                    try:
                        delete_bind_model = InsProductBind.objects.using(proDb).get(id=bind_id)
                        # 历史记录
                        return_delete_bind_his = fun_ins_product_bind_his(delete_bind_model, Constant.ACTION_DELETE,
                                                                          user_id)
                        if return_delete_bind_his == Constant.FAIL:
                            raise IntegrityError
                        # 更新
                        delete_bind_model.status = Constant.STATUS_UNACTIVE
                        delete_bind_model.save(using=proDb)
                    except InsProductBind.DoesNotExist:
                        return Response(
                            {'code': Constant.ERR_NONE_EXIST_CODE, 'message': Constant.ERR_NONE_EXIST_MESSAGE})

                bind_id = already_bind.id
            except InsProductBind.DoesNotExist:
                pass

            # 有数据更新，无数据插入
            try:
                bind_model = InsProductBind.objects.using(proDb).get(id=bind_id)
                # 历史记录
                return_bind_his = fun_ins_product_bind_his(bind_model, Constant.ACTION_UPDATE, user_id)
                if return_bind_his == Constant.FAIL:
                    raise IntegrityError
                # 更新
                bind_model.attach_id = att_id
                bind_model.bind_type = bind_type
                bind_model.relation_type = relation_type
                bind_model.is_relation_payment = is_relation_payment
                bind_model.is_relation_gua = is_relation_gua
                bind_model.is_relation_social = is_relation_social
                bind_model.is_relation_career = is_relation_career
                bind_model.status = Constant.STATUS_ACTIVE
            except InsProductBind.DoesNotExist:
                bind_model = InsProductBind(
                    main_id=main_id,
                    attach_id=att_id,
                    bind_type=bind_type,
                    relation_type=relation_type,
                    is_relation_payment=is_relation_payment,
                    is_relation_gua=is_relation_gua,
                    is_relation_social=is_relation_social,
                    is_relation_career=is_relation_career,
                    status=Constant.STATUS_ACTIVE,
                    updated_by=user_id,
                    created_by=user_id
                )
            bind_model.save(using=proDb)
            bind_id = bind_model.id

            # 计划维度的关联
            if relation_type == Constant.RELATION_TYPE_SCH:
                # 保额绑定删除
                try:
                    del_amount_bind_model = InsAmountBind.objects.using(proDb).get(main_id=main_id, attach_id=att_id)
                    # 删除
                    return_del_amount_bind_his = fun_ins_amount_bind_his(del_amount_bind_model, Constant.ACTION_UPDATE, user_id)
                    if return_del_amount_bind_his == Constant.FAIL:
                        raise IntegrityError
                    # 更新
                    del_amount_bind_model.status = Constant.STATUS_UNACTIVE
                    del_amount_bind_model.save(using=proDb)

                except InsAmountBind.DoesNotExist:
                    pass
                for rel_item in schedule_relation_json:
                    try:
                        if 'id' in rel_item:
                            sch_bind_model = InsScheduleBind.objects.using(proDb).get(id=rel_item['id'], main_sch_id=rel_item['main_sch_id'])
                            # 历史记录
                            return_sch_bind_his = fun_ins_schedule_bind_his(sch_bind_model, Constant.ACTION_UPDATE, user_id)
                            if return_sch_bind_his == Constant.FAIL:
                                raise IntegrityError
                            # 更新
                            sch_bind_model.main_sch_id = int(rel_item['main_sch_id'])
                            sch_bind_model.attach_sch_id = int(rel_item['attach_sch_id'])
                            sch_bind_model.chain_type = int(rel_item['chain_type'])
                            sch_bind_model.is_modifiable = int(rel_item['is_modifiable'])
                            sch_bind_model.is_relation = int(rel_item['is_relation'])
                            sch_bind_model.ratio = float(rel_item['relation_ratio'])
                            sch_bind_model.status = Constant.STATUS_ACTIVE
                        else:
                            raise InsScheduleBind.DoesNotExist
                    except InsScheduleBind.DoesNotExist:
                        sch_bind_model = InsScheduleBind(
                            main_sch_id=int(rel_item['main_sch_id']),
                            attach_sch_id=int(rel_item['attach_sch_id']),
                            chain_type=int(rel_item['chain_type']),
                            is_modifiable=int(rel_item['is_modifiable']),
                            is_relation=int(rel_item['is_relation']),
                            ratio=float(rel_item['relation_ratio']),
                            status=Constant.STATUS_ACTIVE,
                            bind_id=bind_id,
                            main_id=main_id,
                            attach_id=att_id,
                            updated_by=user_id,
                            created_by=user_id
                        )
                    # 保存
                    sch_bind_model.save(using=proDb)
            elif relation_type == Constant.RELATION_TYPE_AMOUNT:
                # 保额维度的关联，删除计划
                del_sch_binds = InsScheduleBind.objects.using(proDb).filter(main_id=main_id, attach_id=att_id, status=Constant.STATUS_ACTIVE)
                for del_item_sch in del_sch_binds:
                    del_sch_bind_model = InsScheduleBind.objects.using(proDb).get(id=del_item_sch.id)
                    # 历史记录
                    return_del_sch_bind_his = fun_ins_schedule_bind_his(del_sch_bind_model, Constant.ACTION_UPDATE, user_id)
                    if return_del_sch_bind_his == Constant.FAIL:
                        raise IntegrityError
                    # 更新
                    del_sch_bind_model.status = Constant.STATUS_UNACTIVE
                    del_sch_bind_model.save(using=proDb)
                # 保额是否可修改
                if amount_relation_json['chain_type'] == Constant.INSURED_AMOUNT_TYPE_VAL:
                    att_is_modifiable = Constant.AMOUNT_MODIFY_NONE
                else:
                    att_is_modifiable = Constant.AMOUNT_MODIFY_YES
                # 保额绑定表
                try:
                    amount_bind_model = InsAmountBind.objects.using(proDb).get(main_id=main_id, attach_id=att_id)
                    # 历史记录
                    return_amount_bind_his = fun_ins_amount_bind_his(amount_bind_model, Constant.ACTION_UPDATE, user_id)
                    if return_amount_bind_his == Constant.FAIL:
                        raise IntegrityError
                    # 更新
                    amount_bind_model.chain_type = int(amount_relation_json['chain_type'])
                    amount_bind_model.is_modifiable = att_is_modifiable
                    amount_bind_model.ratio = float(amount_relation_json['relation_ratio'])
                    amount_bind_model.status = Constant.STATUS_ACTIVE
                except InsAmountBind.DoesNotExist:
                    amount_bind_model = InsAmountBind(
                        main_id=main_id,
                        attach_id=att_id,
                        bind_id=bind_id,
                        chain_type=int(amount_relation_json['chain_type']),
                        is_modifiable=att_is_modifiable,
                        is_relation=Constant.RELATION_YES,
                        ratio=float(amount_relation_json['relation_ratio']),
                        status=Constant.STATUS_ACTIVE,
                        updated_by=user_id,
                        created_by=user_id
                    )
                # 保存
                amount_bind_model.save(using=proDb)
            else:
                # 计划绑定
                del_sch_binds = InsScheduleBind.objects.using(proDb).filter(main_id=main_id, attach_id=att_id,
                                                               status=Constant.STATUS_ACTIVE)
                for del_item_sch in del_sch_binds:
                    del_sch_bind_model = InsScheduleBind.objects.using(proDb).get(id=del_item_sch.id)
                    # 历史记录
                    return_del_sch_bind_his = fun_ins_schedule_bind_his(del_sch_bind_model, Constant.ACTION_UPDATE,
                                                                        user_id)
                    if return_del_sch_bind_his == Constant.FAIL:
                        raise IntegrityError
                    # 更新
                    del_sch_bind_model.status = Constant.STATUS_UNACTIVE
                    del_sch_bind_model.save(using=proDb)
                # 删除保额绑定
                try:
                    del_amount_bind_model = InsAmountBind.objects.using(proDb).get(main_id=main_id, attach_id=att_id)
                    # 删除
                    return_del_amount_bind_his = fun_ins_amount_bind_his(del_amount_bind_model,
                                                                         Constant.ACTION_UPDATE, user_id)
                    if return_del_amount_bind_his == Constant.FAIL:
                        raise IntegrityError
                    # 更新
                    del_amount_bind_model.status = Constant.STATUS_UNACTIVE
                    del_amount_bind_model.save(using=proDb)

                except InsAmountBind.DoesNotExist:
                    pass

            ins_pro = InsProduct.objects.using(proDb).get(id=main_id)
            # 组合产品判断
            if ins_pro.is_composite:
                comp_category_type = ins_pro.category_type_comp
            else:
                comp_category_type = ins_pro.category_type
            return_comp = set_composite_info(main_id, comp_category_type, user_id)
            if return_comp == Constant.FAIL:
                raise IntegrityError

            # 计划维度必绑
            InsProductBindSchedule.objects.using(proDb).filter(main_id=main_id, attach_id=att_id).delete()
            for row_bind_sch in att_bind_schedule_arr:
                att_bind_sch_model = InsProductBindSchedule(
                    bind_id=bind_id,
                    main_id=main_id,
                    attach_id=att_id,
                    main_sch_id=row_bind_sch,
                    bind_type=Constant.BIND_TYPE_MUST
                )
                att_bind_sch_model.save(using=proDb)

            # 缓存刷新时间
            ins_pro_change_redis(ins_pro.category_type)

            return Response({'code': Constant.REQUEST_SUCCESS_CODE, 'message': Constant.REQUEST_SUCCESS_MSG, 'bind_id': bind_id})
    except IntegrityError as err:
        return Response({'code': Constant.REQUEST_FAIL_CODE, 'message': Constant.REQUEST_FAIL_MSG})


# 删除绑定
@api_view(['GET'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def delete_bind(request, id):
    user_id = int(request.user.id)
    try:
        with transaction.atomic(using=proDb):
            bind_model = InsProductBind.objects.using(proDb).get(id=id)
            # 历史记录
            return_bind_his = fun_ins_product_bind_his(bind_model, Constant.ACTION_UPDATE, user_id)
            if return_bind_his == Constant.FAIL:
                raise IntegrityError
            # 更新
            bind_model.status = Constant.STATUS_UNACTIVE
            bind_model.save(using=proDb)

            # 删除排他
            InsProductBindExclude.objects.using(proDb).filter(main_id=bind_model.main_id).delete()

            # 计划维度必绑
            InsProductBindSchedule.objects.using(proDb).filter(bind_id=id).delete()

            # 计划的绑定
            sch_binds = InsScheduleBind.objects.using(proDb).filter(bind_id=id, status=Constant.STATUS_ACTIVE)
            for item_sch in sch_binds:
                sch_bind_model = InsScheduleBind.objects.using(proDb).get(id=item_sch.id)
                # 历史记录
                return_sch_bind_his = fun_ins_schedule_bind_his(sch_bind_model, Constant.ACTION_UPDATE, user_id)
                if return_sch_bind_his == Constant.FAIL:
                    raise IntegrityError
                # 更新
                sch_bind_model.status = Constant.STATUS_UNACTIVE
                sch_bind_model.save(using=proDb)

            ins_pro = InsProduct.objects.using(proDb).get(id=bind_model.main_id)
            # 附加险为推荐或必绑时，修改组合产品
            if bind_model.bind_type != Constant.BIND_TYPE_NOT_MUST:
                if ins_pro.is_composite:
                    comp_category_type = ins_pro.category_type_comp
                else:
                    comp_category_type = ins_pro.category_type
                return_comp = set_composite_info(bind_model.main_id, comp_category_type, user_id)
                if return_comp == Constant.FAIL:
                    raise IntegrityError
                # 缓存刷新时间
            ins_pro_change_redis(ins_pro.category_type)

            return Response({'code': Constant.REQUEST_SUCCESS_CODE, 'message': Constant.REQUEST_SUCCESS_MSG})
    except IntegrityError as err:
        return Response({'code': Constant.REQUEST_FAIL_CODE, 'message': str(err)})


# 删除计划绑定
@api_view(['GET'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def delete_sch_bind(request, id):
    user_id = int(request.user.id)
    try:
        with transaction.atomic(using=proDb):
            bind_model = InsScheduleBind.objects.using(proDb).get(id=id)
            # 历史记录
            return_bind_his = fun_ins_schedule_bind_his(bind_model, Constant.ACTION_UPDATE, user_id)
            if return_bind_his == Constant.FAIL:
                raise IntegrityError
            # 更新
            bind_model.status = Constant.STATUS_UNACTIVE
            bind_model.save(using=proDb)

            # 缓存刷新时间
            ins_pro = InsProduct.objects.using(proDb).get(id=bind_model.main_id)
            ins_pro_change_redis(ins_pro.category_type)

            return Response({'code': Constant.REQUEST_SUCCESS_CODE, 'message': Constant.REQUEST_SUCCESS_MSG})
    except IntegrityError as err:
        return Response({'code': Constant.REQUEST_FAIL_CODE, 'message': str(err)})

