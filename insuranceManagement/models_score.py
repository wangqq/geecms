from django.db import models
from geeCMS.settings import proDb


# 评分颗粒一级目录
class InsProScoreSuper(models.Model):
    id = models.IntegerField(primary_key=True)
    description = models.CharField(max_length=100)
    # category_type = models.IntegerField()
    order_by_id = models.IntegerField()
    is_active = models.IntegerField()

    class Meta:
        app_label = proDb
        db_table = 'ins_pro_score_super'


# 评分颗粒二级目录
class InsProScoreSecond(models.Model):
    id = models.IntegerField(primary_key=True)
    description = models.CharField(max_length=100)
    super_id = models.IntegerField()
    # category_type = models.IntegerField()
    # comment = models.CharField(max_length=255)
    order_by_id = models.IntegerField()
    is_active = models.IntegerField()

    class Meta:
        app_label = proDb
        db_table = 'ins_pro_score_second'


# 评分颗粒三级目录
class InsProScoreThird(models.Model):
    id = models.IntegerField(primary_key=True)
    description = models.CharField(max_length=100)
    comment = models.CharField(max_length=255)
    option_content = models.CharField(max_length=255)
    order_by_id = models.IntegerField()
    # category_type = models.IntegerField()
    # value_operate_type = models.IntegerField()
    # is_option = models.IntegerField()
    # super_id = models.IntegerField()
    # second_id = models.IntegerField()
    # is_score = models.IntegerField()
    is_active = models.IntegerField()

    class Meta:
        app_label = proDb
        db_table = 'ins_pro_score_third'


# 三级评分的规则
class InsProScoreRule(models.Model):
    id = models.IntegerField(primary_key=True)
    score_third_id = models.IntegerField()
    value_min = models.DecimalField(max_digits=10, decimal_places=3)
    value_max = models.DecimalField(max_digits=10, decimal_places=3)
    score = models.DecimalField(max_digits=6, decimal_places=3)
    rule_type = models.IntegerField()
    value_interval = models.DecimalField(max_digits=10, decimal_places=3)
    score_step = models.DecimalField(max_digits=6, decimal_places=3)
    is_include_min = models.IntegerField()
    is_include_max = models.IntegerField()
    is_compare_min = models.IntegerField()
    is_compare_max = models.IntegerField()
    is_active = models.IntegerField()

    class Meta:
        app_label = proDb
        db_table = 'ins_pro_score_rule'


# 三级评分的项目值
class InsProScoreOptions(models.Model):
    id = models.IntegerField(primary_key=True)
    description = models.CharField(max_length=100)
    comment = models.CharField(max_length=255)
    order_by_id = models.IntegerField()
    score_third_id = models.IntegerField()
    is_active = models.IntegerField()

    class Meta:
        app_label = proDb
        db_table = 'ins_pro_particle_options'


# 产品评分表
class InsProScoreAbs(models.Model):
    product_id = models.IntegerField()
    guarantee_pattern = models.IntegerField()
    ensure_profit = models.TextField(blank=True)
    score = models.DecimalField(max_digits=7, decimal_places=2)
    score_pre = models.DecimalField(max_digits=6, decimal_places=2)
    score_other = models.DecimalField(max_digits=6, decimal_places=2)
    created_time = models.DateTimeField(auto_now_add=True)
    created_by = models.CharField(max_length=50)
    updated_time = models.DateTimeField(auto_now=True)
    updated_by = models.CharField(max_length=50)

    class Meta:
        abstract = True


# 主险产品
class InsProductScore(InsProScoreAbs):
    class Meta:
        app_label = proDb
        db_table = 'ins_product_score'


# 附加险产品
class InsProductScoreAtt(InsProScoreAbs):
    class Meta:
        app_label = proDb
        db_table = 'ins_pro_score_att'


# 产品评分历史
class InsProScoreHisAbs(models.Model):
    fid = models.IntegerField()
    product_id = models.IntegerField()
    guarantee_pattern = models.IntegerField()
    ensure_profit = models.TextField(blank=True)
    score = models.DecimalField(max_digits=7, decimal_places=2)
    score_pre = models.DecimalField(max_digits=6, decimal_places=2)
    score_other = models.DecimalField(max_digits=6, decimal_places=2)
    created_time = models.DateTimeField()
    created_by = models.CharField(max_length=50)
    updated_time = models.DateTimeField()
    updated_by = models.CharField(max_length=50)
    action = models.CharField(max_length=50)
    his_created_by = models.IntegerField()
    his_created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True


# 主险产品
class InsProductScoreHis(InsProScoreHisAbs):
    class Meta:
        app_label = proDb
        db_table = 'ins_product_score_his'


# 附加险产品
class InsProductScoreAttHis(InsProScoreHisAbs):
    class Meta:
        app_label = proDb
        db_table = 'ins_pro_score_att_his'


# 产品评分颗粒表
class InsProScoreItemAbs(models.Model):
    product_id = models.IntegerField()
    third_id = models.IntegerField()
    score = models.DecimalField(max_digits=7, decimal_places=2)
    created_time = models.DateTimeField(auto_now_add=True)
    created_by = models.CharField(max_length=50)
    updated_time = models.DateTimeField(auto_now=True)
    updated_by = models.CharField(max_length=50)
    is_active = models.IntegerField()

    class Meta:
        abstract = True


# 主险产品
class InsProductScoreItem(InsProScoreItemAbs):
    class Meta:
        app_label = proDb
        db_table = 'ins_product_score_item'


# 附加险产品
class InsProductScoreItemAtt(InsProScoreItemAbs):
    class Meta:
        app_label = proDb
        db_table = 'ins_pro_score_item_att'


# 产品评分颗粒历史表
class InsProScoreItemHisAbs(models.Model):
    fid = models.IntegerField()
    product_id = models.IntegerField()
    third_id = models.IntegerField()
    score = models.DecimalField(max_digits=7, decimal_places=2)
    created_time = models.DateTimeField(auto_now_add=True)
    created_by = models.CharField(max_length=50)
    updated_time = models.DateTimeField(auto_now=True)
    updated_by = models.CharField(max_length=50)
    is_active = models.IntegerField()
    action = models.CharField(max_length=50)
    his_created_by = models.IntegerField()
    his_created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True


# 主险产品
class InsProductScoreItemHis(InsProScoreItemHisAbs):
    class Meta:
        app_label = proDb
        db_table = 'ins_product_score_item_his'


# 附加险产品
class InsProductScoreItemAttHis(InsProScoreItemHisAbs):
    class Meta:
        app_label = proDb
        db_table = 'ins_pro_score_item_att_his'


