from insuranceManagement.models import InsProduct, InsProductSchedule, InsProductRatio, \
    InsProductRatioCondition, GUARANTEE_PERIOD, PAYMENT_PERIOD, InsTypeDs, InsTypeReturn, InsDisplayList, \
    InsTypeInsurance, InsProductRatioHistory, InsProductScheduleHistory, InsProAtt, \
    InsProductRatioConditionHistory, InsProductCity, \
    InsProSchAtt, InsProRatioConAtt, InsProRatioAtt
from insuranceManagement.models_score import InsProductScore, InsProductScoreAtt
from insuranceManagement.models_composite import InsProductBind, InsCompositePro, InsScheduleBind, InsAmountBind, InsProductBindExclude, InsProductBindSchedule
from insuranceManagement.models_dedu_comp import InsProSchDed, InsProSchDedAtt, InsProSchComp, InsProSchCompAtt
from insurerManagement.models import Insurer, InsurerHistory
from cityManagement.models import BscCity
from customerManagement.models import Customer, CustomerDatabase
from xlrd import *
from django.db.models import F

from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.response import Response
from rest_framework.authentication import SessionAuthentication
from utils.auth import ExpiringTokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.exceptions import AuthenticationFailed

from rest_framework.parsers import MultiPartParser
from rest_framework.decorators import parser_classes

from django.views.decorators.csrf import csrf_exempt
from django.http import StreamingHttpResponse

from static.common.cousterFunction import list_fetchall, dict_fetchall, upload_file, send_mail, reversed_dict,\
    ins_pro_change_redis, set_composite_info
from static.common.log import Logger
import re, os, datetime,time
from static.common.HistoryData import *
from django.db import transaction, IntegrityError, connection
from django.db.models import Q
from insuranceManagement.forms import InsProSchDedCompForm
from geeCMS.param import SERVER_HOST, FILE_FOLDER

from geeCMS.settings import proDb

# Create your views here.

# 上传文件名列表
UPLOAD_FILE_NAME = {
    "1": {"1": "ratio_main_", "2": "notice_main_", "3": "clause_main_", "4": "leaflets_main_"},
    "2": {"1": "ratio_attach_", "2": "notice_attach_", "3": "clause_attach_", "4": "leaflets_attach_"}
}

# 列表页
@api_view(['POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def index(request):
    query_type = request.data.get('type')
    if query_type == Constant.MAIN_TYPE:
        query_manage = InsProduct.objects.using(proDb)
    else:
        query_manage = InsProAtt.objects.using(proDb)
    items = query_manage.filter(~Q(is_active=Constant.STATUS_UNACTIVE)).all()
    # 查询条件添加
    if request.data.get('name'):
        items = items.filter(name__contains=request.data.get('name'))
    if request.data.get('category_type'):
        items = items.filter(category_type=request.data.get('category_type'))
    items = items.order_by('is_active', '-score', 'name', 'created_at')
    count = items.count()

    result = []

    insurance_type = dict_fetchall(InsTypeDs.objects.using(proDb).values_list('id', 'description').filter(is_active=Constant.STATUS_ACTIVE))
    insurance = dict_fetchall(InsTypeInsurance.objects.using(proDb).values_list('id', 'description').filter(is_active=Constant.STATUS_ACTIVE))
    for item in items:
        app_result = {
            'name': item.name,
            'insurer_name': item.insurer.name,
            'username': item.score,
            'category_type': insurance_type[item.category_type],
            'get_category_txt': insurance[item.category],
            'update_at': item.updated_at,
            'id': item.id,
            'file_url': item.file_url,
            'notice_file_url': item.notice_file_url,
            'clause_file_url': item.clause_file_url,
            'leaflets_file_url': item.leaflets_file_url,
            'is_active': item.is_active
        }
        result.append(app_result)

    return Response({'user_id': request.user.id, 'count': count, 'result': result})


# 添加产品
@api_view(['GET', 'POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def create(request):
    insurers = Insurer.objects.using(proDb).values_list('id', 'name')
    user_id = request.user.id
    if request.method == 'POST':
        # 2020/01/08 新增字段
        pro_type = int(request.data.get('type'))
        has_attach = request.data.get('has_attach')
        name = request.data.get('name')
        insurer_id = request.data.get('insurer_id')
        category_type = int(request.data.get('category_type'))
        categories = int(request.data.get('category'))
        # 判断保障利益是否合法
        insurance_current = dict_fetchall(InsTypeInsurance.objects.using(proDb).values_list('id', 'description').filter(category_type=category_type, is_active=Constant.STATUS_ACTIVE))
        if categories not in insurance_current:
            return Response({'code': Constant.DATA_INVAILD_CODE, 'message': "保障利益错误" + str(category_type)})
        payment_method = request.data.get('payment_method')
        info = request.data.get('info')
        policy_pattern = request.data.get('policy_pattern')
        guarantee_pattern = request.data.get('guarantee_pattern')
        start_time = datetime.datetime.strptime(request.data.get('start_time'), '%Y%m%d')
        end_time = datetime.datetime.strptime(request.data.get('end_time'), '%Y%m%d')
        if end_time < start_time:
            return Response({'code': Constant.DATA_INVAILD_CODE, 'message': "有效日期"})
        city_arr = request.data.get('city')
        # 2019/09/11 新增字段
        covered_area = request.data.get('covered_area')
        payment_period = request.data.get('payment_period')
        age_insured_start = request.data.get('age_insured_start')
        age_insured_end = request.data.get('age_insured_end')
        # 年龄有效判定
        if age_insured_end < age_insured_start or age_insured_start < 0:
            return Response({'code': Constant.DATA_INVAILD_CODE, 'message': "年龄错误"})
        no_examination_amount = request.data.get('no_examination_amount')
        min_premium = request.data.get('min_premium')
        # 20201017 组合产品
        is_composite = int(request.data.get('is_composite'))
        category_type_comp = int(request.data.get('category_type_comp'))
        # 20201024 组合产品添加保障利益
        category_comp = int(request.data.get('category_comp'))
        if pro_type == Constant.MAIN_TYPE:
            if not has_attach:
                is_composite = 0
                category_type_comp = None
                category_comp = None
            if not is_composite:
                category_type_comp = None
                category_comp = None
        else:
            has_attach = None
            is_composite = 0
            category_type_comp = None
            category_comp = None

        # 判断保障利益是否合法
        if category_type_comp:
            insurance_current_comp = dict_fetchall(
                InsTypeInsurance.objects.using(proDb).values_list('id', 'description').filter(category_type=category_type_comp,
                                                                                 is_active=Constant.STATUS_ACTIVE))
            if category_comp not in insurance_current_comp:
                return Response({'code': Constant.DATA_INVAILD_CODE, 'message': "组合保障利益错误"})

        try:
            with transaction.atomic(using=proDb):
                if pro_type == Constant.MAIN_TYPE:
                    pro_manage = InsProduct
                else:
                    pro_manage = InsProAtt
                model = pro_manage(
                    name=name,
                    insurer_id=insurer_id,
                    category=categories,
                    category_type=category_type,
                    payment_method=payment_method,
                    info=info,
                    score=0,
                    policy_pattern=policy_pattern,
                    guarantee_pattern=guarantee_pattern,
                    start_time=start_time,
                    end_time=end_time,
                    covered_area=covered_area,
                    payment_period=payment_period,
                    age_insured_start=age_insured_start,
                    age_insured_end=age_insured_end,
                    no_examination_amount=no_examination_amount,
                    min_premium=min_premium,
                    is_active=Constant.STATUS_STOP_SALE,
                    has_attach=has_attach,
                    is_composite=is_composite,
                    category_type_comp=category_type_comp,
                    category_comp=category_comp,
                    updated_by=request.user.id,
                    created_by=request.user.id
                )

                model.save(using=proDb)
                create_id = model.id
                # 城市添加
                if pro_type == Constant.MAIN_TYPE:
                    for city_id in city_arr:
                        city_model = InsProductCity(
                            product_id=create_id,
                            bsc_city_id=city_id
                        )
                        city_model.save(using=proDb)

                his_model = Insurer.objects.using(proDb).get(id=insurer_id)
                # 历史数据
                his_ins_obj = InsurerHistory(
                    fid=his_model.id,
                    name=his_model.name,
                    insurance_num=his_model.insurance_num,
                    created_by_id=his_model.created_by_id,
                    updated_by_id=his_model.updated_by_id,
                    created_at=his_model.created_at,
                    updated_at=his_model.updated_at,
                    action=Constant.ACTION_UPDATE,
                    his_created_by=int(user_id)
                )
                his_ins_obj.save(using=proDb)

                Insurer.objects.using(proDb).filter(id=insurer_id).update(insurance_num=F('insurance_num') + 1)
                # GFP缓存依赖文件变更
                ins_pro_change_redis(category_type)

                return Response({'code': Constant.SUCCESS, 'message': Constant.SUCCESS_MESSAGE})
        except IntegrityError as err:

            # 输出错误日志
            log_msg = "创建【" + name + "】时错误：" + str(err)
            Logger(Constant.LOG_FILENAME_INS_PRO, level='error').logger.error(log_msg)

            return Response({'code': Constant.FAIL, 'message': Constant.FAIL_MESSAGE})

    insurance_type = InsTypeDs.objects.using(proDb).filter(is_active=Constant.STATUS_ACTIVE).values_list('id', 'description')

    ins_display_list = InsDisplayList.objects.using(proDb).filter(type_id=Constant.INS_TRADE)
    insurance = dict_fetchall(InsTypeInsurance.objects.using(proDb).values_list('id', 'description').filter(is_active=Constant.STATUS_ACTIVE))
    return_list = dict_fetchall(InsTypeReturn.objects.using(proDb).values_list('id', 'description').filter(is_active=Constant.STATUS_ACTIVE))
    # 所有城市
    city_list = dict_fetchall(BscCity.objects.using(socialDb).values_list('id', 'name').all())
    # 缴费年限列表
    payment_period_list = PAYMENT_PERIOD
    # 保障区域列表
    covered_area_list = Constant.CONVERED_AREA_LIST

    display_list = []
    for item in ins_display_list:
        v_ins_list = []
        insurance_arr = item.ins_description.split(',')
        for ins_arr in insurance_arr:
            ins_name = insurance[int(ins_arr)]
            v_ins_list.append([ins_arr, ins_name])

        v_return_list = []
        return_arr = item.return_description.split(',')
        for ret_arr in return_arr:
            ret_name = return_list[int(ret_arr)]
            v_return_list.append([ret_arr, ret_name])

        display_list.append([item.ins_id, v_ins_list, v_return_list])

    return Response({'insurers': insurers,
                     'category_type': insurance_type,
                     'category': display_list,
                     'policy_pattern': Constant.POLICY_PATTERN,
                     'guarantee_pattern': Constant.GUARANTEE_PATTERN,
                     'city_list': city_list,
                     'payment_period_list': payment_period_list,
                     'covered_area_list': covered_area_list
                     })


# 产品信息更新
@api_view(['GET', 'POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def update(request, id, type):
    # 公司列表
    insurers = Insurer.objects.using(proDb).values_list('id', 'name')
    insurers_list = dict_fetchall(insurers)
    # 险种类型列表
    insurance_type = InsTypeDs.objects.using(proDb).filter(is_active=Constant.STATUS_ACTIVE).values_list('id', 'description')
    insurance_list = dict_fetchall(insurance_type)
    if int(type) == Constant.MAIN_TYPE:
        query_manage = InsProduct.objects.using(proDb)
        score_manage = InsProductScore.objects.using(proDb)
    else:
        query_manage = InsProAtt.objects.using(proDb)
        score_manage = InsProductScoreAtt.objects.using(proDb)
    model = query_manage.get(id=id)
    his_model = query_manage.get(id=id)
    user_id = request.user.id
    if request.method == 'POST':
        # 邮件发送失败信息
        err_code = ''
        err_message = ''
        name = request.data.get('name')
        try:
            with transaction.atomic(using=proDb):
                # 2020/01/08新增字段
                pro_type = int(request.data.get('type'))
                has_attach = int(request.data.get('has_attach'))
                is_mail = request.data.get('is_mail')
                insurer_id = request.data.get('insurer_id')
                category_type = int(request.data.get('category_type'))
                # 险种发生改变时
                if category_type != model.category_type:
                    ins_pro_change_redis(model.category_type)
                categories = int(request.data.get('category'))
                # 判断保障利益是否合法
                insurance_current = dict_fetchall(InsTypeInsurance.objects.using(proDb).values_list('id', 'description').filter(category_type=category_type, is_active=Constant.STATUS_ACTIVE))
                if categories not in insurance_current:
                    return Response({'code': Constant.DATA_INVAILD_CODE, 'message': "保障利益错误" + str(category_type)})
                policy_pattern = request.data.get('policy_pattern')
                guarantee_pattern = request.data.get('guarantee_pattern')
                is_active = int(request.data.get('is_active'))
                payment_method = request.data.get('payment_method')
                info = request.data.get('info')
                start_time = datetime.datetime.strptime(request.data.get('start_time'), '%Y%m%d')
                end_time = datetime.datetime.strptime(request.data.get('end_time'), '%Y%m%d')
                if end_time < start_time:
                    return Response({'code': Constant.DATA_INVAILD_CODE, 'message': "有效日期"})
                city_arr = request.data.get('city')
                # 2019/09/11 新增字段
                covered_area = request.data.get('covered_area')
                payment_period = request.data.get('payment_period')
                age_insured_start = request.data.get('age_insured_start')
                age_insured_end = request.data.get('age_insured_end')
                # 年龄有效判定
                if age_insured_end < age_insured_start or age_insured_start < 0:
                    return Response({'code': Constant.DATA_INVAILD_CODE, 'message': "年龄错误"})
                no_examination_amount = request.data.get('no_examination_amount')
                min_premium = request.data.get('min_premium')
                # 20201017 组合产品
                is_composite = int(request.data.get('is_composite'))
                category_type_comp = int(request.data.get('category_type_comp'))
                # 20201024 组合产品添加保障利益
                category_comp = int(request.data.get('category_comp'))
                if pro_type == Constant.MAIN_TYPE:
                    # 主险产品
                    if not has_attach:
                        # 无附加险
                        is_composite = 0
                        category_type_comp = None
                        category_comp = None
                    if not is_composite:
                        # 非组合
                        category_type_comp = None
                        category_comp = None
                else:
                    # 附加险时，是否有附加险，是否组合产品，组合产品类型，组合产品保障利益都是None
                    has_attach = None
                    is_composite = 0
                    category_type_comp = None
                    category_comp = None

                # 判断保障利益是否合法
                if category_type_comp:
                    insurance_current_comp = dict_fetchall(InsTypeInsurance.objects.using(proDb).values_list('id', 'description').filter(category_type=category_type_comp, is_active=Constant.STATUS_ACTIVE))
                    if category_comp not in insurance_current_comp:
                        return Response({'code': Constant.DATA_INVAILD_CODE, 'message': "组合保障利益错误"})

                # 险种类型发生改变时，刷新GFP产品缓存
                if category_type_comp != model.category_type_comp and model.category_type_comp:
                    ins_pro_change_redis(model.category_type_comp)

                if int(type) == Constant.MAIN_TYPE:
                    if not has_attach:
                        # 组合产品删除
                        try:
                            comp_pro_delete = InsCompositePro.objects.using(proDb).get(main_id=id, status=Constant.STATUS_ACTIVE)
                            # 历史记录添加
                            return_comp_pro_delete_his = fun_ins_composite_pro_his(
                                comp_pro_delete, Constant.ACTION_DELETE, user_id)
                            if return_comp_pro_delete_his == Constant.FAIL:
                                raise IntegrityError
                            # 状态设置为无效
                            comp_pro_delete.status = Constant.STATUS_UNACTIVE
                            comp_pro_delete.save(using=proDb)
                        except InsCompositePro.DoesNotExist:
                            pass

                    # 现有附加险的个数
                    attach_exist = InsProductBind.objects.using(proDb).filter(main_id=id, status=Constant.STATUS_ACTIVE)
                    # 有绑定的附加险，产品直接改为无附加险状态，删除相应的绑定
                    if not has_attach and attach_exist:
                        # 删除保额绑定的数据
                        del_amount_bind_model = InsAmountBind.objects.using(proDb).filter(main_id=id, status=Constant.STATUS_ACTIVE)
                        for item_amount in del_amount_bind_model:
                            amount_bind_model = InsAmountBind.objects.using(proDb).get(id=item_amount.id)
                            # 删除
                            return_del_amount_bind_his = fun_ins_amount_bind_his(amount_bind_model,
                                                                                 Constant.ACTION_UPDATE, user_id)
                            if return_del_amount_bind_his == Constant.FAIL:
                                raise IntegrityError
                            # 更新
                            amount_bind_model.status = Constant.STATUS_UNACTIVE
                            amount_bind_model.save(using=proDb)

                        # 删除ins_schedule_bind
                        sch_binds = InsScheduleBind.objects.using(proDb).filter(main_id=id, status=Constant.STATUS_ACTIVE)
                        for item_sch in sch_binds:
                            sch_bind_model = InsScheduleBind.objects.using(proDb).get(id=item_sch.id)
                            # 历史记录
                            return_sch_bind_his = fun_ins_schedule_bind_his(sch_bind_model, Constant.ACTION_UPDATE, user_id)
                            if return_sch_bind_his == Constant.FAIL:
                                raise IntegrityError
                            # 更新
                            sch_bind_model.status = Constant.STATUS_UNACTIVE
                            sch_bind_model.save(using=proDb)

                        for row_bind in attach_exist:
                            # 删除绑定
                            bind_model = InsProductBind.objects.using(proDb).get(id=row_bind.id)
                            # 历史记录
                            return_bind_his = fun_ins_product_bind_his(bind_model, Constant.ACTION_UPDATE, user_id)
                            if return_bind_his == Constant.FAIL:
                                raise IntegrityError
                            # 更新
                            bind_model.status = Constant.STATUS_UNACTIVE
                            bind_model.save(using=proDb)

                        # 删除排他
                        InsProductBindExclude.objects.using(proDb).filter(main_id=id).delete()

                        # 计划维度必绑
                        InsProductBindSchedule.objects.using(proDb).filter(main_id=id).delete()

                    # 主险有附加险，但是未绑定附加险，主险改为停售
                    if int(type) == Constant.MAIN_TYPE:
                        if is_active == Constant.STATUS_ACTIVE and has_attach:
                            # 停售改为在售时，判定附加险是否有添加
                            if len(attach_exist) == 0:
                                is_active = Constant.STATUS_STOP_SALE

                # 历史数据
                return_pro_his = fun_ins_product_his(his_model, pro_type, Constant.ACTION_UPDATE, user_id)
                if return_pro_his == Constant.FAIL:
                    err_code = Constant.FAIL
                    err_message = Constant.FAIL_MESSAGE
                    raise IntegrityError

                # 更新
                model.name = name
                model.policy_pattern = policy_pattern
                model.category = categories
                model.category_type = category_type
                model.payment_method = payment_method

                # 终身型或定期型转为其他类型时，停掉另一个分值，其他的改为停售
                if guarantee_pattern != model.guarantee_pattern:
                    if model.guarantee_pattern == Constant.GUARANTEE_PATTERN_ALL:
                        # 删除分值表里的另一项评分值
                        if guarantee_pattern == Constant.GUARANTEE_PATTERN_REGULAR:
                            del_gua = Constant.GUARANTEE_PATTERN_LONG
                        else:
                            del_gua = Constant.GUARANTEE_PATTERN_REGULAR
                        try:
                            score_model = score_manage.get(product_id=id, guarantee_pattern=del_gua)
                            # 历史记录
                            return_pro_his = fun_ins_product_score_his(score_model, int(type), Constant.ACTION_DELETE, user_id)
                            if return_pro_his == Constant.FAIL:
                                err_code = Constant.FAIL
                                err_message = Constant.FAIL_MESSAGE

                            score_model.delete(using=proDb)
                        except:
                            pass
                    else:
                        is_active = Constant.STATUS_STOP_SALE

                # 附加险的情况下，已被绑定，禁止改为停售
                if int(type) == Constant.ATTACH_TYPE and is_active == Constant.STATUS_STOP_SALE:
                    bindlen = InsProductBind.objects.using(proDb).filter(attach_id=id).count()
                    if bindlen:
                        err_message = "已被绑定，状态不可修改"
                        raise IntegrityError
                else:
                    model.is_active = is_active

                model.guarantee_pattern = guarantee_pattern
                model.info = info
                model.insurer_id = insurer_id
                model.updated_by = request.user.id
                model.start_time = start_time
                model.end_time = end_time
                model.covered_area = covered_area
                model.payment_period = payment_period
                model.age_insured_start = age_insured_start
                model.age_insured_end = age_insured_end
                model.no_examination_amount = no_examination_amount
                model.min_premium = min_premium
                model.is_composite = is_composite
                model.category_type_comp = category_type_comp
                model.category_comp = category_comp
                model.has_attach = has_attach
                model.save(using=proDb)

                # 主险产品需要对城市操作, 且发送邮件
                if pro_type == Constant.MAIN_TYPE:
                    # 城市删除
                    InsProductCity.objects.using(proDb).filter(product_id=id).delete()

                    # 城市添加
                    for city_id in city_arr:
                        city_model = InsProductCity(
                            product_id=id,
                            bsc_city_id=city_id
                        )
                        city_model.save(using=proDb)
                    if is_active == Constant.STATUS_ACTIVE:
                        now_time = time.strftime("%Y-%m-%d", time.localtime())
                        # 所有有效客户
                        customer = Customer.objects.filter(status=Constant.STATUS_ACTIVE, start_date__lt=now_time,
                                                           end_date__gt=now_time)
                        for cu_email in customer:
                            if cu_email.status == Constant.STATUS_ACTIVE:
                                    # 是否发送邮件
                                    if is_mail and int(is_mail) == Constant.STATUS_ACTIVE:
                                        return_send = send_mail(cu_email, name, insurance_list[int(category_type)], insurers_list[int(insurer_id)])
                                        if return_send == Constant.FAIL:
                                            err_code = Constant.EMAIL_SEND_FAIL
                                            err_message = Constant.EMAIL_SEND_FAIL_MESSAGE
                                            # 输出错误日志
                                            log_msg = "更新【" + name + "】时，发送邮件失败：" + err_message
                                            Logger(Constant.LOG_FILENAME_INS_PRO, level='error').logger.error(log_msg)
                                            # raise IntegrityError

                                    # 是否需要同步
                                    if cu_email.is_syn == Constant.STATUS_ACTIVE:
                                        # DB连接字符获得
                                        try:
                                            customer_db = CustomerDatabase.objects.get(customer_id=cu_email.id)
                                            customer_dsn = customer_db.dsn

                                            # 连接DB
                                            cursor = connection.cursor()
                                            # 判断是否已经存在
                                            check_sql = "SELECT COUNT(id) as exist_flg FROM "+ customer_dsn + ".`ins_product_selected`  WHERE product_id = " + id
                                            cursor.execute(check_sql)
                                            check_result = cursor.fetchone()
                                            if check_result[0] == 0:
                                                insert_pro_sql = "INSERT INTO "+customer_dsn+".`ins_product_selected` (`product_id`) VALUES ('"+id+"');"
                                                cursor.execute(insert_pro_sql)
                                        except CustomerDatabase.DoesNotExist:
                                            pass

                # 是主险，且组合产品时
                if int(type) == Constant.MAIN_TYPE and has_attach:
                    return_comp = set_composite_info(id, category_type_comp, user_id)
                    if return_comp == Constant.FAIL:
                        raise IntegrityError

                # GFp缓存依赖文件刷新
                ins_pro_change_redis(category_type)

                return Response({'code': Constant.SUCCESS, 'message': Constant.SUCCESS_MESSAGE})
        except IntegrityError as err:
            # 输出错误日志
            log_msg = "更新【" + name + "】时错误：" + str(err)
            Logger(Constant.LOG_FILENAME_INS_PRO, level='error').logger.error(log_msg)

            return Response({'code': err_code, 'message': err_message})

    # 城市列表
    city_list = InsProductCity.objects.using(proDb).filter(product_id=id).values_list('bsc_city_id')

    model_result = {
        'insurer_id': model.insurer_id,
        'category_type': model.category_type,
        'get_category': model.category,
        'payment_method': model.payment_method.split(','),
        'name': model.name,
        'score': model.score,
        'policy_pattern': model.policy_pattern,
        'guarantee_pattern': model.guarantee_pattern,
        'is_active': model.is_active,
        'info': model.info,
        'start_time': model.start_time,
        'end_time': model.end_time,
        'covered_area': model.covered_area,
        'payment_period': model.payment_period.split(','),
        'age_insured_start': model.age_insured_start,
        'age_insured_end': model.age_insured_end,
        'no_examination_amount': model.no_examination_amount,
        'min_premium': model.min_premium,
        'has_attach': model.has_attach,
        'is_composite': model.is_composite,
        'category_type_comp': model.category_type_comp,
        'category_comp': model.category_comp,
        'city_list': city_list
    }

    insurance = dict_fetchall(InsTypeInsurance.objects.using(proDb).values_list('id', 'description').filter(is_active=Constant.STATUS_ACTIVE))
    ins_display_list = InsDisplayList.objects.using(proDb).filter(type_id=Constant.INS_TRADE)
    return_list = dict_fetchall(InsTypeReturn.objects.using(proDb).values_list('id', 'description').filter(is_active=Constant.STATUS_ACTIVE))
    # 所有城市
    city_list = dict_fetchall(BscCity.objects.using(socialDb).values_list('id', 'name').all())
    # 缴费年限列表
    payment_period_list = PAYMENT_PERIOD
    # 保障区域列表
    covered_area_list = Constant.CONVERED_AREA_LIST

    display_list = []
    for item in ins_display_list:
        v_ins_list = []
        insurance_arr = item.ins_description.split(',')
        for ins_arr in insurance_arr:
            ins_name = insurance[int(ins_arr)]
            v_ins_list.append([ins_arr, ins_name])

        v_return_list = []
        return_arr = item.return_description.split(',')
        for ret_arr in return_arr:
            ret_name = return_list[int(ret_arr)]
            v_return_list.append([ret_arr, ret_name])

        display_list.append([item.ins_id, v_ins_list, v_return_list])

    return Response({'model': model_result,
                     'insurers': insurers,
                     'category_type': insurance_type,
                     'category': display_list,
                     'policy_pattern': Constant.POLICY_PATTERN,
                     'guarantee_pattern': Constant.GUARANTEE_PATTERN,
                     'city_list': city_list,
                     'payment_period_list': payment_period_list,
                     'covered_area_list': covered_area_list
                     })


# 产品计划列表
@api_view(['POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def ratio(request):
    query_id = request.data.get('id')
    query_type = request.data.get('type')
    if query_type == Constant.MAIN_TYPE:
        query_manage = InsProductSchedule.objects.using(proDb)
        query_ded = InsProSchDed
        query_comp = InsProSchComp
    else:
        query_manage = InsProSchAtt.objects.using(proDb)
        query_ded = InsProSchDedAtt
        query_comp = InsProSchCompAtt
    model_schedules = query_manage.filter(ins_product_id=query_id, status=Constant.STATUS_ACTIVE)

    # 结果转为json串
    schedules_result = []
    for item in model_schedules:
        # 免赔额
        ded_list = query_ded.objects.using(proDb).filter(ins_product_schedule_id=item.id, status=Constant.STATUS_ACTIVE).values()
        # 赔付比例
        comp_list = query_comp.objects.using(proDb).filter(ins_product_schedule_id=item.id, status=Constant.STATUS_ACTIVE).values()
        app_result = {
            'name': item.name,
            'amount_type': item.amount_type,
            'amount_min': item.amount_min,
            'amount_max': item.amount_max,
            'ins_product_id': item.ins_product_id,
            'id': item.id,
            'ded_list': ded_list,
            'comp_list': comp_list
        }
        schedules_result.append(app_result)
    return Response({'id': query_id, 'model_schedules': schedules_result})


# 产品费率编辑
@api_view(['POST'])
@csrf_exempt
@parser_classes((MultiPartParser,))
def ratio_edit(request, id, schedule_id, user_id):
    # token
    token = request.data.get('fileName')
    query_type = request.data.get('type')
    # token验证
    expir_model = ExpiringTokenAuthentication()
    try:
        ExpiringTokenAuthentication.authenticate_credentials(expir_model, token)
        # 费率文件上传到服务器
        file_obj = request.FILES.get('file', None)
        if int(query_type) == Constant.MAIN_TYPE:
            # 文件名前缀
            file_prefix = 'ratio_'
        else:
            file_prefix = 'ratio_attach_'
        filename = FILE_FOLDER + 'insurance_excel/' + file_prefix + str(id) + 'sch' + str(schedule_id) + '.xlsx'
        # filename = 'ratio'+str(id) + 'sch' + str(schedule_id) + '.xlsx'
        destination = open(filename, 'wb+')
        for chunk in file_obj.chunks():
            destination.write(chunk)
        destination.close()

        user_id = request.user.id
        file_path = r'' + filename + ''
        # 社保列表
        social_dict = Constant.SOCIAL_DICT
        # 职业列表
        career_dict = Constant.CAREER_DICT
        # 性别列表
        sex_dict = Constant.SEX_DICT
        # 保障期限列表
        guarantee_dict = Constant.GUARANTEE_PERIOD_DICT
        # 缴费期限列表
        payment_dict = Constant.PAYMENT_PERIOD_DICT
        # 反转键值对
        reverse_social_dictt = reversed_dict(social_dict)
        reverse_career_dict = reversed_dict(career_dict)
        reverse_sex_dict = reversed_dict(sex_dict)
        reverse_guarantee_dict = reversed_dict(guarantee_dict)
        reverse_payment_dict = reversed_dict(payment_dict)

        # 上传费率的产品名称和计划名称获得
        if int(query_type) == Constant.MAIN_TYPE:
            query_pro = InsProduct
            query_sch = InsProductSchedule
        else:
            query_pro = InsProAtt
            query_sch = InsProSchAtt

        # 查询
        pro_obj = query_pro.objects.using(proDb).get(id=id)
        sch_obj = query_sch.objects.using(proDb).get(id=schedule_id)
        # 错误产品计划名称
        log_msg_pro_name = "产品【" + pro_obj.name + "】的计划【" + sch_obj.name + "】"

        # 费率数组
        ratio_list = []
        # condi数据集合
        con_set = Constant.CON_DATA_SET
        # 读取文件
        wb = open_workbook(file_path)
        if wb:
            table = wb.sheet_by_index(0)
            rows = table.nrows
            cols = table.ncols
            i = 5
            try:
                while (i < rows):
                    j = 1
                    while (j < cols):
                        # 费率
                        ratio_value = table.cell_value(i, j)
                        if ratio_value:
                            # 单元格行列号
                            rc = chr(j + 1) + str(i + 1)
                            try:
                                ratio_value = float(ratio_value)
                            except ValueError:
                                error_msg = rc + '的费率值为数值'
                                # 输出错误日志
                                log_msg = log_msg_pro_name + str(i+1) + "行的费率值应为数值"
                                Logger(Constant.LOG_FILENAME_INS_PRO, level='error').logger.error(log_msg)
                                return Response({"code": Constant.VALUE_ERROR_CODE, 'error_msg': error_msg})
                            # 社保
                            social = table.cell_value(Constant.SOCIAL_ROW_NO, j)
                            if social == Constant.EXPORT_VALUE_UNLIMITED:
                                social_key = None
                            else:
                                social_key = reverse_social_dictt[social]
                            # 职业等级
                            career = table.cell_value(Constant.CAREER_ROW_NO, j)
                            if career == Constant.EXPORT_VALUE_UNLIMITED:
                                career_key = None
                            else:
                                career_key = reverse_career_dict[career]
                            # 保障期
                            guarantee_period = table.cell_value(Constant.GUARANTEE_ROW_NO, j)
                            guarantee_period_key = reverse_guarantee_dict[guarantee_period]
                            # 交费期
                            payment_period = table.cell_value(Constant.PAYMENT_ROW_NO, j)
                            payment_period_key = reverse_payment_dict[payment_period]
                            # 年龄
                            try:
                                age = int(table.cell_value(i, Constant.AGE_COLUMN_NO))
                            except ValueError:
                                error_msg = rc + '的年龄为整数'
                                # 输出错误日志
                                log_msg = log_msg_pro_name + str(i+1) + "行的年龄为整数"
                                Logger(Constant.LOG_FILENAME_INS_PRO, level='error').logger.error(log_msg)
                                return Response({"code": Constant.VALUE_ERROR_CODE, 'error_msg': error_msg})
                            # 性别
                            sex = table.cell_value(Constant.SEX_ROW_NO, j)
                            if sex == Constant.EXPORT_VALUE_UNLIMITED:
                                for s_key, s_val in sex_dict.items():
                                    con_set_dict = con_set[s_key]
                                    # 社保集合添加
                                    if social_key:
                                        con_set_dict['social'].add(social_key)
                                    # 年龄集合添加
                                    con_set_dict['age'].add(age)
                                    # 职业等级集合添加
                                    if career_key:
                                        con_set_dict['career'].add(career_key)
                                    # 保障期限集合添加
                                    con_set_dict['guarantee'].add(guarantee_period_key)
                                    # 缴费期限集合添加
                                    con_set_dict['payment'].add(payment_period_key)
                                    # 存储
                                    v_ratio = {'social': social_key, 'career': career_key, 'sex': int(s_key),
                                                         'guarantee': guarantee_period_key, 'payment': payment_period_key,
                                                         'age': age, 'ratio': ratio_value}
                                    ratio_list.append(v_ratio)
                            else:
                                sex_key = reverse_sex_dict[sex]
                                con_set_dict = con_set[sex_key]
                                # 社保集合添加
                                if social_key:
                                    con_set_dict['social'].add(social_key)
                                # 年龄集合添加
                                con_set_dict['age'].add(age)
                                # 职业等级集合添加
                                if career_key:
                                    con_set_dict['career'].add(career_key)
                                # 保障期限集合添加
                                con_set_dict['guarantee'].add(guarantee_period_key)
                                # 缴费期限集合添加
                                con_set_dict['payment'].add(payment_period_key)
                                # 存储
                                v_ratio = {'social': social_key, 'career': career_key, 'sex': sex_key,
                                                     'guarantee': guarantee_period_key, 'payment': payment_period_key,
                                                     'age': age, 'ratio': ratio_value}
                                ratio_list.append(v_ratio)

                        j = j + 1
                    i = i + 1
            except KeyError as err:
                err_message = "【" + str(err) + "】不是有效数据，有效数据值请参照附件"
                # 输出错误日志
                log_msg = log_msg_pro_name + str(j+1) + "列的属性值" + "【" + str(err) + "】不是有效数据，有效数据值请参照附件"
                Logger(Constant.LOG_FILENAME_INS_PRO, level='error').logger.error(log_msg)
                return Response({"code": Constant.KEY_ERROR_CODE, 'error_msg': err_message})

        try:
            with transaction.atomic(using=proDb):
                # 现有数据设为无效
                if int(query_type) == Constant.MAIN_TYPE:
                    query_con = InsProductRatioCondition
                else:
                    query_con = InsProRatioConAtt
                query_con.objects.using(proDb).filter(ins_product_schedule_id=schedule_id, status=Constant.STATUS_ACTIVE).update(status=Constant.STATUS_UNACTIVE)
                # 费率条件添加
                for con_key, con_item in con_set.items():
                    # 社保字符串
                    if con_item['social']:
                        social_str = ',' . join(con_item['social'])
                    else:
                        social_str = None
                    # 职业等级字符串
                    if con_item['career']:
                        career_str = ',' . join(con_item['career'])
                    else:
                        career_str = None
                    # 年龄字符串
                    age_min = min(con_item['age'])
                    age_max = max(con_item['age'])
                    # 缴费方式字符串
                    payment_str = ',' . join(con_item['payment'])
                    # 保障期限字符串
                    guarantee_str = ',' . join(con_item['guarantee'])

                    try:
                        pro_cond_obj = query_con.objects.using(proDb).get(ins_product_schedule_id=schedule_id,gender=con_key)
                        # 历史记录
                        return_ratio_his = fun_ins_product_ratio_condition_his(pro_cond_obj, query_type, Constant.ACTION_UPDATE, user_id)
                        if return_ratio_his == Constant.FAIL:
                            raise IntegrityError
                        # 更新
                        pro_cond_obj.gender = con_key
                        pro_cond_obj.social_guarantee = social_str
                        pro_cond_obj.age_min = age_min
                        pro_cond_obj.age_max = age_max
                        pro_cond_obj.career_type = career_str
                        pro_cond_obj.payment_period = payment_str
                        pro_cond_obj.guarantee_period = guarantee_str
                        pro_cond_obj.status = Constant.STATUS_ACTIVE
                        pro_cond_obj.save(using=proDb)

                    except query_con.DoesNotExist:
                        pro_cond_obj = query_con(
                            ins_product_id=id,
                            ins_product_schedule_id=schedule_id,
                            gender=con_key,
                            social_guarantee=social_str,
                            age_min=age_min,
                            age_max=age_max,
                            career_type=career_str,
                            payment_period=payment_str,
                            guarantee_period=guarantee_str,
                            status=Constant.STATUS_ACTIVE
                        )
                        pro_cond_obj.save(using=proDb)

                    # 评分更新
                    if int(query_type) == Constant.MAIN_TYPE:
                        query_score = InsProductScore
                    else:
                        query_score = InsProductScoreAtt

                    # # 产品评分类型数组
                    # guarantee_set = set()
                    # # 若为定期型或终生型时
                    # if pro_obj.guarantee_pattern == Constant.GUARANTEE_PATTERN_ALL:
                    #     guarantee_set.add(Constant.GUARANTEE_PATTERN_LONG)
                    #     guarantee_set.add(Constant.GUARANTEE_PATTERN_REGULAR)
                    # else:
                    #     guarantee_set.add(pro_obj.guarantee_pattern)
                    # # 取到评分model
                    # for item_g in guarantee_set:
                    #     score_model = query_score.objects.get(product_id=id, guarantee_pattern=item_g)
                    #     # 历史
                    #     return_score_his = fun_ins_product_score_his(score_model, query_type, Constant.ACTION_UPDATE,
                    #                                                  user_id)
                    #     if return_score_his == Constant.FAIL:
                    #         raise IntegrityError
                    #     # 保费评分重新计算
                    #     score_pre = get_premium_score(id, query_type, pro_obj.category_type, pro_obj.guarantee_pattern)
                    #     score_model.score_pre = score_pre
                    #     score_model.score = score_pre + score_model.score_other
                    #     score_model.updated_by = user_id
                    #     score_model.updated_time = user_id
                    #     # 保存
                    #     score_model.save()

                # 缓存设定文件更新
                ins_pro_change_redis(pro_obj.category_type)

        except IntegrityError as err:
            return Response({'code': Constant.SQL_EXEC_ERR_CODE, 'message': Constant.SQL_EXEC_ERR_MSG})

        # 数据插入DB
        db_result = ratio_db_process(ratio_list, id, schedule_id,user_id, query_type)
        return Response(db_result)
    except AuthenticationFailed:
        return Response({'code': Constant.AUTH_ERR_CODE, 'message': Constant.AUTH_ERR_MSG})


# 产品费率编辑--模板2
@api_view(['POST'])
@csrf_exempt
@parser_classes((MultiPartParser,))
def ratio_edit2(request, id, schedule_id, user_id):
    # token
    token = request.data.get('fileName')
    query_type = request.data.get('type')
    # token验证
    expir_model = ExpiringTokenAuthentication()
    try:
        ExpiringTokenAuthentication.authenticate_credentials(expir_model, token)
        # 费率文件上传到服务器
        file_obj = request.FILES.get('file', None)
        if int(query_type) == Constant.MAIN_TYPE:
            # 文件名前缀
            file_prefix = 'ratio_'
        else:
            file_prefix = 'ratio_attach_'
        filename = FILE_FOLDER + 'insurance_excel/' + file_prefix + str(id) + 'sch' + str(schedule_id) + '.xlsx'
        # filename = 'ratio'+str(id) + 'sch' + str(schedule_id) + '.xlsx'
        destination = open(filename, 'wb+')
        for chunk in file_obj.chunks():
            destination.write(chunk)
        destination.close()

        user_id = request.user.id
        file_path = r'' + filename + ''
        # 社保列表
        social_dict = Constant.SOCIAL_DICT
        reverse_social_dictt = reversed_dict(social_dict)
        # 性别字典
        sex_dict = Constant.SEX_DICT
        reverse_sex_dict = reversed_dict(sex_dict)
        # 费率数组
        ratio_list = []
        # condi数据集合
        con_set = Constant.CON_DATA_SET

        # 上传费率的产品名称和计划名称获得
        if int(query_type) == Constant.MAIN_TYPE:
            query_pro = InsProduct
            query_sch = InsProductSchedule
        else:
            query_pro = InsProAtt
            query_sch = InsProSchAtt

        # 查询
        pro_obj = query_pro.objects.using(proDb).get(id=id)
        sch_obj = query_sch.objects.using(proDb).get(id=schedule_id)
        # 错误产品计划名称
        log_msg_pro_name = "产品【" + pro_obj.name + "】的计划【" + sch_obj.name + "】"

        # 读取文件
        wb = open_workbook(file_path)
        if wb:
            table = wb.sheet_by_index(0)
            rows = table.nrows
            row_first = table.row_values(0)
            for i in range(1, rows):
                row = table.row_values(i)
                # 费率
                ratio_value = row[row_first.index('费率')]
                if ratio_value:
                    try:
                        ratio_value = float(ratio_value)
                    except ValueError:
                        # 单元格行列号
                        rc = chr(row[row_first.index('费率')]) + str(i + 1)
                        error_msg = rc + '的费率值为数值'
                        # 输出错误日志
                        log_msg = log_msg_pro_name + str(i+1) + "行的费率值应为数值"
                        Logger(Constant.LOG_FILENAME_INS_PRO, level='error').logger.error(log_msg)
                        return Response({"code": Constant.VALUE_ERROR_CODE, 'error_msg': error_msg})

                    # 社保
                    social = row[row_first.index('社保')]
                    if social == Constant.EXPORT_VALUE_UNLIMITED:
                        social_key = None
                    else:
                        social_key = reverse_social_dictt[social]
                    # 起始年龄
                    try:
                        start_age = int(row[row_first.index('起始年龄')])
                    except ValueError:
                        rc = chr(row[row_first.index('起始年龄')]) + str(i + 1)
                        error_msg = rc + '的年龄为整数'

                        # 输出错误日志
                        log_msg = log_msg_pro_name + str(i + 1) + "行的起始年龄应为整数"
                        Logger(Constant.LOG_FILENAME_INS_PRO, level='error').logger.error(log_msg)

                        return Response({"code": Constant.VALUE_ERROR_CODE, 'error_msg': error_msg})
                    # 结束年龄
                    try:
                        end_age = int(row[row_first.index('结束年龄')])
                    except ValueError:
                        rc = chr(row[row_first.index('结束年龄')]) + str(i + 1)
                        error_msg = rc + '的年龄为整数'

                        # 输出错误日志
                        log_msg = log_msg_pro_name + str(i + 1) + "行的结束年龄应为整数"
                        Logger(Constant.LOG_FILENAME_INS_PRO, level='error').logger.error(log_msg)

                        return Response({"code": Constant.VALUE_ERROR_CODE, 'error_msg': error_msg})
                    # 性别
                    sex = row[row_first.index('性别')]

                    for age_item in range(start_age, (end_age+1)):
                        # 存储
                        if sex == Constant.EXPORT_VALUE_UNLIMITED:
                            for s_key, s_val in sex_dict.items():
                                con_set_dict = con_set[s_key]
                                # 年龄集合添加
                                con_set_dict['age'].add(age_item)
                                # 社保集合添加
                                if social_key:
                                    con_set_dict['social'].add(social_key)
                                # 存储
                                v_ratio = {'social': social_key, 'career': None, 'sex': int(s_key),
                                           'guarantee': 1, 'payment': 1,
                                           'age': age_item, 'ratio': ratio_value}
                                ratio_list.append(v_ratio)
                        else:
                            sex_key = reverse_sex_dict[sex]
                            con_set_dict = con_set[sex_key]
                            # 年龄集合添加
                            con_set_dict['age'].add(age_item)
                            # 社保集合添加
                            if social_key:
                                con_set_dict['social'].add(social_key)
                            # 存储
                            v_ratio = {'social': social_key, 'career': None, 'sex': int(sex_key),
                                       'guarantee': 1, 'payment': 1,
                                       'age': age_item, 'ratio': ratio_value}
                            ratio_list.append(v_ratio)
        # 费率条件添加
        try:
            with transaction.atomic(using=proDb):
                if int(query_type) == Constant.MAIN_TYPE:
                    query_con = InsProductRatioCondition
                else:
                    query_con = InsProRatioConAtt
                # 现有数据设为无效
                query_con.objects.using(proDb).filter(ins_product_schedule_id=schedule_id, status=Constant.STATUS_ACTIVE).update(status=Constant.STATUS_UNACTIVE)
                # 费率条件添加
                for con_key, con_item in con_set.items():
                    if con_item['age']:
                        age_min = min(con_item['age'])
                        age_max = max(con_item['age'])
                        # 保障期限字符串
                        if con_item['social']:
                            social_str = ',' . join(con_item['social'])
                        else:
                            social_str = None
                        try:
                            pro_cond_obj = query_con.objects.using(proDb).get(ins_product_schedule_id=schedule_id,gender=con_key)
                            # 历史记录
                            return_ratio_his = fun_ins_product_ratio_condition_his(pro_cond_obj, query_type,  Constant.ACTION_UPDATE,
                                                                                   user_id)
                            if return_ratio_his == Constant.FAIL:
                                raise IntegrityError
                            # 更新
                            pro_cond_obj.gender = con_key
                            pro_cond_obj.age_min = age_min
                            pro_cond_obj.age_max = age_max
                            pro_cond_obj.social_guarantee = social_str
                            pro_cond_obj.career_type = None
                            pro_cond_obj.payment_period = 1
                            pro_cond_obj.guarantee_period = 1
                            pro_cond_obj.status = Constant.STATUS_ACTIVE
                            pro_cond_obj.save(using=proDb)

                        except query_con.DoesNotExist:
                            pro_cond_obj = query_con(
                                ins_product_id=id,
                                ins_product_schedule_id=schedule_id,
                                social_guarantee=social_str,
                                gender=con_key,
                                age_min=age_min,
                                age_max=age_max,
                                career_type=None,
                                payment_period=1,
                                guarantee_period=1,
                                status=Constant.STATUS_ACTIVE
                            )
                            pro_cond_obj.save(using=proDb)

                # 缓存设定文件更新
                ins_pro_change_redis(pro_obj.category_type)

        except IntegrityError as err:
            # 输出错误日志
            log_msg = log_msg_pro_name + str(err)
            Logger(Constant.LOG_FILENAME_INS_PRO, level='error').logger.error(log_msg)

            return Response({'code': Constant.SQL_EXEC_ERR_CODE, 'message': Constant.SQL_EXEC_ERR_MSG})
        # 数据插入DB
        db_result = ratio_db_process(ratio_list, id, schedule_id,user_id, query_type)
        return Response(db_result)
    except AuthenticationFailed:
        # 输出错误日志
        Logger(Constant.LOG_FILENAME_INS_PRO, level='error').logger.error(Constant.AUTH_ERR_MSG)
        return Response({'code': Constant.AUTH_ERR_CODE, 'message': Constant.AUTH_ERR_MSG})


# 费率文件下载
@api_view(['GET'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def download_excel(request, id, schedule_id, type):
    if int(type) == Constant.MAIN_TYPE:
        query_pro = InsProduct
        query_sch = InsProductSchedule
        file_prefix = "ratio_"
    else:
        query_pro = InsProAtt
        query_sch = InsProSchAtt
        file_prefix = "ratio_attach_"
    product = query_pro.objects.using(proDb).get(id=id)
    schedule = query_sch.objects.using(proDb).get(id=schedule_id)
    fileName = product.name + '_' + schedule.name
    file_name = file_prefix + str(id) + 'sch' + str(schedule_id)+'.xlsx'
    file_path = FILE_FOLDER + 'insurance_excel/' + file_name
    if os.path.exists(file_path):
        return_url = "https://" + SERVER_HOST + '/insurance_excel/' + file_name
        return Response({'code': Constant.REQUEST_SUCCESS_CODE, 'url': return_url, 'fileName': fileName})
    else:
        return Response({'code': Constant.ERR_NONE_EXIST_CODE, 'msg': Constant.ERR_NONE_EXIST_MESSAGE})


# 产品计划编辑
@api_view(['POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def schedule_edit(request):
    user_id = request.user.id
    if request.method == 'POST':
        try:
            with transaction.atomic(using=proDb):
                # 2020/01/08 新增字段
                pro_type = request.data.get('type')
                schedule_id = request.data.get('id')
                id = request.data.get('product_id')
                name = request.data.get('schedule_name')
                amount_type = request.data.get('amount_type')
                amount_min = request.data.get('amount_min')
                amount_max = request.data.get('amount_max')
                # 免赔额和赔付系数
                deductible = request.data.get('deductible')
                comp_ratio = request.data.get('comp_ratio')
                if deductible:
                    has_deductible = Constant.STATUS_ACTIVE
                else:
                    has_deductible = Constant.STATUS_UNACTIVE

                if comp_ratio:
                    has_comp_ratio = Constant.STATUS_ACTIVE
                else:
                    has_comp_ratio = Constant.STATUS_UNACTIVE

                # 计划的model名称
                if int(pro_type) == Constant.MAIN_TYPE:
                    query_sch = InsProductSchedule
                    query_pro = InsProduct
                    query_ded = InsProSchDed
                    query_comp = InsProSchComp
                else:
                    query_sch = InsProSchAtt
                    query_pro = InsProAtt
                    query_ded = InsProSchDedAtt
                    query_comp = InsProSchCompAtt
                # 新建计划
                if int(schedule_id) == 0:
                    obj_schedule = query_sch(
                        ins_product_id=id,
                        name=name,
                        amount_min=amount_min,
                        amount_max=amount_max,
                        amount_type=amount_type,
                        status=Constant.STATUS_ACTIVE,
                        has_deductible=has_deductible,
                        has_compensation_ratio=has_comp_ratio
                    )
                    obj_schedule.save(using=proDb)
                    return_schedule_id = obj_schedule.id
                else:
                    # 历史数据
                    his_schedule = query_sch.objects.using(proDb).get(id=schedule_id)
                    # 历史记录保存
                    return_sch_his = fun_ins_product_schedule_his(his_schedule, pro_type,  Constant.ACTION_UPDATE, user_id)
                    if return_sch_his == Constant.FAIL:
                        raise IntegrityError
                    # 更新
                    obj_schedule = query_sch.objects.using(proDb).get(id=schedule_id)
                    obj_schedule.name = name
                    obj_schedule.amount_type = amount_type
                    obj_schedule.amount_max = amount_max
                    obj_schedule.amount_min = amount_min
                    obj_schedule.has_deductible = has_deductible
                    obj_schedule.has_compensation_ratio = has_comp_ratio
                    obj_schedule.save(using=proDb)
                    return_schedule_id = obj_schedule.id

                # 是否有多个计划
                schedule_num = query_sch.objects.using(proDb).filter(ins_product_id=id, status=Constant.STATUS_ACTIVE).count()
                ins_product = query_pro.objects.using(proDb).get(id=id)
                has_schedule = ins_product.has_schedule

                # 错误产品计划名称
                log_msg_pro_name = "产品【" + ins_product.name + "】的计划【" + name + "】"
                # 当前是否有计划
                if schedule_num == 1:
                    current_status = 0
                else:
                    current_status = 1

                # 状态改变时，修改产品表
                if has_schedule != current_status:
                    # 历史数据
                    return_pro_his = fun_ins_product_his(ins_product, pro_type, Constant.ACTION_UPDATE, user_id)
                    if return_pro_his == Constant.FAIL:
                        raise IntegrityError

                    # 产品表更新
                    query_pro.objects.using(proDb).filter(id=id).update(has_schedule=current_status)

                # 免赔额信息
                for item_ded in deductible:
                    ded_form = InsProSchDedCompForm(item_ded)
                    if ded_form.is_valid():
                        ded_cleaned_data = ded_form.cleaned_data
                        if item_ded['id']:
                            try:
                                ded_obj = query_ded.objects.using(proDb).get(id=item_ded['id'])
                                # 历史记录
                                return_ded_his = fun_ins_pro_sch_deductible_his(ded_obj, pro_type, Constant.ACTION_UPDATE,
                                                                     user_id)
                                if return_ded_his == Constant.FAIL:
                                    raise IntegrityError
                                # 更新
                                ded_obj.name = ded_cleaned_data.get('name')
                                ded_obj.ratio = ded_cleaned_data.get('ratio')
                                ded_obj.save(using=proDb, update_fields=['name', 'ratio'])
                            except query_ded.DoesNotExist:
                                pass
                        else:
                            ded_obj = query_ded(
                                ins_product_id=id,
                                ins_product_schedule_id=return_schedule_id,
                                name=ded_cleaned_data.get('name'),
                                ratio=ded_cleaned_data.get('ratio'),
                                status=Constant.STATUS_ACTIVE
                            )
                            ded_obj.save(using=proDb)

                    else:
                        # 输出错误日志
                        log_msg = log_msg_pro_name + "免赔额格式有误：" + str(ded_form.errors)
                        Logger(Constant.LOG_FILENAME_INS_PRO, level='error').logger.error(log_msg)
                        raise IntegrityError

                # 赔付系数信息
                for item_comp in comp_ratio:
                    comp_form = InsProSchDedCompForm(item_comp)
                    if comp_form.is_valid():
                        comp_cleaned_data = comp_form.cleaned_data
                        if item_comp['id']:
                            try:
                                comp_obj = query_comp.objects.using(proDb).get(id=item_comp['id'])
                                # 历史记录
                                return_ded_his = fun_ins_pro_sch_comp_ratio_his(comp_obj, pro_type,
                                                                                Constant.ACTION_UPDATE,
                                                                                user_id)
                                if return_ded_his == Constant.FAIL:
                                    raise IntegrityError
                                # 更新
                                comp_obj.name = comp_cleaned_data.get('name')
                                comp_obj.ratio = comp_cleaned_data.get('ratio')
                                comp_obj.save(using=proDb, update_fields=['name', 'ratio'])
                            except query_ded.DoesNotExist:
                                pass
                        else:
                            comp_obj = query_comp(
                                ins_product_id=id,
                                ins_product_schedule_id=return_schedule_id,
                                name=comp_cleaned_data.get('name'),
                                ratio=comp_cleaned_data.get('ratio'),
                                status=Constant.STATUS_ACTIVE
                            )
                            comp_obj.save(using=proDb)
                    else:
                        # 输出错误日志
                        log_msg = log_msg_pro_name + "免赔额格式有误：" + str(comp_form.errors)
                        Logger(Constant.LOG_FILENAME_INS_PRO, level='error').logger.error(log_msg)

                # 缓存设定文件更新
                ins_pro_change_redis(ins_product.category_type)

                return Response(
                    {'code': Constant.REQUEST_SUCCESS_CODE, 'message': Constant.REQUEST_SUCCESS_MSG, 'schedule_id': return_schedule_id})
        except IntegrityError as err:
            return Response({'code': Constant.REQUEST_FAIL_CODE, 'message': str(err)})


# 计划免赔额删除
@api_view(['POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def deductible_delete(request):
    try:
        with transaction.atomic(using=proDb):
            pro_type = request.data.get('type')
            key_id = request.data.get('id')
            schedule_id = request.data.get('schedule_id')

            user_id = request.user.id
            # model名称获得
            if int(pro_type) == Constant.MAIN_TYPE:
                query_sch = InsProductSchedule
                query_ded = InsProSchDed
                query_pro = InsProduct
            else:
                query_sch = InsProSchAtt
                query_ded = InsProSchDedAtt
                query_pro = InsProAtt

            try:
                ded_obj = query_ded.objects.using(proDb).get(id=key_id)
                sch_obj = query_sch.objects.using(proDb).get(id=schedule_id)
                pro_obj = query_pro.objects.using(proDb).get(id=sch_obj.ins_product_id)
                # 历史记录
                return_ded_his = fun_ins_pro_sch_deductible_his(ded_obj, pro_type, Constant.ACTION_UPDATE,
                                                                user_id)
                if return_ded_his == Constant.FAIL:
                    raise IntegrityError
                # 更新
                ded_obj.status = Constant.STATUS_UNACTIVE
                ded_obj.save(using=proDb, update_fields=['status'])

                # 判定相应计划是否还有其他的免赔额
                ded_nums_current = query_ded.objects.using(proDb).filter(ins_product_schedule_id=schedule_id).count()
                # 没有其他免赔额时，更新计划中的免赔额
                if ded_nums_current == 0:
                    sch_obj.has_deductible = Constant.STATUS_UNACTIVE
                    sch_obj.save(using=proDb, update_fields=['has_deductible'])

                # 缓存设定文件更新
                ins_pro_change_redis(pro_obj.category_type)

                return Response(
                    {'code': Constant.REQUEST_SUCCESS_CODE, 'message': Constant.REQUEST_SUCCESS_MSG})
            except query_ded.DoesNotExist:
                return Response({'code': Constant.ERR_NONE_EXIST_CODE, 'message': Constant.ERR_NONE_EXIST_MESSAGE})
            except query_sch.DoesNotExist:
                return Response({'code': Constant.ERR_NONE_EXIST_CODE, 'message': Constant.ERR_NONE_EXIST_MESSAGE})
    except IntegrityError:
        return Response({'code': Constant.REQUEST_FAIL_CODE, 'message': Constant.REQUEST_FAIL_MSG})


# 计划赔付比例删除
@api_view(['POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def comp_ratio_delete(request):
    try:
        with transaction.atomic(using=proDb):
            pro_type = request.data.get('type')
            key_id = request.data.get('id')
            schedule_id = request.data.get('schedule_id')

            user_id = request.user.id
            # model名称获得
            if int(pro_type) == Constant.MAIN_TYPE:
                query_sch = InsProductSchedule
                query_comp = InsProSchComp
                query_pro = InsProduct
            else:
                query_sch = InsProSchAtt
                query_comp = InsProSchCompAtt
                query_pro = InsProAtt

            try:
                comp_obj = query_comp.objects.using(proDb).get(id=key_id)
                sch_obj = query_sch.objects.using(proDb).get(id=schedule_id)
                pro_obj = query_pro.objects.using(proDb).get(id=sch_obj.ins_product_id)
                # 历史记录
                return_his = fun_ins_pro_sch_comp_ratio_his(comp_obj, pro_type, Constant.ACTION_UPDATE,
                                                                user_id)
                if return_his == Constant.FAIL:
                    raise IntegrityError
                # 更新
                comp_obj.status = Constant.STATUS_UNACTIVE
                comp_obj.save(using=proDb, update_fields=['status'])

                # 判定相应计划是否还有其他的免赔额
                nums_current = query_comp.objects.using(proDb).filter(ins_product_schedule_id=schedule_id).count()
                # 没有其他免赔额时，更新计划中的免赔额
                if nums_current == 0:
                    sch_obj.has_compensation_ratio = Constant.STATUS_UNACTIVE
                    sch_obj.save(using=proDb, update_fields=['has_compensation_ratio'])

                # 缓存设定文件更新
                ins_pro_change_redis(pro_obj.category_type)

                return Response(
                    {'code': Constant.REQUEST_SUCCESS_CODE, 'message': Constant.REQUEST_SUCCESS_MSG})
            except query_comp.DoesNotExist:
                return Response({'code': Constant.ERR_NONE_EXIST_CODE, 'message': Constant.ERR_NONE_EXIST_MESSAGE})
            except query_sch.DoesNotExist:
                return Response({'code': Constant.ERR_NONE_EXIST_CODE, 'message': Constant.ERR_NONE_EXIST_MESSAGE})
    except IntegrityError:
        return Response({'code': Constant.REQUEST_FAIL_CODE, 'message': Constant.REQUEST_FAIL_MSG})


# 产品计划删除
@api_view(['POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def schedule_delete(request):
    try:
        with transaction.atomic(using=proDb):
            pro_type = request.data.get('type')
            schedule_id = request.data.get('id')
            product_id = request.data.get('product_id')

            user_id = request.user.id
            # model名称获得
            if int(pro_type) == Constant.MAIN_TYPE:
                query_sch = InsProductSchedule
                query_ratio = InsProductRatio
                query_con = InsProductRatioCondition
                query_pro = InsProduct
                query_ded = InsProSchDed
                query_comp = InsProSchComp
            else:
                query_sch = InsProSchAtt
                query_ratio = InsProRatioAtt
                query_con = InsProRatioConAtt
                query_pro = InsProAtt
                query_ded = InsProSchDedAtt
                query_comp = InsProSchCompAtt

            # 验证附加险是否可以删除,绑定不可删除
            if int(pro_type) == Constant.ATTACH_TYPE:
                bind_len = InsScheduleBind.objects.using(proDb).filter(attach_id=product_id, attach_sch_id=schedule_id, status=Constant.STATUS_ACTIVE).count()
                if bind_len:
                    return Response({'code': Constant.FAIL, 'message': "已被绑定，不可删除"})

            # 历史数据
            his_schedule_single = query_sch.objects.using(proDb).get(id=schedule_id)
            return_sch_his = fun_ins_product_schedule_his(his_schedule_single, pro_type, Constant.ACTION_DELETE, user_id)
            if return_sch_his == Constant.FAIL:
                raise IntegrityError

            # 费率历史数据
            his_ratio = query_ratio.objects.using(proDb).filter(ins_product_schedule_id=schedule_id, status=Constant.STATUS_ACTIVE)
            for item_ratio in his_ratio:
                return_ratio_his = fun_ins_product_ratio_his(item_ratio, pro_type, Constant.ACTION_DELETE, user_id)
                if return_ratio_his == Constant.FAIL:
                    raise IntegrityError
            # 费率删除
            query_ratio.objects.using(proDb).filter(ins_product_schedule_id=schedule_id, status=Constant.STATUS_ACTIVE).update(status=Constant.STATUS_UNACTIVE)

            # 条件历史数据
            his_condition = query_con.objects.using(proDb).filter(ins_product_schedule_id=schedule_id)
            for item_con in his_condition:
                return_con_his = fun_ins_product_ratio_condition_his(item_con, pro_type, Constant.ACTION_DELETE, user_id)
                if return_con_his == Constant.FAIL:
                    raise IntegrityError
                query_con.objects.using(proDb).filter(ins_product_schedule_id=schedule_id).update(status=Constant.STATUS_UNACTIVE)

            # 免赔额历史数据
            his_ded = query_ded.objects.using(proDb).filter(ins_product_schedule_id=schedule_id,
                                                   status=Constant.STATUS_ACTIVE)
            for item_ded in his_ded:
                return_ded_his = fun_ins_pro_sch_deductible_his(item_ded, pro_type, Constant.ACTION_DELETE, user_id)
                if return_ded_his == Constant.FAIL:
                    raise IntegrityError
            # 删除
            query_ded.objects.using(proDb).filter(ins_product_schedule_id=schedule_id, status=Constant.STATUS_ACTIVE).update(
                status=Constant.STATUS_UNACTIVE)

            # 赔付比例历史数据
            his_comp = query_comp.objects.using(proDb).filter(ins_product_schedule_id=schedule_id,
                                               status=Constant.STATUS_ACTIVE)
            for item_comp in his_comp:
                return_comp_his = fun_ins_pro_sch_comp_ratio_his(item_comp, pro_type, Constant.ACTION_DELETE, user_id)
                if return_comp_his == Constant.FAIL:
                    raise IntegrityError
            # 删除
            query_comp.objects.using(proDb).filter(ins_product_schedule_id=schedule_id, status=Constant.STATUS_ACTIVE).update(
                status=Constant.STATUS_UNACTIVE)

            # 更新
            query_sch.objects.using(proDb).filter(id=schedule_id).update(status=Constant.STATUS_UNACTIVE)

            # 是否有多个计划
            schedule_num = query_sch.objects.using(proDb).filter(ins_product_id=product_id, status=Constant.STATUS_ACTIVE).count()
            ins_product = query_pro.objects.using(proDb).get(id=product_id)
            has_schedule = ins_product.has_schedule

            # 当前是否有计划
            if schedule_num == 1:
                current_status = 0
            else:
                current_status = 1

            # 状态改变时，修改产品表
            if has_schedule != current_status:
                # 历史记录保存
                return_pro_his = fun_ins_product_his(ins_product, pro_type, Constant.ACTION_UPDATE, user_id)
                if return_pro_his == Constant.FAIL:
                    raise IntegrityError

            # 产品表更新
            query_pro.objects.using(proDb).filter(id=product_id).update(has_schedule=current_status)
            # 缓存设定文件更新
            ins_pro_change_redis(ins_product.category_type)

            return Response({'code': Constant.SUCCESS, 'message': Constant.SUCCESS_MESSAGE})
    except IntegrityError:
        return Response({'code': Constant.FAIL, 'message': Constant.FAIL_MESSAGE})


# 产品删除
@api_view(['POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def product_delete(request):
    try:
        with transaction.atomic(using=proDb):
            pro_type = request.data.get('type')
            pro_id = request.data.get('id')
            user_id = request.user.id
            # model表的名称
            if int(pro_type) == Constant.MAIN_TYPE:
                query_pro = InsProduct
                query_sch = InsProductSchedule
                query_con = InsProductRatioCondition
                query_ratio = InsProductRatio
                query_ded = InsProSchDed
                query_comp = InsProSchComp
            else:
                query_pro = InsProAtt
                query_sch = InsProSchAtt
                query_con = InsProRatioConAtt
                query_ratio = InsProRatioAtt
                query_ded = InsProSchDedAtt
                query_comp = InsProSchCompAtt

            # 验证附加险是否可以删除,绑定不可删除
            if int(pro_type) == Constant.ATTACH_TYPE:
                bind_len = InsProductBind.objects.using(proDb).filter(attach_id=pro_id, status=Constant.STATUS_ACTIVE).count()
                if bind_len:
                    return Response({'code': Constant.FAIL, 'message': "已被绑定，不可删除"})

            # 历史记录
            his_ratio = query_ratio.objects.using(proDb).filter(ins_product_id=pro_id, status=Constant.STATUS_ACTIVE)
            for item_ratio in his_ratio:
                return_ratio_his = fun_ins_product_ratio_his(item_ratio, pro_type, Constant.ACTION_DELETE, user_id)
                if return_ratio_his == Constant.FAIL:
                    raise IntegrityError
            # 更新
            query_ratio.objects.using(proDb).filter(ins_product_id=pro_id, status=Constant.STATUS_ACTIVE).update(status=Constant.STATUS_UNACTIVE)

            # InsProductSchedule历史数据
            his_schedule = query_sch.objects.using(proDb).filter(ins_product_id=pro_id, status=Constant.STATUS_ACTIVE)
            for item_schedule in his_schedule:
                return_sch_his = fun_ins_product_schedule_his(item_schedule, pro_type, Constant.ACTION_DELETE, user_id)
                if return_sch_his == Constant.FAIL:
                    raise IntegrityError
            # 更新
            query_sch.objects.filter(ins_product_id=pro_id, status=Constant.STATUS_ACTIVE).update(status=Constant.STATUS_UNACTIVE)
            # InsProductRatioCondition历史数据
            his_condition = query_con.objects.using(proDb).filter(ins_product_id=pro_id)
            for item_condition in his_condition:
                return_con_his = fun_ins_product_ratio_condition_his(item_condition, pro_type, Constant.ACTION_DELETE, user_id)
                if return_con_his == Constant.FAIL:
                    raise IntegrityError
            # 更新
            query_con.objects.using(proDb).filter(ins_product_id=pro_id, status=Constant.STATUS_ACTIVE).update(status=Constant.STATUS_UNACTIVE)

            # 免赔额历史数据
            his_ded = query_ded.objects.using(proDb).filter(ins_product_id=pro_id,
                                               status=Constant.STATUS_ACTIVE)
            for item_ded in his_ded:
                return_ded_his = fun_ins_pro_sch_deductible_his(item_ded, pro_type, Constant.ACTION_DELETE, user_id)
                if return_ded_his == Constant.FAIL:
                    raise IntegrityError
            # 删除
            query_ded.objects.using(proDb).filter(ins_product_id=pro_id, status=Constant.STATUS_ACTIVE).update(
                status=Constant.STATUS_UNACTIVE)

            # 赔付比例历史数据
            his_comp = query_comp.objects.using(proDb).filter(ins_product_id=pro_id,
                                                 status=Constant.STATUS_ACTIVE)
            for item_comp in his_comp:
                return_comp_his = fun_ins_pro_sch_comp_ratio_his(item_comp, pro_type, Constant.ACTION_DELETE, user_id)
                if return_comp_his == Constant.FAIL:
                    raise IntegrityError
            # 删除
            query_comp.objects.using(proDb).filter(ins_product_id=pro_id, status=Constant.STATUS_ACTIVE).update(
                status=Constant.STATUS_UNACTIVE)
            # 历史数据
            his_product = query_pro.objects.using(proDb).get(id=pro_id)
            return_pro_his = fun_ins_product_his(his_product, pro_type, Constant.ACTION_DELETE, user_id)
            if return_pro_his == Constant.FAIL:
                raise IntegrityError
            # 更新
            query_pro.objects.using(proDb).filter(id=pro_id).update(is_active=Constant.STATUS_UNACTIVE)
            if int(pro_type) == Constant.MAIN_TYPE:

                # 删除排他
                InsProductBindExclude.objects.using(proDb).filter(main_id=pro_id).delete()

                # 计划维度必绑
                InsProductBindSchedule.objects.using(proDb).filter(main_id=pro_id).delete()

                # 删除保额绑定的数据
                del_amount_bind_model = InsAmountBind.objects.using(proDb).filter(main_id=pro_id, status=Constant.STATUS_ACTIVE)
                for item_amount in del_amount_bind_model:
                    amount_bind_model = InsAmountBind.objects.using(proDb).get(id=item_amount.id)
                    # 删除
                    return_del_amount_bind_his = fun_ins_amount_bind_his(amount_bind_model,
                                                                         Constant.ACTION_UPDATE, user_id)
                    if return_del_amount_bind_his == Constant.FAIL:
                        raise IntegrityError
                    # 更新
                    amount_bind_model.status = Constant.STATUS_UNACTIVE
                    amount_bind_model.save(using=proDb)

                # 删除ins_schedule_bind
                sch_binds = InsScheduleBind.objects.using(proDb).filter(main_id=pro_id, status=Constant.STATUS_ACTIVE)
                for item_sch in sch_binds:
                    sch_bind_model = InsScheduleBind.objects.using(proDb).get(id=item_sch.id)
                    # 历史记录
                    return_sch_bind_his = fun_ins_schedule_bind_his(sch_bind_model, Constant.ACTION_UPDATE, user_id)
                    if return_sch_bind_his == Constant.FAIL:
                        raise IntegrityError
                    # 更新
                    sch_bind_model.status = Constant.STATUS_UNACTIVE
                    sch_bind_model.save(using=proDb)

                attach_exist = InsProductBind.objects.using(proDb).filter(main_id=pro_id, status=Constant.STATUS_ACTIVE)
                for row_bind in attach_exist:
                    # 删除绑定
                    bind_model = InsProductBind.objects.using(proDb).get(id=row_bind.id)
                    # 历史记录
                    return_bind_his = fun_ins_product_bind_his(bind_model, Constant.ACTION_UPDATE, user_id)
                    if return_bind_his == Constant.FAIL:
                        raise IntegrityError
                    # 更新
                    bind_model.status = Constant.STATUS_UNACTIVE
                    bind_model.save(using=proDb)
                # 组合产品删除
                try:
                    comp_pro_delete = InsCompositePro.objects.using(proDb).get(main_id=pro_id, status=Constant.STATUS_ACTIVE)
                    # 历史记录添加
                    return_comp_pro_delete_his = fun_ins_composite_pro_his(
                        comp_pro_delete, Constant.ACTION_DELETE, user_id)
                    if return_comp_pro_delete_his == Constant.FAIL:
                        return Constant.FAIL
                    # 状态设置为无效
                    comp_pro_delete.status = Constant.STATUS_UNACTIVE
                    comp_pro_delete.save(using=proDb)
                except InsCompositePro.DoesNotExist:
                    pass
                # 缓存设定文件更新
                ins_pro_change_redis(his_product.category_type)

            return Response({'code': Constant.SUCCESS, 'message': Constant.SUCCESS_MESSAGE})
    except IntegrityError:
        return Response({'code': Constant.FAIL, 'message': Constant.FAIL_MESSAGE})


# 费率源文件上传
@api_view(['POST'])
@csrf_exempt
@parser_classes((MultiPartParser,))
def upload_ratio_file(request):
    # token
    token = request.data.get('fileName')
    # token验证
    expir_model = ExpiringTokenAuthentication()
    try:
        ExpiringTokenAuthentication.authenticate_credentials(expir_model, token)
        pro_id = request.data.get('id')
        pro_type = request.data.get('type')
        upload_type = request.data.get('upload_type')
        file_obj = request.FILES.get('file', None)
        file_suffix = request.data.get('file_suffix')
        user_id = request.data.get('user_id')

        filename = UPLOAD_FILE_NAME[pro_type][upload_type] + str(pro_id) + '.'

        if int(pro_type) == Constant.MAIN_TYPE:
            query_pro = InsProduct
            # 目录
            path = '/main/' + str(pro_id)
        else:
            query_pro = InsProAtt
            # 目录
            path = '/attach/' + str(pro_id)
        # 存储文件
        file_url = upload_file(file_obj, filename, file_suffix, path)
        model = query_pro.objects.using(proDb).get(id=pro_id)
        his_model = query_pro.objects.using(proDb).get(id=pro_id)

        try:
            with transaction.atomic(using=proDb):
                # 历史数据
                return_pro_his = fun_ins_product_his(his_model, pro_type, Constant.ACTION_UPDATE, user_id)
                if return_pro_his == Constant.FAIL:
                    raise IntegrityError

                # 更新
                upload_type_int = int(upload_type)
                if upload_type_int == 1:
                    model.file_url = file_url
                elif upload_type_int == 2:
                    model.notice_file_url = file_url
                elif upload_type_int == 3:
                    model.clause_file_url = file_url
                elif upload_type_int == 4:
                    model.leaflets_file_url = file_url
                model.updated_by = int(user_id)
                model.save()
                return Response({'code': Constant.SUCCESS, 'message': Constant.SUCCESS_MESSAGE})
        except IntegrityError:
            return Response({'code': Constant.FAIL, 'message': Constant.FAIL_MESSAGE})
    except AuthenticationFailed:
        return Response({'code': Constant.AUTH_ERR_CODE, 'message': Constant.AUTH_ERR_MSG})


# 源文件下载
@api_view(['POST'])
@csrf_exempt
@parser_classes((MultiPartParser,))
def file_download(request):
    def file_iterator(file_name, chunk_size=512):
        with open(file_name) as f:
            while True:
                c = f.read(chunk_size)
                if c:
                    yield c
                else:
                    break
    the_file_name = "file_name.txt"
    response = StreamingHttpResponse(file_iterator(the_file_name))
    response['Content-Type'] = 'application/octet-stream'
    response['Content-Disposition'] = 'attachment;filename="{0}"'.format(the_file_name)
    return response


# 提交数据库
def ratio_db_process(data, id, schedule_id,user_id , query_type):
    try:
        with transaction.atomic(using=proDb):
            if int(query_type) == Constant.MAIN_TYPE:
                query_ratio = InsProductRatio
            else:
                query_ratio = InsProRatioAtt
            # 更新
            query_ratio.objects.using(proDb).filter(ins_product_id=id,ins_product_schedule_id=schedule_id, status=Constant.STATUS_ACTIVE).update(status=Constant.STATUS_UNACTIVE)
            for ex_val in data:
                exist = query_ratio.objects.using(proDb).filter(age=ex_val['age'], gender=ex_val['sex'],
                                                   has_social_security=ex_val['social'], career_type=ex_val['career'],
                                                   payment_period=ex_val['payment'],
                                                   guarantee_period=ex_val['guarantee'], ins_product_id=id,
                                                   ins_product_schedule_id=schedule_id)
                if ex_val['social']:
                    exist = exist.filter(has_social_security=ex_val['social'])
                if ex_val['career']:
                    exist = exist.filter(has_social_security=ex_val['career'])
                exist = exist.first()

                if exist:
                    # 历史记录
                    return_ratio_his = fun_ins_product_ratio_his(exist, query_type, Constant.ACTION_UPDATE, user_id)
                    if return_ratio_his == Constant.FAIL:
                        raise IntegrityError
                    # 更新
                    exist.ratio = ex_val['ratio']
                    exist.status = Constant.STATUS_ACTIVE
                    exist.save(using=proDb)
                else:
                    obj_ratio = query_ratio(
                        age=ex_val['age'],
                        gender=ex_val['sex'],
                        has_social_security=ex_val['social'],
                        career_type=ex_val['career'],
                        payment_period=ex_val['payment'],
                        guarantee_period=ex_val['guarantee'],
                        ratio=ex_val['ratio'],
                        ins_product_id=id,
                        ins_product_schedule_id=schedule_id,
                        status=Constant.STATUS_ACTIVE
                    )
                    obj_ratio.save(using=proDb)

            return {'code': Constant.REQUEST_SUCCESS_CODE, 'message': Constant.REQUEST_SUCCESS_MSG}
    except IntegrityError as err:
        return {'code': Constant.SQL_EXEC_ERR_CODE, 'message': Constant.SQL_EXEC_ERR_MSG}

