from insuranceManagement.models import InsProduct, InsTypeDs, InsProductSchedule, InsProAtt, InsProSchAtt
from insuranceManagement.models_score import  InsProductScore, InsProScoreSuper, InsProScoreSecond, \
    InsProScoreThird,InsProductScoreAtt

from insuranceManagement.forms import InsProScoreForm
from django.db import connection

from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.response import Response
from rest_framework.authentication import SessionAuthentication
from utils.auth import ExpiringTokenAuthentication
from rest_framework.permissions import IsAuthenticated

from static.common.cousterFunction import list_fetchall, dict_fetchall, get_premium_score, ins_pro_change_redis
import re, os, datetime, json, time
from static.common.HistoryData import *
from django.db.models import Min, Max
from static.common.log import Logger
from django.db import DataError
from geeCMS.settings import proDb, proDbName

# Create your views here.


# param id:产品id
@api_view(['POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def pro_score_init(request):
    pro_id = request.data.get('id')
    pro_type = request.data.get('type')
    # model名称
    if int(pro_type) == Constant.MAIN_TYPE:
        query_score = InsProductScore
        query_pro = InsProduct
    else:
        query_score = InsProductScoreAtt
        query_pro = InsProAtt
    # 评分model
    score_model = query_score.objects.using(proDb).order_by('guarantee_pattern').filter(product_id=pro_id)
    # # 初期化初始值
    param_score_init = []
    for item in score_model:
        app_result = {
            'guarantee_pattern': item.guarantee_pattern,
            'ensure_profit': item.ensure_profit,
            'score': item.score
        }
        param_score_init.append(app_result)

    product_model = query_pro.objects.using(proDb).get(id=pro_id)
    # 产品的属性
    guarantee_pattern = [product_model.guarantee_pattern]
    if product_model.guarantee_pattern == Constant.GUARANTEE_PATTERN_ALL:
        guarantee_pattern = [Constant.GUARANTEE_PATTERN_REGULAR, Constant.GUARANTEE_PATTERN_LONG]
    # 保障类型列表
    guarantee_name_list = Constant.GUARANTEE_PATTERN_DICT
    # 依据险种取得产品利益
    benefit_model = InsTypeDs.objects.using(proDb).get(id=product_model.category_type)

    # 一级分类
    super_list = dict_fetchall(InsProScoreSuper.objects.using(proDb).values_list('id', 'description').order_by('order_by_id').filter(is_active=Constant.STATUS_ACTIVE))
    # 二级分类
    second_list = dict_fetchall(InsProScoreSecond.objects.using(proDb).values_list('id', 'description').order_by('order_by_id').filter(is_active=Constant.STATUS_ACTIVE))
    # 三级分类
    third_list = dict_fetchall(InsProScoreThird.objects.using(proDb).values_list('id', 'description').order_by('order_by_id').filter(is_active=Constant.STATUS_ACTIVE))
    # 三级选项值
    third_option_list = dict_fetchall(InsProScoreThird.objects.using(proDb).values_list('id', 'option_content').order_by('order_by_id').filter(is_active=Constant.STATUS_ACTIVE))
    # 三级评分规则
    third_comment_list = dict_fetchall(InsProScoreThird.objects.using(proDb).values_list('id', 'comment').order_by('order_by_id').filter(is_active=Constant.STATUS_ACTIVE))

    return Response({
        'benefit_list': json.loads(benefit_model.benefit_list),
        'super_list': super_list,
        'second_list': second_list,
        'third_list': third_list,
        'third_option_list': third_option_list,
        'third_comment_list': third_comment_list,
        'guarantee_pattern': guarantee_pattern,
        'guarantee_name_list': guarantee_name_list,
        'param_score_init': param_score_init,
    })


# 产品权益保存
@api_view(['POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def pro_score_save(request):
    if request.method == 'POST':
        # 当前操作者id
        user_id = request.user.id
        # 获取评分参数
        score_param = request.data.get('score_param')
        pro_id = request.data.get('id')
        pro_type = request.data.get('type')

        # model名称
        if int(pro_type) == Constant.MAIN_TYPE:
            query_score = InsProductScore
            query_pro = InsProduct
            query_sck = InsProductSchedule
        else:
            query_score = InsProductScoreAtt
            query_pro = InsProAtt
            query_sck = InsProSchAtt

        log_msg_pro_name = ''
        score_pre = ''
        score_total = ''

        try:
            with transaction.atomic(using=proDb):
                # 该产品信息取得
                model_pro = query_pro.objects.using(proDb).get(id=pro_id)
                log_msg_pro_name = model_pro.name
                # 循环json数组,初期化最大评分
                score_max = 0
                for key, value in score_param.items():
                    # 评分的保障类型
                    guarantee_pattern = int(key)
                    # 初始化评分
                    score = 0
                    # 评分的一级块
                    for super_key, super_value in value['super'].items():
                        # 评分的二级块
                        for second_key, second_value in super_value['second'].items():
                            # 评分的三级块
                            for third_key, third_value in second_value['third'].items():
                                if int(third_key) == 1:
                                    # 高发轻症数量：8分满分（5种缺一种减2分）
                                    if third_value >= 5:
                                        score += 8
                                    elif (5-third_value) > 0:
                                        score += 8-(5-third_value)*2
                                elif int(third_key) == 2:
                                    # 轻症数量：10分满分（选择20（含）以下-3分，20至40（含）-6分，40至60-9分，60以上10分）
                                    if third_value > 60:
                                        score += 10
                                    elif third_value > 40:
                                        score += 9
                                    elif third_value > 20:
                                        score += 6
                                    elif third_value > 0:
                                        score += 3
                                elif int(third_key) == 3:
                                    # 赔付次数：8分满分（选择1-2分，2-4分，3-6分，大等4-8分）
                                    if third_value >= 4:
                                        score += 8
                                    elif third_value > 0 and (4-third_value) > 0:
                                        score += 8-(4-third_value)*2
                                elif int(third_key) == 4:
                                    # 轻症/中症是否抵扣重疾保额：是：扣10分；否：不扣分
                                    if third_value == 1:
                                        score -= 10
                                elif int(third_key) == 5:
                                    # 首次赔付额度：7分满分（轻症赔付30%保额为满分基准分，超过减少按比例加减分）
                                    if third_value > 0.3:
                                        score += 7 + round(((third_value - 0.3) / 0.3) * 7, 2)
                                    elif third_value == 0.3:
                                        score += 7
                                    elif third_value > 0 and (0.3 - third_value) > 0:
                                        score += 7 - round(((0.3 - third_value) / 0.3) * 7, 2)
                                elif int(third_key) == 6:
                                    # 是否递增赔付：赔付后轻症/中症递增 3（选择 含递增3分 不含不得分）
                                    if third_value == 1:
                                        score += 3
                                    elif third_value == 2:
                                        score += 1
                                elif int(third_key) == 7:
                                    # 间隔期：2（无间隔 90天 90天以上）
                                    if third_value == 1:
                                        score += 2
                                    elif third_value == 2:
                                        score += 1
                                elif int(third_key) == 8:
                                    # 是否分组 ：不分组  2分；分组  0分
                                    if third_value == 1:
                                        score += 2
                                elif int(third_key) == 9:
                                    # T重疾数量 ：50以下 0  50-70 2 70-100 4 100以上5
                                    if third_value > 100:
                                        score += 5
                                    elif third_value > 70:
                                        score += 4
                                    elif third_value > 50:
                                        score += 2
                                elif int(third_key) == 10:
                                    # 是否多次赔付：10（选择 1-0分，2-5分 大等3-10分）
                                    if third_value >= 3:
                                        score += 10
                                    elif third_value == 2:
                                        score += 5
                                elif int(third_key) == 11:
                                    # 前期是否能额外给付：2（选择 30%满分 不含0分 不满或超过30%按比例加减分）
                                    if third_value > 0.3:
                                        score += 2 + round(((third_value - 0.3) / 0.3) * 2, 2)
                                    elif third_value == 0.3:
                                        score += 2
                                    elif third_value > 0 and (0.3 - third_value) > 0:
                                        score += 2 - round(((0.3 - third_value) / 0.3) * 2, 2)
                                elif int(third_key) == 12:
                                    # 间隔期：2（无间隔 180天1分 365天0）
                                    if third_value == 1:
                                        score += 2
                                    elif third_value == 2:
                                        score += 1
                                elif int(third_key) == 13:
                                    # 是否递增赔付 ：1/0（递增1分 不递增0分）
                                    if third_value == 1:
                                        score += 1
                                elif int(third_key) == 14:
                                    # 是否分组 ：不分组  2分分组且癌症单列一组   1分分组（按数量）   0分
                                    if third_value == 2:
                                        score += 2
                                    elif third_value == 3:
                                        score += 1
                                elif int(third_key) == 15:
                                    # 重疾赔付后能否赔付轻症 ：2（有此项2分 无此项0分）
                                    if third_value == 1:
                                        score += 2
                                elif int(third_key) == 16:
                                    # 18岁前赔付条件 ：1（赔付超100%保费的1分）
                                    if third_value == 1:
                                        score += 1
                                elif int(third_key) == 17:
                                    # 身故赔付条件 ：5（赔保额5分/赔保费2分/赔现价0分）
                                    if third_value == 1:
                                        score += 5
                                    elif third_value == 2:
                                        score += 2
                                elif int(third_key) == 18:
                                    # 是否带全残 ：3/0（带全残3分 不带0分）
                                    if third_value == 1:
                                        score += 3
                                elif int(third_key) == 19:
                                    # 疾病终末期  ：5/0（带疾病终末期5分 不带0分）
                                    if third_value == 1:
                                        score += 5
                                elif int(third_key) == 20:
                                    # 长短 ：1（ 高于90天不得分 高于180天减10分）
                                    if third_value == 1:
                                        score += 1
                                    elif third_value == 3:
                                        score -= 10
                                elif int(third_key) == 21:
                                    # 轻症出险能否继续合同  ：2（加分）
                                    if third_value == 1:
                                        score += 2
                                elif int(third_key) == 23:
                                    # 70岁阿兹海默赔付限制  ：扣5（5分起扣 含此项扣除5分）
                                    if third_value == 1:
                                        score -= 5
                                elif int(third_key) == 24:
                                    # 癌症多次赔付(间隔期)  ：3（3年以上不计分）
                                    if third_value == 1:
                                        score += 3
                                elif int(third_key) == 25:
                                    # 癌症多次赔付(原位癌)  ：5/3分（选择 取决于转移条件,有限制3，无限制5，不存在0）
                                    if third_value == 1:
                                        score += 3
                                    elif third_value == 2:
                                        score += 5
                                elif int(third_key) == 26:
                                    # 癌症多次赔付(重度癌症)  ：7/3分（选择 取决于转移条件,有限制3，无限制5，不存在0）
                                    if third_value == 1:
                                        score += 3
                                    elif third_value == 2:
                                        score += 7
                                elif int(third_key) == 27:
                                    # 急性心肌梗塞二次赔付  ：1/0
                                    if third_value == 1:
                                        score += 1
                                elif int(third_key) == 28:
                                    # 脑中风二次赔付  ：1/0
                                    if third_value == 1:
                                        score += 1
                                elif int(third_key) == 29:
                                    # 高发疾病种类数量:2（1-3种 1分 3以上 2分）
                                    if third_value > 3:
                                        score += 2
                                    elif third_value > 0:
                                        score += 1
                                elif int(third_key) == 30:
                                    # 疾病种类数量：1（10一下 0.5 10以上 1 不含 0）
                                    if third_value > 10:
                                        score += 1
                                    elif third_value > 0:
                                        score += 0.5
                                elif int(third_key) == 31:
                                    # 递增额度多少:2（有递增2分 无递增 0分）
                                    if third_value == 1:
                                        score += 2
                                elif int(third_key) == 32:
                                    # 递增额度多少:3（极度宽松3 宽松 2 正常1 正常但不能人工核保0 严格 -1）
                                    if third_value == 1:
                                        score += 3
                                    elif third_value == 2:
                                        score += 2
                                    elif third_value == 3:
                                        score += 1
                                    elif third_value == 4:
                                        score += 0
                                    elif third_value == 5:
                                        score -= 1
                                elif int(third_key) == 33:
                                    # 免体检额：1（50万以上可加分 排序 可取小数点，递增10w,加0.1，50w一下0分）
                                    if third_value > 500000:
                                        score += 1 + round(((third_value - 500000) / 100000) * 0.1, 2)
                                    elif third_value == 500000:
                                        score += 1
                                elif int(third_key) == 34:
                                    # 特色附加险,含有3分，不含0分
                                    if third_value == 1:
                                        score += 3
                                elif int(third_key) == 35:
                                    # 含30年缴费期：a:含，但35岁不可以，分数=1；b:含，且35岁以上可30年缴费，分数=1 + 0.5
                                    if third_value == 1:
                                        score += 1
                                    elif third_value == 2:
                                        score += 1.5
                                elif int(third_key) == 36:
                                    # 是否覆盖医保范围外费用报销：不覆盖-50分,覆盖0分
                                    if third_value == 1:
                                        score -= 50
                                elif int(third_key) == 37:
                                    # 覆盖地区：全球：5分，全球除美 ：4分，亚太：3分，大中华：2分，国内：0分
                                    if third_value == 1:
                                        score += 5
                                    elif third_value == 2:
                                        score += 4
                                    elif third_value == 3:
                                        score += 3
                                    elif third_value == 4:
                                        score += 2
                                elif int(third_key) == 38:
                                    # 涵盖医院范围(多选)：昂贵：1分，私立 ：1分，特需部：3分，地段医院：1分
                                    if "1" in third_value:
                                        score += 1
                                    if "2" in third_value:
                                        score += 1
                                    if "3" in third_value:
                                        score += 3
                                    if "4" in third_value:
                                        score += 1
                                elif int(third_key) == 39:
                                    # 外购药能否报销：明确可以报销：6分，未明确 ：0分，明确不可以报销：-6分
                                    if third_value == 1:
                                        score += 6
                                    elif third_value == 3:
                                        score -= 6
                                elif int(third_key) == 40:
                                    # 免赔额：包含无免赔：10分，1万免赔 ：0分，5000免赔/10000免赔：5分
                                    if third_value == 1:
                                        score += 10
                                    elif third_value == 3:
                                        score += 5
                                elif int(third_key) == 41:
                                    # 续保条件：无规定：0分，允许连续投保，费率整体调整 ：3分，保障期在一年以上：6分
                                    if third_value == 2:
                                        score += 3
                                    elif third_value == 3:
                                        score += 6
                                elif int(third_key) == 42:
                                    # 保险公司类型：健康险：4分，寿险：2分，财险：0分
                                    if third_value == 1:
                                        score += 4
                                    elif third_value == 2:
                                        score += 2
                                elif int(third_key) == 43:
                                    # 支付方式：直付：4分，垫付：1分，事后报销：0分
                                    if third_value == 1:
                                        score += 4
                                    elif third_value == 2:
                                        score += 1
                                elif int(third_key) == 44:
                                    # 健康告知及核保：自行填写分值，范围：0分~10分
                                    if third_value > 10:
                                        score += 40
                                    elif third_value > 0:
                                        score += third_value
                                elif int(third_key) == 45:
                                    # 是否覆盖既往症：覆盖：10分，不覆盖：0分
                                    if third_value == 1:
                                        score += 10
                                elif int(third_key) == 46:
                                    # 服务：自行填写分值，范围：0分~5分
                                    if third_value > 5:
                                        score += 5
                                    elif third_value > 0:
                                        score += third_value
                                elif int(third_key) == 47:
                                    # 健康告知/核保 ：自行填写分值，范围：0分~15分
                                    if third_value > 15:
                                        score += 15
                                    elif third_value > 0:
                                        score += third_value
                                elif int(third_key) == 48:
                                    # 免责条款：3免责：10分，5免责：5分，5以上：0分
                                    if third_value == 1:
                                        score += 10
                                    elif third_value == 2:
                                        score += 5
                                elif int(third_key) == 49:
                                    # 等待期：90天及以下：10分，90天以上且低于180天（含180天）：5分，180天以上：0分
                                    if third_value == 1:
                                        score += 10
                                    elif third_value == 2:
                                        score += 5
                                elif int(third_key) == 50:
                                    # 缴费期少于保障期：是：-5分，否：0分
                                    if third_value == 1:
                                        score -= 5
                                elif int(third_key) == 51:
                                    # 是否含全残：是：0分，否：-20分
                                    if third_value == 2:
                                        score -= 20
                                elif int(third_key) == 52:
                                    # 仅保障中国大陆：是：-20分，否：0分
                                    if third_value == 1:
                                        score -= 20
                                elif int(third_key) == 53:
                                    # 保障区域范围：全球：10分，境内：0分
                                    if third_value == 1:
                                        score += 10
                                elif int(third_key) == 54:
                                    # 含猝死：含：5分，不含：0分
                                    if third_value == 1:
                                        score += 5
                                elif int(third_key) == 55:
                                    # 伤残赔付标准：10%-100%：10分，其他：0分
                                    if third_value == 1:
                                        score += 10
                                elif int(third_key) == 56:
                                    # 赔付条件是否限意外发生后180天内：是：0分，否：5分
                                    if third_value == 2:
                                        score += 5
                                elif int(third_key) == 57:
                                    # 是否仅赔付身故：是：-50分，否：0分
                                    if third_value == 1:
                                        score -= 50
                                elif int(third_key) == 58:
                                    # 特别条款 ：自行填写分值，范围：-20分~20分
                                    if third_value > 20:
                                        score += 20
                                    elif third_value > -20:
                                        score += third_value
                                    else:
                                        score -= 20
                    # 保费的评分确定
                    score_pre = get_premium_score(pro_id, pro_type, model_pro.category_type, guarantee_pattern)
                    # 总的评分
                    score_total = score + score_pre
                    if score_total < -9999:
                        score_total = -9999
                    # 设定一个产品的最高分值
                    if score_max < score_total:
                        score_max = score_total
                    # 新建的标志位
                    create_flg = True
                    # 查询是否已经存在
                    try:
                        model_score = query_score.objects.using(proDb).get(product_id=pro_id, guarantee_pattern=guarantee_pattern)
                        return_score_his = fun_ins_product_score_his(model_score, pro_type, Constant.ACTION_UPDATE, user_id)
                        create_flg = False
                        if return_score_his == Constant.FAIL:
                            raise IntegrityError
                        model_score.ensure_profit = {key: value}
                        model_score.score = score_total
                        model_score.score_pre = score_pre
                        model_score.score_other = score
                        model_score.updated_by = user_id
                    except query_score.DoesNotExist:
                        # 创建评分model
                        model_score = query_score(
                            product_id=pro_id,
                            guarantee_pattern=guarantee_pattern,
                            ensure_profit={key: value},
                            score=score_total,
                            score_pre=score_pre,
                            score_other=score,
                            created_by=user_id,
                            updated_by=user_id
                        )
                    score_data = {"product_id": pro_id, "guarantee_pattern": guarantee_pattern, "ensure_profit": {key: value}, "score": score}
                    # 初期化评分form
                    form_score = InsProScoreForm(score_data, instance=model_score)
                    if form_score.is_valid():
                        # 评分model保存
                        model_score.save(using=proDb)
                        if create_flg:
                            # 评分历史model
                            return_score_his = fun_ins_product_score_his(model_score, pro_type, Constant.ACTION_INSERT, user_id)
                            if return_score_his == Constant.FAIL:
                                raise IntegrityError
                    else:

                        return Response({'code': Constant.INVAILD, 'message': form_score.errors})

                # 更新产品表的评分值
                his_model = query_pro.objects.using(proDb).get(id=pro_id)
                # 历史数据
                return_pro_his = fun_ins_product_his(his_model, pro_type, Constant.ACTION_UPDATE, user_id)
                if return_pro_his == Constant.FAIL:
                    raise IntegrityError

                # 更新
                model_pro.score = score_max
                model_pro.save(using=proDb)

                ins_pro_change_redis(model_pro.category_type)

                return Response({'code': Constant.SUCCESS, 'message': Constant.SUCCESS_MESSAGE})
        except IntegrityError as err:
            # 输出错误日志
            log_msg = log_msg_pro_name + "的保费分值【" + str(score_pre) + "】，总分【" + str(score_total) + "】" + "错误信息【" + str(err) + "】"
            Logger(Constant.LOG_FILENAME_INS_PRO_SCORE, level='error').logger.error(log_msg)
            return Response({'code': Constant.FAIL, 'message': Constant.FAIL_MESSAGE})
        except DataError as err:
            # 输出错误日志
            log_msg = log_msg_pro_name + "的保费分值【" + str(score_pre) + "】，总分【" + str(score_total) + "】" + "错误信息【" + str(err) + "】"
            Logger(Constant.LOG_FILENAME_INS_PRO_SCORE, level='error').logger.error(log_msg)
            return Response({'code': Constant.FAIL, 'message': Constant.FAIL_MESSAGE})

