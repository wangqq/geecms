from django import forms
from insuranceManagement.models import InsProduct, InsProAtt
from insuranceManagement.models_dedu_comp import InsProSchDed, InsProSchDedAtt, InsProSchComp, InsProSchCompAtt
from insuranceManagement.models_score import InsProductScore
from django.contrib.admin import widgets
from django.core.exceptions import ValidationError
from ckeditor.fields import RichTextFormField, RichTextField


class InsProductForm(forms.ModelForm):

    info = RichTextFormField(label=u'简介')

    class Meta:
        model = InsProduct
        fields=['info']


class InsProScoreForm(forms.ModelForm):
    product_id = forms.IntegerField(
        label=u"产品ID",
        widget=widgets.AdminIntegerFieldWidget,
    )

    guarantee_pattern = forms.IntegerField(
        label=u"产品保障类型",
        widget=widgets.AdminIntegerFieldWidget,
    )

    ensure_profit = forms.CharField(
        label=u"产品保障利益",
        widget=widgets.AdminTextInputWidget,
    )

    class Meta:
        model = InsProductScore
        fields = ['product_id', 'guarantee_pattern', 'ensure_profit']


class InsProAttAddForm(forms.ModelForm):
    name = forms.CharField(
        label=u"产品名称",
        required=True
    ),
    insurer = forms.IntegerField(
        label=u"保险公司ID",
        required=True
    ),
    category = forms.IntegerField(
        label=u"险种",
        required=True
    ),
    category_type = forms.IntegerField(
        label=u"险种类型",
        required=True
    ),
    payment_method = forms.CharField(
        label=u"支付方式",
        required=True
    ),
    info = forms.CharField(
        label=u"备注"
    ),
    policy_pattern = forms.IntegerField(
        label=u"产品类型",
        required=True
    ),
    guarantee_pattern = forms.IntegerField(
        label=u"保障类型",
        required=True
    ),
    start_time = forms.DateTimeField(
        label=u"有效期开始时间",
        required=True
    ),
    end_time = forms.DateTimeField(
        label=u"有效期截止时间",
        required=True
    ),
    covered_area = forms.IntegerField(
        label=u"保障区域",
        required=True
    ),
    payment_period = forms.CharField(
        label=u"缴费年限",
        required=True
    ),
    age_insured_start = forms.IntegerField(
        label=u"开始年龄",
        required=True
    ),
    age_insured_end = forms.IntegerField(
        label=u"截止年龄",
        required=True
    ),
    min_premium = forms.IntegerField(
        label=u"最小保费",
        required=True
    )

    class Meta:
        model = InsProAtt
        fields = ['name', 'insurer', 'category', 'category_type', 'payment_method', 'info', 'policy_pattern',
                  'guarantee_pattern', 'start_time', 'end_time', 'covered_area', 'payment_period', 'age_insured_start',
                  'age_insured_end', 'min_premium']


class InsProSchDedCompForm(forms.ModelForm):
    name = forms.CharField(label=u'免赔额名称', required=True)
    ratio = forms.DecimalField(label=u'免赔额系数', required=True)

    class Meta:
        model = InsProSchDed
        fields = ['name', 'ratio']


