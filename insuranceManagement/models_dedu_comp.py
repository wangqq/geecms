from django.db import models
from geeCMS.settings import proDb


# 免赔额抽象类
class InsProSchDedAbs(models.Model):
    ins_product_id = models.IntegerField()
    ins_product_schedule_id = models.IntegerField()
    name = models.CharField(max_length=30)
    ratio = models.DecimalField(decimal_places=5, max_digits=10)
    status = models.IntegerField()

    class Meta:
        abstract = True


# 主险产品表
class InsProSchDed(InsProSchDedAbs):
    class Meta:
        app_label = proDb
        db_table = 'ins_product_schedule_deductible'


# 附加险产品表
class InsProSchDedAtt(InsProSchDedAbs):
    class Meta:
        app_label = proDb
        db_table = 'ins_pro_sch_ded_att'


# 免赔额历史表
class InsProSchDedHisAbs(models.Model):
    fid = models.IntegerField()
    ins_product_id = models.IntegerField()
    ins_product_schedule_id = models.IntegerField()
    name = models.CharField(max_length=30)
    ratio = models.DecimalField(decimal_places=5, max_digits=10)
    status = models.IntegerField()
    action = models.CharField(max_length=50)
    his_created_by = models.IntegerField()
    his_created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True


# 主险产品
class InsProSchDedHis(InsProSchDedHisAbs):
    class Meta:
        app_label = proDb
        db_table = 'ins_product_schedule_deductible_his'


# 附加险产品
class InsProSchDedAttHis(InsProSchDedHisAbs):
    class Meta:
        app_label = proDb
        db_table = 'ins_pro_sch_ded_att_his'


# 赔付比例抽象类
class InsProSchCompAbs(models.Model):
    ins_product_id = models.IntegerField()
    ins_product_schedule_id = models.IntegerField()
    name = models.CharField(max_length=30)
    ratio = models.DecimalField(decimal_places=5, max_digits=10)
    status = models.IntegerField()

    class Meta:
        abstract = True


# 主险产品表
class InsProSchComp(InsProSchCompAbs):
    class Meta:
        app_label = proDb
        db_table = 'ins_product_schedule_comp_ratio'


# 附加险产品表
class InsProSchCompAtt(InsProSchCompAbs):
    class Meta:
        app_label = proDb
        db_table = 'ins_pro_sch_comp_ratio_att'


# 免赔额历史表
class InsProSchCompHisAbs(models.Model):
    fid = models.IntegerField()
    ins_product_id = models.IntegerField()
    ins_product_schedule_id = models.IntegerField()
    name = models.CharField(max_length=30)
    ratio = models.DecimalField(decimal_places=5, max_digits=10)
    status = models.IntegerField()
    action = models.CharField(max_length=50)
    his_created_by = models.IntegerField()
    his_created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True


# 主险产品
class InsProSchCompHis(InsProSchCompHisAbs):
    class Meta:
        app_label = proDb
        db_table = 'ins_product_schedule_comp_ratio_his'


# 附加险产品
class InsProSchCompAttHis(InsProSchCompHisAbs):
    class Meta:
        app_label = proDb
        db_table = 'ins_pro_sch_comp_ratio_att_his'
