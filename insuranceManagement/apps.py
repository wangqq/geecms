from django.apps import AppConfig


class InsurancemanagementConfig(AppConfig):
    name = 'insuranceManagement'
