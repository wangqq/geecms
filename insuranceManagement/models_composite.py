from django.db import models
from geeCMS.settings import proDb


# 绑定产品表
class InsProductBind(models.Model):
    main_id = models.IntegerField()
    attach_id = models.IntegerField()
    bind_type = models.IntegerField()
    relation_type = models.IntegerField()
    is_relation_payment = models.IntegerField()
    is_relation_gua = models.IntegerField()
    is_relation_social = models.IntegerField()
    is_relation_career = models.IntegerField()
    status = models.IntegerField()
    create_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True)
    created_by = models.IntegerField()
    updated_by = models.IntegerField()

    class Meta:
        app_label = proDb
        db_table = 'ins_product_bind'


class InsProductBindHistory(models.Model):
    fid = models.IntegerField()
    main_id = models.IntegerField()
    attach_id = models.IntegerField()
    bind_type = models.IntegerField()
    relation_type = models.IntegerField()
    is_relation_payment = models.IntegerField()
    is_relation_gua = models.IntegerField()
    is_relation_social = models.IntegerField()
    is_relation_career = models.IntegerField()
    status = models.IntegerField()
    create_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True)
    created_by = models.IntegerField()
    updated_by = models.IntegerField()
    action = models.CharField(max_length=50)
    his_created_by = models.IntegerField()
    his_created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        app_label = proDb
        db_table = 'ins_product_bind_his'


# 计划关联对照表
class InsScheduleBind(models.Model):
    main_sch_id = models.IntegerField()
    attach_sch_id = models.IntegerField()
    chain_type = models.IntegerField()
    is_modifiable = models.IntegerField()
    is_relation = models.IntegerField()
    ratio = models.DecimalField(decimal_places=3, max_digits=6)
    status = models.IntegerField()
    bind_id = models.IntegerField()
    main_id = models.IntegerField()
    attach_id = models.IntegerField()
    create_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True)
    created_by = models.IntegerField()
    updated_by = models.IntegerField()

    class Meta:
        app_label = proDb
        db_table = 'ins_schedule_bind'


class InsScheduleBindHistory(models.Model):
    fid = models.IntegerField()
    main_sch_id = models.IntegerField()
    attach_sch_id = models.IntegerField()
    chain_type = models.IntegerField()
    is_modifiable = models.IntegerField()
    is_relation = models.IntegerField()
    ratio = models.DecimalField(decimal_places=3, max_digits=6)
    status = models.IntegerField()
    bind_id = models.IntegerField()
    main_id = models.IntegerField()
    attach_id = models.IntegerField()
    create_time = models.DateTimeField()
    update_time = models.DateTimeField()
    created_by = models.IntegerField()
    updated_by = models.IntegerField()
    action = models.CharField(max_length=50)
    his_created_by = models.IntegerField()
    his_created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        app_label = proDb
        db_table = 'ins_schedule_bind_his'


# 计划关联对照表
class InsAmountBind(models.Model):
    main_id = models.IntegerField()
    attach_id = models.IntegerField()
    bind_id = models.IntegerField()
    chain_type = models.IntegerField()
    is_modifiable = models.IntegerField()
    is_relation = models.IntegerField()
    ratio = models.DecimalField(decimal_places=3, max_digits=6)
    status = models.IntegerField()
    create_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True)
    created_by = models.IntegerField()
    updated_by = models.IntegerField()

    class Meta:
        app_label = proDb
        db_table = 'ins_amount_bind'


class InsAmountBindHis(models.Model):
    fid = models.IntegerField()
    main_id = models.IntegerField()
    attach_id = models.IntegerField()
    bind_id = models.IntegerField()
    chain_type = models.IntegerField()
    is_modifiable = models.IntegerField()
    is_relation = models.IntegerField()
    ratio = models.DecimalField(decimal_places=3, max_digits=6)
    status = models.IntegerField()
    create_time = models.DateTimeField()
    update_time = models.DateTimeField()
    created_by = models.IntegerField()
    updated_by = models.IntegerField()
    action = models.CharField(max_length=50)
    his_created_by = models.IntegerField()
    his_created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        app_label = proDb
        db_table = 'ins_amount_bind_his'


# 组合产品表
class InsCompositePro(models.Model):
    main_id = models.IntegerField()
    comp_pro_id = models.CharField(max_length=200)
    pro_nums = models.IntegerField()
    score = models.DecimalField(max_digits=7, decimal_places=2)
    category_type = models.IntegerField()
    status = models.IntegerField()
    create_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True)
    created_by = models.IntegerField()
    updated_by = models.IntegerField()

    class Meta:
        app_label = proDb
        db_table = 'ins_composite_pro'


class InsCompositeProHis(models.Model):
    fid = models.IntegerField()
    main_id = models.IntegerField()
    comp_pro_id = models.CharField(max_length=200)
    pro_nums = models.IntegerField()
    score = models.DecimalField(max_digits=7, decimal_places=2)
    category_type = models.IntegerField()
    status = models.IntegerField()
    create_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True)
    created_by = models.IntegerField()
    updated_by = models.IntegerField()
    action = models.CharField(max_length=50)
    his_created_by = models.IntegerField()
    his_created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        app_label = proDb
        db_table = 'ins_composite_pro_his'


# 附加险排他列表
class InsProductBindExclude(models.Model):
    main_id = models.IntegerField()
    main_sch_id = models.IntegerField()
    attach_id = models.IntegerField()
    attach_id_exc = models.IntegerField()
    create_time = models.DateTimeField(auto_now_add=True)

    class Meta:
        app_label = proDb
        db_table = 'ins_product_bind_exclude'


# 计划维度附加险必绑列表
class InsProductBindSchedule(models.Model):
    bind_id = models.IntegerField()
    main_id = models.IntegerField()
    attach_id = models.IntegerField()
    main_sch_id = models.IntegerField()
    bind_type = models.IntegerField()
    create_time = models.DateTimeField(auto_now_add=True)

    class Meta:
        app_label = proDb
        db_table = 'ins_product_bind_schedule'

