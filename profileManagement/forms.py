from django import forms
from django.contrib.auth import (
    password_validation,
)
from employeeManagement.models import AuthUser
from django.contrib.admin import widgets
from django.core.exceptions import ValidationError


class ChangePasswordForm(forms.ModelForm):
    password_old = forms.CharField(
        label=u"现用密码",
        widget=forms.PasswordInput(attrs={'placeholder':u"请输入现用密码"}),
        error_messages = {"required":"密码不能为空"},
    )
    password = forms.CharField(
        label=u"新密码",
        widget=forms.PasswordInput(),
        error_messages={"required": "密码不能为空"},
    )
    password_confirm = forms.CharField(
        label=u"确认密码",
        widget=forms.PasswordInput(),
        error_messages={"required": "密码不能为空"},
    )

    def clean(self):
        cleaned_data = super(ChangePasswordForm, self).clean()
        if not self.is_valid():
            raise forms.ValidationError(u"所有项都为必填项")
        elif cleaned_data['password'] != cleaned_data['password_confirm']:
            # raise forms.ValidationError(u"两次输入的密码不一样")
            self._errors['password_confirm'] = self.error_class([u"两次输入的密码不一样"])
        else:
            cleaned_data = super(ChangePasswordForm, self).clean()
        return cleaned_data

    def validate_password(self):
        password_old = self.cleaned_data.get('password_old')
        password = self.cleaned_data.get('password')
        password_confirm = self.cleaned_data.get('password_confirm')

        if password != password_confirm:
            self._errors['password_confirm'] = self.error_class([u""])


    def validate_password_old(self):
        self._errors['password_old'] = self.error_class(['xxxxxx'])



    class Meta:
        model = AuthUser
        fields = ['password', 'password_old', 'password_confirm']
    #
    # def clean_username(self):
    #     cleaned_data = super(AuthUserAddForm, self).clean()
    #     username = cleaned_data.get('username')
    #     exist_check = AuthUser.objects.filter(username=username).count()
    #     if exist_check > 0:
    #         self._errors['username'] = self.error_class(["【" + username + "】,已经存在。"])
    #     return username
    #
    # def clean_email(self):
    #     cleaned_data = super(AuthUserAddForm, self).clean()
    #     email = cleaned_data.get('email')
    #     exist_check = AuthUser.objects.filter(email=email).count()
    #     if exist_check > 0:
    #         self._errors['email'] = self.error_class(["【" + email + "】,已经存在。"])
    #     return email
    #
    # def clean_password(self):
    #     cleaned_data = super(AuthUserAddForm, self).clean()
    #     password = cleaned_data.get('password')
    #     try:
    #         password_validation.validate_password(password)
    #     except ValidationError as err:
    #         self._errors['password'] = self.error_class([err])
    #     return password

#
# class AuthUserChangeForm(forms.ModelForm):
#     password = forms.CharField(
#         label=u"密码",
#         widget=forms.PasswordInput,
#         help_text=password_validation.password_validators_help_text_html(),
#     )
#     is_active = forms.ChoiceField(
#         label=u"状态",
#         choices=STATUS_CHOICE
#     )
#     is_superuser = forms.ChoiceField(
#         label=u"权限",
#         choices=POWER_CHOICE
#     )
#
#     class Meta:
#         model = AuthUser
#         fields = ['password', 'is_active', 'is_superuser']
#
#     def clean_password(self):
#         cleaned_data = super(AuthUserChangeForm, self).clean()
#         password = cleaned_data.get('password')
#         try:
#             password_validation.validate_password(password)
#         except ValidationError as err:
#             self._errors['password'] = self.error_class([err])
#         return password


class ChangeNameForm(forms.ModelForm):
    first_name = forms.CharField(label=u'名')

    class Meta:
        model = AuthUser
        fields = ['first_name']

