from profileManagement.forms import ChangePasswordForm, ChangeNameForm
from employeeManagement.models import AuthUser
import django
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from django.contrib import messages

from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.response import Response
from profileManagement.serializers import AuthUserSerializer
from static.common.constant import Constant
from rest_framework.authentication import SessionAuthentication
from utils.auth import ExpiringTokenAuthentication
from rest_framework.permissions import IsAuthenticated

from django.contrib import auth
from rest_framework.authtoken.models import Token
from django.core.cache import cache
# Create your views here.


@api_view(['POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def password_change(request):
    form_obj = ChangePasswordForm(request.data)
    if request.method == 'POST':
        if form_obj.is_valid():
            password_old = request.data.get('password_old', '')
            user = django.contrib.auth.authenticate(username=request.user.username, password=password_old)
            if user is not None:
                password = request.data.get('password')
                user.set_password(password)
                user.save()
                return Response({'code': Constant.SUCCESS, 'message': Constant.SUCCESS_MESSAGE})
            else:
                return Response({'code': Constant.NONE_EXIST, 'message': Constant.NONE_EXIST_MESSAGE})
        else:
            return Response({'code': Constant.INVAILD, 'message': form_obj.errors})


@api_view(['GET', 'POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def home(request):
    user_id = request.user.id
    try:
        user = AuthUser.objects.get(id=user_id)
    except ObjectDoesNotExist:
        return HttpResponse(status=404)
    if request.method == 'POST':
        form = ChangeNameForm(request.data, instance=user)
        if form.is_valid():
            first_name = request.data.get('first_name')
            AuthUser.objects.filter(id=user_id).update(first_name=first_name)
            messages.success(request, 'success')
            return Response({'code': Constant.SUCCESS, 'message': Constant.SUCCESS_MESSAGE})
        else:
            return Response({'code': Constant.INVAILD, 'message': form.errors})
    serializer = AuthUserSerializer(user)
    return Response(serializer.data)


@api_view(['POST'])
def login(request):
    if request.method == 'POST':
        username = request.data.get('username', '')
        password = request.data.get('password', '')
        user = auth.authenticate(username=username, password=password)
        if user is not None and user.is_active:
            auth.login(request, user)
            try:
                token = Token.objects.get(user_id=request.user.id)
                token.delete()
            except ObjectDoesNotExist:
                pass
            token = Token.objects.create(user=user)
            token_cache = 'token_' + token.key
            cache.set(token_cache, token, Constant.EFFECTIVE_TIME_LENGTH)
            return Response({'code': Constant.SUCCESS, 'message': Constant.SUCCESS_MESSAGE, 'token': token.key})
        else:
            return Response({'code': Constant.FAIL})


@api_view(['GET'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def logout(request):
    user = request.user
    auth.logout(request)
    token = Token.objects.get(user=user)
    token_cache = 'token_' + token.key
    cache.delete(token_cache)
    token.delete()
    return Response({'code': Constant.SUCCESS, 'message': Constant.SUCCESS_MESSAGE})
