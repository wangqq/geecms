import datetime, time
import _random
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.response import Response
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from utils.auth import ExpiringTokenAuthentication

from django.db import connection
from static.common.cousterFunction import dictfetchall, get_diff_days_2_now, get_specified_date, get_specified_month, get_specified_year, get_city_dict
from static.common.log import Logger
from static.common.constant import Constant


# Create your views here.

# 查询所有用户使用情况，默认为VIP团队（自住注册账号）
@api_view(['GET'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def user_use_info(request):
    add_name_where = ''
    param = ()
    format_param = '%Y-%m-%d'
    if request.GET.get('name'):
        name_param = '%%' + request.GET.get('name') + '%%'
        add_name_where = add_name_where + " AND (tu.mobile like %s OR tu.email like %s OR tu.firstname like %s OR tua.nickname like %s)"
        name_tuple = (name_param, name_param, name_param, name_param)
        param = param + name_tuple
        format_param = '%%Y-%%m-%%d'
    # 团队
    if request.GET.get('team'):
        add_name_where = add_name_where + " AND tu.customer_id=%s"
        team_param = (str(request.GET.get('team')),)
        param = param + team_param
        format_param = '%%Y-%%m-%%d'
    # 用户ID
    if request.GET.get('id'):
        add_name_where = add_name_where + " AND tu.id=%s"
        user_param = (str(request.GET.get('id')),)
        param = param + user_param
        format_param = '%%Y-%%m-%%d'
    # 查询用户SQL文
    user_use_sql = '''SELECT tu.id, tu.user_all_id, tu.mobile, tu.email, tu.customer_id, tu.firstname, 
                        tu.work_area,tu.work_company,tu.work_no,tu.work_time_length,
                        date_format(tu.created_at, "'''+format_param+'''") AS created_at,
                        tua.nickname, tua.user_origin, tcd.dsn, tc.name AS teamName
                        FROM `user` AS tu, user_all AS tua, customer_database AS tcd,customer AS tc
                         WHERE tu.user_all_id = tua.id AND tu.customer_id = tcd.customer_id AND tc.id = tcd.customer_id''' + add_name_where

    # 查询
    cursor = connection.cursor()
    if param:
        cursor.execute(user_use_sql, param)
    else:
        cursor.execute(user_use_sql)
    user_use_result = dictfetchall(cursor)

    # 用户使用数据数组
    user_data = []
    # 城市信息获取
    city_dict = get_city_dict()
    # 处理返回的用户信息
    for row_user in user_use_result:
        # 查询不同状态下的客户数量
        db_name = row_user["dsn"]
        tab_clt_status = db_name + ".clt_status"
        tab_clt_client = db_name + ".clt_client"
        client_status_num_sql = '''SELECT tcs.description,COUNT(tcc.id) AS num FROM ''' + tab_clt_status +\
                                ''' AS tcs LEFT JOIN ''' + tab_clt_client +''' AS tcc ON tcc.user_id = ''' + str(row_user['id']) + ''' AND tcs.id = tcc.status_id
                                 WHERE tcs.id != 1  GROUP BY tcs.id ORDER BY tcs.id'''
        cursor.execute(client_status_num_sql)
        client_status_num_result = dictfetchall(cursor)
        # 总客户数量
        client_num_total = 0
        # 每个状态的字典
        client_status = []
        for row_client in client_status_num_result:
            client_status.append([row_client["description"], row_client["num"]])
            # 加总客户总量
            client_num_total += row_client["num"]

        # 判断当前用户的视频权限，是否含有配偶的用户
        tab_clt_member = db_name + ".clt_member"
        video_period_sql = '''SELECT COUNT(tcc.id) AS num FROM ''' + tab_clt_client + ''' AS tcc , ''' + tab_clt_member +\
                       ''' AS tcm WHERE tcc.status_id >= 11 AND tcm.status_id != 1 
                       AND tcm.rel_type_id = 2  AND tcc.user_id = ''' + str(row_user['id']) + ''' AND tcc.id = tcm.client_id GROUP BY tcc.id '''
        cursor.execute(video_period_sql)
        video_period_result = cursor.fetchone()

        video_period_ok_sql = '''SELECT COUNT(tcc.id) AS num FROM ''' + tab_clt_client + ''' AS tcc
                       WHERE tcc.status_id >= 11 AND tcc.user_id = ''' + str(row_user['id'])
        cursor.execute(video_period_ok_sql)
        video_period_ok_result = cursor.fetchone()
        # 视频等级，默认为二阶段
        video_period = '二阶段'
        if video_period_ok_result and video_period_ok_result[0] > 1 and video_period_result and video_period_result[0] > 0:
            video_period = '三阶段'

        # 所有视频的个数
        video_time_total_sql = '''SELECT SUM(tv.video_duration) AS num FROM video_course AS tvc,video AS tv WHERE tvc.video_id=tv.id AND tvc.status=1'''
        cursor.execute(video_time_total_sql)
        video_time_total_result = cursor.fetchone()
        if video_time_total_result:
            video_time_total = video_time_total_result[0]
        else:
            video_time_total = 0

        # 已经观看的视频个数和时长
        video_time_sql = '''SELECT SUM(tl.video_time) AS time_length
                             FROM (
                              SELECT video_id,MAX(video_time) AS video_time FROM video_play  WHERE user_all_id = '''+ str(row_user['user_all_id']) + '''
                              GROUP BY video_id) AS tl'''
        cursor.execute(video_time_sql)
        video_time_result = cursor.fetchone()
        if video_time_result[0]:
            video_time = video_time_result[0]
        else:
            video_time = 0
        # 观看进度
        if video_time_total:
            video_proportion = round(video_time/video_time_total, 4)
        else:
            video_proportion = 0

        # 注册天数
        register_day = get_diff_days_2_now(str(row_user['created_at'])) + 1
        # 登录的天数
        login_day_result = '''SELECT COUNT(id) FROM log_login WHERE user_id=''' + str(row_user['id']) +\
        ''' GROUP BY date_format(created_at, '%Y%m%d') '''
        cursor.execute(login_day_result)
        login_day_result = cursor.fetchone()
        if login_day_result:
            login_day = login_day_result[0]
        else:
            login_day = 0

        # 活跃占比
        active_proportion = round(login_day/register_day, 4)
        # 用户昵称
        if row_user['firstname']:
            firstname = row_user['firstname']
        else:
            firstname = ''
        # 微信昵称
        if row_user['nickname']:
            nickname = row_user['nickname']
        else:
            nickname = ''

        # 账号
        if row_user['mobile']:
            username = row_user['mobile']
        elif row_user['email']:
            username = row_user['email']
        elif nickname == '' and firstname == '':
            slice = _random.Random().getrandbits(13)
            username = '游客' + str(slice) + str(row_user['id'])
        else:
            username = ''

        # 是否生成
        is_channel_sql = '''SELECT COUNT(id) FROM channel WHERE matching=''' + str(row_user['user_all_id'])
        cursor.execute(is_channel_sql)
        is_channel_result = cursor.fetchone()
        if is_channel_result and is_channel_result[0]:
            is_channel = '1'
        else:
            is_channel = '0'

        # 返回值
        user_data.append({
            'userId': row_user['user_all_id'],
            'username': username,
            'firstname': firstname,
            'nickname': nickname,
            'teamName': row_user['teamName'],
            'work_area': city_dict[int(row_user['work_area'])] if row_user['work_area'] and int(row_user['work_area']) in city_dict else '',
            'work_company': row_user['work_company'],
            'work_no': row_user['work_no'],
            'work_time_length': row_user['work_time_length'],
            'client_num_total': client_num_total,
            'client_status': client_status,
            'video_period': video_period,
            'video_proportion': video_proportion,
            'video_time': video_time,
            'active_proportion': active_proportion,
            'is_channel': is_channel
        })

    # 团队账号
    team_sql = '''SELECT id,`name` FROM customer WHERE status=1'''
    cursor.execute(team_sql)
    team = dictfetchall(cursor)
    # 关闭DB连接
    cursor.close()
    return_result = {
        'team': team,
        'userData': user_data
    }

    return Response(return_result)


# 概览-活跃用户数-日
@api_view(['POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def overview_active_day(request):
    single_date = request.data.get('singleDate')

    # 建立连接
    cursor = connection.cursor()

    # 单日活跃用户数
    active_num = get_active_num(cursor, single_date, single_date, 1)

    # 前一天
    single_date_before = get_specified_date(single_date, -1)
    # 获得前一日活跃用户数
    before_active_num = get_active_num(cursor, single_date_before, single_date_before, 1)
    # 环比值
    chain_day = get_diff_ratio(before_active_num, active_num)

    # 一年前的日期值
    single_date_before_year = get_specified_year(single_date, -1)
    # 获得前一日活跃用户数
    before_year_active_num = get_active_num(cursor, single_date_before_year, single_date_before_year, 1)
    # 同比值
    chain_year = get_diff_ratio(before_year_active_num, active_num)

    # 关闭DB连接
    cursor.close()
    # 页面显示数据
    result = {
        "currentDay": single_date,
        "currentNum": active_num,
        "currentQOQ": chain_day,
        "currentYOY": chain_year,
    }

    return Response(result)


# 概览-活跃用户数-周
@api_view(['POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def overview_active_week(request):
    start_time = request.data.get('startTime')
    end_time = request.data.get('endTime')

    # 建立连接
    cursor = connection.cursor()

    # 活跃用户信息
    active_user_info = get_active_user(cursor, start_time, end_time, 3)

    # 查询的周数组
    dayList = []
    for i in range(0, 7):
        dayList.append(get_specified_date(start_time, i))
    #  查询周的活跃用户
    active_num_week = get_active_num(cursor, start_time, end_time, 3)
    active_num_list = []
    for w in dayList:
        # 获得当前日期的活跃用户数
        w_active_num = 0
        # 当前日期前一天
        w_before = get_specified_date(w, -1)
        w_before_active_num = 0
        # 一年前的日期值
        w_before_year = get_specified_year(w, -1)
        w_before_year_active_num = 0
        w_active_user_info = []
        # 循环成员
        for item_u in active_user_info:
            # 取得成员是否有活跃
            vu_active_num = get_active_num_new(cursor, w, w, item_u['userId'])
            if vu_active_num > 0:
                w_active_user_info.append(item_u)
                w_active_num += 1
            # 前一日活跃用户数
            vu_before_active_num = get_active_num_new(cursor, w_before, w_before, item_u['userId'])
            if vu_before_active_num > 0:
                w_before_active_num = w_before_active_num + 1
            # 获得前一日活跃用户数
            vu_before_year_active_num = get_active_num_new(cursor, w_before_year, w_before_year, item_u['userId'])
            if vu_before_year_active_num > 0:
                w_before_year_active_num += 1

        # 环比值
        w_chain_day = get_diff_ratio(w_before_active_num, w_active_num)
        # 同比值
        w_chain_year = get_diff_ratio(w_before_year_active_num, w_active_num)

        # 数据追加
        active_num_list.append({
            'day': w,
            'num': w_active_num,
            'chain_day': w_chain_day,
            'chain_year': w_chain_year,
            'active_user': w_active_user_info
        })

    # 前一周的活跃用户数
    week_before_start = get_specified_date(start_time, -7)
    week_before_end = get_specified_date(end_time, -7)
    week_active_num_before = get_active_num(cursor, week_before_start, week_before_end, 3)
    # 周环比
    week_chain_h = get_diff_ratio(week_active_num_before, active_num_week)

    # 周同比获得
    t_week_start = get_specified_year(start_time, -1)
    t_week_end = get_specified_year(end_time, -1)
    t_week_active_num = get_active_num(cursor, t_week_start, t_week_end, 3)
    # 周环比
    week_chain_t = get_diff_ratio(t_week_active_num, active_num_week)

    # 关闭DB连接
    cursor.close()
    # 页面显示数据
    result = {
        "activeList": active_num_list,
        "weekNum": active_num_week,
        "weekQOQ": week_chain_h,
        "weekYOY": week_chain_t,
        "active_user": active_user_info
    }

    return Response(result)


# 概览-活跃用户数-月
@api_view(['POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def overview_active_month(request):
    start_time = request.data.get('startTime')
    next_month_first_day = get_specified_month(start_time, 1)
    end_time = get_specified_date(next_month_first_day, -1)

    # 建立连接
    cursor = connection.cursor()
    # 活跃用户信息
    active_user_info = get_active_user(cursor, start_time, end_time, 12)

    # 查询的周数组
    dayList = []
    for i in range(0, 32):
        v_day = get_specified_date(start_time, i)
        dayList.append(v_day)
        if v_day == end_time:
            break
    #  查询周的活跃用户
    month_active_num = 0
    active_num_list = []
    for w in dayList:
        w_active_num = 0
        # 当前日期前一天
        w_before = get_specified_date(w, -1)
        w_before_active_num = 0
        # 一年前的日期值
        w_before_year = get_specified_year(w, -1)
        w_before_year_active_num = 0
        w_active_user_info = []
        # 循环成员
        for item_u in active_user_info:
            # 取得成员是否有活跃
            vu_active_num = get_active_num_new(cursor, w, w, item_u['userId'])
            if vu_active_num > 0:
                w_active_user_info.append(item_u)
                w_active_num += 1
            # 前一日活跃用户数
            vu_before_active_num = get_active_num_new(cursor, w_before, w_before, item_u['userId'])
            if vu_before_active_num > 0:
                w_before_active_num = w_before_active_num + 1
            # 获得前一日活跃用户数
            vu_before_year_active_num = get_active_num_new(cursor, w_before_year, w_before_year,  item_u['userId'])
            if vu_before_year_active_num > 0:
                w_before_year_active_num += 1

        # 环比值
        w_chain_day = get_diff_ratio(w_before_active_num, w_active_num)

        # 同比值
        w_chain_year = get_diff_ratio(w_before_year_active_num, w_active_num)

        # 数据追加
        active_num_list.append({
            'day': w,
            'num': w_active_num,
            'chain_day': w_chain_day,
            'chain_year': w_chain_year,
            'active_user': w_active_user_info
        })

    # 关闭DB连接
    cursor.close()
    # 页面显示数据
    result = {
        "activeList": active_num_list,
        "monthNum": month_active_num,
        "active_user": active_user_info
    }

    return Response(result)


# 概览-登录页访问量
@api_view(['POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def overview_login_visit_day(request):
    start_time = request.data.get('startTime')
    single_date = request.data.get('singleDate')

    # 建立连接
    cursor = connection.cursor()

    # 访问总量
    total_num_sql = '''SELECT COUNT(id) AS num FROM `log_login_visit`'''
    cursor.execute(total_num_sql)
    total_num_result = cursor.fetchone()
    total_num = total_num_result[0]

    # 单日访问量
    register_num = get_visit_num(cursor, single_date, single_date)

    # 前一天
    single_date_before = get_specified_date(single_date, -1)
    # 获得前一日注册数
    before_num = get_visit_num(cursor, single_date_before, single_date_before)
    # 环比值
    chain_day = get_diff_ratio(before_num, register_num)

    # 一年前的日期值
    single_date_before_year = get_specified_year(single_date, -1)
    # 获得前一日注册数
    before_year_num = get_visit_num(cursor, single_date_before_year, single_date_before_year)
    # 同比值
    chain_year = get_diff_ratio(before_year_num, register_num)

    # 查询的周数组
    dayList = []
    for i in range(0, 7):
        dayList.append(get_specified_date(start_time, i))
    #  查询每日的注册量
    num_list = []
    for w in dayList:
        # 当日注册数
        w_num = get_visit_num(cursor, w, w)

        # 当前日期前一天
        w_before = get_specified_date(w, -1)
        # 前一日注册数
        w_before_active_num = get_visit_num(cursor, w_before, w_before)
        # 环比值
        w_chain_day = get_diff_ratio(w_before_active_num, w_num)

        # 一年前的日期值
        w_before_year = get_specified_year(w, -1)
        # 去年这一日期的注册数
        w_before_year_active_num = get_visit_num(cursor, w_before_year, w_before_year)
        # 同比值
        w_chain_year = get_diff_ratio(w_before_year_active_num, w_num)

        # 数据追加
        num_list.append({
            'day': w,
            'num': w_num,
            'chain_day': w_chain_day,
            'chain_year': w_chain_year
        })

    # 关闭DB连接
    cursor.close()
    # 页面显示数据
    result = {
        "totalNum": total_num,
        "currentDay": single_date,
        "currentNum": register_num,
        "currentQOQ": chain_day,
        "currentYOY": chain_year,
        "numList": num_list
    }

    return Response(result)


@api_view(['POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def overview_login_visit_month(request):
    start_time = request.data.get('startTime')
    single_date = request.data.get('singleDate')

    # 建立连接
    cursor = connection.cursor()
    # 月访问量
    next_month_first_day = get_specified_month(single_date, 1)
    end_date = get_specified_date(next_month_first_day, -1)
    register_num = get_visit_num(cursor, single_date, end_date)

    # 前一月
    single_date_before = get_specified_month(single_date, -1)
    end_date_before = get_specified_date(single_date, -1)
    # 获得前一月访问量
    before_num = get_visit_num(cursor, single_date_before, end_date_before)
    # 环比值
    chain_day = get_diff_ratio(before_num, register_num)

    # 一年前的日期值
    single_date_before_year = get_specified_year(single_date, -1)
    next_month_first_year = get_specified_month(single_date_before_year, 1)
    end_date_before_year = get_specified_date(next_month_first_year, -1)

    # 获得一年前的日期值访问量
    before_year_num = get_visit_num(cursor, single_date_before_year, end_date_before_year)
    # 同比值
    chain_year = get_diff_ratio(before_year_num, register_num)

    # 查询的周数组
    dayList = []
    for i in range(0, 7):
        dayList.append(get_specified_month(start_time, i))
    #  查询每日的访问量
    num_list = []
    for w in dayList:
        # 当月访问数
        w_next_month_first_day = get_specified_month(w, 1)
        w_end_date = get_specified_date(w_next_month_first_day, -1)
        w_num = get_visit_num(cursor, w, w_end_date)

        # 当前日期前一月
        w_before = get_specified_month(w, -1)
        w_before_end = get_specified_month(w_end_date, -1)
        # 前一月访问数
        w_before_active_num = get_visit_num(cursor, w_before, w_before_end)
        # 环比值
        w_chain_day = get_diff_ratio(w_before_active_num, w_num)

        # 一年前的日期值
        w_before_year = get_specified_year(w, -1)
        w_before_year_end = get_specified_month(w_end_date, -1)
        # 前一年相同月份的访问数
        w_before_year_active_num = get_visit_num(cursor, w_before_year, w_before_year_end)
        # 同比值
        w_chain_year = get_diff_ratio(w_before_year_active_num, w_num)

        # 数据追加
        num_list.append({
            'day': w,
            'num': w_num,
            'chain_day': w_chain_day,
            'chain_year': w_chain_year
        })

    # 关闭DB连接
    cursor.close()
    # 页面显示数据
    result = {
        "currentMonth": single_date,
        "currentNum": register_num,
        "currentQOQ": chain_day,
        "currentYOY": chain_year,
        "numList": num_list
    }

    return Response(result)


# 概览-用户注册量
@api_view(['POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def overview_register_day(request):
    start_time = request.data.get('startTime')
    single_date = request.data.get('singleDate')

    # 建立连接
    cursor = connection.cursor()

    # 注册总量
    total_num_sql = '''SELECT COUNT(id) AS num FROM `user`'''
    cursor.execute(total_num_sql)
    total_num_result = cursor.fetchone()
    total_num = total_num_result[0]

    # 单日注册数
    register_num = get_register_num(cursor, single_date, single_date)

    # 前一天
    single_date_before = get_specified_date(single_date, -1)
    # 获得前一日注册数
    before_num = get_register_num(cursor, single_date_before, single_date_before)
    # 环比值
    chain_day = get_diff_ratio(before_num, register_num)

    # 一年前的日期值
    single_date_before_year = get_specified_year(single_date, -1)
    # 获得前一日注册数
    before_year_num = get_register_num(cursor, single_date_before_year, single_date_before_year)
    # 同比值
    chain_year = get_diff_ratio(before_year_num, register_num)

    # 查询的周数组
    dayList = []
    for i in range(0, 7):
        dayList.append(get_specified_date(start_time, i))
    #  查询每日的注册量
    num_list = []
    for w in dayList:
        # 当日注册数
        w_num = get_register_num(cursor, w, w)

        # 当前日期前一天
        w_before = get_specified_date(w, -1)
        # 前一日注册数
        w_before_active_num = get_register_num(cursor, w_before, w_before)
        # 环比值
        w_chain_day = get_diff_ratio(w_before_active_num, w_num)

        # 一年前的日期值
        w_before_year = get_specified_year(w, -1)
        # 去年这一日期的注册数
        w_before_year_active_num = get_register_num(cursor, w_before_year, w_before_year)
        # 同比值
        w_chain_year = get_diff_ratio(w_before_year_active_num, w_num)

        # 数据追加
        num_list.append({
            'day': w,
            'num': w_num,
            'chain_day': w_chain_day,
            'chain_year': w_chain_year
        })

    # 关闭DB连接
    cursor.close()
    # 页面显示数据
    result = {
        "totalNum": total_num,
        "currentDay": single_date,
        "currentNum": register_num,
        "currentQOQ": chain_day,
        "currentYOY": chain_year,
        "numList": num_list
    }

    return Response(result)


# 概览-注册用户详细信息
@api_view(['POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def register_user(request):
    start_time = request.data.get('startTime')
    end_time = request.data.get('endTime')

    format_param = '%Y-%m-%d'
    # 查询用户SQL文
    user_sql = '''SELECT tu.id, tu.user_all_id, tu.mobile, tu.email,  tu.firstname, 
                        date_format(tu.created_at, "'''+format_param+'''") AS created_at,
                        tua.nickname, tua.parent_id
                        FROM `user` AS tu, user_all AS tua
                         WHERE tu.user_all_id = tua.id AND unix_timestamp( tu.created_at) >=''' +\
                     str(time.mktime(time.strptime(start_time + ' 00:00:00', '%Y-%m-%d %H:%M:%S'))) + \
                    '''  AND unix_timestamp( tu.created_at) <=''' + \
                     str(time.mktime(time.strptime(end_time + ' 23:59:59', '%Y-%m-%d %H:%M:%S')))

    # 建立连接
    cursor = connection.cursor()
    cursor.execute(user_sql)
    result = []
    user_result = dictfetchall(cursor)
    for row_u in user_result:
        # 账号
        parent_account = None
        # 父级
        if row_u['parent_id']:
            parent_sql = '''SELECT tu.id, tu.user_all_id, tu.mobile, tu.email,  tu.firstname,tua.nickname
                            FROM `user`AS tu, user_all AS tua
                             WHERE tua.id=''' + str(row_u['parent_id']) + ''' AND tu.user_all_id = tua.id'''
            cursor.execute(parent_sql)
            parent_result = dictfetchall(cursor)
            for row_p in parent_result:
                if row_p['firstname']:
                    parent_account = row_p['firstname']
                elif row_p['email']:
                    parent_account = row_p['email']
                elif row_p['mobile']:
                    parent_account = row_p['mobile']
                elif row_p['nickname']:
                    parent_account = row_p['nickname']
                else:
                    slice = _random.Random().getrandbits(13)
                    parent_account = '游客' + str(slice) + str(row_p['user_all_id'])
        # 用户信息
        if row_u['firstname']:
            v_user_account = row_u['firstname']
        elif row_u['email']:
            v_user_account = row_u['email']
        elif row_u['mobile']:
            v_user_account = row_u['mobile']
        elif row_u['nickname']:
            v_user_account = row_u['nickname']
        else:
            slice = _random.Random().getrandbits(13)
            v_user_account = '游客' + str(slice) + str(row_u['user_all_id'])

        result.append({
            'user_account': v_user_account,
            'parent_account': parent_account,
        })
    # 关闭DB连接
    cursor.close()
    # 页面显示数据

    return Response(result)


@api_view(['POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def overview_register_month(request):
    start_time = request.data.get('startTime')
    single_date = request.data.get('singleDate')

    # 建立连接
    cursor = connection.cursor()
    # 月活注册数
    next_month_first_day = get_specified_month(single_date, 1)
    end_date = get_specified_date(next_month_first_day, -1)
    register_num = get_register_num(cursor, single_date, end_date)

    # 前一月
    single_date_before = get_specified_month(single_date, -1)
    end_date_before = get_specified_date(single_date, -1)
    # 获得前一月注册数
    before_num = get_register_num(cursor, single_date_before, end_date_before)
    # 环比值
    chain_day = get_diff_ratio(before_num, register_num)

    # 一年前的日期值
    single_date_before_year = get_specified_year(single_date, -1)
    next_month_first_year = get_specified_month(single_date_before_year, 1)
    end_date_before_year = get_specified_date(next_month_first_year, -1)

    # 获得一年前的日期值注册数
    before_year_num = get_register_num(cursor, single_date_before_year, end_date_before_year)
    # 同比值
    chain_year = get_diff_ratio(before_year_num, register_num)

    # 查询的周数组
    dayList = []
    for i in range(0, 7):
        dayList.append(get_specified_month(start_time, i))
    #  查询每日的注册量
    num_list = []
    for w in dayList:
        # 当月注册数
        w_next_month_first_day = get_specified_month(w, 1)
        w_end_date = get_specified_date(w_next_month_first_day, -1)
        w_num = get_register_num(cursor, w, w_end_date)

        # 当前日期前一月
        w_before = get_specified_month(w, -1)
        w_before_end = get_specified_month(w_end_date, -1)
        # 前一月注册数
        w_before_active_num = get_register_num(cursor, w_before, w_before_end)
        # 环比值
        w_chain_day = get_diff_ratio(w_before_active_num, w_num)

        # 一年前的日期值
        w_before_year = get_specified_year(w, -1)
        w_before_year_end = get_specified_year(w_end_date, -1)
        # 前一年相同月份的注册数
        w_before_year_active_num = get_register_num(cursor, w_before_year, w_before_year_end)
        # 同比值
        w_chain_year = get_diff_ratio(w_before_year_active_num, w_num)

        # 数据追加
        num_list.append({
            'day': w,
            'num': w_num,
            'chain_day': w_chain_day,
            'chain_year': w_chain_year
        })

    # 关闭DB连接
    cursor.close()
    # 页面显示数据
    result = {
        "currentMonth": single_date,
        "currentNum": register_num,
        "currentQOQ": chain_day,
        "currentYOY": chain_year,
        "numList": num_list
    }

    return Response(result)


# 课程不同阶段的用户数量
@api_view(['GET'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def overview_level(request):
    # 建立连接
    cursor = connection.cursor()
    # 用户总量
    user_total_sql = '''SELECT tall.id,tuc.id AS userId,tuc.dsn FROM user_all AS tall 
                        LEFT JOIN (
                        SELECT tu.id,tu.user_all_id,tc.dsn
                        FROM `user` AS tu,customer_database AS tc 
                        WHERE tu.customer_id=tc.customer_id
                        ) AS tuc ON tuc.user_all_id = tall.id
                        WHERE tall.user_origin IN(1,3)'''
    cursor.execute(user_total_sql)
    user_total_result = dictfetchall(cursor)
    total_user = len(user_total_result)
    one_level_num = 0
    two_level_num = 0
    three_level_num = 0

    for row_all in user_total_result:
        # 判定当前用户是否已经注册
        if row_all['userId']:
            # 已注册,连接器相应DB
            tab_clt_client = row_all['dsn'] + ".clt_client"

            # 判断当前用户的视频权限，是否含有配偶的用户
            tab_clt_member = row_all['dsn'] + ".clt_member"
            video_period_sql = '''SELECT COUNT(tcc.id) AS num FROM ''' + tab_clt_client + ''' AS tcc , ''' + tab_clt_member + \
                               ''' AS tcm WHERE tcc.status_id >= 11 AND tcm.status_id != 1 AND tcm.rel_type_id = 2
                               AND tcc.user_id =  ''' + str(row_all['userId']) + ''' AND tcc.id = tcm.client_id  '''
            cursor.execute(video_period_sql)
            video_period_result = cursor.fetchone()
            # 视频等级
            if video_period_result and video_period_result[0] > 0:
                three_level_num += 1
            else:
                two_level_num += 1
        else:
            # 未注册过，当前用户属于1阶段
            one_level_num += 1
    # 各阶段课程比例
    if total_user:
        one_level_ratio = round(one_level_num/total_user, 4)
        two_level_ratio = round(two_level_num/total_user, 4)
        three_level_ratio = round(three_level_num / total_user, 4)
    else:
        one_level_ratio = 0
        two_level_ratio = 0
        three_level_ratio = 0

    # 关闭DB连接
    cursor.close()

    # 阶段数据数组
    levelList = []
    levelList.append({
        'title': '一阶段',
        'total': one_level_num,
        'ratio': one_level_ratio
    })
    levelList.append({
        'title': '二阶段',
        'total': two_level_num,
        'ratio': two_level_ratio
    })
    levelList.append({
        'title': '三阶段',
        'total': three_level_num,
        'ratio': three_level_ratio
    })
    # 返回的数据
    result = {
        'total_user': total_user,
        'levelList': levelList
    }
    return Response(result)


# 课程视频
@api_view(['GET'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def overview_video(request):
    # 建立连接
    cursor = connection.cursor()
    # 查询课列表
    course_sql = '''SELECT head,`level` FROM video_course WHERE status=1 GROUP BY `level` ORDER BY `level` '''
    cursor.execute(course_sql)
    course_result = dictfetchall(cursor)

    # data初期化
    result = []

    for row_c in course_result:
        # 获得每节课的视频列表
        video_sql = '''SELECT tv.id, tv.title, tv.video_duration FROM video_course AS tc, video AS tv WHERE tc.level= ''' + str(row_c['level']) +\
            ''' AND tc.video_id=tv.id ORDER BY tc.srot_id,tv.srot_id '''
        cursor.execute(video_sql)
        video_result = dictfetchall(cursor)
        videoNumList = []
        for row_v in video_result:
            # 查询观看量
            play_sql = '''SELECT COUNT(id) FROM video_play WHERE video_id= ''' + str(row_v['id'])
            cursor.execute(play_sql)
            play_result = cursor.fetchone()
            play_num = play_result[0]
            # 查询下载量
            download_sql = '''SELECT COUNT(id) FROM file_download_log WHERE file_type=1 AND file_id='''+str(row_v['id'])
            cursor.execute(download_sql)
            download_result = cursor.fetchone()
            download_num = download_result[0]
            videoNumList.append({
                'videoTitle': row_v['title'],
                'video_duration': row_v['video_duration'],
                'play_num': play_num,
                'download_num': download_num
            })

        result.append({
            'courseTitle': row_c['head'],
            'video': videoNumList
        })

    # 关闭DB连接
    cursor.close()
    return Response(result)


# 活跃用户数取得
def get_active_num(cursor, start_date, end_date, num):
    # 活跃用户数
    active_num_sql = '''SELECT  COUNT(tg.user_id) AS active_num 
 FROM (SELECT ta.user_id FROM (SELECT user_id FROM log_login WHERE unix_timestamp( created_at) >=''' +\
                     str(time.mktime(time.strptime(start_date + ' 00:00:00', '%Y-%m-%d %H:%M:%S'))) + \
                    '''  AND unix_timestamp( created_at) <=''' + \
                     str(time.mktime(time.strptime(end_date + ' 23:59:59', '%Y-%m-%d %H:%M:%S'))) +\
        " GROUP BY DATE_FORMAT(created_at,'%y-%m-%d'), user_id) AS ta GROUP BY ta.user_id HAVING COUNT(ta.user_id) >= " + str(num) + ") AS tg"
    cursor.execute(active_num_sql)
    active_num_result = cursor.fetchone()
    return active_num_result[0]


# 新版活跃用户数取得
def get_active_num_new(cursor, start_date, end_date, user_id):
    # 活跃用户数
    active_num_sql = '''SELECT count(user_id) as num FROM log_login WHERE user_id = '''+str(user_id)+''' AND unix_timestamp( created_at) >=''' +\
                     str(time.mktime(time.strptime(start_date + ' 00:00:00', '%Y-%m-%d %H:%M:%S'))) + \
                    '''  AND unix_timestamp( created_at) <=''' + \
                     str(time.mktime(time.strptime(end_date + ' 23:59:59', '%Y-%m-%d %H:%M:%S')))
    cursor.execute(active_num_sql)
    active_num_result = cursor.fetchone()
    if active_num_result and active_num_result[0]:
        active_num = active_num_result[0]
    else:
        active_num = 0
    return active_num


# 获取活跃用户详情
def get_active_user(cursor, start_date, end_date, num):
    # 活跃用户数
    active_user_sql = '''SELECT  tg.user_id , SUM(tg.login_num) AS active_num 
 FROM (SELECT ta.user_id,COUNT(ta.user_id) AS login_num FROM (SELECT user_id FROM log_login WHERE unix_timestamp( created_at) >=''' +\
                     str(time.mktime(time.strptime(start_date + ' 00:00:00', '%Y-%m-%d %H:%M:%S'))) + \
                    '''  AND unix_timestamp( created_at) <=''' + \
                     str(time.mktime(time.strptime(end_date + ' 23:59:59', '%Y-%m-%d %H:%M:%S'))) +\
        " GROUP BY DATE_FORMAT(created_at,'%y-%m-%d'), user_id) AS ta GROUP BY ta.user_id  HAVING COUNT(ta.user_id) >= " + str(num) + ") AS tg GROUP BY tg.user_id"
    cursor.execute(active_user_sql)
    user_result = dictfetchall(cursor)
    active_list = []
    active_dict = {}
    active_key_dict = {}
    for item in user_result:
        key_sort = str(item['active_num']) + "***" + str(item['user_id'])
        if item['active_num'] not in active_list:
            active_list.append(item['active_num'])
        if item['active_num'] in active_key_dict:
            v_key_list = active_key_dict[item['active_num']]
        else:
            v_key_list = []
        v_key_list.append(item['user_id'])
        v_key_list.sort(reverse=True)
        active_key_dict[item['active_num']] =v_key_list
        # 用户信息查询
        dsn_id_sql = "SELECT tc.dsn,tu.firstname,tu.mobile, tu.email,tu.customer_id,tua.nickname FROM `user` AS tu, user_all AS tua,customer_database AS tc " \
                     "WHERE tu.id=" + str(item['user_id']) +" AND tu.user_all_id = tua.id AND tu.customer_id = tc.customer_id"
        cursor.execute(dsn_id_sql)
        dsn_result_all = dictfetchall(cursor)
        dsn_result = dsn_result_all[0]
        # 查询不同状态下的客户数量
        db_name = dsn_result['dsn']
        tab_clt_client = db_name + ".clt_client"
        client_status_num_sql = '''SELECT COUNT(tcc.id) AS num FROM  ''' + tab_clt_client + ''' AS tcc 
        WHERE tcc.user_id = ''' + str(item['user_id']) + ''' AND tcc.status_id != 1 GROUP BY tcc.user_id'''
        cursor.execute(client_status_num_sql)
        client_status_num_result = cursor.fetchone()
        # 总客户数量
        if client_status_num_result and client_status_num_result[0]:
            client_num_total = client_status_num_result[0]
        else:
            client_num_total = 0
        # 用户名称
        if dsn_result['firstname']:
            userName = dsn_result['firstname']
        elif dsn_result['nickname']:
            userName = dsn_result['nickname']
        elif dsn_result['mobile']:
            userName = dsn_result['mobile']
        elif dsn_result['email']:
            userName = dsn_result['email']
        else:
            userName = ''

        active_dict[key_sort] = {
            "userId": str(item['user_id']),
            "customerId": str(dsn_result['customer_id']),
            'userName': userName,
            'clientNum': client_num_total,
            'active_num': item['active_num']
        }
    active_list.sort(reverse=True)
    return_result = []
    for item in active_list:
        # user_id 取得
        for item_u in active_key_dict[item]:
            u_key = str(item) + "***" + str(item_u)
            return_result.append(active_dict[u_key])

    return return_result


# 登录页访问量取得
def get_visit_num(cursor, start_date, end_date):
    # 活跃用户数
    num_sql = '''SELECT COUNT(id) AS num FROM log_login_visit WHERE unix_timestamp( created_at) >=''' +\
                     str(time.mktime(time.strptime(start_date + ' 00:00:00', '%Y-%m-%d %H:%M:%S'))) + \
                    '''  AND unix_timestamp( created_at) <=''' + \
                     str(time.mktime(time.strptime(end_date + ' 23:59:59', '%Y-%m-%d %H:%M:%S')))
    cursor.execute(num_sql)
    num_result = cursor.fetchone()
    return num_result[0]


# 注册用户数取得
def get_register_num(cursor, start_date, end_date):
    # 活跃用户数
    num_sql = '''SELECT COUNT(id) AS num FROM user WHERE unix_timestamp( created_at) >=''' +\
                     str(time.mktime(time.strptime(start_date + ' 00:00:00', '%Y-%m-%d %H:%M:%S'))) + \
                    '''  AND unix_timestamp( created_at) <=''' + \
                     str(time.mktime(time.strptime(end_date + ' 23:59:59', '%Y-%m-%d %H:%M:%S')))
    cursor.execute(num_sql)
    num_result = cursor.fetchone()
    return num_result[0]


# 获得环比值
def get_diff_ratio(before_num, current_num):
    if before_num:
        chain = round((current_num - before_num) / before_num, 4)
    else:
        chain = current_num
    return chain


# 视频概览-阶段一
@api_view(['GET'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def overview_video_level_one(request):
    # 建立连接
    cursor = connection.cursor()
    # 获得每节课的视频列表
    video_sql = '''SELECT tv.id, tv.title, tv.video_duration, tv.down_num FROM video_course AS tc, video AS tv WHERE tc.level= 1 AND tc.video_id=tv.id ORDER BY tc.level,tc.`course_id` ,tc.srot_id '''
    cursor.execute(video_sql)
    video_result = dictfetchall(cursor)
    # 返回列表
    result = []
    for row_v in video_result:
        # 返回的列表初期化
        video_register_user_list = []
        video_visitor_user_list = []
        # 查询观看量
        play_sql = '''SELECT COUNT(id) FROM video_play WHERE video_id= ''' + str(row_v['id'])
        cursor.execute(play_sql)
        play_result = cursor.fetchone()
        play_num = play_result[0]
        # 查询下载量
        download_num = row_v['down_num']
        # 视频被观看的用户
        video_user_sql = '''SELECT vp.user_all_id,tua.unique_id FROM video_play AS vp,user_all as tua WHERE vp.video_id= ''' + str(row_v['id']) + '  AND tua.id=vp.user_all_id GROUP BY vp.video_id,vp.user_all_id'
        cursor.execute(video_user_sql)
        video_user_result = dictfetchall(cursor)
        for row_user in video_user_result:
            # userAll 表查询
            user_all_gfp_sql = '''SELECT nickname FROM `user_all` WHERE id=  ''' + str(
                row_user['user_all_id'])
            cursor.execute(user_all_gfp_sql)
            user_all_gfp_result = cursor.fetchone()
            # 观看进度
            play_max_time_sql = '''SELECT MAX(video_time) FROM video_play WHERE video_id= ''' + str(
                row_v['id']) + ' AND user_all_id=' + str(row_user['user_all_id'])
            cursor.execute(play_max_time_sql)
            play_max_time_result = cursor.fetchone()
            if play_max_time_result[0]:
                play_max_time = play_max_time_result[0]
            else:
                play_max_time = 0
            if row_v['video_duration'] > 0:
                play_progress = round(play_max_time / row_v['video_duration'], 2)
            else:
                play_progress = 1
            # 观看时长
            play_total_time_sql = '''SELECT SUM(video_time) FROM video_play WHERE video_id= ''' + str(
                row_v['id']) + ' AND user_all_id=' + str(row_user['user_all_id'])
            cursor.execute(play_total_time_sql)
            play_total_time_result = cursor.fetchone()
            if play_total_time_result and play_total_time_result[0]:
                play_total_time = play_total_time_result[0]
            else:
                play_total_time = 0

            # 判定是注册用户还是游客
            user_gfp_sql = '''SELECT id,customer_id,email,mobile,firstname FROM `user` WHERE user_all_id=  ''' + str(
                row_user['user_all_id'])
            cursor.execute(user_gfp_sql)
            user_gfp_result = dictfetchall(cursor)
            # 当前用户已经注册
            if user_gfp_result:
                user_gfp = user_gfp_result[0]
                # 用户昵称
                if user_gfp['firstname']:
                    firstname = user_gfp['firstname']
                else:
                    firstname = ''
                # 微信昵称
                if user_all_gfp_result and user_all_gfp_result[0]:
                    nickname = user_all_gfp_result[0]
                else:
                    nickname = ''
                # 账号
                if user_gfp['mobile']:
                    username = user_gfp['mobile']
                elif user_gfp['email']:
                    username = user_gfp['email']
                elif nickname == '' and firstname == '':
                    slice = _random.Random().getrandbits(13)
                    username = '游客' + str(slice) + str(row_user['user_all_id'])
                else:
                    username = ''
                video_register_user_list.append({
                    'userName': username,
                    'firstname': firstname,
                    'nickname': nickname,
                    'playProgress': play_progress,
                    'playTotalTime': play_total_time,
                })
            else:
                # 判断是否是注册用户
                check_register_sql = 'SELECT COUNT(id) FROM user_all WHERE unique_id="' + row_user['unique_id'] + '"'
                cursor.execute(check_register_sql)
                check_register_result = cursor.fetchone()
                if check_register_result[0] and check_register_result[0] > 1:
                    check_register = True
                else:
                    check_register = False

                # 账号
                if user_all_gfp_result and user_all_gfp_result[0]:
                    username = user_all_gfp_result[0]
                else:
                    slice = _random.Random().getrandbits(13)
                    username = '游客' + str(slice) + str(row_user['user_all_id'])
                video_visitor_user_list.append({
                    'register': check_register,
                    'userName': username,
                    'playProgress': play_progress,
                    'playTotalTime': play_total_time,
                })
        result.append({
            'title': row_v['title'],
            'playNum': play_num,
            'downloadNum': download_num,
            'videoRegisterUserList': video_register_user_list,
            'videoVisitorUserList': video_visitor_user_list,
        })
    # 关闭DB连接
    cursor.close()
    return Response(result)


# 视频概览-阶段二或三
@api_view(['GET'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def overview_video_level(request, level):
    # 建立连接
    cursor = connection.cursor()
    # 获得每节课的视频列表
    video_sql = '''SELECT tv.id, tv.title, tv.video_duration, tv.down_num FROM video_course AS tc, video AS tv WHERE tc.level= ''' + str(level) + ''' AND tc.video_id=tv.id ORDER BY tc.level,tc.`course_id` ,tc.srot_id '''
    cursor.execute(video_sql)
    video_result = dictfetchall(cursor)
    # 返回列表
    result = []
    for row_v in video_result:
        # 返回的列表初期化
        user_list = []
        # 查询观看量
        play_sql = '''SELECT COUNT(id) FROM video_play WHERE video_id= ''' + str(row_v['id'])
        cursor.execute(play_sql)
        play_result = cursor.fetchone()
        play_num = play_result[0]
        # 查询下载量
        download_num = row_v['down_num']

        # 视频被观看的用户
        video_user_sql = '''SELECT user_all_id FROM video_play WHERE video_id= ''' + str(row_v['id']) + ' GROUP BY video_id,user_all_id'
        cursor.execute(video_user_sql)
        video_user_result = dictfetchall(cursor)

        try:
            for row_user in video_user_result:
                # 视频被观看的用户
                user_sql = '''SELECT tall.nickname,tu.user_all_id,tu.id,tu.customer_id,tu.email,tu.mobile,tu.firstname,tcd.dsn 
                              FROM user_all AS tall, `user` AS tu, customer_database AS tcd
                               WHERE tu.user_all_id=''' +str(row_user['user_all_id'])+''' AND tu.user_all_id=tall.id AND tu.customer_id = tcd.customer_id'''
                cursor.execute(user_sql)
                user_result = dictfetchall(cursor)
                if len(user_result):
                    user_info = user_result[0]
                    # 观看进度
                    play_max_time_sql = '''SELECT MAX(video_time) FROM video_play WHERE video_id= ''' + str(
                        row_v['id']) + ' AND user_all_id=' + str(row_user['user_all_id'])
                    cursor.execute(play_max_time_sql)
                    play_max_time_result = cursor.fetchone()
                    if play_max_time_result[0]:
                        play_max_time = play_max_time_result[0]
                    else:
                        play_max_time = 0
                    if row_v['video_duration'] > 0:
                        play_progress = round(play_max_time / row_v['video_duration'], 2)
                    else:
                        play_progress = 1
                    # 观看时长
                    play_total_time_sql = '''SELECT SUM(video_time) FROM video_play WHERE video_id= ''' + str(
                        row_v['id']) + ' AND user_all_id=' + str(row_user['user_all_id'])
                    cursor.execute(play_total_time_sql)
                    play_total_time_result = cursor.fetchone()
                    if play_total_time_result and play_total_time_result[0]:
                        play_total_time = play_total_time_result[0]
                    else:
                        play_total_time = 0

                    # 用户昵称
                    if user_info['firstname']:
                        firstname = user_info['firstname']
                    else:
                        firstname = ''
                    # 微信昵称
                    if user_info['nickname']:
                        nickname = user_info['nickname']
                    else:
                        nickname = ''
                    # 账号
                    if user_info['mobile']:
                        username = user_info['mobile']
                    elif user_info['email']:
                        username = user_info['email']
                    elif nickname == '' and firstname == '':
                        slice = _random.Random().getrandbits(13)
                        username = '游客' + str(slice) + str(row_user['user_all_id'])
                    else:
                        username = ''

                    if int(level) == 2:
                        user_list.append({
                            'userName': username,
                            'firstname': firstname,
                            'nickname': nickname,
                            'playProgress': play_progress,
                            'playTotalTime': play_total_time,
                        })
                    else:
                        # 判断当前用户的视频权限，是否含有配偶的用户
                        db_name = user_info["dsn"]
                        tab_clt_client = db_name + ".clt_client"
                        tab_clt_member = db_name + ".clt_member"
                        video_period_sql = '''SELECT COUNT(tcc.id) AS num FROM ''' + tab_clt_client + ''' AS tcc , ''' + tab_clt_member + \
                                           ''' AS tcm WHERE tcc.status_id >= 11 AND tcm.status_id != 1 
                                           AND tcm.rel_type_id = 2  AND tcc.user_id = ''' + str(user_info['id']) + ''' AND tcc.id = tcm.client_id  '''
                        cursor.execute(video_period_sql)
                        video_period_result = cursor.fetchone()
                        # level为3时，满足条件，才可以添加
                        if video_period_result[0] > 0:
                            user_list.append({
                                'userName': username,
                                'firstname': firstname,
                                'nickname': nickname,
                                'playProgress': play_progress,
                                'playTotalTime': play_total_time,
                            })
        except Exception as err:
            pass

        result.append({
            'title': row_v['title'],
            'playNum': play_num,
            'downloadNum': download_num,
            'videoRegisterUserList': user_list,
        })
    # 关闭DB连接
    cursor.close()
    return Response(result)


# 视频分析
@api_view(['GET'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def video_user(request, id):
    # 建立连接
    cursor = connection.cursor()
    # 视频被观看的用户
    user_sql = '''SELECT tall.nickname,tu.user_all_id,tu.id,tu.customer_id,tu.email,tu.mobile,tu.firstname,tcd.dsn 
                        FROM user_all AS tall, `user` AS tu, customer_database AS tcd
                         WHERE tu.user_all_id='''+ str(id) +''' AND tu.user_all_id=tall.id AND tu.customer_id = tcd.customer_id'''
    cursor.execute(user_sql)
    user_result_list = dictfetchall(cursor)
    # 查询无结果时，返回false
    if not user_result_list:
        return Response({'code': Constant.ERR_NONE_EXIST_CODE, 'msg': Constant.ERR_NONE_EXIST_MESSAGE})
    user_result = user_result_list[0]
    # 视频阶段
    db_name = user_result["dsn"]
    tab_clt_client = db_name + ".clt_client"
    tab_clt_member = db_name + ".clt_member"
    video_period_sql = '''SELECT COUNT(tcc.id) AS num FROM ''' + tab_clt_client + ''' AS tcc , ''' + tab_clt_member + \
                       ''' AS tcm WHERE tcc.status_id >= 11 AND tcm.status_id != 1 
                       AND tcm.rel_type_id = 2  AND tcc.user_id = ''' + str( user_result['id']) + ''' AND tcc.id = tcm.client_id  '''
    cursor.execute(video_period_sql)
    video_period_result = cursor.fetchone()
    # level默认为二阶段，当有两个且一个为夫妻的销售完成状态
    level = '二阶段'
    level_sql = ''
    if video_period_result[0] > 0:
        level = '三阶段'
        level_sql = ' OR level=3'

    # 用户昵称
    if user_result['firstname']:
        firstname = user_result['firstname']
    else:
        firstname = ''
    # 微信昵称
    if user_result['nickname']:
        nickname = user_result['nickname']
    else:
        nickname = ''
    # 账号
    if user_result['mobile']:
        username = user_result['mobile']
    elif user_result['email']:
        username = user_result['email']
    elif nickname == '' and firstname == '':
        slice = _random.Random().getrandbits(13)
        username = '游客' + str(slice) + str(user_result['id'])
    else:
        username = ''

    # 查询课列表
    course_sql = '''SELECT head,`level` FROM video_course WHERE status=1 AND (`level`=1 OR `level`=2'''+level_sql +''') GROUP BY `level` ORDER BY `level` '''
    cursor.execute(course_sql)
    course_result = dictfetchall(cursor)
    # data初期化
    video_list = []
    video_total_time = 0
    video_total_progress = 0
    video_num = 0

    for row_c in course_result:
        # 获得每节课的视频列表
        video_sql = '''SELECT tv.id, tv.title, tv.video_duration FROM video_course AS tc, video AS tv WHERE tc.level= ''' + str(row_c['level']) + ''' AND tc.video_id=tv.id ORDER BY tc.srot_id,tv.srot_id '''
        cursor.execute(video_sql)
        video_result = dictfetchall(cursor)
        video_num += len(video_result)
        for row_v in video_result:
            # 观看进度
            play_max_time_sql = '''SELECT MAX(video_time) FROM video_play WHERE video_id= ''' + str(
                row_v['id']) + ' AND user_all_id=' + str(user_result['user_all_id'])
            cursor.execute(play_max_time_sql)
            play_max_time_result = cursor.fetchone()
            if play_max_time_result[0]:
                play_max_time = play_max_time_result[0]
            else:
                play_max_time = 0
            if row_v['video_duration'] > 0:
                play_progress = round(play_max_time / row_v['video_duration'], 2)
            else:
                play_progress = 1
            video_total_progress += play_progress
            # 观看时长
            play_total_time_sql = '''SELECT SUM(video_time) FROM video_play WHERE video_id= ''' + str(
                row_v['id']) + ' AND user_all_id=' + str(user_result['user_all_id'])
            cursor.execute(play_total_time_sql)
            play_total_time_result = cursor.fetchone()
            if play_total_time_result and play_total_time_result[0]:
                play_total_time = play_total_time_result[0]
            else:
                play_total_time = 0
            video_total_time += play_total_time

            video_list.append({
                'videoName': row_v['title'],
                'courseName': row_c['head'],
                'playProgress': play_progress,
                'playTotalTime': play_total_time
            })
    # 视频总进度
    if video_num:
        video_total_progress = round(video_total_progress/video_num , 2)
    else:
        video_total_progress = 1

    result = {
        'userName': username,
        'firstname': firstname,
        'nickname': nickname,
        'level': level,
        'playTotalProgress': video_total_progress,
        'videoTotalTime': video_total_time,
        'videoList': video_list
    }
    # 关闭DB连接
    cursor.close()
    return Response(result)


# 页面停留时长
@api_view(['GET'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def page_stay_time(request, id):
    # 建立连接
    cursor = connection.cursor()
    # 视频被观看的用户
    user_sql = '''SELECT tall.nickname,tu.user_all_id,tu.id,tu.customer_id,tu.email,tu.mobile,tu.firstname,
                date_format(tu.created_at, '%Y-%m-%d') AS created_at,tcd.dsn
                        FROM user_all AS tall, `user` AS tu, customer_database AS tcd
                        WHERE tu.user_all_id='''+ str(id) +''' AND tu.user_all_id=tall.id AND tu.customer_id = tcd.customer_id'''
    cursor.execute(user_sql)
    user_result_list = dictfetchall(cursor)
    # 查询无结果时，返回false
    if not user_result_list:
        return Response({'code': Constant.ERR_NONE_EXIST_CODE, 'msg': Constant.ERR_NONE_EXIST_MESSAGE})
    user_result = user_result_list[0]
    # 用户昵称
    if user_result['firstname']:
        firstname = user_result['firstname']
    else:
        firstname = ''
    # 微信昵称
    if user_result['nickname']:
        nickname = user_result['nickname']
    else:
        nickname = ''
    # 账号名称
    if user_result['mobile']:
        username = user_result['mobile']
    elif user_result['email']:
        username = user_result['email']
    elif nickname == '' and firstname == '':
        slice = _random.Random().getrandbits(13)
        username = '游客' + str(slice) + str(user_result['id'])
    else:
        username = ''
    # 查询不同状态下的客户数量
    db_name = user_result["dsn"]
    tab_clt_status = db_name + ".clt_status"
    tab_clt_client = db_name + ".clt_client"
    client_status_num_sql = '''SELECT tcs.description,COUNT(tcc.id) AS num FROM ''' + tab_clt_status + \
                            ''' AS tcs LEFT JOIN ''' + tab_clt_client + ''' AS tcc ON tcc.user_id = ''' + str(
        user_result['id']) + ''' AND tcs.id = tcc.status_id
                                WHERE tcs.id != 1  GROUP BY tcs.id ORDER BY tcs.id'''
    cursor.execute(client_status_num_sql)
    client_status_num_result = dictfetchall(cursor)
    # 总客户数量
    client_num_total = 0
    # 每个状态的字典
    client_status = []
    for row_client in client_status_num_result:
        client_status.append([row_client["description"], row_client["num"]])
        # 加总客户总量
        client_num_total += row_client["num"]

    tab_clt_member = db_name + ".clt_member"
    # 账号下的未删除客户
    client_sql = '''SELECT tcc.id,tcc.name,COUNT(tcm.id) AS num FROM ''' + tab_clt_client + ''' AS tcc , ''' + tab_clt_member + \
                       ''' AS tcm WHERE tcc.status_id != 1 AND tcm.status_id != 1 
                       AND tcc.user_id = ''' + str(user_result['id']) + ''' AND tcc.id = tcm.client_id GROUP BY  tcc.id '''
    cursor.execute(client_sql)
    client_result = dictfetchall(cursor)
    # 客户下拉列表
    client_list = []
    client_dict = {}
    for row_c in client_result:
        client_dict[row_c['id']]={
                'name': row_c['name'],
                'num': row_c['num'],
            }
        client_list.append({
            row_c['id']:row_c['name']
        })
    # 所有页面
    page_sql = '''SELECT id, title FROM r_page WHERE status=1  AND id != 1 ORDER BY srot_id'''
    cursor.execute(page_sql)
    page_list = dictfetchall(cursor)

    # 活跃比
    register_day = get_diff_days_2_now(str(user_result['created_at'])) + 1
    # 登录的天数
    login_day_result = '''SELECT COUNT(id) FROM log_login WHERE user_id=''' + str(user_result['id']) +\
    ''' GROUP BY date_format(created_at, '%Y%m%d') '''
    cursor.execute(login_day_result)
    login_day_result = cursor.fetchone()
    if login_day_result:
        login_day = login_day_result[0]
    else:
        login_day = 0

    # 活跃占比
    active_proportion = round(login_day/register_day, 4)

    # 页面停留时长
    page_stay_select = '''SELECT trp.title,trpt.client_id,SUM(trpt.page_time) AS time_len'''
    pate_stay_total_time_select = 'SELECT SUM(trpt.page_time) AS time'

    from_sql = ''' FROM r_page AS trp, r_page_stay_time AS trpt'''

    # 添加条件
    add_where = ''
    # 查询具体客户
    if request.GET.get('client'):
        add_where += ' AND trpt.client_id=' + str(request.GET.get('client'))
    # 查询具体页面
    if request.GET.get('page'):
        add_where += ' AND trpt.page_id=' + str(request.GET.get('page'))

    # 共同的where条件
    common_where = ''' WHERE trp.id != 1 AND trpt.user_all_id='''+id+''' AND trp.id=trpt.page_id ''' + add_where

    # 总时长
    pate_stay_total_time_sql = pate_stay_total_time_select + from_sql + common_where
    cursor.execute(pate_stay_total_time_sql)
    pate_stay_total_time_result = cursor.fetchone()
    if pate_stay_total_time_result:
        pate_stay_total_time = pate_stay_total_time_result[0]
    else:
        pate_stay_total_time = 0

    # 前十的总时长
    pate_stay_top_ten_time_sql = pate_stay_total_time_sql + ' limit 10'
    cursor.execute(pate_stay_top_ten_time_sql)
    pate_stay_top_ten_time_result = cursor.fetchone()
    if pate_stay_top_ten_time_result:
        pate_stay_top_ten_time = pate_stay_top_ten_time_result[0]
    else:
        pate_stay_top_ten_time = 0
    # 每个客户页面停留
    page_stay_sql = page_stay_select + from_sql + common_where + ' GROUP BY trpt.client_id,trpt.page_id'
    cursor.execute(page_stay_sql)
    page_stay_result = dictfetchall(cursor)
    # 客户停留详情列表
    client_stay_list = []
    for row_cp in page_stay_result:
        if row_cp['client_id'] in client_dict:
            client_info = client_dict[row_cp['client_id']]
            client_stay_list.append({
                'name': client_info['name'],
                'num': client_info['num'],
                'page': row_cp['title'],
                'time_len': row_cp['time_len'],
            })

    # 关闭DB连接
    cursor.close()
    return_result = {
        'clientList': client_list,
        'pageList': page_list,
        'userName': username,
        'firstname': firstname,
        'nickname': nickname,
        'clientNumTotal': client_num_total,
        'clientStatus': client_status,
        'activeProportion': active_proportion,
        'pateStayTotalTime': pate_stay_total_time,
        'pateStayTopTenTime': pate_stay_top_ten_time,
        'clientStayList': client_stay_list
    }

    return Response(return_result)


# 子集
@api_view(['POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def overview_channel(request):
    # 建立连接
    cursor = connection.cursor()
    id = request.data.get('id')
    user_sql = '''SELECT tall.nickname,tu.user_all_id,tu.id,tu.email,tu.mobile,tu.firstname,ch.matching,ch.canal,ch.label,ch.create_time 
                            FROM user_all AS tall, `user` AS tu, channel AS ch
                             WHERE tu.user_all_id=''' + str(id) + ''' AND tu.user_all_id=tall.id AND tall.id = ch.matching'''
    cursor.execute(user_sql)
    user_result_list = dictfetchall(cursor)
    # 查询无结果时，返回false
    if not user_result_list:
        return Response({'code': Constant.ERR_NONE_EXIST_CODE, 'msg': Constant.ERR_NONE_EXIST_MESSAGE})
    user_result = user_result_list[0]

    # 用户昵称
    if user_result['firstname']:
        firstname = user_result['firstname']
    else:
        firstname = ''
    # 微信昵称
    if user_result['nickname']:
        nickname = user_result['nickname']
    else:
        nickname = ''
    # 账号
    if user_result['mobile']:
        username = user_result['mobile']
    elif user_result['email']:
        username = user_result['email']
    elif nickname == '' and firstname == '':
        slice = _random.Random().getrandbits(13)
        username = '游客' + str(slice) + str(user_result['id'])
    else:
        username = ''
    #子账号
    start_time = request.data.get('startTime')
    end_time = request.data.get('endTime')
    channel_word = ""

    if start_time:
        channel_word += '''  AND unix_timestamp(co.create_time) >=''' + \
              str(time.mktime(time.strptime(start_time + ' 00:00:00', '%Y-%m-%d %H:%M:%S')))
    if end_time:
        channel_word += '''  AND unix_timestamp(co.create_time) <=''' + \
              str(time.mktime(time.strptime(end_time + ' 23:59:59', '%Y-%m-%d %H:%M:%S')))
    channel_sql = ''' SELECT co.user_all_id,co.create_time,ao.nickname,ao.unique_id,au.email,au.mobile,au.firstname 
                            FROM channel_open as co
                            inner join user_all as ao on co.user_all_id = ao.id
							left join `user` as au on au.user_all_id = ao.id
                            where co.parent_user_all_id=''' + str(user_result['user_all_id']) + channel_word + ' GROUP BY co.user_all_id'
    cursor.execute(channel_sql)
    channel_res = dictfetchall(cursor)
    channel_frequency = len(channel_res)
    channel_stay_list = []
    for channel in channel_res:
        # 用户昵称
        if channel['firstname']:
            cat_firstname = channel['firstname']
        else:
            cat_firstname = ''
        # 微信昵称
        if channel['nickname']:
            cat_nickname = channel['nickname']
        else:
            cat_nickname = ''

        if channel['mobile']:
            castname = channel['mobile']
        elif channel['email']:
            castname = channel['email']
        elif cat_firstname == '' and cat_nickname == '':
            slice = _random.Random().getrandbits(13)
            castname = '游客' + str(slice) + str(channel['user_all_id'])
        else:
            castname = ''
        channel_datime = channel['create_time']
        channel_data = datetime.datetime.date(channel_datime)

        # 判断是否是注册用户
        check_register_sql = 'SELECT COUNT(id) FROM user_all WHERE unique_id="' + channel['unique_id'] + '"'
        cursor.execute(check_register_sql)
        check_register_result = cursor.fetchone()
        if check_register_result[0] and check_register_result[0] > 1:
            check_register = True
        else:
            check_register = False

        channel_stay_list.append({
            'register': check_register,
            'castName': castname,
            'firstname': cat_firstname,
            'nickname': cat_nickname,
            'createTime' : channel_data
        })
    channel_temp = user_result['create_time']
    channel_user_temp = datetime.datetime.date(channel_temp)
    return_result = {
        'userName': username,
        'firstname': firstname,
        'nickname': nickname,
        'canal': user_result['canal'],
        'label': user_result['label'],
        'createTime': channel_user_temp,
        'channelFrequency': channel_frequency,
        'channelStayList' : channel_stay_list
    }
    # 关闭DB连接
    cursor.close()
    return Response(return_result)
