from django.db import models
from employeeManagement.models import AuthUser
from ckeditor.fields import RichTextField


# Create your models here.

class BscCity(models.Model):
    name = models.CharField(max_length=10)

    class Meta:
        app_label = 'socialDb'
        db_table = 'bsc_city'

    def __str__(self):
        return self.name


class City(models.Model):
    bsc_city = models.ForeignKey(BscCity, default=None)
    # province_id = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(AuthUser,related_name='city_created')
    updated_by = models.ForeignKey(AuthUser,related_name='city_updated')
    yanglaobx = models.DecimalField(decimal_places=2, max_digits=100, default=0)
    yanglaobx_comp = models.DecimalField(decimal_places=2, max_digits=100, default=0)
    yiliaobx = models.DecimalField(decimal_places=2, max_digits=100, default=0)
    yiliaobx_comp = models.DecimalField(decimal_places=2, max_digits=100, default=0)
    shiyebx = models.DecimalField(decimal_places=2, max_digits=100, default=0)
    shiyebx_comp = models.DecimalField(decimal_places=2, max_digits=100, default=0)
    gongshangbx = models.DecimalField(decimal_places=2, max_digits=100, default=0)
    gongshangbx_comp = models.DecimalField(decimal_places=2, max_digits=100, default=0)
    shengyubx = models.DecimalField(decimal_places=2, max_digits=100, default=0)
    shengyubx_comp = models.DecimalField(decimal_places=2, max_digits=100, default=0)
    gongjijin_min = models.DecimalField(decimal_places=2, max_digits=100, default=0)
    gongjijin_comp_min = models.DecimalField(decimal_places=2, max_digits=100, default=0)
    gongjijin_max = models.DecimalField(decimal_places=2, max_digits=100, default=0)
    gongjijin_comp_max = models.DecimalField(decimal_places=2, max_digits=100, default=0)
    baoxiaoliucheng = RichTextField('报销流程',blank=True)

    class Meta:
        app_label = 'socialDb'
        db_table = "city"
        ordering = ['-id']

        permissions = (
            ("view_list", "Can see city list"),
        )

    def __str__(self):
        return self.id


class CityHistory(models.Model):
    fid = models.IntegerField()
    # province_id = models.IntegerField()
    bsc_city_id = models.IntegerField()
    yanglaobx = models.DecimalField(decimal_places=2, max_digits=100, default=0)
    yanglaobx_comp = models.DecimalField(decimal_places=2, max_digits=100, default=0)
    yiliaobx = models.DecimalField(decimal_places=2, max_digits=100, default=0)
    yiliaobx_comp = models.DecimalField(decimal_places=2, max_digits=100, default=0)
    # dabingyibao = models.DecimalField(decimal_places=2, max_digits=100, default=0)
    shiyebx = models.DecimalField(decimal_places=2, max_digits=100, default=0)
    shiyebx_comp = models.DecimalField(decimal_places=2, max_digits=100, default=0)
    gongshangbx = models.DecimalField(decimal_places=2, max_digits=100, default=0)
    gongshangbx_comp = models.DecimalField(decimal_places=2, max_digits=100, default=0)
    shengyubx = models.DecimalField(decimal_places=2, max_digits=100, default=0)
    shengyubx_comp = models.DecimalField(decimal_places=2, max_digits=100, default=0)
    gongjijin_min = models.DecimalField(decimal_places=2, max_digits=100, default=0)
    gongjijin_comp_min = models.DecimalField(decimal_places=2, max_digits=100, default=0)
    gongjijin_max = models.DecimalField(decimal_places=2, max_digits=100, default=0)
    gongjijin_comp_max = models.DecimalField(decimal_places=2, max_digits=100, default=0)
    baoxiaoliucheng = RichTextField('报销流程', blank=True)
    created_by_id = models.IntegerField()
    updated_by_id = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    action = models.CharField(max_length=50)
    his_created_by = models.IntegerField()
    his_created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        app_label = 'socialDb'
        db_table = "city_his"


class MedInsuranceType(models.Model):
    bsc_city_id = models.IntegerField()
    description = models.CharField(max_length=100)
    is_active = models.IntegerField()
    created_by = models.CharField(max_length=50)
    updated_by = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        app_label = 'socialDb'
        db_table = 'med_insurance_type'


# 社保类型历史表
class MedInsuranceTypeHis(models.Model):
    fid = models.IntegerField()
    bsc_city_id = models.IntegerField()
    description = models.CharField(max_length=100)
    is_active = models.IntegerField()
    created_by = models.CharField(max_length=50)
    updated_by = models.CharField(max_length=50)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    action = models.CharField(max_length=50)
    his_created_by = models.IntegerField()
    his_created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        app_label = 'socialDb'
        db_table = 'med_insurance_type_his'


class CityMedType(models.Model):
    bsc_city = models.ForeignKey(BscCity, default=None)
    med_insurance_type = models.ForeignKey(MedInsuranceType, default=None)
    age_start = models.IntegerField()
    age_end = models.IntegerField()
    menzhenbaoxiao = models.CharField(max_length=100, blank=True)
    zhuyuanbaoxiao = models.CharField(max_length=100, blank=True)
    menzhen_pay_standard = models.IntegerField(blank=True)
    menzhen_standard_unit = models.IntegerField(blank=True)
    zhuyuan_pay_standard = models.IntegerField(blank=True)
    zhuyuan_standard_unit = models.IntegerField(blank=True)
    yibao_info = models.CharField(max_length=100000, blank=True)
    wx_yibao_info = models.CharField(max_length=200, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(AuthUser, related_name='city_med_created')
    updated_by = models.ForeignKey(AuthUser, related_name='city_med_updated')

    class Meta:
        app_label = 'socialDb'
        db_table = 'city_med_type'


class CityMedTypeHistory(models.Model):
    fid = models.IntegerField()
    bsc_city_id = models.IntegerField()
    med_insurance_type_id = models.IntegerField()
    age_start = models.IntegerField()
    age_end = models.IntegerField()
    menzhenbaoxiao = models.CharField(max_length=100, blank=True)
    zhuyuanbaoxiao = models.CharField(max_length=100, blank=True)
    menzhen_pay_standard = models.IntegerField(blank=True)
    menzhen_standard_unit = models.IntegerField(blank=True)
    zhuyuan_pay_standard = models.IntegerField(blank=True)
    zhuyuan_standard_unit = models.IntegerField(blank=True)
    yibao_info = models.CharField(max_length=100000, blank=True)
    wx_yibao_info = models.CharField(max_length=200, blank=True)
    created_by_id = models.IntegerField()
    updated_by_id = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    action = models.CharField(max_length=50)
    his_created_by = models.IntegerField()
    his_created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        app_label = 'socialDb'
        db_table = 'city_med_type_his'


class CityMedPremium(models.Model):
    bsc_city = models.ForeignKey(BscCity, default=None)
    med_insurance_type = models.ForeignKey(MedInsuranceType, default=None)
    age_start = models.IntegerField()
    age_end = models.IntegerField()
    premium = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(AuthUser, related_name='city_premium_created')
    updated_by = models.ForeignKey(AuthUser, related_name='city_premium_updated')

    class Meta:
        app_label = 'socialDb'
        db_table = 'city_med_premium'


class CityMedPremiumHistory(models.Model):
    fid = models.IntegerField()
    bsc_city = models.ForeignKey(BscCity, default=None)
    med_insurance_type = models.ForeignKey(MedInsuranceType, default=None)
    age_start = models.IntegerField()
    age_end = models.IntegerField()
    premium = models.IntegerField()
    created_by_id = models.IntegerField()
    updated_by_id = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    action = models.CharField(max_length=50)
    his_created_by = models.IntegerField()
    his_created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        app_label = 'socialDb'
        db_table = 'city_med_premium_his'


class InsMedCycle(models.Model):
    id = models.IntegerField(primary_key=True)
    description = models.CharField(max_length=100)
    is_active = models.IntegerField()

    class Meta:
        app_label = 'socialDb'
        db_table = 'ins_med_cycle'


class CareerType(models.Model):
    id = models.IntegerField(primary_key=True)
    med_id = models.CharField(max_length=100)
    description = models.CharField(max_length=100)
    is_active = models.IntegerField()
    display_order = models.IntegerField(blank=True)

    class Meta:
        app_label = 'socialDb'
        db_table = 'career_type'


class CareerSocialMatch(models.Model):
    bsc_city_id = models.IntegerField()
    career_id = models.IntegerField()
    med_id = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        app_label = 'socialDb'
        db_table = 'career_social_match'


# 职业匹配历史表
class CareerSocialMatchHis(models.Model):
    fid = models.IntegerField()
    bsc_city_id = models.IntegerField()
    career_id = models.IntegerField()
    med_id = models.CharField(max_length=100)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    action = models.CharField(max_length=50)
    his_created_by = models.IntegerField()
    his_created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        app_label = 'socialDb'
        db_table = 'career_social_match_his'


# 五险一金基础表
class BscSclInsType(models.Model):
    id = models.IntegerField(primary_key=True)
    description = models.CharField(max_length=255)
    is_active = models.IntegerField()
    display_order = models.IntegerField()

    class Meta:
        app_label = 'socialDb'
        db_table = "bsc_scl_insurance_type"
        ordering = ['display_order']


# 缴纳基数表
class CityPayBase(models.Model):
    bsc_city_id = models.IntegerField()
    ins_type_id = models.IntegerField()
    amount_min = models.DecimalField(decimal_places=2, max_digits=10, default=0)
    amount_max = models.DecimalField(decimal_places=2, max_digits=10, default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(AuthUser, related_name='city_pay_created')
    updated_by = models.ForeignKey(AuthUser, related_name='city_pay_updated')

    class Meta:
        app_label = 'socialDb'
        db_table = "city_pay_base"
        ordering = ['-id']


class CityPayBaseHis(models.Model):
    fid = models.IntegerField()
    bsc_city_id = models.IntegerField()
    ins_type_id = models.IntegerField()
    amount_min = models.DecimalField(decimal_places=2, max_digits=10, default=0)
    amount_max = models.DecimalField(decimal_places=2, max_digits=10, default=0)
    created_by_id = models.IntegerField()
    updated_by_id = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    action = models.CharField(max_length=50)
    his_created_by = models.IntegerField()
    his_created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        app_label = 'socialDb'
        db_table = "city_pay_base_his"
        ordering = ['-id']


# 缴纳比例表表
class CityPayRatio(models.Model):
    bsc_city_id = models.IntegerField()
    ins_type_id = models.IntegerField()
    ratio_min = models.DecimalField(decimal_places=4, max_digits=5, default=0)
    ratio_max = models.DecimalField(decimal_places=4, max_digits=5, default=0)
    ratio_default = models.DecimalField(decimal_places=4, max_digits=5, default=0)
    ratio_min_comp = models.DecimalField(decimal_places=4, max_digits=5, default=0)
    ratio_max_comp = models.DecimalField(decimal_places=4, max_digits=5, default=0)
    ratio_default_comp = models.DecimalField(decimal_places=4, max_digits=5, default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(AuthUser, related_name='city_pay_ratio_created')
    updated_by = models.ForeignKey(AuthUser, related_name='city_pay_ratio_updated')

    class Meta:
        app_label = 'socialDb'
        db_table = "city_pay_ratio"
        ordering = ['-id']


class CityPayRatioHis(models.Model):
    fid = models.IntegerField()
    bsc_city_id = models.IntegerField()
    ins_type_id = models.IntegerField()
    ratio_min = models.DecimalField(decimal_places=4, max_digits=5, default=0)
    ratio_max = models.DecimalField(decimal_places=4, max_digits=5, default=0)
    ratio_default = models.DecimalField(decimal_places=4, max_digits=5, default=0)
    ratio_min_comp = models.DecimalField(decimal_places=4, max_digits=5, default=0)
    ratio_max_comp = models.DecimalField(decimal_places=4, max_digits=5, default=0)
    ratio_default_comp = models.DecimalField(decimal_places=4, max_digits=5, default=0)
    created_by_id = models.IntegerField()
    updated_by_id = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    action = models.CharField(max_length=50)
    his_created_by = models.IntegerField()
    his_created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        app_label = 'socialDb'
        db_table = "city_pay_ratio_his"
        ordering = ['-id']

