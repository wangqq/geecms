from django import forms
from cityManagement.models import City, BscCity, MedInsuranceType, CityMedType, CityMedPremium, CityPayBase, CityPayRatio
from ckeditor.fields import RichTextFormField
from static.common.constant import Constant

from geeCMS.settings import socialDb


class CityForm(forms.Form):
    city_model = BscCity.objects.using(socialDb).all()
    city_list = []
    for i in city_model:
        test = []
        test.append(i.id)
        test.append(i.name)
        city_list.append(test)
    bsc_city_id = forms.ChoiceField(choices=city_list, label=u'城市', required=False)
    yanglaobx = forms.DecimalField(label=u'养老保险', required=False)
    yanglaobx_comp = forms.DecimalField(label=u'养老保险（公司）', required=False)
    yiliaobx = forms.DecimalField(label=u'医疗保险', required=False)
    yiliaobx_comp = forms.DecimalField(label=u'医疗保险（公司）', required=False)
    shiyebx = forms.DecimalField(label=u'失业保险', required=False)
    shiyebx_comp = forms.DecimalField(label=u'失业保险（公司）', required=False)
    gongshangbx = forms.DecimalField(label=u'工伤保险', required=False)
    gongshangbx_comp = forms.DecimalField(label=u'工伤保险（公司）', required=False)
    shengyubx = forms.DecimalField(label=u'生育保险', required=False)
    shengyubx_comp = forms.DecimalField(label=u'生育保险（公司）', required=False)
    gongjijin_min = forms.DecimalField(label=u'公积金', required=False)
    gongjijin_comp_min = forms.DecimalField(label=u'公积金（公司）', required=False)
    gongjijin_max = forms.DecimalField(label=u'公积金（上限）', required=False)
    gongjijin_comp_max = forms.DecimalField(label=u'公积金（公司）（上限）', required=False)
    baoxiaoliucheng = RichTextFormField(label=u'报销流程简介', required=False)

    class Meta:
        model = City
        exclude = ['created_at', 'updated_at', 'created_by', 'updated_by', 'province_id']


class CityChangeForm(forms.ModelForm):
    yanglaobx = forms.DecimalField(label=u'养老保险', required=False)
    yanglaobx_comp = forms.DecimalField(label=u'养老保险(公司)', required=False)
    yiliaobx = forms.DecimalField(label=u'医疗保险', required=False)
    yiliaobx_comp = forms.DecimalField(label=u'医疗保险（公司）', required=False)
    shiyebx = forms.DecimalField(label=u'失业保险', required=False)
    shiyebx_comp = forms.DecimalField(label=u'失业保险（公司）', required=False)
    gongshangbx = forms.DecimalField(label=u'工伤保险', required=False)
    gongshangbx_comp = forms.DecimalField(label=u'工伤保险（公司）', required=False)
    shengyubx = forms.DecimalField(label=u'生育保险', required=False)
    shengyubx_comp = forms.DecimalField(label=u'生育保险（公司）', required=False)
    gongjijin_min = forms.DecimalField(label=u'公积金（下限）', required=False)
    gongjijin_comp_min = forms.DecimalField(label=u'公积金（公司）（下限）', required=False)
    gongjijin_max = forms.DecimalField(label=u'公积金（上限）', required=False)
    gongjijin_comp_max = forms.DecimalField(label=u'公积金（公司）（上限）', required=False)
    baoxiaoliucheng = RichTextFormField(label=u'报销流程简介', required=False)

    class Meta:
        model = City
        exclude = ['created_at', 'updated_at', 'created_by', 'updated_by', 'province_id', 'bsc_city']


class CityMedTypeForm(forms.Form):
    city_model = BscCity.objects.using(socialDb).all()
    med_model = MedInsuranceType.objects.using(socialDb).filter(is_active=Constant.STATUS_ACTIVE)
    city_list = []
    med_list = []
    for i in city_model:
        test = []
        test.append(i.id)
        test.append(i.name)
        city_list.append(test)
    for i in med_model:
        test = []
        test.append(i.id)
        test.append(i.description)
        med_list.append(test)
    bsc_city_id = forms.ChoiceField(choices=city_list, label=u'城市', required=False)
    med_insurance_type_id = forms.ChoiceField(choices=med_list, label=u'社保类型', required=False)
    menzhenbaoxiao = forms.DecimalField(label=u'门诊报销', required=False)
    zhuyuanbaoxiao = forms.DecimalField(label=u'住院报销', required=False)
    menzhen_pay_standard = forms.IntegerField(label=u'门诊起付标准', required=False)
    zhuyuan_pay_standard = forms.IntegerField(label=u'住院起付标准', required=False)
    yibao_info = RichTextFormField(label=u'医保简介', required=False)
    wx_yibao_info = forms.URLField(label=u'小程序医保简介', required=False)

    class Meta:
        model = CityMedType
        exclude = ['created_at', 'updated_at', 'created_by', 'updated_by']


class CityMedTypeChangeForm(forms.ModelForm):
    menzhenbaoxiao = forms.DecimalField(label=u'门诊报销', required=False)
    zhuyuanbaoxiao = forms.DecimalField(label=u'住院报销', required=False)
    menzhen_pay_standard = forms.IntegerField(label=u'门诊起付标准', required=False)
    zhuyuan_pay_standard = forms.IntegerField(label=u'住院起付标准', required=False)
    yibao_info = RichTextFormField(label=u'医保简介', required=False)
    wx_yibao_info = forms.URLField(label=u'小程序医保简介', required=False)

    class Meta:
        model = CityMedType
        exclude = ['created_at', 'updated_at', 'created_by', 'updated_by', 'bsc_city', 'med_insurance_type']


class CityMedPremiumForm(forms.Form):
    age_start = forms.IntegerField(label=u'起始年龄', required=True)
    age_end = forms.IntegerField(label=u'终止年龄', required=True)
    premium = forms.IntegerField(label=u'保费', required=True)

    class Meta:
        model = CityMedPremium
        exclude = ['created_at', 'updated_at', 'created_by', 'updated_by', 'bsc_city', 'med_insurance_type']


class CityMedPremiumChangeForm(forms.ModelForm):
    age_start = forms.IntegerField(label=u'起始年龄', required=True)
    age_end = forms.IntegerField(label=u'终止年龄', required=True)
    premium = forms.IntegerField(label=u'保费', required=True)

    class Meta:
        model = CityMedPremium
        exclude = ['created_at', 'updated_at', 'created_by', 'updated_by', 'bsc_city', 'med_insurance_type']


# 缴纳基数form
class CityPayBaseForm(forms.Form):
    amount_min = forms.DecimalField(label=u'最小值', required=True)
    amount_max = forms.DecimalField(label=u'最大值', required=True)

    class Meta:
        model = CityPayBase
        field = ['amount_min', 'amount_max']


# 缴纳比例form
class CityPayRatioForm(forms.Form):
    ratio_min = forms.DecimalField(label=u'个人最小值', required=True)
    ratio_max = forms.DecimalField(label=u'个人最大值', required=True)
    ratio_default = forms.DecimalField(label=u'个人默认值', required=True)
    ratio_min_comp = forms.DecimalField(label=u'公司最小值', required=True)
    ratio_max_comp = forms.DecimalField(label=u'公司最大值', required=True)
    ratio_default_comp = forms.DecimalField(label=u'公司默认值', required=True)

    class Meta:
        model = CityPayRatio
        field = ['ratio_min', 'ratio_max', 'ratio_default', 'ratio_min_comp', 'ratio_max_comp', 'ratio_default_comp']

