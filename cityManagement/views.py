# Create your views here.
from cityManagement.models import City, BscCity, CityMedType, MedInsuranceType, CityHistory, CityMedTypeHistory,CityMedPremium,CityMedPremiumHistory,InsMedCycle,\
    CareerType, CareerSocialMatch, BscSclInsType, CityPayBase, CityPayBaseHis, CityPayRatio, CityPayRatioHis
from cityManagement.forms import CityForm, CityChangeForm, CityMedTypeForm, CityMedTypeChangeForm,CityMedPremiumForm,\
    CityMedPremiumChangeForm, CityPayBaseForm, CityPayRatioForm
from customerManagement.models import CustomerDatabase

from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.response import Response
from static.common.constant import Constant
from rest_framework.authentication import SessionAuthentication
from utils.auth import ExpiringTokenAuthentication
from rest_framework.permissions import IsAuthenticated

from django.db import transaction, IntegrityError
from static.common.cousterFunction import dict_fetchall

from static.common.HistoryData import *
from django.db import connection

from geeCMS.settings import socialDb

@api_view(['GET'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def index(request):
    if request.GET.get('name'):
        items = BscCity.objects.using(socialDb).filter(name__contains=request.GET.get('name'))
    else:
        items = BscCity.objects.using(socialDb).all()
    result = []
    for item in items:
        app_result = {
            'name': item.name,
            'id': item.id
        }
        result.append(app_result)
    return Response(result)


@api_view(['GET', 'POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def create(request):
    if request.method == 'POST':
        # 基数信息
        bsc_city_id = request.data.get('bsc_city_id')
        pay_base = request.data.get('pay_base')
        pay_ratio = request.data.get('pay_ratio')
        # 重复检证
        exist = CityPayBase.objects.using(socialDb).filter(bsc_city_id=bsc_city_id)
        if exist:
            return Response({'code': Constant.EXIST, 'message': Constant.EXIST_MESSAGE})
        else:
            user_id = request.user.id
            try:
                with transaction.atomic(using=socialDb):
                    # 基数信息
                    for pay_item in pay_base:
                        pay_form = CityPayBaseForm(pay_item)
                        if pay_form.is_valid():
                            pay_cleaned_data = pay_form.cleaned_data
                            pay_obj = CityPayBase(
                                bsc_city_id=bsc_city_id,
                                ins_type_id=pay_item['id'],
                                amount_min=pay_cleaned_data.get('amount_min'),
                                amount_max=pay_cleaned_data.get('amount_max'),
                                created_by_id=user_id
                            )
                            pay_obj.save(using=socialDb)
                        else:
                            return Response({'code': Constant.INVAILD, 'message': pay_form.errors})

                    # 缴纳比例信息
                    for ratio_item in pay_ratio:
                        ratio_form = CityPayRatioForm(ratio_item)
                        if ratio_form.is_valid():
                            ratio_cleaned_data = ratio_form.cleaned_data
                            ratio_obj = CityPayRatio(
                                bsc_city_id=bsc_city_id,
                                ins_type_id=ratio_item['id'],
                                ratio_min=ratio_cleaned_data.get('ratio_min'),
                                ratio_max=ratio_cleaned_data.get('ratio_max'),
                                ratio_default=ratio_cleaned_data.get('ratio_default'),
                                ratio_min_comp=ratio_cleaned_data.get('ratio_min_comp'),
                                ratio_max_comp=ratio_cleaned_data.get('ratio_max_comp'),
                                ratio_default_comp=ratio_cleaned_data.get('ratio_default_comp'),
                                created_by_id=user_id
                            )
                            ratio_obj.save(using=socialDb)
                        else:
                            return Response({'code': Constant.INVAILD, 'message': ratio_form.errors})

                    return Response({'code': Constant.REQUEST_SUCCESS_CODE, 'message': Constant.REQUEST_SUCCESS_MSG})
            except IntegrityError as err:
                return Response({'code': Constant.REQUEST_FAIL_CODE, 'message': str(err)})

    city_list = BscCity.objects.using(socialDb).values_list('id', 'name')
    # 社保公积金项目名称
    ins_type_list = dict_fetchall(BscSclInsType.objects.using(socialDb).values_list("id", "description").filter(is_active=Constant.STATUS_ACTIVE))

    return Response({'city_list': city_list, "ins_type_list": ins_type_list})


@api_view(['GET', 'POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def update(request, id):
    if request.method == 'POST':
        # 基数信息
        pay_base = request.data.get('pay_base')
        pay_ratio = request.data.get('pay_ratio')
        try:
            with transaction.atomic(using=socialDb):
                user_id = request.user.id
                # 基数信息
                for pay_item in pay_base:
                    pay_form = CityPayBaseForm(pay_item)
                    if pay_form.is_valid():
                        pay_cleaned_data = pay_form.cleaned_data
                        try:
                            pay_obj = CityPayBase.objects.using(socialDb).get(bsc_city_id=id,ins_type_id=pay_item['id'])
                            # 历史记录
                            pay_his = CityPayBaseHis(
                                fid=pay_obj.id,
                                bsc_city_id=pay_obj.bsc_city_id,
                                ins_type_id=pay_obj.ins_type_id,
                                amount_min=pay_obj.amount_min,
                                amount_max=pay_obj.amount_max,
                                created_by_id=pay_obj.created_by_id,
                                updated_by_id=pay_obj.updated_by_id,
                                created_at=pay_obj.created_at,
                                updated_at=pay_obj.updated_at,
                                action=Constant.ACTION_UPDATE,
                                his_created_by=int(user_id)
                            )
                            pay_his.save(using=socialDb)
                            # 更新
                            pay_obj.amount_min = pay_cleaned_data.get('amount_min')
                            pay_obj.amount_max = pay_cleaned_data.get('amount_max')
                            pay_obj.created_by_id = int(user_id)
                            pay_obj.updated_by_id = int(user_id)
                            pay_obj.save(using=socialDb , update_fields=['amount_min', 'amount_max', 'updated_by_id'])
                        except CityPayBase.DoesNotExist:
                            pay_obj = CityPayBase(
                                bsc_city_id=id,
                                ins_type_id=pay_item['id'],
                                amount_min=pay_cleaned_data.get('amount_min'),
                                amount_max=pay_cleaned_data.get('amount_max'),
                                created_by_id=user_id
                            )
                            pay_obj.save(using=socialDb)
                    else:
                        return Response({'code': Constant.INVAILD, 'message': pay_form.errors})

                # 缴纳比例信息
                for ratio_item in pay_ratio:
                    ratio_form = CityPayRatioForm(ratio_item)
                    if ratio_form.is_valid():
                        ratio_cleaned_data = ratio_form.cleaned_data
                        try:
                            ratio_obj = CityPayRatio.objects.using(socialDb).get(bsc_city_id=id, ins_type_id=ratio_item['id'])
                            # 历史记录
                            ratio_his = CityPayRatioHis(
                                fid=ratio_obj.id,
                                bsc_city_id=ratio_obj.bsc_city_id,
                                ins_type_id=ratio_obj.ins_type_id,
                                ratio_min=ratio_obj.ratio_min,
                                ratio_max=ratio_obj.ratio_max,
                                ratio_default=ratio_obj.ratio_default,
                                ratio_min_comp=ratio_obj.ratio_min_comp,
                                ratio_max_comp=ratio_obj.ratio_max_comp,
                                ratio_default_comp=ratio_obj.ratio_default_comp,
                                created_by_id=ratio_obj.created_by_id,
                                updated_by_id=ratio_obj.updated_by_id,
                                created_at=ratio_obj.created_at,
                                updated_at=ratio_obj.updated_at,
                                action=Constant.ACTION_UPDATE,
                                his_created_by=int(user_id)
                            )
                            ratio_his.save(using=socialDb)
                            # 更新
                            ratio_obj.ratio_min = ratio_cleaned_data.get('ratio_min')
                            ratio_obj.ratio_max = ratio_cleaned_data.get('ratio_max')
                            ratio_obj.ratio_default = ratio_cleaned_data.get('ratio_default')
                            ratio_obj.ratio_min_comp = ratio_cleaned_data.get('ratio_min_comp')
                            ratio_obj.ratio_max_comp = ratio_cleaned_data.get('ratio_max_comp')
                            ratio_obj.ratio_default_comp = ratio_cleaned_data.get('ratio_default_comp')
                            ratio_obj.created_by_id = int(user_id)
                            ratio_obj.updated_by_id = int(user_id)
                            ratio_obj.save(using=socialDb,update_fields=['ratio_min', 'ratio_max', 'ratio_default', 'ratio_min_comp', 'ratio_max_comp', 'ratio_default_comp', 'updated_by_id'])
                        except CityPayRatio.DoesNotExist:
                            ratio_obj = CityPayRatio(
                                bsc_city_id=id,
                                ins_type_id=ratio_item['id'],
                                ratio_min=ratio_cleaned_data.get('ratio_min'),
                                ratio_max=ratio_cleaned_data.get('ratio_max'),
                                ratio_default=ratio_cleaned_data.get('ratio_default'),
                                ratio_min_comp=ratio_cleaned_data.get('ratio_min_comp'),
                                ratio_max_comp=ratio_cleaned_data.get('ratio_max_comp'),
                                ratio_default_comp=ratio_cleaned_data.get('ratio_default_comp'),
                                created_by_id=user_id
                            )
                            ratio_obj.save(using=socialDb)
                    else:
                        return Response({'code': Constant.INVAILD, 'message': ratio_form.errors})

                return Response({'code': Constant.REQUEST_SUCCESS_CODE, 'message': Constant.REQUEST_SUCCESS_MSG})
        except IntegrityError as err:
            return Response({'code': Constant.REQUEST_FAIL_CODE, 'message': str(err)})

    # 返回值
    result = {}

    # 缴纳比例
    pay_ratio_list = CityPayRatio.objects.using(socialDb).filter(bsc_city_id=id).values()
    result["pay_ratio"] = pay_ratio_list

    # 缴纳基数
    pay_base_list = CityPayBase.objects.using(socialDb).filter(bsc_city_id=id).values("id", "bsc_city_id", "ins_type_id", "amount_min", "amount_max")
    result["pay_base"] = pay_base_list

    # 社保公积金项目名称
    ins_type_list = dict_fetchall(BscSclInsType.objects.using(socialDb).values_list("id", "description").filter(is_active=Constant.STATUS_ACTIVE))
    result["ins_type_list"] = ins_type_list
    return Response(result)


# 社保类型列表
@api_view(['GET'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def index_social(request, id):
    result = MedInsuranceType.objects.using(socialDb).filter(bsc_city_id=id,is_active=Constant.STATUS_ACTIVE).values("id", "description")
    return Response(result)


# 社保类型添加
@api_view(['POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
@transaction.atomic(using=socialDb)
def create_social(request):
    user_id = request.user.id
    name = request.data.get('name')
    city_id = int(request.data.get('city_id'))
    id = request.data.get('id')
    try:
        if id:
            med_obj = MedInsuranceType.objects.using(socialDb).get(id=id)
        else:
            med_obj = MedInsuranceType.objects.using(socialDb).get(bsc_city_id=city_id, description=name)
        # 插入历史
        return_med_his = fun_med_insurance_type_his(med_obj, Constant.ACTION_UPDATE, user_id)
        if return_med_his == Constant.FAIL:
            raise IntegrityError
        # 更新信息
        med_obj.description = name
        med_obj.is_active = Constant.STATUS_ACTIVE
        if not med_obj.updated_by:
            med_obj.created_by = user_id
        med_obj.updated_by = user_id
        med_obj.save(using=socialDb)
        return Response({'code': Constant.REQUEST_SUCCESS_CODE, 'message': Constant.REQUEST_SUCCESS_MSG, 'med_id': med_obj.id})
    except MedInsuranceType.DoesNotExist:
        obj = MedInsuranceType(
            bsc_city_id=city_id,
            description=name,
            is_active=Constant.STATUS_ACTIVE,
            created_by=user_id,
        )
        obj.save(using=socialDb)

        med_obj = MedInsuranceType.objects.using(socialDb).get(bsc_city_id=city_id, description=name)
        # 插入历史
        return_med_his = fun_med_insurance_type_his(med_obj, Constant.ACTION_INSERT, user_id)
        if return_med_his == Constant.FAIL:
            raise IntegrityError
        return Response({'code': Constant.REQUEST_SUCCESS_CODE, 'message': Constant.REQUEST_SUCCESS_MSG, 'med_id': med_obj.id})
    except IntegrityError:
        return Response({'code': Constant.REQUEST_FAIL_CODE, 'message': Constant.REQUEST_FAIL_MSG})


# 编辑社保信息
@api_view(['GET', 'POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def social_info(request, id):
    social_obj = MedInsuranceType.objects.using(socialDb).get(id=id)
    if request.method == 'POST':
        yibao_info = request.data.get('yibao_info')
        wx_yibao_info = request.data.get('wx_yibao_info')
        med_info = request.data.get('med_info')
        med_premium = request.data.get('med_premium')
        del_med_premium = request.data.get('del_med_premium')
        del_med_info = request.data.get('del_med_info')
        try:
            with transaction.atomic(using=socialDb):
                user_id = request.user.id
                med_age_set = set()
                # 年龄段检测
                for med_age_item in med_info:
                    old_len = len(med_age_set)
                    add_len = med_age_item["age_end"] - med_age_item["age_start"] + 1
                    for i in range(med_age_item["age_start"], (med_age_item["age_end"] + 1)):
                        med_age_set.add(i)
                    now_len = len(med_age_set)
                    if now_len < (old_len + add_len):
                        return Response({'code': Constant.VALUE_AGE_ERROR_CODE, 'message': Constant.VALUE_AGE_ERROR_MSG})

                pre_age_set = set()
                # 年龄段检测
                for pre_age_item in med_premium:
                    old_len = len(pre_age_set)
                    add_len = pre_age_item["age_end"] - pre_age_item["age_start"] + 1
                    for i in range(pre_age_item["age_start"], (pre_age_item["age_end"] + 1)):
                        pre_age_set.add(i)
                    now_len = len(pre_age_set)
                    if now_len < (old_len + add_len):
                        return Response(
                            {'code': Constant.VALUE_AGE_ERROR_CODE, 'message': Constant.VALUE_AGE_ERROR_MSG})

                # 医保图片信息
                for med_item in med_info:
                    if med_item["id"]:
                        try:
                            med_obj = CityMedType.objects.using(socialDb).get(id=med_item["id"])
                            # 历史记录
                            return_ratio_his = fun_city_med_type_his(med_obj, Constant.ACTION_UPDATE,user_id)
                            if return_ratio_his == Constant.FAIL:
                                raise IntegrityError
                            # 详情更新
                            med_obj.med_insurance_type_id = social_obj.id
                            med_obj.created_by_id = int(user_id)
                            med_obj.updated_by_id = int(user_id)
                            med_obj.age_start = med_item["age_start"]
                            med_obj.age_end = med_item["age_end"]
                            med_obj.menzhenbaoxiao = med_item["menzhenbaoxiao"]
                            med_obj.zhuyuanbaoxiao = med_item["zhuyuanbaoxiao"]
                            med_obj.menzhen_pay_standard = med_item["menzhen_pay_standard"]
                            med_obj.menzhen_standard_unit = med_item["menzhen_standard_unit"]
                            med_obj.zhuyuan_pay_standard = med_item["zhuyuan_pay_standard"]
                            med_obj.zhuyuan_standard_unit = med_item["zhuyuan_standard_unit"]
                            med_obj.yibao_info = yibao_info
                            med_obj.wx_yibao_info = wx_yibao_info
                            med_obj.save(using=socialDb)
                        except CityMedType.DoesNotExist:
                            return Response(
                                {'code': Constant.ERR_NONE_EXIST_CODE, 'message': Constant.ERR_NONE_EXIST_MESSAGE})
                    else:
                        # 新建
                        med_obj = CityMedType(
                            bsc_city_id=social_obj.bsc_city_id,
                            med_insurance_type_id=social_obj.id,
                            created_by_id=user_id,
                            updated_by_id=user_id,
                            age_start=med_item["age_start"],
                            age_end=med_item["age_end"],
                            menzhenbaoxiao=med_item["menzhenbaoxiao"],
                            zhuyuanbaoxiao=med_item["zhuyuanbaoxiao"],
                            menzhen_pay_standard=med_item["menzhen_pay_standard"],
                            menzhen_standard_unit=med_item["menzhen_standard_unit"],
                            zhuyuan_pay_standard=med_item["zhuyuan_pay_standard"],
                            zhuyuan_standard_unit=med_item["zhuyuan_standard_unit"],
                            yibao_info=yibao_info,
                            wx_yibao_info=wx_yibao_info
                        )
                        med_obj.save(using=socialDb)

                # 年龄段
                for pre_item in med_premium:
                    if pre_item['id']:
                        med_premium_obj = CityMedPremium.objects.using(socialDb).get(id=pre_item['id'])
                        med_premium_form = CityMedPremiumChangeForm(pre_item, instance=med_premium_obj)
                        if med_premium_form.is_valid():
                            med_premium_cleaned_data = med_premium_form.cleaned_data
                            # 历史记录
                            return_ratio_his = fun_city_med_premium_his(med_premium_obj, Constant.ACTION_UPDATE,user_id)
                            if return_ratio_his == Constant.FAIL:
                                raise IntegrityError

                            # 详情更新
                            med_premium_obj.created_by_id = int(user_id)
                            med_premium_obj.updated_by_id = int(user_id)
                            med_premium_obj.age_start = med_premium_cleaned_data.get('age_start')
                            med_premium_obj.age_end = med_premium_cleaned_data.get('age_end')
                            med_premium_obj.premium = med_premium_cleaned_data.get('premium')
                            med_premium_obj.save(using=socialDb,
                                update_fields=['age_start', 'age_end', 'premium'])
                        else:
                            return Response({'code': Constant.INVAILD, 'message': med_premium_form.errors})
                    else:
                        med_premium_form = CityMedPremiumForm(pre_item)
                        if med_premium_form.is_valid():
                            med_premium_cleaned_data = med_premium_form.cleaned_data
                            med_premium_obj = CityMedPremium(
                                bsc_city_id=social_obj.bsc_city_id,
                                med_insurance_type_id=id,
                                created_by_id=user_id,
                                updated_by_id=user_id,
                                age_start=med_premium_cleaned_data.get('age_start'),
                                age_end=med_premium_cleaned_data.get('age_end'),
                                premium=med_premium_cleaned_data.get('premium')
                            )
                            med_premium_obj.save(using=socialDb)
                        else:
                            return Response({'code': Constant.INVAILD, 'message': med_premium_form.errors})
                # 删除的年龄段
                for del_item in del_med_premium:
                    # 历史记录
                    his_del_premium = CityMedPremium.objects.using(socialDb).get(id=del_item)
                    return_ratio_his = fun_city_med_premium_his(his_del_premium, Constant.ACTION_DELETE,user_id)
                    if return_ratio_his == Constant.FAIL:
                        raise IntegrityError
                    CityMedPremium.objects.using(socialDb).get(id=del_item).delete()

                # 删除医保信息
                for del_info in del_med_info:
                    # 历史记录
                    med_delete_obj = CityMedType.objects.using(socialDb).get(id=del_info)
                    # 历史记录
                    return_med_delete_his = fun_city_med_type_his(med_delete_obj, Constant.ACTION_DELETE, user_id)
                    if return_med_delete_his == Constant.FAIL:
                        raise IntegrityError

                    CityMedType.objects.using(socialDb).get(id=del_info).delete()

                return Response({'code': Constant.REQUEST_SUCCESS_CODE, 'message': Constant.REQUEST_SUCCESS_MSG})
        except IntegrityError as err:
            return Response({'code': Constant.SQL_EXEC_ERR_CODE, 'message': str(err)})

    # 初期显示信息取得
    med_json = CityMedType.objects.using(socialDb).filter(bsc_city_id=social_obj.bsc_city_id, med_insurance_type_id=id).\
        values("id",
               "age_start", "age_end",
               "menzhenbaoxiao", "menzhen_pay_standard", "menzhen_standard_unit",
               "zhuyuanbaoxiao","zhuyuan_pay_standard","zhuyuan_standard_unit",
               "yibao_info", "wx_yibao_info")
    if med_json:
        med_yibao = {
            'yibao_info': med_json[0]["yibao_info"],
            'wx_yibao_info': med_json[0]["wx_yibao_info"]
        }
    else:
        med_yibao = {
            'yibao_info': "",
            'wx_yibao_info': ""
        }

    # 年龄段查询
    premium_json = CityMedPremium.objects.using(socialDb).filter(med_insurance_type_id=id).values("id", "bsc_city_id", "med_insurance_type_id", "age_start", "age_end", "premium")
    # 单位
    unitList = InsMedCycle.objects.using(socialDb).values_list('id', 'description').filter(is_active=Constant.STATUS_ACTIVE)

    result = {
        'med_json': med_json,
        'med_yibao': med_yibao,
        'premium_json': premium_json,
        'unitList': unitList
    }
    return Response(result)


# 社保类型删除
@api_view(['GET'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def delete_social(request, id):
    try:
        with transaction.atomic(using=socialDb):
            # 查询用户SQL文
            customer = CustomerDatabase.objects.filter(is_active=1).all()
            # 可以删除
            deleteFlg = True
            # 建立DB连接
            cursor = connection.cursor()
            for item_c in customer:
                # 查询不同状态下的客户数量
                db_name = item_c.dsn
                tab_clt_insurance = db_name + ".clt_insurance"
                clt_ins_num_sql = '''SELECT COUNT(id) AS num FROM ''' + tab_clt_insurance + \
                                        ''' WHERE is_active = 1 AND type_id=''' + str(id)
                cursor.execute(clt_ins_num_sql)
                clt_ins_num_result = cursor.fetchone()
                if clt_ins_num_result[0] > 0:
                    deleteFlg = False
                    break

            if deleteFlg:
                user_id = request.user.id
                med_obj = MedInsuranceType.objects.using(socialDb).get(id=id)
                # 插入历史
                return_med_his = fun_med_insurance_type_his(med_obj, Constant.ACTION_DELETE, user_id)
                if return_med_his == Constant.FAIL:
                    raise IntegrityError

                med_obj.delete()
                return Response({'code': Constant.REQUEST_SUCCESS_CODE, 'message': Constant.REQUEST_SUCCESS_MSG})
            else:
                return Response({'code': Constant.REQUEST_FAIL_CODE, 'message': '已被使用，不可删除'})
    except MedInsuranceType.DoesNotExist:
        return Response({'code': Constant.SQL_DATA_NOT_EXIST_CODE, 'message': Constant.SQL_DATA_NOT_EXIST_MSG})
    except IntegrityError:
        return Response({'code': Constant.REQUEST_FAIL_CODE, 'message': Constant.REQUEST_FAIL_MSG})


# 职业类型与社保类型的匹配设定
@api_view(['GET', 'POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def update_career_social_match(request, id):
    if request.method == 'POST':
        match_info = request.data.get('match_info')
        user_id = request.user.id
        try:
            with transaction.atomic(using=socialDb):
                # 设定匹配关系
                for item in match_info:
                    if ('id' in item) and item['id']:
                        # 更新
                        match_obj = CareerSocialMatch.objects.using(socialDb).get(id=item['id'], bsc_city_id=id)
                        # 插入历史
                        return_match_his = fun_career_social_match_his(match_obj, Constant.ACTION_UPDATE, user_id)
                        if return_match_his == Constant.FAIL:
                            raise IntegrityError

                        match_obj.med_id = item['med_id']
                        match_obj.save(using=socialDb,update_fields=['med_id'])
                    else:
                        # 创建
                        match_obj = CareerSocialMatch(
                            bsc_city_id=id,
                            career_id=item['career_id'],
                            med_id=item['med_id']
                        )
                        match_obj.save(using=socialDb)
                        # 插入历史
                        return_match_his = fun_career_social_match_his(match_obj, Constant.ACTION_INSERT, user_id)
                        if return_match_his == Constant.FAIL:
                            raise IntegrityError
                return Response({'code': Constant.REQUEST_SUCCESS_CODE, 'message': Constant.REQUEST_SUCCESS_MSG})
        except IntegrityError:
            return Response({'code': Constant.REQUEST_FAIL_CODE, 'message': Constant.REQUEST_FAIL_MSG})
        except CareerSocialMatch.DoesNotExist:
            return Response({'code': Constant.SQL_DATA_NOT_EXIST_CODE, 'message': Constant.SQL_DATA_NOT_EXIST_MSG})
    # 职业信息列表
    careerList = dict_fetchall(CareerType.objects.using(socialDb).values_list('id', 'description').filter(is_active=Constant.STATUS_ACTIVE).order_by('display_order'))
    # 社保列表
    socialList = MedInsuranceType.objects.using(socialDb).values('id', 'description').filter(bsc_city_id=id, is_active=Constant.STATUS_ACTIVE)
    socialArr = dict_fetchall(MedInsuranceType.objects.using(socialDb).values_list('id', 'description').filter(bsc_city_id=id, is_active=Constant.STATUS_ACTIVE))

    match_list = []

    for s_key, s_value in careerList.items():
        app_result = {
            'career_id': s_key,
            'career': s_value,
        }
        # 查询是否存在
        try:
            match_arr = CareerSocialMatch.objects.using(socialDb).get(bsc_city_id=id, career_id=s_key)
            # 社保列表
            social_arr = []
            # 拆解数组
            if match_arr.med_id:
                if Constant.SPLIT_JOINT_CHAR in match_arr.med_id:
                    medList = match_arr.med_id.split(Constant.SPLIT_JOINT_CHAR)
                else:
                    medList = [match_arr.med_id]
                # 社保取得
                for item_soc in medList:
                    int_item_soc = int(item_soc)
                    if socialArr and int_item_soc in socialArr:
                        social_arr.append(int_item_soc)

            app_result['id'] = match_arr.id
            app_result['social_arr'] = social_arr

        except CareerSocialMatch.DoesNotExist:
            app_result['id'] = ''
            app_result['social_arr'] = []

        match_list.append(app_result)


    # 返回的数组
    result = {
        'data_list': match_list,
        'social_list': socialList
    }

    return Response(result)
