from django.apps import AppConfig


class CitymanagementConfig(AppConfig):
    name = 'cityManagement'
