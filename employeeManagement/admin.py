from django.contrib import admin
from .models import AuthUser


class AuthUserAdmin(admin.ModelAdmin):
    list_display = ('username', 'first_name', 'last_name', 'email', 'is_active')
    fields = ('username', 'email', 'first_name', 'last_name', 'is_active', 'is_superuser')
    search_fields = ('username','date_joined','is_active')

admin.site.register(AuthUser, AuthUserAdmin)
