from employeeManagement.models import AuthUser
from employeeManagement.forms import AuthUserAddForm, AuthUserChangeForm, AuthUserPwChangeForm
from django.db import transaction, IntegrityError
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.models import Permission, User

from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.response import Response
from static.common.constant import Constant
from rest_framework.authentication import SessionAuthentication
from utils.auth import ExpiringTokenAuthentication
from rest_framework.permissions import IsAuthenticated


# Create your views here.

@api_view(['GET'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
@permission_required('employeeManagement.view_employee_list')
def index(request):
    user_item = AuthUser.objects.all()

    if request.GET.get('timeStart'):
        user_item = user_item.filter(date_joined__gte=request.GET.get('timeStart'))
    if request.GET.get('timeEnd'):
        user_item = user_item.filter(date_joined__lte=request.GET.get('timeEnd'))
    if request.GET.get('name'):
        user_item = user_item.filter(username__contains=request.GET.get('name'))
    if request.GET.get('is_active'):
        user_item = user_item.filter(is_active=request.GET.get('is_active'))
    count = user_item.count()

    result = []
    for item in user_item:
        app_result = {
            'email': item.email,
            'first_name': item.first_name,
            'date_joined': item.date_joined,
            'get_status_txt': Constant.STATUS_CHOICE[item.is_active],
            'id': item.id
        }
        result.append(app_result)
    return Response({'count': count, 'result': result})


@api_view(['POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def create(request):
    user_name = request.user.username
    form = AuthUserAddForm(request.data)
    if request.method == 'POST':
        if form.is_valid():
            try:
                with transaction.atomic():
                    obj = AuthUser(
                        password=form.cleaned_data.get('password'),
                        is_superuser=int(form.cleaned_data.get('is_superuser')),
                        first_name=form.cleaned_data.get('first_name'),
                        email=form.cleaned_data.get('email'),
                        username = form.cleaned_data.get('email'),
                        is_active=int(form.cleaned_data.get('is_active')),
                        created_by=user_name
                    )
                    obj.save()
                    user = User.objects.get(username=form.cleaned_data.get('email'))
                    if form.cleaned_data.get('is_superuser') == '1':
                        permission = Permission.objects.get(codename='view_employee_list')
                        user.user_permissions.add(permission)
                    return Response({'code': Constant.SUCCESS, 'message': Constant.SUCCESS_MESSAGE})
            except IntegrityError:
                return Response({'code': Constant.FAIL, 'message': Constant.FAIL_MESSAGE})

        else:
            return Response({'code': Constant.INVAILD, 'message': form.errors})


@api_view(['GET', 'POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def change(request, id):
    obj = AuthUser.objects.get(id=id)
    obj.is_active = int(obj.is_active)
    obj.is_superuser = int(obj.is_superuser)
    if request.method == 'POST':
        if request.data.get('password') == '':
            form = AuthUserChangeForm(request.data, instance=obj)
        else:
            form = AuthUserPwChangeForm(request.data, instance=obj)
        try:
            with transaction.atomic():
                if form.is_valid():
                    form.save()
                    user = User.objects.get(id=id)
                    permission = Permission.objects.get(codename='view_employee_list')
                    if obj.is_superuser == 0 and user.has_perm('employeeManagement.view_employee_list'):
                        user.user_permissions.remove(permission)
                    return Response({'code': Constant.SUCCESS, 'message': Constant.SUCCESS_MESSAGE})
                else:
                    return Response({'code': Constant.INVAILD, 'message': form.errors})

        except IntegrityError:
            return Response({'code': Constant.FAIL, 'message': Constant.FAIL_MESSAGE})

    result = {
        'email': obj.email,
        'last_name': obj.last_name,
        'first_name': obj.first_name,
        'is_active': obj.is_active,
        'is_superuser': obj.is_superuser
    }
    return Response(result)
