from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class EmployeemanagementConfig(AppConfig):
    name = 'employeeManagement'
    verbose_name = _("Employee management")
