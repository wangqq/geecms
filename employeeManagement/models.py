from django.db import models
from django.contrib.auth import validators
from django.utils import six, timezone
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.hashers import make_password


# Create your models here.

class AuthUser(models.Model):
    STATUS_CHOICE = (
        (1, '正常'),
        (0, '禁用'),
    )
    id = models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)
    password = models.CharField(max_length=128, verbose_name='password')
    last_login = models.DateTimeField(default=timezone.now, verbose_name='last login')
    is_superuser = models.BooleanField(
        default=False,
        help_text='Designates that this user has all permissions without explicitly assigning them.',
        verbose_name='superuser status'
    )
    username = models.CharField(
        help_text='Required. 30 characters or fewer. Letters, digits and @/./+/-/_ only.', unique=True,
        max_length=30, verbose_name='username',
        validators=[
            validators.UnicodeUsernameValidator() if six.PY3 else validators.ASCIIUsernameValidator()
        ],
    )
    first_name = models.CharField(max_length=30, verbose_name='first name', blank=True)
    last_name = models.CharField(max_length=30, verbose_name='last name', blank=False)
    email = models.EmailField(max_length=75, verbose_name='email address', blank=True)
    is_staff = models.BooleanField(
        default=True, help_text='Designates whether the user can log into this admin site.',
        verbose_name='staff status'
    )
    is_active = models.BooleanField(
        default=True, verbose_name='active', help_text=(
            'Designates whether this user should be treated as active. Unselect this instead of deleting '
            'accounts.'
        )
    )
    date_joined = models.DateTimeField(default=timezone.now, verbose_name='date joined')
    created_by = models.CharField(max_length=255, verbose_name='创建者')
    openid = models.CharField(max_length=255, verbose_name='Openid')
    wechat_id = models.CharField(max_length=255, verbose_name='微信号')

    class Meta:
        db_table = "auth_user"
        verbose_name = _("AuthUser")
        verbose_name_plural = _('AuthUser')
        ordering = ['-date_joined', ]

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.password = make_password(self.password)
        super(AuthUser, self).save(force_insert, force_update, using,
                                   update_fields)

    def __str__(self):
        return self.username

    def get_status_txt(self):
        if self.is_active == 1:
            return '正常'
        elif self.is_active == 0:
            return '禁用'
        else:
            return '-'

    def name(self):
        return self.last_name + self.first_name
