from django import forms
from django.contrib.auth import (
    password_validation,
)
from employeeManagement.models import AuthUser
from django.contrib.admin import widgets
from django.core.exceptions import ValidationError

STATUS_CHOICE = (
    (1, '正常'),
    (0, '禁用'),
)
POWER_CHOICE = (
    (1, '管理员'),
    (0, '普通员工'),
)


class AuthUserAddForm(forms.ModelForm):
    email = forms.EmailField(
        label=u"登陆账号",
        widget=forms.TextInput()
    )
    first_name = forms.CharField(
        label = u"名",
        required = False

    )
    password = forms.CharField(
        label=u"密码",
        widget=forms.PasswordInput,
        help_text=password_validation.password_validators_help_text_html(),
    )
    is_active = forms.ChoiceField(
        label=u"状态",
        choices=STATUS_CHOICE
    )
    is_superuser = forms.ChoiceField(
        label=u"权限",
        choices=POWER_CHOICE
    )

    class Meta:
        model = AuthUser
        fields = ['email', 'first_name', 'password', 'is_active', 'is_superuser']

    def clean_username(self):
        cleaned_data = super(AuthUserAddForm, self).clean()
        username = cleaned_data.get('username')
        exist_check = AuthUser.objects.filter(username=username).count()
        if exist_check > 0:
            self._errors['username'] = self.error_class(["【" + username + "】,已经存在。"])
        return username

    def clean_email(self):
        cleaned_data = super(AuthUserAddForm, self).clean()
        email = cleaned_data.get('email')
        exist_check = AuthUser.objects.filter(email=email).count()
        if exist_check > 0:
            self._errors['email'] = self.error_class(["【" + email + "】，已经存在。"])
        return email

    def clean_password(self):
        cleaned_data = super(AuthUserAddForm, self).clean()
        password = cleaned_data.get('password')
        try:
            password_validation.validate_password(password)
        except ValidationError as err:
            self._errors['password'] = self.error_class([err])
        return password


class AuthUserChangeForm(forms.ModelForm):
    email = forms.EmailField(
        label=u"登录账号",
        widget=forms.TextInput(attrs={"readonly":"readonly"})
    )
    first_name = forms.CharField(
        label = u"名",

    )
    is_active = forms.ChoiceField(
        label=u"状态",
        choices=STATUS_CHOICE
    )
    is_superuser = forms.ChoiceField(
        label=u"权限",
        choices=POWER_CHOICE
    )

    class Meta:
        model = AuthUser
        fields = ['is_active', 'is_superuser', 'first_name', 'email']


class AuthUserPwChangeForm(forms.ModelForm):
    email = forms.EmailField(
        label=u"登录账号",
        widget=forms.TextInput(attrs={"readonly":"readonly"})
    )

    password = forms.CharField(
        label=u"密码",
        widget=forms.PasswordInput,
        help_text=password_validation.password_validators_help_text_html(),
    )
    first_name = forms.CharField(
        label = u"名",

    )
    is_active = forms.ChoiceField(
        label=u"状态",
        choices=STATUS_CHOICE
    )
    is_superuser = forms.ChoiceField(
        label=u"权限",
        choices=POWER_CHOICE
    )

    class Meta:
        model = AuthUser
        fields = ['is_active', 'is_superuser', 'first_name', 'password', 'email']

    def clean_password(self):
        cleaned_data = super(AuthUserPwChangeForm, self).clean()
        password = cleaned_data.get('password')
        try:
            password_validation.validate_password(password)
        except ValidationError as err:
            self._errors['password'] = self.error_class([err])
        return password
