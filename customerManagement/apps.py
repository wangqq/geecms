from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class CustomermanagementConfig(AppConfig):
    name = 'customerManagement'
    # verbose_name = _("Customer management")
