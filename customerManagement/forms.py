from django import forms
from django.contrib.auth import (
    password_validation,
)
from customerManagement.models import Customer, User
from django.contrib.admin import widgets
from static.common.constant import Constant
from django.core.exceptions import ValidationError
from django.forms.extras.widgets import SelectDateWidget

STATUS_CHOICE = (
    (10, '正常'),
    (20, '禁用'),
)

STATUS_CHOICE_CUSTOMER = (
    (1, '正常'),
    (0, '禁用'),
)


STATUS_CHOICE_DOWN = (
    (1, '正常'),
    (0, '禁用'),
)


class CustomerForm(forms.ModelForm):
    """
    Customer company information
    # """

    name = forms.CharField(
        label=u"公司名称",
    )

    email = forms.CharField(
        label=u"邮箱",
    )

    status = forms.ChoiceField(
        label=u"状态",
        choices=STATUS_CHOICE_CUSTOMER
    )
    word_download_status = forms.ChoiceField(
        label=u"word导出状态",
        choices=STATUS_CHOICE_DOWN
    )

    customer_num = forms.IntegerField(
        label=u"账号数",
        widget=widgets.AdminIntegerFieldWidget,
    )

    start_date = forms.DateField(
        label=u"开始日期",
        widget=widgets.AdminDateWidget,
    )
    end_date = forms.DateField(
        label=u"结束日期",
        widget=widgets.AdminDateWidget,
    )

    class Meta:

        model = Customer
        fields = ['name', 'customer_num', 'start_date', 'end_date', 'status', 'word_download_status']

    def clean(self):
        cleaned_data = super(CustomerForm, self).clean()
        start_date = cleaned_data.get('start_date')
        end_date = cleaned_data.get('end_date')

        if start_date and end_date and start_date > end_date:
            self._errors['start_date'] = self.error_class([u"开始日期不能大于结束日期"])
        else:
            return cleaned_data

    def clean_name(self):
        cleaned_data = super(CustomerForm, self).clean()
        name = cleaned_data.get('name')
        exist_check = Customer.objects.filter(name=name).count()
        if exist_check > 0:
            self._errors['name'] = self.error_class(["【" + name + "】,已经存在。"])
        return name

    def clean_customer_num(self):
        cleaned_data = super(CustomerForm, self).clean()
        customer_num = cleaned_data.get('customer_num')
        if customer_num > 10000:
            self._errors['customer_num'] = self.error_class([u"最大为10000"])
        return customer_num


class CustomerChangeForm(forms.ModelForm):

    name = forms.CharField(
        label=u"公司名称",
        widget=forms.TextInput(attrs={"readonly": "readonly"})
    )

    status = forms.ChoiceField(
        label=u"状态",
        choices=STATUS_CHOICE_CUSTOMER
    )

    word_download_status = forms.ChoiceField(
        label=u"word导出状态",
        choices=STATUS_CHOICE_DOWN
    )

    customer_num = forms.IntegerField(
        label=u"账号数",
        widget=widgets.AdminIntegerFieldWidget,
    )

    start_date = forms.DateField(
        label=u"开始日期",
        widget=widgets.AdminDateWidget,
    )
    end_date = forms.DateField(
        label=u"结束日期",
        widget=widgets.AdminDateWidget,
    )

    class Meta:
        model = Customer
        fields = ['name', 'customer_num', 'start_date', 'end_date', 'status', 'word_download_status']

    def clean(self):
        cleaned_data = super(CustomerChangeForm, self).clean()
        start_date = cleaned_data.get('start_date')
        end_date = cleaned_data.get('end_date')

        if start_date and end_date and start_date > end_date:
            self._errors['start_date'] = self.error_class([u"开始日期不能大于结束日期"])
        else:
            return cleaned_data

    def clean_customer_num(self):
        cleaned_data = super(CustomerChangeForm, self).clean()
        customer_num = cleaned_data.get('customer_num')
        if customer_num > 10000:
            self._errors['customer_num'] = self.error_class([u"最大为10000"])
        return customer_num


class UserForm(forms.ModelForm):
    """
    Customer login information
    # """
    password_hash = forms.CharField(
        label=u"密码",
        widget=forms.PasswordInput,
        help_text=password_validation.password_validators_help_text_html(),
    )

    class Meta:
        model = User
        fields = ['customer', 'email', 'password_hash', 'status']


class UserChangeForm(forms.ModelForm):

    email = forms.EmailField(
        label=u"登陆账号",
        widget=forms.TextInput(attrs={"readonly": "readonly"})
    )
    password_hash = forms.CharField(
        label=u"密码",
        widget=forms.PasswordInput,
        required=False
    )

    class Meta:
        model = User
        fields = ['email', 'password_hash']

    def clean(self):
        cleaned_data = super(UserChangeForm, self).clean()
        return cleaned_data



    def clean_email(self):
        cleaned_data = super(UserChangeForm, self).clean()
        email = cleaned_data.get('email')
        # exist_check = User.objects.filter(email=email).count()
        # if exist_check > 0:
        #     self._errors['email'] = self.error_class(["【" + email + "】,已经存在。"])
        return email

    def clean_password_hash(self):
        cleaned_data = super(UserChangeForm, self).clean()
        password_hash = cleaned_data.get('password_hash')
        # try:
        #     password_validation.validate_password(password_hash)
        # except ValidationError as err:
        #     self._errors['password_hash'] = self.error_class([err])
        return password_hash


class UserAddForm(forms.ModelForm):

    email = forms.EmailField(
        label=u"登陆账号",
        widget=widgets.AdminEmailInputWidget,
    )
    firstname = forms.CharField(
        label = u"姓名",
    )
    password_hash = forms.CharField(
        label=u"密码",
        widget=forms.PasswordInput,
        help_text=password_validation.password_validators_help_text_html(),
        required=True
    )

    class Meta:
        model = User
        fields = ['email', 'password_hash']
