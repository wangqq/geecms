from django.db import models
from django.utils.translation import ugettext_lazy as _

STATUS_CHOICE = (
    (10, '正常'),
    (20, '禁用'),
)


# Create your models here.

class Customer(models.Model):
    # id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255, verbose_name='公司名称')
    email = models.CharField(max_length=100, verbose_name='邮箱')
    customer_num = models.PositiveIntegerField(verbose_name='客户数')
    start_date = models.DateField(verbose_name='账号开始时间')
    end_date = models.DateField(verbose_name='账号结束时间')
    license_id = models.IntegerField()
    created_by = models.CharField(max_length=255, verbose_name='创建人')
    updated_by = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    user = models.ForeignKey('User', to_field='id', verbose_name='登录账号', related_name='usr')
    status = models.IntegerField()
    word_download_status = models.IntegerField()
    is_syn = models.IntegerField(default=0)

    class Meta:
        db_table = "customer"
        verbose_name = _("Customer")
        verbose_name_plural = _('Customers')

    def __str__(self):
        return self.name

    def get_status_txt(self):
        # if self.status == 1:
        #     return '正常'
        # return '禁用'
        return self.status == 1 and '正常' or '禁用'


class CustomerHistory(models.Model):
    fid = models.IntegerField()
    name = models.CharField(max_length=255, verbose_name='公司名称')
    email = models.CharField(max_length=100, verbose_name='邮箱')
    customer_num = models.PositiveIntegerField(verbose_name='客户数')
    start_date = models.DateField(verbose_name='账号开始时间')
    end_date = models.DateField(verbose_name='账号结束时间')
    license_id = models.IntegerField()
    created_by = models.CharField(max_length=255, verbose_name='创建人')
    updated_by = models.CharField(max_length=255)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    status = models.IntegerField()
    user_id = models.IntegerField()
    word_download_status = models.IntegerField()
    is_syn = models.IntegerField(default=0)
    action = models.CharField(max_length=50)
    his_created_by = models.IntegerField()
    his_created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = "customer_his"


class CustomerContact(models.Model):
    id = models.IntegerField(primary_key=True)
    customer = models.ForeignKey(Customer, to_field='id')
    name = models.CharField(max_length=255)
    mobile = models.CharField(max_length=255)
    is_active = models.BooleanField()

    class Meta:
        db_table = "customer_contact"
        verbose_name = _("Customer contact")
        verbose_name_plural = _('Customer contacts')

    def __str__(self):
        return self.customer


class CustomerDatabase(models.Model):
    id = models.IntegerField(primary_key=True)
    customer = models.ForeignKey(Customer, to_field='id')
    dsn = models.CharField(max_length=255)
    username = models.CharField(max_length=255)
    password = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True)

    class Meta:
        db_table = "customer_database"
        verbose_name = _("Customer database")
        verbose_name_plural = _('Customer databases')

    def __str__(self):
        return self.customer


class User(models.Model):
    user_all_id= models.IntegerField()
    mobile = models.CharField(max_length=11)
    email = models.EmailField(max_length=100, verbose_name='登录账号')
    password_hash = models.CharField(max_length=32, verbose_name='密码')
    password_reset_token = models.CharField(max_length=255)
    customer = models.ForeignKey(Customer, to_field='id', verbose_name='公司名称', related_name='cust')
    status = models.SmallIntegerField(verbose_name='状态', choices=STATUS_CHOICE)
    created_by = models.CharField(max_length=255, verbose_name='创建者')
    updated_by = models.CharField(max_length=255, verbose_name='维护人员')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    updated_at = models.DateTimeField(auto_now=True)
    auth_key = models.CharField(max_length=255)
    firstname = models.CharField(max_length=255, verbose_name='姓')
    agreed= models.IntegerField(default=0)
    password_change= models.IntegerField(default=0)
    register_type= models.IntegerField(default=1)
    img_path = models.CharField(max_length=200, verbose_name='头像路径',default='img/touxiang.jpg')
    start_date = models.DateTimeField(verbose_name='有效开始时间')
    end_date = models.DateTimeField(verbose_name='有效结束时间')
    work_area = models.EmailField(max_length=255, verbose_name='执业区域')
    work_company = models.EmailField(max_length=255, verbose_name='公司')
    work_no = models.EmailField(max_length=255, verbose_name='展业证号')
    work_time_length = models.EmailField(max_length=255, verbose_name='从业时间')

    class Meta:
        db_table = "user"
        verbose_name = _("User")
        verbose_name_plural = _('Users')
        ordering = ['-created_at', ]

    # ----------------------------------------------------------------------

    def __str__(self):
        return self.email

    def name(self):
        return self.firstname

    name.short_description = '员工姓名'

    def des_status(self):
        return self.status == 10 and '正常' or '禁用'

    des_status.short_description = '状态'

    def customer_name(self):
        return self.customer.name

    customer_name.short_description = "公司名称"

    def customer_num(self):
        return self.customer.customer_num

    customer_num.short_description = "客户数"


class UserHistory(models.Model):
    fid = models.IntegerField()
    user_all_id= models.IntegerField()
    mobile = models.CharField(max_length=11)
    email = models.EmailField(max_length=100, verbose_name='登录账号')
    password_hash = models.CharField(max_length=32, verbose_name='密码')
    password_reset_token = models.CharField(max_length=255)
    customer_id = models.IntegerField()
    status = models.SmallIntegerField(verbose_name='状态', choices=STATUS_CHOICE)
    created_by = models.CharField(max_length=255, verbose_name='创建者')
    updated_by = models.CharField(max_length=255, verbose_name='维护人员')
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    auth_key = models.CharField(max_length=255)
    firstname = models.CharField(max_length=255, verbose_name='姓')
    agreed= models.IntegerField(default=0)
    password_change= models.IntegerField(default=0)
    register_type= models.IntegerField(default=1)
    img_path = models.CharField(max_length=200, verbose_name='头像路径',default='img/touxiang.jpg')
    start_date = models.DateTimeField(verbose_name='有效开始时间')
    end_date = models.DateTimeField(verbose_name='有效结束时间')
    work_area = models.EmailField(max_length=255, verbose_name='执业区域')
    work_company = models.EmailField(max_length=255, verbose_name='公司')
    work_no = models.EmailField(max_length=255, verbose_name='展业证号')
    work_time_length = models.EmailField(max_length=255, verbose_name='从业时间')
    action = models.CharField(max_length=50)
    his_created_by = models.IntegerField()
    his_created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = "user_his"


class UserAll(models.Model):
    unique_id = models.EmailField(max_length=255, verbose_name='用户身份标识')
    unionid = models.EmailField(max_length=255)
    openid = models.EmailField(max_length=255)
    nickname = models.EmailField(max_length=255, verbose_name='昵称')
    user_origin= models.IntegerField(default=1)
    mobile = models.CharField(max_length=11)
    email = models.EmailField(max_length=100, verbose_name='邮箱')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = "user_all"


class UserAllHistory(models.Model):
    fid = models.IntegerField()
    unique_id = models.EmailField(max_length=255, verbose_name='用户身份标识')
    unionid = models.EmailField(max_length=255)
    openid = models.EmailField(max_length=255)
    nickname = models.EmailField(max_length=255, verbose_name='昵称')
    user_origin= models.IntegerField(default=1)
    mobile = models.CharField(max_length=11)
    email = models.EmailField(max_length=100, verbose_name='邮箱')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    updated_at = models.DateTimeField(auto_now=True)
    action = models.CharField(max_length=50)
    his_created_by = models.IntegerField()
    his_created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = "user_all_his"
