from django.contrib import admin
from customerManagement.models import User
from customerManagement.forms import UserForm


# Register your models here.


@admin.register(User)
class UserSearch(admin.ModelAdmin):
    list_display = ('customer_name', 'customer_num', 'email', 'created_at', 'des_status', 'updated_by')
    ordering = ['-created_at', ]
    form = UserForm
