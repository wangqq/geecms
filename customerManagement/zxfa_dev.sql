/*
Navicat MySQL Data Transfer

Source Server         : 后台
Source Server Version : 50638
Source Host           : 120.55.53.140:3306
Source Database       : zxfa_dev1

Target Server Type    : MYSQL
Target Server Version : 50638
File Encoding         : 65001

Date: 2020-01-20 17:06:21
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for anlys_balance
-- ----------------------------
DROP TABLE IF EXISTS `anlys_balance`;
CREATE TABLE `anlys_balance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `salary` double DEFAULT NULL,
  `bonus` double DEFAULT NULL,
  `divident` double DEFAULT NULL,
  `total_income` double DEFAULT NULL,
  `expense` double DEFAULT NULL COMMENT '固定支出',
  `morgage` double DEFAULT NULL COMMENT '房贷支出',
  `charge` double DEFAULT NULL COMMENT '自由支出',
  `total_expense` double DEFAULT NULL,
  `balance_by_month` double DEFAULT NULL COMMENT '月底结余',
  `balance_by_year` double DEFAULT NULL COMMENT '年度结余',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='收支平衡表';

-- ----------------------------
-- Records of anlys_balance
-- ----------------------------

-- ----------------------------
-- Table structure for anlys_children_education
-- ----------------------------
DROP TABLE IF EXISTS `anlys_children_education`;
CREATE TABLE `anlys_children_education` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='孩子教育表';

-- ----------------------------
-- Records of anlys_children_education
-- ----------------------------

-- ----------------------------
-- Table structure for anlys_hump_savings
-- ----------------------------
DROP TABLE IF EXISTS `anlys_hump_savings`;
CREATE TABLE `anlys_hump_savings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='养老储蓄表';

-- ----------------------------
-- Records of anlys_hump_savings
-- ----------------------------

-- ----------------------------
-- Table structure for anlys_medical_security
-- ----------------------------
DROP TABLE IF EXISTS `anlys_medical_security`;
CREATE TABLE `anlys_medical_security` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='医疗保障表';

-- ----------------------------
-- Records of anlys_medical_security
-- ----------------------------

-- ----------------------------
-- Table structure for anlys_personal_protection
-- ----------------------------
DROP TABLE IF EXISTS `anlys_personal_protection`;
CREATE TABLE `anlys_personal_protection` (
  `id` double NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `expense_ten_years` double DEFAULT NULL,
  `expense_parents` double DEFAULT NULL,
  `expense_children` double DEFAULT NULL,
  `asset_floating` double DEFAULT NULL COMMENT '流动资产',
  `asset_fixed` double DEFAULT NULL COMMENT '固定资产',
  `asset_company` double DEFAULT NULL COMMENT '企业实体',
  `asset_loan` double DEFAULT NULL,
  `balance` double DEFAULT NULL,
  `reserve` double DEFAULT NULL,
  `gap` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='人身保障表';

-- ----------------------------
-- Records of anlys_personal_protection
-- ----------------------------

-- ----------------------------
-- Table structure for anlys_summary
-- ----------------------------
DROP TABLE IF EXISTS `anlys_summary`;
CREATE TABLE `anlys_summary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `protection_comment` varchar(255) DEFAULT NULL,
  `protection_budget` double DEFAULT NULL,
  `medical_comment` varchar(255) DEFAULT NULL,
  `medical_budget` double DEFAULT NULL,
  `hump_comment` varchar(255) DEFAULT NULL,
  `hump_budget` double DEFAULT NULL,
  `children_comment` varchar(255) DEFAULT NULL,
  `children_budget` double DEFAULT NULL,
  `budget_summary` varchar(255) DEFAULT NULL,
  `summary` varchar(255) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='财务分析总结表';

-- ----------------------------
-- Records of anlys_summary
-- ----------------------------

-- ----------------------------
-- Table structure for auth_assignment
-- ----------------------------
DROP TABLE IF EXISTS `auth_assignment`;
CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for auth_group
-- ----------------------------
DROP TABLE IF EXISTS `auth_group`;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_group
-- ----------------------------

-- ----------------------------
-- Table structure for auth_group_permissions
-- ----------------------------
DROP TABLE IF EXISTS `auth_group_permissions`;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`) USING BTREE,
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`) USING BTREE,
  CONSTRAINT `auth_group_permissions_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_group_permissions_ibfk_2` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_group_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for auth_item
-- ----------------------------
DROP TABLE IF EXISTS `auth_item`;
CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`) USING BTREE,
  KEY `idx-auth_item-type` (`type`) USING BTREE,
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of auth_item
-- ----------------------------
INSERT INTO `auth_item` VALUES ('admin', '1', null, null, null, '1500284718', '1500284718');
INSERT INTO `auth_item` VALUES ('normal', '1', null, null, null, '1500284761', '1500284761');
INSERT INTO `auth_item` VALUES ('normal-office', '1', '内勤员工', null, null, '1506670343', '1506670383');
INSERT INTO `auth_item` VALUES ('office-power', '2', '内勤权限', null, null, '1506670401', '1506670401');
INSERT INTO `auth_item` VALUES ('staff/create', '2', null, null, null, '1500284998', '1500284998');
INSERT INTO `auth_item` VALUES ('staff/index', '2', null, null, null, '1500284950', '1500284950');
INSERT INTO `auth_item` VALUES ('staff/update', '2', null, null, null, '1500285009', '1500285009');

-- ----------------------------
-- Table structure for auth_item_child
-- ----------------------------
DROP TABLE IF EXISTS `auth_item_child`;
CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`) USING BTREE,
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of auth_item_child
-- ----------------------------
INSERT INTO `auth_item_child` VALUES ('normal-office', 'office-power');
INSERT INTO `auth_item_child` VALUES ('admin', 'staff/create');
INSERT INTO `auth_item_child` VALUES ('normal-office', 'staff/create');
INSERT INTO `auth_item_child` VALUES ('admin', 'staff/index');
INSERT INTO `auth_item_child` VALUES ('normal-office', 'staff/index');
INSERT INTO `auth_item_child` VALUES ('admin', 'staff/update');
INSERT INTO `auth_item_child` VALUES ('normal-office', 'staff/update');

-- ----------------------------
-- Table structure for auth_permission
-- ----------------------------
DROP TABLE IF EXISTS `auth_permission`;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`) USING BTREE,
  CONSTRAINT `auth_permission_ibfk_1` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_permission
-- ----------------------------

-- ----------------------------
-- Table structure for auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `auth_rule`;
CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of auth_rule
-- ----------------------------

-- ----------------------------
-- Table structure for auth_user
-- ----------------------------
DROP TABLE IF EXISTS `auth_user`;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_user
-- ----------------------------

-- ----------------------------
-- Table structure for auth_user_groups
-- ----------------------------
DROP TABLE IF EXISTS `auth_user_groups`;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`) USING BTREE,
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`) USING BTREE,
  CONSTRAINT `auth_user_groups_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_user_groups
-- ----------------------------

-- ----------------------------
-- Table structure for auth_user_user_permissions
-- ----------------------------
DROP TABLE IF EXISTS `auth_user_user_permissions`;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`) USING BTREE,
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`) USING BTREE,
  CONSTRAINT `auth_user_user_permissions_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `auth_user_user_permissions_ibfk_2` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_user_user_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for bsc_action
-- ----------------------------
DROP TABLE IF EXISTS `bsc_action`;
CREATE TABLE `bsc_action` (
  `id` tinyint(4) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统操作类型表';

-- ----------------------------
-- Records of bsc_action
-- ----------------------------

-- ----------------------------
-- Table structure for bsc_auth_item
-- ----------------------------
DROP TABLE IF EXISTS `bsc_auth_item`;
CREATE TABLE `bsc_auth_item` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` longtext,
  `rule_name` varchar(64) DEFAULT NULL,
  `data` longtext,
  `created_by` varchar(255) NOT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='权限明细表';

-- ----------------------------
-- Records of bsc_auth_item
-- ----------------------------

-- ----------------------------
-- Table structure for bsc_auth_role_item
-- ----------------------------
DROP TABLE IF EXISTS `bsc_auth_role_item`;
CREATE TABLE `bsc_auth_role_item` (
  `role_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  PRIMARY KEY (`role_id`,`item_id`),
  KEY `fk_auth_role_item__auth_item` (`item_id`),
  CONSTRAINT `fk_auth_role_item__auth_item` FOREIGN KEY (`item_id`) REFERENCES `bsc_auth_item` (`id`),
  CONSTRAINT `fk_auth_role_item__role` FOREIGN KEY (`role_id`) REFERENCES `bsc_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色与权限关联表';

-- ----------------------------
-- Records of bsc_auth_role_item
-- ----------------------------

-- ----------------------------
-- Table structure for bsc_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `bsc_auth_rule`;
CREATE TABLE `bsc_auth_rule` (
  `name` varchar(64) NOT NULL,
  `data` longtext,
  `created_by` varchar(255) NOT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='权限规则表';

-- ----------------------------
-- Records of bsc_auth_rule
-- ----------------------------

-- ----------------------------
-- Table structure for bsc_child_insurance_type
-- ----------------------------
DROP TABLE IF EXISTS `bsc_child_insurance_type`;
CREATE TABLE `bsc_child_insurance_type` (
  `id` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bsc_child_insurance_type
-- ----------------------------
INSERT INTO `bsc_child_insurance_type` VALUES ('1', '少儿医保', '1');
INSERT INTO `bsc_child_insurance_type` VALUES ('2', '住院基金', '1');
INSERT INTO `bsc_child_insurance_type` VALUES ('3', '学平险', '0');

-- ----------------------------
-- Table structure for bsc_city
-- ----------------------------
DROP TABLE IF EXISTS `bsc_city`;
CREATE TABLE `bsc_city` (
  `id` tinyint(11) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bsc_city
-- ----------------------------

-- ----------------------------
-- Table structure for bsc_company
-- ----------------------------
DROP TABLE IF EXISTS `bsc_company`;
CREATE TABLE `bsc_company` (
  `id` int(11) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `contact1` int(11) DEFAULT NULL,
  `contact2` int(11) DEFAULT NULL,
  `license_id` int(11) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_company_license` (`license_id`) USING BTREE,
  CONSTRAINT `bsc_company_ibfk_1` FOREIGN KEY (`license_id`) REFERENCES `sys_license` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='公司表';

-- ----------------------------
-- Records of bsc_company
-- ----------------------------

-- ----------------------------
-- Table structure for bsc_rel_type
-- ----------------------------
DROP TABLE IF EXISTS `bsc_rel_type`;
CREATE TABLE `bsc_rel_type` (
  `id` tinyint(4) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='成员关系表';

-- ----------------------------
-- Records of bsc_rel_type
-- ----------------------------
INSERT INTO `bsc_rel_type` VALUES ('1', '自己');
INSERT INTO `bsc_rel_type` VALUES ('2', '配偶');
INSERT INTO `bsc_rel_type` VALUES ('3', '子女');
INSERT INTO `bsc_rel_type` VALUES ('4', '父母');

-- ----------------------------
-- Table structure for bsc_role
-- ----------------------------
DROP TABLE IF EXISTS `bsc_role`;
CREATE TABLE `bsc_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_role_company` (`company_id`),
  CONSTRAINT `fk_role_company` FOREIGN KEY (`company_id`) REFERENCES `bsc_company` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色表';

-- ----------------------------
-- Records of bsc_role
-- ----------------------------

-- ----------------------------
-- Table structure for bsc_scl_insurance
-- ----------------------------
DROP TABLE IF EXISTS `bsc_scl_insurance`;
CREATE TABLE `bsc_scl_insurance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` tinyint(4) NOT NULL COMMENT '所在城市',
  `lower_limit_amount` double(8,0) DEFAULT '0' COMMENT '缴费下限金额',
  `upper_limit_amount` double(8,0) DEFAULT NULL COMMENT '缴费上限金额',
  `effective_start_date` datetime(4) DEFAULT NULL COMMENT '有效开始日',
  `effective_end_date` datetime(4) DEFAULT NULL COMMENT '有效截止日',
  `type_id` tinyint(4) NOT NULL COMMENT '类型',
  `company_type_id` tinyint(4) DEFAULT NULL COMMENT '单位性质',
  `ratio` double(8,3) NOT NULL COMMENT '比例',
  PRIMARY KEY (`id`),
  KEY `fk_bsc_scl_insurance__city` (`city_id`),
  CONSTRAINT `fk_bsc_scl_insurance__city` FOREIGN KEY (`city_id`) REFERENCES `bsc_city` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='社保基数比例表';

-- ----------------------------
-- Records of bsc_scl_insurance
-- ----------------------------

-- ----------------------------
-- Table structure for bsc_scl_insurance_medical
-- ----------------------------
DROP TABLE IF EXISTS `bsc_scl_insurance_medical`;
CREATE TABLE `bsc_scl_insurance_medical` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` tinyint(11) DEFAULT NULL COMMENT '市',
  `year` varchar(4) DEFAULT NULL COMMENT '年',
  `type_id` int(4) NOT NULL COMMENT '类型,1:门急诊，2：住院',
  `med_card_rel` tinyint(4) DEFAULT NULL COMMENT '社保卡，1：余额用完，2：和社保卡没有关系',
  `own_beer_amount` int(8) DEFAULT NULL COMMENT '自负段',
  `ratio` double(8,3) DEFAULT NULL COMMENT '报销比例',
  `upper_limit_amount` double(8,0) DEFAULT NULL COMMENT '最高限额',
  `reim_scopr` varchar(20) DEFAULT NULL COMMENT '报销范围',
  PRIMARY KEY (`id`),
  KEY `fk_type_id` (`type_id`),
  KEY `fk_city_id` (`city_id`),
  CONSTRAINT `fk_city_id` FOREIGN KEY (`city_id`) REFERENCES `bsc_city` (`id`),
  CONSTRAINT `fk_type_id` FOREIGN KEY (`type_id`) REFERENCES `bsc_scl_insurance_medical_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='社会医保参照表';

-- ----------------------------
-- Records of bsc_scl_insurance_medical
-- ----------------------------

-- ----------------------------
-- Table structure for bsc_scl_insurance_medical_type
-- ----------------------------
DROP TABLE IF EXISTS `bsc_scl_insurance_medical_type`;
CREATE TABLE `bsc_scl_insurance_medical_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='社会医保参照类型表';

-- ----------------------------
-- Records of bsc_scl_insurance_medical_type
-- ----------------------------

-- ----------------------------
-- Table structure for bsc_scl_insurance_type
-- ----------------------------
DROP TABLE IF EXISTS `bsc_scl_insurance_type`;
CREATE TABLE `bsc_scl_insurance_type` (
  `id` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `display_order` tinyint(4) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bsc_scl_insurance_type
-- ----------------------------
INSERT INTO `bsc_scl_insurance_type` VALUES ('1', '养老', '1', '1');
INSERT INTO `bsc_scl_insurance_type` VALUES ('2', '医疗', '1', '2');
INSERT INTO `bsc_scl_insurance_type` VALUES ('3', '失业', '1', '3');
INSERT INTO `bsc_scl_insurance_type` VALUES ('4', '工伤', '1', '4');
INSERT INTO `bsc_scl_insurance_type` VALUES ('5', '生育', '1', '5');
INSERT INTO `bsc_scl_insurance_type` VALUES ('6', '公积金', '1', '6');
INSERT INTO `bsc_scl_insurance_type` VALUES ('7', '补充公积金', '0', null);
INSERT INTO `bsc_scl_insurance_type` VALUES ('8', '补充医保金', '0', null);
INSERT INTO `bsc_scl_insurance_type` VALUES ('9', '补充养老金', '0', null);

-- ----------------------------
-- Table structure for bsc_status
-- ----------------------------
DROP TABLE IF EXISTS `bsc_status`;
CREATE TABLE `bsc_status` (
  `id` tinyint(4) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bsc_status
-- ----------------------------
INSERT INTO `bsc_status` VALUES ('0', '被删除');
INSERT INTO `bsc_status` VALUES ('10', '有效');

-- ----------------------------
-- Table structure for bsc_title
-- ----------------------------
DROP TABLE IF EXISTS `bsc_title`;
CREATE TABLE `bsc_title` (
  `id` tinyint(4) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='称呼表';

-- ----------------------------
-- Records of bsc_title
-- ----------------------------
INSERT INTO `bsc_title` VALUES ('1', '先生');
INSERT INTO `bsc_title` VALUES ('2', '女士');

-- ----------------------------
-- Table structure for bsc_user
-- ----------------------------
DROP TABLE IF EXISTS `bsc_user`;
CREATE TABLE `bsc_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `auth_key` varchar(32) DEFAULT NULL,
  `realname` varchar(255) DEFAULT NULL,
  `password_hash` varchar(255) NOT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `gender` smallint(6) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `company_id` int(11) NOT NULL DEFAULT '1',
  `license_id` int(11) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_company_id` (`company_id`),
  CONSTRAINT `fk_user_company` FOREIGN KEY (`company_id`) REFERENCES `bsc_company` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of bsc_user
-- ----------------------------

-- ----------------------------
-- Table structure for bsc_user_role
-- ----------------------------
DROP TABLE IF EXISTS `bsc_user_role`;
CREATE TABLE `bsc_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_role__role` (`role_id`),
  KEY `fk_user_role__user` (`user_id`),
  CONSTRAINT `fk_user_role__role` FOREIGN KEY (`role_id`) REFERENCES `bsc_role` (`id`),
  CONSTRAINT `fk_user_role__user` FOREIGN KEY (`user_id`) REFERENCES `bsc_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='基本用户角色表';

-- ----------------------------
-- Records of bsc_user_role
-- ----------------------------

-- ----------------------------
-- Table structure for clt_asset
-- ----------------------------
DROP TABLE IF EXISTS `clt_asset`;
CREATE TABLE `clt_asset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `comments` text COMMENT '备注',
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_balance_client` (`client_id`) USING BTREE,
  KEY `fk_balance__balance_type` (`type_id`) USING BTREE,
  CONSTRAINT `clt_asset_ibfk_2` FOREIGN KEY (`client_id`) REFERENCES `clt_client` (`id`),
  CONSTRAINT `clt_asset_type_id` FOREIGN KEY (`type_id`) REFERENCES `clt_asset_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户资产表';

-- ----------------------------
-- Table structure for clt_asset_his
-- ----------------------------
DROP TABLE IF EXISTS `clt_asset_his`;
CREATE TABLE `clt_asset_his` (
  `his_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '历史主键',
  `id` int(11) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `comments` text COMMENT '备注',
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `action` varchar(50) DEFAULT NULL COMMENT '操作类型',
  `his_created_by` int(11) DEFAULT NULL COMMENT '历史记录创建者',
  `his_created_at` timestamp NULL DEFAULT NULL COMMENT '历史记录创建时间',
  PRIMARY KEY (`his_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户资产历史表';

-- ----------------------------
-- Table structure for clt_asset_type
-- ----------------------------
DROP TABLE IF EXISTS `clt_asset_type`;
CREATE TABLE `clt_asset_type` (
  `id` int(11) NOT NULL DEFAULT '0',
  `super_id` tinyint(4) DEFAULT NULL COMMENT '一级分类ID',
  `type` tinyint(4) DEFAULT '1' COMMENT '系统定义1或自定义2',
  `description` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `display_order` tinyint(4) DEFAULT NULL COMMENT '一级分类ID',
  `created_by` varchar(50) NOT NULL DEFAULT 'default' COMMENT '创建者ID',
  `created_at` timestamp NOT NULL DEFAULT '2017-01-01 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `fk_asset_super_id` (`super_id`),
  CONSTRAINT `fk_asset_super_id` FOREIGN KEY (`super_id`) REFERENCES `clt_asset_type_super` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='资产类型表';

-- ----------------------------
-- Records of clt_asset_type
-- ----------------------------
INSERT INTO `clt_asset_type` VALUES ('1', '1', '1', '活期', '1', '1', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_asset_type` VALUES ('2', '1', '1', '定期', '1', '2', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_asset_type` VALUES ('3', '1', '1', '理财', '1', '9', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_asset_type` VALUES ('5', '1', '1', '黄金', '1', '5', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_asset_type` VALUES ('6', '1', '1', '外汇', '1', '4', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_asset_type` VALUES ('7', '1', '1', '基金', '1', '6', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_asset_type` VALUES ('8', '1', '1', '股票', '1', '10', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_asset_type` VALUES ('9', '2', '1', '自住房产', '1', '1', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_asset_type` VALUES ('11', '2', '1', '车辆(含牌照)', '1', '3', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_asset_type` VALUES ('12', '3', '1', '实业', '1', '1', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_asset_type` VALUES ('15', '1', '1', '债券', '1', '3', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_asset_type` VALUES ('16', '1', '1', '私募基金', '1', '7', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_asset_type` VALUES ('17', '2', '1', '车位', '1', '2', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_asset_type` VALUES ('18', '2', '1', '投资/出租房产或商铺', '1', '4', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_asset_type` VALUES ('19', '1', '1', '债权', '1', '8', 'default', '2017-01-01 00:00:00');

-- ----------------------------
-- Table structure for clt_asset_type_his
-- ----------------------------
DROP TABLE IF EXISTS `clt_asset_type_his`;
CREATE TABLE `clt_asset_type_his` (
  `his_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '历史主键',
  `id` int(11) NOT NULL,
  `super_id` tinyint(4) DEFAULT NULL COMMENT '一级分类ID',
  `type` tinyint(4) DEFAULT '1' COMMENT '系统定义1或自定义2',
  `description` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `display_order` tinyint(4) DEFAULT NULL COMMENT '一级分类ID',
  `created_by` varchar(50) NOT NULL DEFAULT 'default' COMMENT '创建者ID',
  `created_at` timestamp NOT NULL DEFAULT '2017-01-01 00:00:00' COMMENT '创建时间',
  `action` varchar(50) DEFAULT NULL COMMENT '操作类型',
  `his_created_by` int(11) DEFAULT NULL COMMENT '历史记录创建者',
  `his_created_at` timestamp NULL DEFAULT NULL COMMENT '历史记录创建时间',
  PRIMARY KEY (`his_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='资产类型历史表';

-- ----------------------------
-- Table structure for clt_asset_type_super
-- ----------------------------
DROP TABLE IF EXISTS `clt_asset_type_super`;
CREATE TABLE `clt_asset_type_super` (
  `id` tinyint(4) NOT NULL COMMENT '主键',
  `description` varchar(255) NOT NULL COMMENT '名称',
  `display_order` tinyint(4) DEFAULT NULL COMMENT '排序',
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '删除flg',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='资产负债类型一级分类表';

-- ----------------------------
-- Records of clt_asset_type_super
-- ----------------------------
INSERT INTO `clt_asset_type_super` VALUES ('1', '流动资产', '1', '1');
INSERT INTO `clt_asset_type_super` VALUES ('2', '固定资产', '2', '1');
INSERT INTO `clt_asset_type_super` VALUES ('3', '实业资产', '3', '1');

-- ----------------------------
-- Table structure for clt_client
-- ----------------------------
DROP TABLE IF EXISTS `clt_client`;
CREATE TABLE `clt_client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `status_id` tinyint(4) DEFAULT NULL,
  `image_path` varchar(255) DEFAULT 'url(img/single-men.png)' COMMENT '首页客户背景图',
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `analyse_time` timestamp NULL DEFAULT NULL COMMENT '分析规划首次点击时间',
  `complete_time` timestamp NULL DEFAULT NULL COMMENT '销售完成首次点击时间',
  `last_modify_time` timestamp NULL DEFAULT NULL COMMENT '客户相关信息最后更新时间',
  `comment` text COMMENT '备注',
  PRIMARY KEY (`id`),
  KEY `fk_client_status` (`status_id`) USING BTREE,
  CONSTRAINT `clt_client_ibfk_1` FOREIGN KEY (`status_id`) REFERENCES `clt_status` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户表';

-- ----------------------------
-- Table structure for clt_client_his
-- ----------------------------
DROP TABLE IF EXISTS `clt_client_his`;
CREATE TABLE `clt_client_his` (
  `his_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '历史主键',
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `status_id` tinyint(4) DEFAULT NULL,
  `image_path` varchar(255) DEFAULT NULL COMMENT '首页客户背景图',
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `analyse_time` timestamp NULL DEFAULT NULL COMMENT '分析规划首次点击时间',
  `complete_time` timestamp NULL DEFAULT NULL COMMENT '销售完成首次点击时间',
  `last_modify_time` timestamp NULL DEFAULT NULL COMMENT '客户相关信息最后更新时间',
  `comment` text COMMENT '备注',
  `action` varchar(50) DEFAULT NULL COMMENT '操作类型',
  `his_created_by` int(11) DEFAULT NULL COMMENT '历史记录创建者',
  `his_created_at` timestamp NULL DEFAULT NULL COMMENT '历史记录创建时间',
  PRIMARY KEY (`his_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户历史表';

-- ----------------------------
-- Table structure for clt_client_set
-- ----------------------------
DROP TABLE IF EXISTS `clt_client_set`;
CREATE TABLE `clt_client_set` (
  `client_id` int(11) NOT NULL COMMENT '客户ID',
  `cycle_reserve` int(1) DEFAULT '0' COMMENT '紧急准备金周期',
  `ier_reserve` varchar(5) DEFAULT '0' COMMENT '紧急准备金乘数',
  `ier_reserve_amount` double(20,0) DEFAULT '0' COMMENT '紧急准备金乘数金额',
  `modify_reserve` int(1) DEFAULT '0' COMMENT '用户修改flg',
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`client_id`),
  CONSTRAINT `clt_client_set_fk` FOREIGN KEY (`client_id`) REFERENCES `clt_client` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户设置表';

-- ----------------------------
-- Table structure for clt_client_set_his
-- ----------------------------
DROP TABLE IF EXISTS `clt_client_set_his`;
CREATE TABLE `clt_client_set_his` (
  `his_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '历史主键',
  `client_id` int(11) DEFAULT NULL COMMENT '客户ID',
  `cycle_reserve` int(1) DEFAULT '0' COMMENT '紧急准备金周期',
  `ier_reserve` varchar(5) DEFAULT '0' COMMENT '紧急准备金乘数',
  `ier_reserve_amount` double(20,0) DEFAULT '0' COMMENT '紧急准备金乘数金额',
  `modify_reserve` int(1) DEFAULT '0' COMMENT '用户修改flg',
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `action` varchar(50) DEFAULT NULL COMMENT '操作类型',
  `his_created_by` int(11) DEFAULT NULL COMMENT '历史记录创建者',
  `his_created_at` timestamp NULL DEFAULT NULL COMMENT '历史记录创建时间',
  PRIMARY KEY (`his_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户设置历史表表';

-- ----------------------------
-- Table structure for clt_debt
-- ----------------------------
DROP TABLE IF EXISTS `clt_debt`;
CREATE TABLE `clt_debt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL COMMENT '客户ID',
  `type_id` int(11) DEFAULT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `comments` text COMMENT '备注',
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_balance_client` (`client_id`) USING BTREE,
  KEY `fk_balance__balance_type` (`type_id`) USING BTREE,
  CONSTRAINT `clt_debt_ibfk_2` FOREIGN KEY (`client_id`) REFERENCES `clt_client` (`id`),
  CONSTRAINT `clt_debt_type_id` FOREIGN KEY (`type_id`) REFERENCES `clt_debt_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户负债表';

-- ----------------------------
-- Table structure for clt_debt_his
-- ----------------------------
DROP TABLE IF EXISTS `clt_debt_his`;
CREATE TABLE `clt_debt_his` (
  `his_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '历史主键',
  `id` int(11) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL COMMENT '客户ID',
  `type_id` int(11) DEFAULT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `comments` text COMMENT '备注',
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `action` varchar(50) DEFAULT NULL COMMENT '操作类型',
  `his_created_by` int(11) DEFAULT NULL COMMENT '历史记录创建者',
  `his_created_at` timestamp NULL DEFAULT NULL COMMENT '历史记录创建时间',
  PRIMARY KEY (`his_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户负债历史表';

-- ----------------------------
-- Table structure for clt_debt_type
-- ----------------------------
DROP TABLE IF EXISTS `clt_debt_type`;
CREATE TABLE `clt_debt_type` (
  `id` int(11) NOT NULL DEFAULT '0',
  `super_id` tinyint(4) DEFAULT NULL COMMENT '一级分类ID',
  `type` tinyint(4) DEFAULT '1' COMMENT '系统定义1或自定义2',
  `description` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `display_order` tinyint(4) DEFAULT NULL COMMENT '一级分类ID',
  `created_by` varchar(50) NOT NULL DEFAULT 'default' COMMENT '创建者ID',
  `created_at` timestamp NOT NULL DEFAULT '2017-01-01 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `fk_debt_super_id` (`super_id`),
  CONSTRAINT `fk_debt_super_id` FOREIGN KEY (`super_id`) REFERENCES `clt_debt_type_super` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='资产类型表';

-- ----------------------------
-- Records of clt_debt_type
-- ----------------------------
INSERT INTO `clt_debt_type` VALUES ('1', '1', '1', '房贷', '1', '1', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_debt_type` VALUES ('3', '1', '1', '外债', '1', '4', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_debt_type` VALUES ('4', '1', '1', '车贷', '1', '2', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_debt_type` VALUES ('5', '1', '1', '消费贷', '1', '3', 'default', '2017-01-01 00:00:00');

-- ----------------------------
-- Table structure for clt_debt_type_his
-- ----------------------------
DROP TABLE IF EXISTS `clt_debt_type_his`;
CREATE TABLE `clt_debt_type_his` (
  `his_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '历史主键',
  `id` int(11) NOT NULL,
  `super_id` tinyint(4) DEFAULT NULL COMMENT '一级分类ID',
  `type` tinyint(4) DEFAULT '1' COMMENT '系统定义1或自定义2',
  `description` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `display_order` tinyint(4) DEFAULT NULL COMMENT '一级分类ID',
  `created_by` varchar(50) NOT NULL DEFAULT 'default' COMMENT '创建者ID',
  `created_at` timestamp NOT NULL DEFAULT '2017-01-01 00:00:00' COMMENT '创建时间',
  `action` varchar(50) DEFAULT NULL COMMENT '操作类型',
  `his_created_by` int(11) DEFAULT NULL COMMENT '历史记录创建者',
  `his_created_at` timestamp NULL DEFAULT NULL COMMENT '历史记录创建时间',
  PRIMARY KEY (`his_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='资产类型历史表';


-- ----------------------------
-- Table structure for clt_debt_type_super
-- ----------------------------
DROP TABLE IF EXISTS `clt_debt_type_super`;
CREATE TABLE `clt_debt_type_super` (
  `id` tinyint(4) NOT NULL COMMENT '主键',
  `description` varchar(255) NOT NULL COMMENT '名称',
  `display_order` tinyint(4) DEFAULT NULL COMMENT '排序',
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '删除flg',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='资产负债类型一级分类表';

-- ----------------------------
-- Records of clt_debt_type_super
-- ----------------------------
INSERT INTO `clt_debt_type_super` VALUES ('1', '负债', '1', '1');

-- ----------------------------
-- Table structure for clt_expense
-- ----------------------------
DROP TABLE IF EXISTS `clt_expense`;
CREATE TABLE `clt_expense` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `member_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `cycle_id` tinyint(4) NOT NULL DEFAULT '1',
  `amount` double NOT NULL DEFAULT '0',
  `comments` text COMMENT '备注',
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_expense_member` (`member_id`) USING BTREE,
  KEY `fk_expense__expense_type` (`type_id`) USING BTREE,
  KEY `fk_expense_client` (`client_id`) USING BTREE,
  CONSTRAINT `clt_expense_ibfk_2` FOREIGN KEY (`client_id`) REFERENCES `clt_client` (`id`),
  CONSTRAINT `clt_expense_ibfk_3` FOREIGN KEY (`member_id`) REFERENCES `clt_member` (`id`),
  CONSTRAINT `clt_expense_type_id` FOREIGN KEY (`type_id`) REFERENCES `clt_expense_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户支出表';

-- ----------------------------
-- Table structure for clt_expense_his
-- ----------------------------
DROP TABLE IF EXISTS `clt_expense_his`;
CREATE TABLE `clt_expense_his` (
  `his_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '历史主键',
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `member_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `cycle_id` tinyint(4) NOT NULL DEFAULT '1',
  `amount` double NOT NULL DEFAULT '0',
  `comments` text COMMENT '备注',
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `action` varchar(50) DEFAULT NULL COMMENT '操作类型',
  `his_created_by` int(11) DEFAULT NULL COMMENT '历史记录创建者',
  `his_created_at` timestamp NULL DEFAULT NULL COMMENT '历史记录创建时间',
  PRIMARY KEY (`his_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户支出历史表';

-- ----------------------------
-- Table structure for clt_expense_type
-- ----------------------------
DROP TABLE IF EXISTS `clt_expense_type`;
CREATE TABLE `clt_expense_type` (
  `id` int(11) NOT NULL DEFAULT '0',
  `super_id` tinyint(4) DEFAULT NULL COMMENT '一级分类ID',
  `second_id` tinyint(4) DEFAULT NULL COMMENT '二级分类',
  `type` tinyint(4) DEFAULT '1' COMMENT '系统定义1或自定义2',
  `cycle_id` tinyint(4) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `description_tip` varchar(4096) DEFAULT NULL,
  `display_order` tinyint(4) DEFAULT NULL,
  `created_by` varchar(50) NOT NULL DEFAULT 'default' COMMENT '创建者ID',
  `created_at` timestamp NOT NULL DEFAULT '2017-01-01 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `fk_super_id` (`super_id`),
  KEY `fk_second_id` (`second_id`),
  CONSTRAINT `fk_second_id` FOREIGN KEY (`second_id`) REFERENCES `clt_expense_type_second` (`id`),
  CONSTRAINT `fk_super_id` FOREIGN KEY (`super_id`) REFERENCES `clt_expense_type_super` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='费用类型表';

-- ----------------------------
-- Records of clt_expense_type
-- ----------------------------
INSERT INTO `clt_expense_type` VALUES ('1', '1', '1', '1', '1', '一日三餐', '1', '', '1', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_expense_type` VALUES ('2', '1', '1', '1', '1', '外出聚餐', '1', '', '2', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_expense_type` VALUES ('3', '1', '1', '1', '1', '水果零食', '1', '', '3', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_expense_type` VALUES ('4', '1', '1', '1', '1', '烟酒茶饮', '1', '', '4', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_expense_type` VALUES ('5', '1', '1', '1', '1', '食材采购', '1', '父母采购或自行采购', '5', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_expense_type` VALUES ('6', '1', '2', '1', '1', '交通费', '1', '', '6', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_expense_type` VALUES ('7', '1', '2', '1', '1', '停车费', '1', '', '7', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_expense_type` VALUES ('8', '1', '2', '1', '1', '用车费', '1', '', '8', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_expense_type` VALUES ('9', '1', '3', '1', '1', '房租', '1', '', '9', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_expense_type` VALUES ('10', '1', '3', '1', '1', '物业费', '1', '', '10', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_expense_type` VALUES ('11', '1', '3', '1', '1', '水费', '1', '', '11', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_expense_type` VALUES ('12', '1', '3', '1', '1', '电费', '1', '', '12', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_expense_type` VALUES ('13', '1', '3', '1', '1', '煤气费', '1', '', '13', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_expense_type` VALUES ('14', '1', '3', '1', '1', '采暖费', '1', '', '14', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_expense_type` VALUES ('15', '1', '3', '1', '1', '家政支出', '1', '', '15', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_expense_type` VALUES ('16', '1', '3', '1', '1', '有线电视', '1', '', '16', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_expense_type` VALUES ('17', '1', '3', '1', '1', '宽带费', '1', '', '17', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_expense_type` VALUES ('18', '1', '3', '1', '1', '通讯费', '1', '', '18', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_expense_type` VALUES ('19', '1', '4', '1', '1', '置装费', '1', '', '19', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_expense_type` VALUES ('20', '1', '4', '1', '1', '化妆品费', '1', '', '20', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_expense_type` VALUES ('21', '1', '4', '1', '1', '美容美发', '1', '', '21', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_expense_type` VALUES ('22', '1', '5', '1', '1', '医疗费', '1', '', '22', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_expense_type` VALUES ('23', '1', '5', '1', '1', '日用品', '1', '', '23', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_expense_type` VALUES ('24', '1', '5', '1', '1', '保费', '1', '', '24', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_expense_type` VALUES ('25', '1', '5', '1', '1', '零用', '1', '', '25', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_expense_type` VALUES ('26', '1', '5', '1', '1', '购物', '1', '', '26', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_expense_type` VALUES ('27', '1', '6', '1', '1', '抚养费', '1', '', '27', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_expense_type` VALUES ('28', '1', '6', '1', '1', '赡养费', '1', '', '28', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_expense_type` VALUES ('29', '1', '7', '1', '1', '宠物开销', '1', '', '29', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_expense_type` VALUES ('30', '1', '6', '1', '1', '教育开销', '1', '', '30', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_expense_type` VALUES ('31', '1', '7', '1', '1', '健身运动', '1', '', '31', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_expense_type` VALUES ('32', '1', '7', '1', '1', '休闲玩乐', '1', '', '32', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_expense_type` VALUES ('33', '1', '8', '1', '1', '年节费', '1', '', '33', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_expense_type` VALUES ('34', '2', null, '1', '1', '房贷', '1', '', '1', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_expense_type` VALUES ('35', '2', null, '1', '1', '车贷', '1', '', '2', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_expense_type` VALUES ('36', '2', null, '1', '1', '消费贷', '1', '', '3', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_expense_type` VALUES ('37', '2', null, '1', '1', '外债', '1', '', '4', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_expense_type` VALUES ('38', '3', null, '1', '1', '旅游', '1', '', '1', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_expense_type` VALUES ('39', '3', null, '1', '1', '弹性', '1', '', '2', 'default', '2017-01-01 00:00:00');

-- ----------------------------
-- Table structure for clt_expense_type_his
-- ----------------------------
DROP TABLE IF EXISTS `clt_expense_type_his`;
CREATE TABLE `clt_expense_type_his` (
  `his_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '历史主键',
  `id` int(11) NOT NULL DEFAULT '0',
  `super_id` tinyint(4) DEFAULT NULL COMMENT '一级分类ID',
  `second_id` tinyint(4) DEFAULT NULL COMMENT '二级分类',
  `type` tinyint(4) DEFAULT '1' COMMENT '系统定义1或自定义2',
  `cycle_id` tinyint(4) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `description_tip` varchar(4096) DEFAULT NULL,
  `display_order` tinyint(4) DEFAULT NULL,
  `created_by` varchar(50) NOT NULL DEFAULT 'default' COMMENT '创建者ID',
  `created_at` timestamp NOT NULL DEFAULT '2017-01-01 00:00:00' COMMENT '创建时间',
  `action` varchar(50) DEFAULT NULL COMMENT '操作类型',
  `his_created_by` int(11) DEFAULT NULL COMMENT '历史记录创建者',
  `his_created_at` timestamp NULL DEFAULT NULL COMMENT '历史记录创建时间',
  PRIMARY KEY (`his_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='支出类型历史表';

-- ----------------------------
-- Table structure for clt_expense_type_second
-- ----------------------------
DROP TABLE IF EXISTS `clt_expense_type_second`;
CREATE TABLE `clt_expense_type_second` (
  `id` tinyint(4) NOT NULL COMMENT '主键',
  `super_id` tinyint(4) DEFAULT NULL COMMENT '一级分类ID',
  `description` varchar(255) NOT NULL COMMENT '名称',
  `display_order` tinyint(4) DEFAULT NULL COMMENT '排序',
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '删除flg',
  PRIMARY KEY (`id`),
  KEY `fk_super_second_id` (`super_id`),
  CONSTRAINT `fk_super_second_id` FOREIGN KEY (`super_id`) REFERENCES `clt_expense_type_super` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='支出类型二级分类表';

-- ----------------------------
-- Records of clt_expense_type_second
-- ----------------------------
INSERT INTO `clt_expense_type_second` VALUES ('1', '1', '食', '1', '1');
INSERT INTO `clt_expense_type_second` VALUES ('2', '1', '行', '2', '1');
INSERT INTO `clt_expense_type_second` VALUES ('3', '1', '住', '3', '1');
INSERT INTO `clt_expense_type_second` VALUES ('4', '1', '衣', '4', '1');
INSERT INTO `clt_expense_type_second` VALUES ('5', '1', '用', '5', '1');
INSERT INTO `clt_expense_type_second` VALUES ('6', '1', '养', '6', '1');
INSERT INTO `clt_expense_type_second` VALUES ('7', '1', '娱', '7', '1');
INSERT INTO `clt_expense_type_second` VALUES ('8', '1', '礼', '8', '1');

-- ----------------------------
-- Table structure for clt_expense_type_super
-- ----------------------------
DROP TABLE IF EXISTS `clt_expense_type_super`;
CREATE TABLE `clt_expense_type_super` (
  `id` tinyint(4) NOT NULL COMMENT '主键',
  `description` varchar(255) NOT NULL COMMENT '名称',
  `display_order` tinyint(4) DEFAULT NULL COMMENT '排序',
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '删除flg',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='支出类型一级分类表';

-- ----------------------------
-- Records of clt_expense_type_super
-- ----------------------------
INSERT INTO `clt_expense_type_super` VALUES ('1', '固定支出', '1', '1');
INSERT INTO `clt_expense_type_super` VALUES ('2', '还贷支出', '2', '1');
INSERT INTO `clt_expense_type_super` VALUES ('3', '自由支出', '3', '1');

-- ----------------------------
-- Table structure for clt_health
-- ----------------------------
DROP TABLE IF EXISTS `clt_health`;
CREATE TABLE `clt_health` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL COMMENT '客户ID',
  `member_id` int(11) DEFAULT NULL COMMENT '成员ID',
  `health_info` text COMMENT '健康状况',
  `display_color` varchar(100) DEFAULT NULL COMMENT '颜色标签',
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_health_member` (`member_id`) USING BTREE,
  KEY `fk_health_client` (`client_id`) USING BTREE,
  CONSTRAINT `clt_health_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `clt_client` (`id`),
  CONSTRAINT `clt_health_ibfk_2` FOREIGN KEY (`member_id`) REFERENCES `clt_member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户健康信息表';

-- ----------------------------
-- Table structure for clt_health_his
-- ----------------------------
DROP TABLE IF EXISTS `clt_health_his`;
CREATE TABLE `clt_health_his` (
  `his_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '历史主键',
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL COMMENT '客户ID',
  `member_id` int(11) DEFAULT NULL COMMENT '成员ID',
  `health_info` text COMMENT '健康状况',
  `display_color` varchar(100) DEFAULT NULL COMMENT '颜色标签',
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `action` varchar(50) DEFAULT NULL COMMENT '操作类型',
  `his_created_by` int(11) DEFAULT NULL COMMENT '历史记录创建者',
  `his_created_at` timestamp NULL DEFAULT NULL COMMENT '历史记录创建时间',
  PRIMARY KEY (`his_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户健康信息历史表';

-- ----------------------------
-- Table structure for clt_income
-- ----------------------------
DROP TABLE IF EXISTS `clt_income`;
CREATE TABLE `clt_income` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `member_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `cycle_id` tinyint(4) NOT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `comments` text COMMENT '备注',
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_income_member` (`member_id`) USING BTREE,
  KEY `fk_income__income_type` (`type_id`) USING BTREE,
  KEY `fk_income_client` (`client_id`) USING BTREE,
  CONSTRAINT `clt_income_ibfk_2` FOREIGN KEY (`client_id`) REFERENCES `clt_client` (`id`),
  CONSTRAINT `clt_income_ibfk_3` FOREIGN KEY (`member_id`) REFERENCES `clt_member` (`id`),
  CONSTRAINT `clt_income_type_id` FOREIGN KEY (`type_id`) REFERENCES `clt_income_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户收入表';

-- ----------------------------
-- Table structure for clt_income_his
-- ----------------------------
DROP TABLE IF EXISTS `clt_income_his`;
CREATE TABLE `clt_income_his` (
  `his_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '历史主键',
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `member_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `cycle_id` tinyint(4) NOT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `comments` text COMMENT '备注',
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `action` varchar(50) DEFAULT NULL COMMENT '操作类型',
  `his_created_by` int(11) DEFAULT NULL COMMENT '历史记录创建者',
  `his_created_at` timestamp NULL DEFAULT NULL COMMENT '历史记录创建时间',
  PRIMARY KEY (`his_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户收入历史表';

-- ----------------------------
-- Table structure for clt_income_type
-- ----------------------------
DROP TABLE IF EXISTS `clt_income_type`;
CREATE TABLE `clt_income_type` (
  `id` int(11) NOT NULL DEFAULT '0',
  `super_id` tinyint(4) DEFAULT NULL COMMENT '一级分类ID',
  `second_id` tinyint(4) DEFAULT '1' COMMENT '二级分类',
  `type` tinyint(4) DEFAULT '1' COMMENT '系统定义1或自定义2',
  `cycle_id` tinyint(4) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `display_order` tinyint(4) DEFAULT NULL COMMENT '一级分类ID',
  `created_by` varchar(50) NOT NULL DEFAULT 'default' COMMENT '创建者ID',
  `created_at` timestamp NOT NULL DEFAULT '2017-01-01 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `fk_income_super_id` (`super_id`),
  KEY `fk_second_id_income` (`second_id`),
  CONSTRAINT `fk_income_super_id` FOREIGN KEY (`super_id`) REFERENCES `clt_income_type_super` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_second_id_income` FOREIGN KEY (`second_id`) REFERENCES `clt_income_type_second` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户收入类型表';

-- ----------------------------
-- Records of clt_income_type
-- ----------------------------
INSERT INTO `clt_income_type` VALUES ('1', '1', null, '1', '1', '工资', '1', '1', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_income_type` VALUES ('2', '1', null, '1', '1', '房贴', '1', '2', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_income_type` VALUES ('3', '3', null, '1', '2', '分红', '1', '2', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_income_type` VALUES ('4', '1', null, '1', '1', '车贴', '1', '3', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_income_type` VALUES ('5', '2', null, '1', '2', '奖金', '1', '2', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_income_type` VALUES ('6', '1', null, '1', '1', '饭贴', '1', '4', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_income_type` VALUES ('7', '2', null, '1', '1', '绩效奖金', '1', '1', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_income_type` VALUES ('8', '2', null, '1', '1', '运动补贴', '1', '3', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_income_type` VALUES ('9', '3', null, '1', '1', '额外收入', '1', '8', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_income_type` VALUES ('11', '3', null, '1', '1', '投资所得', '1', '3', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_income_type` VALUES ('12', '3', null, '1', '1', '租金收入', '1', '1', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_income_type` VALUES ('13', '3', null, '1', '1', '公积金提现', '1', '4', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_income_type` VALUES ('14', '3', null, '1', '1', '赠予', '1', '5', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_income_type` VALUES ('15', '3', null, '1', '1', '理赔', '1', '6', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_income_type` VALUES ('16', '3', null, '1', '1', '中奖', '1', '7', 'default', '2017-01-01 00:00:00');

-- ----------------------------
-- Table structure for clt_income_type_his
-- ----------------------------
DROP TABLE IF EXISTS `clt_income_type_his`;
CREATE TABLE `clt_income_type_his` (
  `his_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '历史主键',
  `id` int(11) NOT NULL,
  `super_id` tinyint(4) DEFAULT NULL COMMENT '一级分类ID',
  `second_id` tinyint(4) DEFAULT '1' COMMENT '二级分类',
  `type` tinyint(4) DEFAULT '1' COMMENT '系统定义1或自定义2',
  `cycle_id` tinyint(4) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `display_order` tinyint(4) DEFAULT NULL COMMENT '一级分类ID',
  `created_by` varchar(50) NOT NULL DEFAULT 'default' COMMENT '创建者ID',
  `created_at` timestamp NOT NULL DEFAULT '2017-01-01 00:00:00' COMMENT '创建时间',
  `action` varchar(50) DEFAULT NULL COMMENT '操作类型',
  `his_created_by` int(11) DEFAULT NULL COMMENT '历史记录创建者',
  `his_created_at` timestamp NULL DEFAULT NULL COMMENT '历史记录创建时间',
  PRIMARY KEY (`his_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户收入类型表';

-- ----------------------------
-- Table structure for clt_income_type_second
-- ----------------------------
DROP TABLE IF EXISTS `clt_income_type_second`;
CREATE TABLE `clt_income_type_second` (
  `id` tinyint(4) NOT NULL COMMENT '主键',
  `super_id` tinyint(4) DEFAULT NULL COMMENT '一级分类ID',
  `description` varchar(255) NOT NULL COMMENT '名称',
  `display_order` tinyint(4) DEFAULT NULL COMMENT '排序',
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '删除flg',
  PRIMARY KEY (`id`),
  KEY `fk_super_second_id` (`super_id`) USING BTREE,
  CONSTRAINT `clt_income_type_second_ibfk_1` FOREIGN KEY (`super_id`) REFERENCES `clt_income_type_super` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='收入类型二级分类表';

-- ----------------------------
-- Records of clt_income_type_second
-- ----------------------------
INSERT INTO `clt_income_type_second` VALUES ('1', '1', '工资收入', '1', '1');
INSERT INTO `clt_income_type_second` VALUES ('2', '1', '奖金收入', '2', '1');
INSERT INTO `clt_income_type_second` VALUES ('3', '1', '其他收入', '3', '1');
INSERT INTO `clt_income_type_second` VALUES ('4', '2', '投资收入', '1', '1');
INSERT INTO `clt_income_type_second` VALUES ('5', '2', '租金收入', '2', '1');
INSERT INTO `clt_income_type_second` VALUES ('6', '2', '分红收入', '3', '1');

-- ----------------------------
-- Table structure for clt_income_type_super
-- ----------------------------
DROP TABLE IF EXISTS `clt_income_type_super`;
CREATE TABLE `clt_income_type_super` (
  `id` tinyint(4) NOT NULL COMMENT '主键',
  `description` varchar(255) NOT NULL COMMENT '名称',
  `display_order` tinyint(4) DEFAULT NULL COMMENT '排序',
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '删除flg',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='收入类型一级分类表';

-- ----------------------------
-- Records of clt_income_type_super
-- ----------------------------
INSERT INTO `clt_income_type_super` VALUES ('1', '全年工资', '1', '1');
INSERT INTO `clt_income_type_super` VALUES ('2', '全年奖金', '2', '1');
INSERT INTO `clt_income_type_super` VALUES ('3', '其他收入', '3', '1');

-- ----------------------------
-- Table structure for clt_info_agg
-- ----------------------------
DROP TABLE IF EXISTS `clt_info_agg`;
CREATE TABLE `clt_info_agg` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `client_id` int(11) NOT NULL COMMENT '客户ID',
  `child_rear_exp` double(20,0) DEFAULT '0' COMMENT '抚养孩子费用',
  `parents_supp_exp` double(20,0) DEFAULT '0' COMMENT '赡养父母费用',
  `month_exp` double(20,0) DEFAULT '0' COMMENT '月度总支出',
  `year_exp` double(20,0) DEFAULT '0' COMMENT '年度总支出',
  `total_exp` double(20,0) DEFAULT '0' COMMENT '总支出',
  `fixed_exp` double(20,0) DEFAULT '0' COMMENT '固定支出',
  `repay_loan_exp` double(20,0) DEFAULT '0' COMMENT '还贷支出',
  `free_exp` double(20,0) DEFAULT '0' COMMENT '自由支出',
  `raise_exp` double(20,0) DEFAULT '0' COMMENT '【养】支出',
  `month_inc` double(20,0) DEFAULT '0' COMMENT '月度总收入',
  `year_inc` double(20,0) DEFAULT '0' COMMENT '年度总收入',
  `total_inc` double(20,0) DEFAULT '0' COMMENT '总收入',
  `total_member_inc` double(20,0) DEFAULT '0' COMMENT '成员总收入',
  `salary_year` double(20,0) DEFAULT '0' COMMENT '全年工资',
  `bonus_year` double(20,0) DEFAULT '0' COMMENT '全年奖金',
  `other_inc` double(20,0) DEFAULT '0' COMMENT '其他收入',
  `month_surplus` double(20,0) DEFAULT '0' COMMENT '月度结余',
  `year_surplus` double(20,0) DEFAULT '0' COMMENT '年度结余',
  `float_asset` double(20,0) DEFAULT '0' COMMENT '流动资产',
  `float_asset_remove` double(20,0) DEFAULT '0' COMMENT '流动资产(去除短期消耗性支出)',
  `fixed_asset` double(20,0) DEFAULT '0' COMMENT '固定资产',
  `fixed_asset_realize` double(20,0) DEFAULT '0' COMMENT '固定资产（去除自住房产，加上投资性支出）',
  `industrial_asset` double(20,0) DEFAULT '0' COMMENT '实业资产',
  `debt` double(20,0) DEFAULT '0' COMMENT '负债',
  `net_assets` double(20,0) DEFAULT '0' COMMENT '净资产',
  `net_assets_realize` double(20,0) DEFAULT '0' COMMENT '净资产（去除自住房产）',
  `consume_recent_exp` double(20,0) DEFAULT '0' COMMENT '短期消耗性支出',
  `invest_recent_exp` double(20,0) DEFAULT '0' COMMENT '短期投资性支出',
  `total_recent_exp` double(20,0) DEFAULT '0' COMMENT '短期总支出',
  `emergency_reserve` double(20,0) DEFAULT '0' COMMENT '紧急准备金',
  `member_inc_ratio` varchar(10) DEFAULT NULL COMMENT '成员收入占比',
  PRIMARY KEY (`id`),
  KEY `fk_agg_client` (`client_id`) USING BTREE,
  CONSTRAINT `clt_info_agg_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `clt_client` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户数据总计表';

-- ----------------------------
-- Table structure for clt_info_agg_member
-- ----------------------------
DROP TABLE IF EXISTS `clt_info_agg_member`;
CREATE TABLE `clt_info_agg_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `client_id` int(11) NOT NULL COMMENT '客户ID',
  `member_id` int(11) DEFAULT NULL COMMENT '成员ID',
  `month_exp` double(20,0) DEFAULT '0' COMMENT '月度总支出',
  `year_exp` double(20,0) DEFAULT '0' COMMENT '年度总支出',
  `total_exp` double(20,0) DEFAULT '0' COMMENT '总支出',
  `month_inc` double(20,0) DEFAULT '0' COMMENT '月度总收入',
  `year_inc` double(20,0) DEFAULT '0' COMMENT '年度总收入',
  `total_inc` double(20,0) DEFAULT '0' COMMENT '总收入',
  `float_asset` double(20,0) DEFAULT '0' COMMENT '流动资产',
  `fixed_asset` double(20,0) DEFAULT '0' COMMENT '固定资产',
  `industrial_asset` double(20,0) DEFAULT '0' COMMENT '实业资产',
  `debt` double(20,0) DEFAULT '0' COMMENT '负债',
  `net_assets` double(20,0) DEFAULT '0' COMMENT '净资产',
  `total_recent_exp` double(20,0) DEFAULT '0' COMMENT '短期总支出',
  `income_ratio_per` varchar(10) DEFAULT NULL COMMENT '个人收入占比',
  `life_coverage_have` double(20,0) DEFAULT '0' COMMENT '人寿险现有保额',
  `acc_coverage_have` double(20,0) DEFAULT '0' COMMENT '意外险现有保额',
  `sev_coverage_have` double(20,0) DEFAULT '0' COMMENT '重疾险现有保额',
  PRIMARY KEY (`id`),
  KEY `fk_agg_client` (`client_id`) USING BTREE,
  CONSTRAINT `clt_info_agg_member_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `clt_client` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='成员数据总计表';

-- ----------------------------
-- Table structure for clt_insurance
-- ----------------------------
DROP TABLE IF EXISTS `clt_insurance`;
CREATE TABLE `clt_insurance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `type_id` tinyint(4) NOT NULL,
  `base_amount` double(9,2) NOT NULL DEFAULT '0.00' COMMENT '基数',
  `base_amount_accum` double(9,2) NOT NULL DEFAULT '0.00' COMMENT '公积金基数',
  `custom_ratio` double(6,5) DEFAULT '0.00000' COMMENT '个人公积金比例',
  `custom_ratio_comp` double(6,5) DEFAULT '0.00000' COMMENT '公司公积金比例',
  `comments` text COMMENT '备注',
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_pay_per` tinyint(4) NOT NULL DEFAULT '1' COMMENT '个人是否缴纳',
  `is_pay_comp` tinyint(4) NOT NULL DEFAULT '1' COMMENT '公司是否缴纳',
  PRIMARY KEY (`id`),
  KEY `fk_insurance_member` (`member_id`),
  CONSTRAINT `fk_insurance_member` FOREIGN KEY (`member_id`) REFERENCES `clt_member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户现有保障表';


-- ----------------------------
-- Table structure for clt_insurance_his
-- ----------------------------
DROP TABLE IF EXISTS `clt_insurance_his`;
CREATE TABLE `clt_insurance_his` (
  `his_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '历史主键',
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `type_id` tinyint(4) NOT NULL,
  `base_amount` double(9,2) NOT NULL DEFAULT '0.00' COMMENT '基数',
  `base_amount_accum` double(9,2) NOT NULL DEFAULT '0.00' COMMENT '公积金基数',
  `custom_ratio` double(6,5) DEFAULT '0.00000' COMMENT '个人公积金比例',
  `custom_ratio_comp` double(6,5) DEFAULT '0.00000' COMMENT '公司公积金比例',
  `comments` text COMMENT '备注',
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_pay_per` tinyint(4) NOT NULL DEFAULT '1' COMMENT '个人是否缴纳',
  `is_pay_comp` tinyint(4) NOT NULL DEFAULT '1' COMMENT '公司是否缴纳',
  `action` varchar(50) DEFAULT NULL COMMENT '操作类型',
  `his_created_by` int(11) DEFAULT NULL COMMENT '历史记录创建者',
  `his_created_at` timestamp NULL DEFAULT NULL COMMENT '历史记录创建时间',
  PRIMARY KEY (`his_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户现有保障历史表';

-- ----------------------------
-- Table structure for clt_item_comment
-- ----------------------------
DROP TABLE IF EXISTS `clt_item_comment`;
CREATE TABLE `clt_item_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `item_type_id` tinyint(4) NOT NULL,
  `type_id` tinyint(4) NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for clt_member
-- ----------------------------
DROP TABLE IF EXISTS `clt_member`;
CREATE TABLE `clt_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `title_id` tinyint(4) DEFAULT NULL,
  `surname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `forename` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `middlename` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `gender_id` tinyint(4) NOT NULL DEFAULT '1',
  `birthday` date DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `rel_type_id` tinyint(4) DEFAULT NULL,
  `status_id` tinyint(4) DEFAULT NULL,
  `company_type_id` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1：企业，2：机关',
  `city_id` int(11) DEFAULT NULL,
  `live_id` int DEFAULT NULL COMMENT '居住地',
  `update_at` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_client_id` (`client_id`),
  KEY `fk_member__member_rel_type` (`rel_type_id`),
  KEY `fk_member_status` (`status_id`),
  KEY `fk_member_title` (`title_id`),
  CONSTRAINT `clt_member_ibfk_2` FOREIGN KEY (`client_id`) REFERENCES `clt_client` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `clt_member_ibfk_3` FOREIGN KEY (`rel_type_id`) REFERENCES `bsc_rel_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `clt_member_ibfk_4` FOREIGN KEY (`status_id`) REFERENCES `clt_member_status` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `clt_member_ibfk_5` FOREIGN KEY (`title_id`) REFERENCES `bsc_title` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户家庭成员表';

-- ----------------------------
-- Table structure for clt_member_age_change
-- ----------------------------
DROP TABLE IF EXISTS `clt_member_age_change`;
CREATE TABLE `clt_member_age_change` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL COMMENT '客户ID',
  `member_id` int(11) NOT NULL COMMENT '成员ID',
  `status` tinyint(4) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='年龄变更表';

-- ----------------------------
-- Table structure for clt_member_his
-- ----------------------------
DROP TABLE IF EXISTS `clt_member_his`;
CREATE TABLE `clt_member_his` (
  `his_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '历史主键',
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `title_id` tinyint(4) DEFAULT NULL,
  `surname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `forename` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `middlename` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `gender_id` tinyint(4) NOT NULL DEFAULT '1',
  `birthday` date DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `rel_type_id` tinyint(4) DEFAULT NULL,
  `status_id` tinyint(4) DEFAULT NULL,
  `company_type_id` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1：企业，2：机关',
  `city_id` int(11) DEFAULT NULL,
  `live_id` int DEFAULT NULL COMMENT '居住地',
  `update_at` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  `action` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '操作类型',
  `his_created_by` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `his_created_at` timestamp NULL DEFAULT NULL COMMENT '历史记录创建时间',
  PRIMARY KEY (`his_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户家庭成员历史表';

-- ----------------------------
-- Table structure for clt_member_set
-- ----------------------------
DROP TABLE IF EXISTS `clt_member_set`;
CREATE TABLE `clt_member_set` (
  `member_id` int(11) NOT NULL,
  `medical_conclusion` tinyint(4) DEFAULT '2' COMMENT '医疗保障总结',
  `medical_type` varchar(100) DEFAULT '' COMMENT '医疗险种类',
  `policy_pattern` tinyint(4) DEFAULT '1' COMMENT '保单类型',
  `guarantee_pattern` tinyint(4) DEFAULT '2' COMMENT '保单保障类型',
  `medical_hosp_display` tinyint(4) DEFAULT NULL COMMENT '医疗保障产品选择医疗默认显示',
  `medical_ill_display` tinyint(4) DEFAULT NULL COMMENT '医疗保障产品选择重疾默认显示',
  `protection_life_display` tinyint(4) DEFAULT NULL COMMENT '人身保障产品选择r人寿默认显示',
  `protection_acc_display` tinyint(4) DEFAULT NULL COMMENT '人身保障产品选择意外默认显示',
  `is_modify` int DEFAULT NULL COMMENT '是否已修改：1：修改',
  PRIMARY KEY (`member_id`),
  CONSTRAINT `clt_member_set_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `clt_member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户家庭设置表';

-- ----------------------------
-- Table structure for clt_member_set_his
-- ----------------------------
DROP TABLE IF EXISTS `clt_member_set_his`;
CREATE TABLE `clt_member_set_his` (
  `his_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '历史主键',
  `member_id` int(11) DEFAULT NULL,
  `medical_conclusion` tinyint(4) DEFAULT '2' COMMENT '医疗保障总结',
  `medical_type` varchar(100) DEFAULT '' COMMENT '医疗险种类',
  `policy_pattern` tinyint(4) DEFAULT '1' COMMENT '保单类型',
  `guarantee_pattern` tinyint(4) DEFAULT '2' COMMENT '保单保障类型',
  `medical_hosp_display` tinyint(4) DEFAULT NULL COMMENT '医疗保障产品选择医疗默认显示',
  `medical_ill_display` tinyint(4) DEFAULT NULL COMMENT '医疗保障产品选择重疾默认显示',
  `protection_life_display` tinyint(4) DEFAULT NULL COMMENT '人身保障产品选择r人寿默认显示',
  `protection_acc_display` tinyint(4) DEFAULT NULL COMMENT '人身保障产品选择意外默认显示',
  `is_modify` int DEFAULT NULL COMMENT '是否已修改：1：修改',
  `action` varchar(50) DEFAULT NULL COMMENT '操作类型',
  `his_created_by` int(11) DEFAULT NULL COMMENT '历史记录创建者',
  `his_created_at` timestamp NULL DEFAULT NULL COMMENT '历史记录创建时间',
  PRIMARY KEY (`his_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户家庭设置历史表';

-- ----------------------------
-- Table structure for clt_member_status
-- ----------------------------
DROP TABLE IF EXISTS `clt_member_status`;
CREATE TABLE `clt_member_status` (
  `id` tinyint(4) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户家庭成员状态表';

-- ----------------------------
-- Records of clt_member_status
-- ----------------------------
INSERT INTO `clt_member_status` VALUES ('1', '删除');
INSERT INTO `clt_member_status` VALUES ('10', '活动');

-- ----------------------------
-- Table structure for clt_protection_set
-- ----------------------------
DROP TABLE IF EXISTS `clt_protection_set`;
CREATE TABLE `clt_protection_set` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `client_id` int(11) NOT NULL COMMENT '客户ID',
  `type_id` tinyint(1) NOT NULL DEFAULT '1' COMMENT '默认值类型：  1：父母，2：孩子',
  `amount` int(11) NOT NULL DEFAULT '0' COMMENT '每年费用',
  `year` tinyint(4) NOT NULL DEFAULT '0' COMMENT '年数',
  `higher_amount` int(11) DEFAULT '0' COMMENT '高等费用',
  `modify_flg` tinyint(1) DEFAULT NULL COMMENT '修改flg',
  `years_tip` varchar(100) DEFAULT NULL COMMENT '提示年数',
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_expense_client` (`client_id`) USING BTREE,
  CONSTRAINT `clt_protection_set_ibfk_2` FOREIGN KEY (`client_id`) REFERENCES `clt_client` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='人身保障设定表';

-- ----------------------------
-- Table structure for clt_protection_set_his
-- ----------------------------
DROP TABLE IF EXISTS `clt_protection_set_his`;
CREATE TABLE `clt_protection_set_his` (
  `his_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '历史主键',
  `id` int(11) NOT NULL COMMENT '主键',
  `client_id` int(11) NOT NULL COMMENT '客户ID',
  `type_id` tinyint(1) NOT NULL DEFAULT '1' COMMENT '默认值类型：  1：父母，2：孩子',
  `amount` int(11) NOT NULL DEFAULT '0' COMMENT '每年费用',
  `year` tinyint(4) NOT NULL DEFAULT '0' COMMENT '年数',
  `higher_amount` int(11) DEFAULT '0' COMMENT '高等费用',
  `modify_flg` tinyint(1) DEFAULT NULL COMMENT '修改flg',
  `years_tip` varchar(100) DEFAULT NULL COMMENT '提示年数',
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `action` varchar(50) DEFAULT NULL COMMENT '操作类型',
  `his_created_by` varchar(100) DEFAULT NULL,
  `his_created_at` timestamp NULL DEFAULT NULL COMMENT '历史记录创建时间',
  PRIMARY KEY (`his_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='人身保障设定表';

-- ----------------------------
-- Table structure for clt_recent_expense
-- ----------------------------
DROP TABLE IF EXISTS `clt_recent_expense`;
CREATE TABLE `clt_recent_expense` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `member_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `comments` text COMMENT '备注',
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_expense_member` (`member_id`) USING BTREE,
  KEY `fk_expense__expense_type` (`type_id`) USING BTREE,
  KEY `fk_expense_client` (`client_id`) USING BTREE,
  CONSTRAINT `clt_recent_expense_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `clt_client` (`id`),
  CONSTRAINT `clt_recent_expense_type_id` FOREIGN KEY (`type_id`) REFERENCES `clt_recent_expense_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户近期支出表';

-- ----------------------------
-- Table structure for clt_recent_expense_his
-- ----------------------------
DROP TABLE IF EXISTS `clt_recent_expense_his`;
CREATE TABLE `clt_recent_expense_his` (
  `his_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '历史主键',
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `member_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `comments` text COMMENT '备注',
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `action` varchar(50) DEFAULT NULL COMMENT '操作类型',
  `his_created_by` int(11) DEFAULT NULL COMMENT '历史记录创建者',
  `his_created_at` timestamp NULL DEFAULT NULL COMMENT '历史记录创建时间',
  PRIMARY KEY (`his_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户近期支出历史表';

-- ----------------------------
-- Table structure for clt_recent_expense_type
-- ----------------------------
DROP TABLE IF EXISTS `clt_recent_expense_type`;
CREATE TABLE `clt_recent_expense_type` (
  `id` int(11) NOT NULL DEFAULT '0',
  `super_id` tinyint(4) DEFAULT NULL COMMENT '一级分类ID',
  `type` tinyint(4) DEFAULT '1' COMMENT '系统定义1或自定义2',
  `description` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `display_order` tinyint(4) DEFAULT NULL COMMENT '一级分类ID',
  `created_by` varchar(50) NOT NULL DEFAULT 'default' COMMENT '创建者ID',
  `created_at` timestamp NOT NULL DEFAULT '2017-01-01 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `fk_recent_super_id` (`super_id`),
  CONSTRAINT `fk_recent_super_id` FOREIGN KEY (`super_id`) REFERENCES `clt_recent_expense_type_super` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='近期支出类型表';

-- ----------------------------
-- Records of clt_recent_expense_type
-- ----------------------------
INSERT INTO `clt_recent_expense_type` VALUES ('1', '1', '1', '购房/换房（自住）', '1', '1', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_recent_expense_type` VALUES ('2', '2', '1', '购房(投资)', '1', '1', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_recent_expense_type` VALUES ('3', '2', '1', '购车/换车', '1', '2', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_recent_expense_type` VALUES ('5', '1', '1', '教育/留学/深造', '1', '3', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_recent_expense_type` VALUES ('8', '1', '1', '自住房产装修', '1', '2', 'default', '2017-01-01 00:00:00');
INSERT INTO `clt_recent_expense_type` VALUES ('11', '1', '1', '偿还债务', '1', '4', 'default', '2017-01-01 00:00:00');

-- ----------------------------
-- Table structure for clt_recent_expense_type_his
-- ----------------------------
DROP TABLE IF EXISTS `clt_recent_expense_type_his`;
CREATE TABLE `clt_recent_expense_type_his` (
  `his_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '历史主键',
  `id` int(11) NOT NULL,
  `super_id` tinyint(4) DEFAULT NULL COMMENT '一级分类ID',
  `type` tinyint(4) DEFAULT '1' COMMENT '系统定义1或自定义2',
  `description` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `display_order` tinyint(4) DEFAULT NULL COMMENT '一级分类ID',
  `created_by` varchar(50) NOT NULL DEFAULT 'default' COMMENT '创建者ID',
  `created_at` timestamp NOT NULL DEFAULT '2017-01-01 00:00:00' COMMENT '创建时间',
  `action` varchar(50) DEFAULT NULL COMMENT '操作类型',
  `his_created_by` int(11) DEFAULT NULL COMMENT '历史记录创建者',
  `his_created_at` timestamp NULL DEFAULT NULL COMMENT '历史记录创建时间',
  PRIMARY KEY (`his_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='近期支出类型历史表';

-- ----------------------------
-- Table structure for clt_recent_expense_type_super
-- ----------------------------
DROP TABLE IF EXISTS `clt_recent_expense_type_super`;
CREATE TABLE `clt_recent_expense_type_super` (
  `id` tinyint(4) NOT NULL COMMENT '主键',
  `description` varchar(255) NOT NULL COMMENT '名称',
  `display_order` tinyint(4) DEFAULT NULL COMMENT '排序',
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '删除flg',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='短期支出类型一级分类表';

-- ----------------------------
-- Records of clt_recent_expense_type_super
-- ----------------------------
INSERT INTO `clt_recent_expense_type_super` VALUES ('1', '消耗支出', '1', '1');
INSERT INTO `clt_recent_expense_type_super` VALUES ('2', '投资支出', '2', '1');

-- ----------------------------
-- Table structure for clt_rtr_expense
-- ----------------------------
DROP TABLE IF EXISTS `clt_rtr_expense`;
CREATE TABLE `clt_rtr_expense` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `type_id` tinyint(4) NOT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_actvie` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_rtr_expense__client` (`client_id`),
  KEY `fk_rtr_expense__rtr_expense_type` (`type_id`),
  CONSTRAINT `fk_rtr_expense__client` FOREIGN KEY (`client_id`) REFERENCES `clt_client` (`id`),
  CONSTRAINT `fk_rtr_expense__rtr_expense_type` FOREIGN KEY (`type_id`) REFERENCES `clt_rtr_expense_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户退休后费用表';

-- ----------------------------
-- Records of clt_rtr_expense
-- ----------------------------

-- ----------------------------
-- Table structure for clt_rtr_expense_type
-- ----------------------------
DROP TABLE IF EXISTS `clt_rtr_expense_type`;
CREATE TABLE `clt_rtr_expense_type` (
  `id` tinyint(4) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户退休后费用类型表';

-- ----------------------------
-- Records of clt_rtr_expense_type
-- ----------------------------

-- ----------------------------
-- Table structure for clt_status
-- ----------------------------
DROP TABLE IF EXISTS `clt_status`;
CREATE TABLE `clt_status` (
  `id` tinyint(4) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户状态表';

-- ----------------------------
-- Records of clt_status
-- ----------------------------
INSERT INTO `clt_status` VALUES ('1', '删除');
INSERT INTO `clt_status` VALUES ('10', '资料收集');
INSERT INTO `clt_status` VALUES ('11', '分析规划');
INSERT INTO `clt_status` VALUES ('12', '销售完成');

-- ----------------------------
-- Table structure for ins_ac_cash_rtn_condition
-- ----------------------------
DROP TABLE IF EXISTS `ins_ac_cash_rtn_condition`;
CREATE TABLE `ins_ac_cash_rtn_condition` (
  `id` tinyint(4) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='满期现金返还条件';

-- ----------------------------
-- Records of ins_ac_cash_rtn_condition
-- ----------------------------
INSERT INTO `ins_ac_cash_rtn_condition` VALUES ('1', '缴费满期');
INSERT INTO `ins_ac_cash_rtn_condition` VALUES ('2', '合同满期');

-- ----------------------------
-- Table structure for ins_ac_cash_rtn_cycle
-- ----------------------------
DROP TABLE IF EXISTS `ins_ac_cash_rtn_cycle`;
CREATE TABLE `ins_ac_cash_rtn_cycle` (
  `id` tinyint(4) NOT NULL,
  `description` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='生存现金返还周期';

-- ----------------------------
-- Records of ins_ac_cash_rtn_cycle
-- ----------------------------
INSERT INTO `ins_ac_cash_rtn_cycle` VALUES ('1', '每月');
INSERT INTO `ins_ac_cash_rtn_cycle` VALUES ('2', '每1年');
INSERT INTO `ins_ac_cash_rtn_cycle` VALUES ('3', '每2年');
INSERT INTO `ins_ac_cash_rtn_cycle` VALUES ('4', '每3年');

-- ----------------------------
-- Table structure for ins_analysis_result
-- ----------------------------
DROP TABLE IF EXISTS `ins_analysis_result`;
CREATE TABLE `ins_analysis_result` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ins_client_id` int(11) NOT NULL COMMENT '客户ID',
  `analysis_result` text COMMENT '分析结果',
  PRIMARY KEY (`id`),
  KEY `fk_ins_client_id` (`ins_client_id`),
  CONSTRAINT `fk_ins_client_id_ibfk_1` FOREIGN KEY (`ins_client_id`) REFERENCES `clt_client` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='个人保险分析结果表';

-- ----------------------------
-- Records of ins_analysis_result
-- ----------------------------

-- ----------------------------
-- Table structure for ins_member_product
-- ----------------------------
DROP TABLE IF EXISTS `ins_member_product`;
CREATE TABLE `ins_member_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `client_id` int(11) NOT NULL COMMENT '客户ID',
  `member_id` int(11) NOT NULL COMMENT '成员ID',
  `type_id` tinyint(1) DEFAULT NULL COMMENT '保险类型：0：商业保险，1；团体保险',
  `group_id` int(11) DEFAULT NULL COMMENT '团体保险ID',
  `pro_name` varchar(200) DEFAULT NULL COMMENT '产品名称',
  `company` varchar(200) DEFAULT NULL COMMENT '公司',
  `pro_type_id` tinyint(1) DEFAULT NULL COMMENT '险种类型ID',
  `pro_type` varchar(255) DEFAULT NULL COMMENT '险种',
  `data_purchase` varchar(8) DEFAULT NULL COMMENT '购入日期',
  `term_type_id` tinyint(1) DEFAULT NULL COMMENT '期限类型ID',
  `time_payment_id` tinyint(4) DEFAULT NULL COMMENT '缴费年限ID',
  `return_type_id` tinyint(1) DEFAULT NULL COMMENT '返还类型ID',
  `compensate_hosp_id` tinyint(4) DEFAULT NULL COMMENT '赔付医院',
  `compensate_ratio` double(5,2) DEFAULT NULL COMMENT '赔付比例',
  `severe_number` int(11) DEFAULT NULL COMMENT '重症数量',
  `mild_number` int(11) DEFAULT NULL COMMENT '轻症数量',
  `deductible_limit` int(11) DEFAULT NULL COMMENT '免赔额度',
  `reim_range` tinyint(4) DEFAULT NULL COMMENT '报销范围',
  `coverage` double(10,2) DEFAULT NULL COMMENT '保额',
  `premium` double(10,2) DEFAULT NULL COMMENT '保费',
  `ins_type` tinyint(1) DEFAULT '1' COMMENT '保险类型',
  `main_product_id` int(11) DEFAULT NULL COMMENT '主险ID',
  `effective_date` date DEFAULT NULL COMMENT '生效日期',
  `is_active` tinyint(4) DEFAULT '1' COMMENT '是否有效',
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户产品表';

-- ----------------------------
-- Table structure for ins_member_product_adjust
-- ----------------------------
DROP TABLE IF EXISTS `ins_member_product_adjust`;
CREATE TABLE `ins_member_product_adjust` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `policy_id` int(11) NOT NULL COMMENT '保单ID',
  `coverage` double(10,2) NOT NULL DEFAULT '0.00' COMMENT '调整后的金额',
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_policy_id` (`policy_id`),
  CONSTRAINT `fk_policy_id` FOREIGN KEY (`policy_id`) REFERENCES `ins_member_product` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='个人保险调整表';

-- ----------------------------
-- Records of ins_member_product_adjust
-- ----------------------------

-- ----------------------------
-- Table structure for ins_member_product_def
-- ----------------------------
DROP TABLE IF EXISTS `ins_member_product_def`;
CREATE TABLE `ins_member_product_def` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `client_id` int(11) NOT NULL COMMENT '客户ID',
  `member_id` int(11) NOT NULL COMMENT '成员ID',
  `category_type` tinyint(4) NOT NULL COMMENT '险种类型',
  `category` tinyint(4) NOT NULL COMMENT '险种',
  `category_type_where` int(11) DEFAULT NULL COMMENT '产品类型条件',
  `category_where` int(11) DEFAULT NULL COMMENT '保障利益条件',
  `pro_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '产品种类，1：主险，2：附加险',
  `is_composite` tinyint(1) DEFAULT NULL COMMENT '是否组合，1：是，0不',
  `bind_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '绑定类型：1：必须绑定，2：推荐绑定，3：非必须绑定',
  `guarantee_pattern` tinyint(4) DEFAULT '1' COMMENT '保单保障类型',
  `product_id` int(11) NOT NULL DEFAULT '0' COMMENT '产品ID',
  `ratio_id` int(11) NOT NULL COMMENT '自选产品费率id',
  `ratio` decimal(15,5) NOT NULL DEFAULT '0.00000' COMMENT '费率',
  `amount_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '保额类型：1、固定；2、不固定',
  `amount_min` int(11) DEFAULT NULL COMMENT '保额下限值',
  `amount` int(11) DEFAULT NULL COMMENT '保额',
  `parent_id` int(11) DEFAULT NULL COMMENT '主产品ID',
  `sub_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '子产品ID',
  `pro_type_same` tinyint(4) DEFAULT NULL COMMENT '附加险和主险的类型是否相同，1：相同，2：不想同',
  `company_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '公司名称',
  `pro_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '产品名称',
  `sch_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '计划名称',
  `has_social_security` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '社保',
  `career_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '职业类型',
  `guarantee_period` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '保障期限',
  `payment_period` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '缴费年限',
  `created_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `updated_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户推荐产品表';

-- ----------------------------
-- Table structure for ins_member_product_his
-- ----------------------------
DROP TABLE IF EXISTS `ins_member_product_his`;
CREATE TABLE `ins_member_product_his` (
  `his_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '历史主键',
  `id` int(11) NOT NULL COMMENT '自增主键',
  `client_id` int(11) NOT NULL COMMENT '客户ID',
  `member_id` int(11) NOT NULL COMMENT '成员ID',
  `type_id` tinyint(1) DEFAULT NULL COMMENT '保险类型：0：商业保险，1；团体保险',
  `group_id` int(11) DEFAULT NULL COMMENT '团体保险ID',
  `pro_name` varchar(200) DEFAULT NULL COMMENT '产品名称',
  `company` varchar(200) DEFAULT NULL COMMENT '公司',
  `pro_type_id` tinyint(1) DEFAULT NULL COMMENT '险种类型ID',
  `pro_type` varchar(255) DEFAULT NULL COMMENT '险种',
  `data_purchase` varchar(8) DEFAULT NULL COMMENT '购入日期',
  `term_type_id` tinyint(1) DEFAULT NULL COMMENT '期限类型ID',
  `time_payment_id` tinyint(4) DEFAULT NULL COMMENT '缴费年限',
  `return_type_id` tinyint(1) DEFAULT NULL COMMENT '返还类型ID',
  `compensate_hosp_id` tinyint(4) DEFAULT NULL COMMENT '赔付医院',
  `compensate_ratio` double(5,2) DEFAULT NULL COMMENT '赔付比例',
  `severe_number` int(11) DEFAULT NULL COMMENT '重症数量',
  `mild_number` int(11) DEFAULT NULL COMMENT '轻症数量',
  `deductible_limit` int(11) DEFAULT NULL COMMENT '免赔额度',
  `reim_range` tinyint(4) DEFAULT NULL COMMENT '报销范围',
  `coverage` double(10,2) DEFAULT NULL COMMENT '保额',
  `premium` double(10,2) DEFAULT NULL COMMENT '保费',
  `ins_type` tinyint(1) DEFAULT '1' COMMENT '保险类型',
  `main_product_id` int(11) DEFAULT NULL COMMENT '主险ID',
  `effective_date` date DEFAULT NULL COMMENT '生效日期',
  `is_active` tinyint(4) DEFAULT '1' COMMENT '是否有效',
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `action` varchar(50) DEFAULT NULL COMMENT '操作类型',
  `his_created_by` int(11) DEFAULT NULL COMMENT '历史记录创建者',
  `his_created_at` timestamp NULL DEFAULT NULL COMMENT '历史记录创建时间',
  PRIMARY KEY (`his_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户保险产品历史表';

-- ----------------------------
-- Table structure for ins_member_product_item
-- ----------------------------
DROP TABLE IF EXISTS `ins_member_product_item`;
CREATE TABLE `ins_member_product_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `product_id` int(11) NOT NULL COMMENT '客户ID',
  `type_id` int(11) DEFAULT NULL COMMENT '类型ID',
  `cycle_id` tinyint(4) DEFAULT NULL COMMENT '返还周期',
  `coverage` double(12,2) DEFAULT NULL COMMENT '保额',
  `adjust_coverage` double(12,2) DEFAULT NULL COMMENT '调整保额',
  `adjust_cycle_id` tinyint(4) DEFAULT NULL COMMENT '调整后返还周期',
  `comment` text COMMENT '备注',
  `is_count` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否计算在现有保额内',
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_product_id_type` (`product_id`),
  KEY `fk_type_id_type` (`type_id`),
  CONSTRAINT `fk_product_id_type` FOREIGN KEY (`product_id`) REFERENCES `ins_member_product` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户保单详情表';

-- ----------------------------
-- Table structure for ins_member_product_item_his
-- ----------------------------
DROP TABLE IF EXISTS `ins_member_product_item_his`;
CREATE TABLE `ins_member_product_item_his` (
  `his_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '历史主键',
  `id` int(11) NOT NULL COMMENT '自增主键',
  `product_id` int(11) NOT NULL COMMENT '客户ID',
  `type_id` int(11) DEFAULT NULL COMMENT '类型ID',
  `cycle_id` tinyint(4) DEFAULT NULL COMMENT '返还周期',
  `coverage` double(12,2) DEFAULT NULL COMMENT '保额',
  `adjust_coverage` double(12,2) DEFAULT NULL COMMENT '调整保额',
  `adjust_cycle_id` tinyint(4) DEFAULT NULL COMMENT '调整后返还周期',
  `comment` text COMMENT '备注',
  `is_count` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否计算在现有保额内',
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `action` varchar(50) DEFAULT NULL COMMENT '操作类型',
  `his_created_by` int(11) DEFAULT NULL COMMENT '历史记录创建者',
  `his_created_at` timestamp NULL DEFAULT NULL COMMENT '历史记录创建时间',
  PRIMARY KEY (`his_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户保单详情历史表';

-- ----------------------------
-- Table structure for ins_member_product_sel
-- ----------------------------
DROP TABLE IF EXISTS `ins_member_product_sel`;
CREATE TABLE `ins_member_product_sel` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `client_id` int(11) NOT NULL COMMENT '客户ID',
  `member_id` int(11) NOT NULL COMMENT '成员ID',
  `ratio_id` int(10) NOT NULL COMMENT '自选产品费率id',
  `deductible_id` int(10) DEFAULT NULL COMMENT '免赔额ID',
  `compensation_id` int(10) DEFAULT NULL COMMENT '赔付比例ID',
  `type` enum('1','2') NOT NULL DEFAULT '1' COMMENT '产品种类，1：主险，2：附加险',
  `amount_min` int(10) DEFAULT NULL COMMENT '保额下限值',
  `amount` int(10) DEFAULT NULL COMMENT '保额',
  `parent_id` int(11) DEFAULT NULL COMMENT '主产品ID',
  `sub_id` varchar(255) DEFAULT NULL COMMENT '子产品ID',
  `pro_type_same` enum('1','2') DEFAULT NULL COMMENT '附加险和主险的类型是否相同，1：相同，2：不想同',
  `is_active` enum('1','0') NOT NULL DEFAULT '1' COMMENT '是否有效，1：有效，0：无效',
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户自选产品表';


-- ----------------------------
-- Table structure for ins_member_product_sel_his
-- ----------------------------
DROP TABLE IF EXISTS `ins_member_product_sel_his`;
CREATE TABLE `ins_member_product_sel_his` (
  `his_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '历史主键',
  `id` int(11) NOT NULL COMMENT '自增主键',
  `client_id` int(11) NOT NULL COMMENT '客户ID',
  `member_id` int(11) NOT NULL COMMENT '成员ID',
  `ratio_id` int(10) NOT NULL COMMENT '自选产品费率id',
  `deductible_id` int(10) DEFAULT NULL COMMENT '免赔额ID',
  `compensation_id` int(10) DEFAULT NULL COMMENT '赔付比例ID',
  `type` enum('1','2') NOT NULL DEFAULT '1' COMMENT '产品种类，1：主险，2：附加险',
  `amount_min` int(10) DEFAULT NULL COMMENT '保额下限值',
  `amount` int(10) DEFAULT NULL COMMENT '保额',
  `parent_id` int(11) DEFAULT NULL COMMENT '主产品ID',
  `sub_id` varchar(255) DEFAULT NULL COMMENT '子产品ID',
  `pro_type_same` enum('1','2') DEFAULT NULL COMMENT '附加险和主险的类型是否相同，1：相同，2：不想同',
  `is_active` enum('1','0') NOT NULL DEFAULT '1' COMMENT '是否有效，1：有效，0：无效',
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `action` varchar(50) DEFAULT NULL COMMENT '操作类型',
  `his_created_by` varchar(100) DEFAULT NULL,
  `his_created_at` timestamp NULL DEFAULT NULL COMMENT '历史记录创建时间',
  PRIMARY KEY (`his_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户自选产品历史表';

-- ----------------------------
-- Table structure for ins_member_product_type
-- ----------------------------
DROP TABLE IF EXISTS `ins_member_product_type`;
CREATE TABLE `ins_member_product_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `customer_id` tinyint(4) DEFAULT '1' COMMENT '类型：1：默认，2:自定义',
  `type_id` tinyint(4) DEFAULT '0' COMMENT '险种类型',
  `description` varchar(255) DEFAULT '' COMMENT '描述',
  `is_active` tinyint(4) DEFAULT '1' COMMENT '是否有效',
  `display_order` tinyint(4) DEFAULT NULL COMMENT '排序ID',
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8 COMMENT='客户产品项目类型表';

-- ----------------------------
-- Records of ins_member_product_type
-- ----------------------------
INSERT INTO `ins_member_product_type` VALUES ('1', '1', '0', '身故', '1', '1', null, null, null, null);
INSERT INTO `ins_member_product_type` VALUES ('2', '1', '0', '身故/伤残', '1', '2', null, null, null, null);
INSERT INTO `ins_member_product_type` VALUES ('3', '1', '1', '意外身故', '1', '1', null, null, null, null);
INSERT INTO `ins_member_product_type` VALUES ('4', '1', '1', '意外伤残', '1', '2', null, null, null, null);
INSERT INTO `ins_member_product_type` VALUES ('5', '1', '1', '意外医疗报销', '1', '3', null, null, null, null);
INSERT INTO `ins_member_product_type` VALUES ('6', '1', '1', '意外住院补贴/天', '1', '4', null, null, null, null);
INSERT INTO `ins_member_product_type` VALUES ('7', '1', '1', '生存现金返还', '1', '5', null, null, null, null);
INSERT INTO `ins_member_product_type` VALUES ('8', '1', '1', '满期现金返还', '1', '6', null, null, null, null);
INSERT INTO `ins_member_product_type` VALUES ('9', '1', '2', '年度赔付限额', '1', '1', null, null, null, null);
INSERT INTO `ins_member_product_type` VALUES ('10', '1', '2', '终身赔付限额', '1', '2', null, null, null, null);
INSERT INTO `ins_member_product_type` VALUES ('11', '1', '3', '重症赔付限额', '1', '1', null, null, null, null);
INSERT INTO `ins_member_product_type` VALUES ('12', '1', '3', '轻症赔付限额', '1', '2', null, null, null, null);
INSERT INTO `ins_member_product_type` VALUES ('13', '1', '2', '一次赔付限额', '1', '3', null, null, null, null);
INSERT INTO `ins_member_product_type` VALUES ('14', '2', '2', '住院年度赔付限额', '1', null, '22', null, '2018-02-03 16:53:51', '2018-02-03 16:53:51');
INSERT INTO `ins_member_product_type` VALUES ('15', '2', '2', '住院终身赔付限额', '1', null, '22', null, '2018-02-03 16:53:51', '2018-02-03 16:53:51');
INSERT INTO `ins_member_product_type` VALUES ('16', '2', '2', '住院一次赔付限额', '1', null, '22', null, '2018-02-03 16:53:51', '2018-02-03 16:53:51');
INSERT INTO `ins_member_product_type` VALUES ('17', '2', '2', '住院年度赔付限额', '1', null, '22', null, '2018-02-03 17:00:07', '2018-02-03 17:00:07');
INSERT INTO `ins_member_product_type` VALUES ('18', '2', '2', '住院终身赔付限额', '1', null, '22', null, '2018-02-03 17:00:07', '2018-02-03 17:00:07');
INSERT INTO `ins_member_product_type` VALUES ('19', '2', '2', '住院一次赔付限额', '1', null, '22', null, '2018-02-03 17:00:07', '2018-02-03 17:00:07');
INSERT INTO `ins_member_product_type` VALUES ('20', '2', '2', '住院年度赔付限额', '1', null, '22', null, '2018-03-02 16:20:55', '2018-03-02 16:20:55');
INSERT INTO `ins_member_product_type` VALUES ('21', '2', '2', '住院终身赔付限额', '1', null, '22', null, '2018-03-02 16:20:55', '2018-03-02 16:20:55');
INSERT INTO `ins_member_product_type` VALUES ('22', '2', '2', '住院一次赔付限额', '1', null, '22', null, '2018-03-02 16:20:55', '2018-03-02 16:20:55');
INSERT INTO `ins_member_product_type` VALUES ('23', '2', '2', '门急诊年度赔付限额', '1', null, '22', null, '2018-03-02 16:20:55', '2018-03-02 16:20:55');
INSERT INTO `ins_member_product_type` VALUES ('24', '2', '2', '门急诊终身赔付限额', '1', null, '22', null, '2018-03-02 16:20:55', '2018-03-02 16:20:55');
INSERT INTO `ins_member_product_type` VALUES ('25', '2', '2', '门急诊一次赔付限额', '1', null, '22', null, '2018-03-02 16:20:55', '2018-03-02 16:20:55');
INSERT INTO `ins_member_product_type` VALUES ('26', '2', '2', '住院年度赔付限额', '1', null, '6', null, '2018-03-06 10:59:41', '2018-03-06 10:59:41');
INSERT INTO `ins_member_product_type` VALUES ('27', '2', '2', '住院终身赔付限额', '1', null, '6', null, '2018-03-06 10:59:41', '2018-03-06 10:59:41');
INSERT INTO `ins_member_product_type` VALUES ('28', '2', '2', '住院一次赔付限额', '1', null, '6', null, '2018-03-06 10:59:42', '2018-03-06 10:59:42');
INSERT INTO `ins_member_product_type` VALUES ('29', '2', '2', '手术年度赔付限额', '1', null, '6', null, '2018-03-06 10:59:42', '2018-03-06 10:59:42');
INSERT INTO `ins_member_product_type` VALUES ('30', '2', '2', '手术终身赔付限额', '1', null, '6', null, '2018-03-06 10:59:42', '2018-03-06 10:59:42');
INSERT INTO `ins_member_product_type` VALUES ('31', '2', '2', '手术一次赔付限额', '1', null, '6', null, '2018-03-06 10:59:42', '2018-03-06 10:59:42');
INSERT INTO `ins_member_product_type` VALUES ('32', '2', '2', '门急诊年度赔付限额', '1', null, '6', null, '2018-03-06 10:59:42', '2018-03-06 10:59:42');
INSERT INTO `ins_member_product_type` VALUES ('33', '2', '2', '门急诊终身赔付限额', '1', null, '6', null, '2018-03-06 10:59:42', '2018-03-06 10:59:42');
INSERT INTO `ins_member_product_type` VALUES ('34', '2', '2', '门急诊一次赔付限额', '1', null, '6', null, '2018-03-06 10:59:42', '2018-03-06 10:59:42');
INSERT INTO `ins_member_product_type` VALUES ('35', '2', '2', '体检年度赔付限额', '1', null, '6', null, '2018-03-06 10:59:42', '2018-03-06 10:59:42');
INSERT INTO `ins_member_product_type` VALUES ('36', '2', '2', '体检终身赔付限额', '1', null, '6', null, '2018-03-06 10:59:42', '2018-03-06 10:59:42');
INSERT INTO `ins_member_product_type` VALUES ('37', '2', '2', '体检一次赔付限额', '1', null, '6', null, '2018-03-06 10:59:42', '2018-03-06 10:59:42');
INSERT INTO `ins_member_product_type` VALUES ('38', '2', '2', '住院年度赔付限额', '1', null, '6', null, '2018-04-25 09:56:00', '2018-04-25 09:56:00');
INSERT INTO `ins_member_product_type` VALUES ('39', '2', '2', '住院终身赔付限额', '1', null, '6', null, '2018-04-25 09:56:00', '2018-04-25 09:56:00');
INSERT INTO `ins_member_product_type` VALUES ('40', '2', '2', '住院一次赔付限额', '1', null, '6', null, '2018-04-25 09:56:00', '2018-04-25 09:56:00');
INSERT INTO `ins_member_product_type` VALUES ('41', '2', '2', '住院年度赔付限额', '1', null, '6', null, '2018-04-25 12:19:40', '2018-04-25 12:19:40');
INSERT INTO `ins_member_product_type` VALUES ('42', '2', '2', '住院终身赔付限额', '1', null, '6', null, '2018-04-25 12:19:40', '2018-04-25 12:19:40');
INSERT INTO `ins_member_product_type` VALUES ('43', '2', '2', '住院一次赔付限额', '1', null, '6', null, '2018-04-25 12:19:40', '2018-04-25 12:19:40');
INSERT INTO `ins_member_product_type` VALUES ('44', '2', '2', '住院年度赔付限额', '1', null, '6', null, '2018-04-25 13:48:14', '2018-04-25 13:48:14');
INSERT INTO `ins_member_product_type` VALUES ('45', '2', '2', '住院终身赔付限额', '1', null, '6', null, '2018-04-25 13:48:14', '2018-04-25 13:48:14');
INSERT INTO `ins_member_product_type` VALUES ('46', '2', '2', '住院一次赔付限额', '1', null, '6', null, '2018-04-25 13:48:14', '2018-04-25 13:48:14');
INSERT INTO `ins_member_product_type` VALUES ('47', '2', '2', '住院年度赔付限额', '1', null, '25', null, '2018-04-27 11:31:33', '2018-04-27 11:31:33');
INSERT INTO `ins_member_product_type` VALUES ('48', '2', '2', '住院终身赔付限额', '1', null, '25', null, '2018-04-27 11:31:33', '2018-04-27 11:31:33');
INSERT INTO `ins_member_product_type` VALUES ('49', '2', '2', '住院一次赔付限额', '1', null, '25', null, '2018-04-27 11:31:33', '2018-04-27 11:31:33');
INSERT INTO `ins_member_product_type` VALUES ('50', '2', '2', '门急诊年度赔付限额', '1', null, '25', null, '2018-04-27 11:32:11', '2018-04-27 11:32:11');
INSERT INTO `ins_member_product_type` VALUES ('51', '2', '2', '门急诊终身赔付限额', '1', null, '25', null, '2018-04-27 11:32:11', '2018-04-27 11:32:11');
INSERT INTO `ins_member_product_type` VALUES ('52', '2', '2', '门急诊一次赔付限额', '1', null, '25', null, '2018-04-27 11:32:11', '2018-04-27 11:32:11');
INSERT INTO `ins_member_product_type` VALUES ('53', '2', '2', '住院年度赔付限额', '1', null, '25', null, '2018-04-27 11:40:30', '2018-04-27 11:40:30');
INSERT INTO `ins_member_product_type` VALUES ('54', '2', '2', '住院终身赔付限额', '1', null, '25', null, '2018-04-27 11:40:30', '2018-04-27 11:40:30');
INSERT INTO `ins_member_product_type` VALUES ('55', '2', '2', '住院一次赔付限额', '1', null, '25', null, '2018-04-27 11:40:30', '2018-04-27 11:40:30');
INSERT INTO `ins_member_product_type` VALUES ('56', '2', '2', '住院年度赔付限额', '1', null, '25', null, '2018-04-27 14:26:06', '2018-04-27 14:26:06');
INSERT INTO `ins_member_product_type` VALUES ('57', '2', '2', '住院终身赔付限额', '1', null, '25', null, '2018-04-27 14:26:06', '2018-04-27 14:26:06');
INSERT INTO `ins_member_product_type` VALUES ('58', '2', '2', '住院一次赔付限额', '1', null, '25', null, '2018-04-27 14:26:06', '2018-04-27 14:26:06');
INSERT INTO `ins_member_product_type` VALUES ('59', '2', '2', '门急诊年度赔付限额', '1', null, '25', null, '2018-04-27 14:26:06', '2018-04-27 14:26:06');
INSERT INTO `ins_member_product_type` VALUES ('60', '2', '2', '门急诊终身赔付限额', '1', null, '25', null, '2018-04-27 14:26:06', '2018-04-27 14:26:06');
INSERT INTO `ins_member_product_type` VALUES ('61', '2', '2', '门急诊一次赔付限额', '1', null, '25', null, '2018-04-27 14:26:06', '2018-04-27 14:26:06');
INSERT INTO `ins_member_product_type` VALUES ('62', '2', '2', '生育年度赔付限额', '1', null, '9', null, '2018-05-03 18:55:11', '2018-05-03 18:55:11');
INSERT INTO `ins_member_product_type` VALUES ('63', '2', '2', '生育终身赔付限额', '1', null, '9', null, '2018-05-03 18:55:11', '2018-05-03 18:55:11');
INSERT INTO `ins_member_product_type` VALUES ('64', '2', '2', '生育一次赔付限额', '1', null, '9', null, '2018-05-03 18:55:11', '2018-05-03 18:55:11');
INSERT INTO `ins_member_product_type` VALUES ('65', '2', '2', '住院年度赔付限额', '1', null, '6', null, '2018-05-04 10:28:49', '2018-05-04 10:28:49');
INSERT INTO `ins_member_product_type` VALUES ('66', '2', '2', '住院终身赔付限额', '1', null, '6', null, '2018-05-04 10:28:49', '2018-05-04 10:28:49');
INSERT INTO `ins_member_product_type` VALUES ('67', '2', '2', '住院一次赔付限额', '1', null, '6', null, '2018-05-04 10:28:49', '2018-05-04 10:28:49');
INSERT INTO `ins_member_product_type` VALUES ('68', '2', '2', '门急诊年度赔付限额', '1', null, '6', null, '2018-05-04 10:29:18', '2018-05-04 10:29:18');
INSERT INTO `ins_member_product_type` VALUES ('69', '2', '2', '门急诊终身赔付限额', '1', null, '6', null, '2018-05-04 10:29:18', '2018-05-04 10:29:18');
INSERT INTO `ins_member_product_type` VALUES ('70', '2', '2', '门急诊一次赔付限额', '1', null, '6', null, '2018-05-04 10:29:18', '2018-05-04 10:29:18');
INSERT INTO `ins_member_product_type` VALUES ('71', '2', '2', '住院年度赔付限额', '1', null, '6', null, '2018-05-04 10:56:31', '2018-05-04 10:56:31');
INSERT INTO `ins_member_product_type` VALUES ('72', '2', '2', '住院终身赔付限额', '1', null, '6', null, '2018-05-04 10:56:31', '2018-05-04 10:56:31');
INSERT INTO `ins_member_product_type` VALUES ('73', '2', '2', '住院一次赔付限额', '1', null, '6', null, '2018-05-04 10:56:31', '2018-05-04 10:56:31');
INSERT INTO `ins_member_product_type` VALUES ('74', '2', '2', '住院年度赔付限额', '1', null, '6', null, '2018-05-04 11:03:01', '2018-05-04 11:03:01');
INSERT INTO `ins_member_product_type` VALUES ('75', '2', '2', '住院终身赔付限额', '1', null, '6', null, '2018-05-04 11:03:01', '2018-05-04 11:03:01');
INSERT INTO `ins_member_product_type` VALUES ('76', '2', '2', '住院一次赔付限额', '1', null, '6', null, '2018-05-04 11:03:01', '2018-05-04 11:03:01');
INSERT INTO `ins_member_product_type` VALUES ('77', '2', '2', '住院年度赔付限额', '1', null, '6', null, '2018-05-04 11:24:56', '2018-05-04 11:24:56');
INSERT INTO `ins_member_product_type` VALUES ('78', '2', '2', '住院终身赔付限额', '1', null, '6', null, '2018-05-04 11:24:56', '2018-05-04 11:24:56');
INSERT INTO `ins_member_product_type` VALUES ('79', '2', '2', '住院一次赔付限额', '1', null, '6', null, '2018-05-04 11:24:56', '2018-05-04 11:24:56');

-- ----------------------------
-- Table structure for ins_product_selected
-- ----------------------------
DROP TABLE IF EXISTS `ins_product_selected`;
CREATE TABLE `ins_product_selected` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL COMMENT '产品ID',
  `score` int(3) DEFAULT NULL COMMENT '评分',
  `update_at` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ins_product_selected_his
-- ----------------------------
DROP TABLE IF EXISTS `ins_product_selected_his`;
CREATE TABLE `ins_product_selected_his` (
  `his_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '历史主键',
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL COMMENT '产品ID',
  `score` int(3) DEFAULT NULL COMMENT '评分',
  `action` varchar(50) DEFAULT NULL COMMENT '操作类型',
  `his_created_by` int(11) DEFAULT NULL COMMENT '历史记录创建者',
  `his_created_at` timestamp NULL DEFAULT NULL COMMENT '历史记录创建时间',
  PRIMARY KEY (`his_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='保险产品管理历史表';

-- ----------------------------
-- Table structure for ins_rb_cycle
-- ----------------------------
DROP TABLE IF EXISTS `ins_rb_cycle`;
CREATE TABLE `ins_rb_cycle` (
  `id` tinyint(4) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='报销周期';

-- ----------------------------
-- Records of ins_rb_cycle
-- ----------------------------
INSERT INTO `ins_rb_cycle` VALUES ('1', '每次');
INSERT INTO `ins_rb_cycle` VALUES ('2', '每年');

-- ----------------------------
-- Table structure for ins_sk_coverage_type
-- ----------------------------
DROP TABLE IF EXISTS `ins_sk_coverage_type`;
CREATE TABLE `ins_sk_coverage_type` (
  `id` tinyint(4) NOT NULL,
  `description` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='轻症包含类型';

-- ----------------------------
-- Records of ins_sk_coverage_type
-- ----------------------------
INSERT INTO `ins_sk_coverage_type` VALUES ('1', '终身');
INSERT INTO `ins_sk_coverage_type` VALUES ('2', '10 年');
INSERT INTO `ins_sk_coverage_type` VALUES ('3', '20 年');
INSERT INTO `ins_sk_coverage_type` VALUES ('4', '30 年');
INSERT INTO `ins_sk_coverage_type` VALUES ('5', '至65岁');
INSERT INTO `ins_sk_coverage_type` VALUES ('6', '至70岁');
INSERT INTO `ins_sk_coverage_type` VALUES ('7', '至80岁');

-- ----------------------------
-- Table structure for ins_sk_due_time
-- ----------------------------
DROP TABLE IF EXISTS `ins_sk_due_time`;
CREATE TABLE `ins_sk_due_time` (
  `id` tinyint(4) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='长期重疾到期类型';

-- ----------------------------
-- Records of ins_sk_due_time
-- ----------------------------
INSERT INTO `ins_sk_due_time` VALUES ('1', '终身');
INSERT INTO `ins_sk_due_time` VALUES ('2', '至70岁');
INSERT INTO `ins_sk_due_time` VALUES ('3', '至80岁');
INSERT INTO `ins_sk_due_time` VALUES ('4', '至88岁');
INSERT INTO `ins_sk_due_time` VALUES ('5', '至100岁');

-- ----------------------------
-- Table structure for log_login
-- ----------------------------
DROP TABLE IF EXISTS `log_login`;
CREATE TABLE `log_login` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `machine_name` varchar(255) DEFAULT NULL,
  `os` varchar(255) DEFAULT NULL,
  `license_id` int(11) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_login_license` (`license_id`),
  KEY `fk_login_user` (`user_id`),
  CONSTRAINT `fk_login_license` FOREIGN KEY (`license_id`) REFERENCES `sys_license` (`id`),
  CONSTRAINT `fk_login_user` FOREIGN KEY (`user_id`) REFERENCES `bsc_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户登录记录表';

-- ----------------------------
-- Records of log_login
-- ----------------------------

-- ----------------------------
-- Table structure for log_operate
-- ----------------------------
DROP TABLE IF EXISTS `log_operate`;
CREATE TABLE `log_operate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_id` tinyint(4) NOT NULL DEFAULT '1',
  `user_id` int(11) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `controller_name` varchar(255) DEFAULT NULL,
  `time` datetime NOT NULL,
  `result` varchar(100) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `ip` varchar(50) DEFAULT NULL,
  `machine_name` varchar(50) DEFAULT NULL,
  `os` varchar(50) DEFAULT NULL,
  `license_id` int(11) DEFAULT NULL,
  `created_by` varchar(255) NOT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_operate_action` (`action_id`),
  KEY `fk_operate_client` (`client_id`),
  KEY `fk_operate_license` (`license_id`),
  KEY `fk_operate_user` (`user_id`),
  CONSTRAINT `fk_operate_action` FOREIGN KEY (`action_id`) REFERENCES `bsc_action` (`id`),
  CONSTRAINT `fk_operate_client` FOREIGN KEY (`client_id`) REFERENCES `clt_client` (`id`),
  CONSTRAINT `fk_operate_license` FOREIGN KEY (`license_id`) REFERENCES `sys_license` (`id`),
  CONSTRAINT `fk_operate_user` FOREIGN KEY (`user_id`) REFERENCES `bsc_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户操作日志表';

-- ----------------------------
-- Records of log_operate
-- ----------------------------

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `route` varchar(256) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `data` blob,
  PRIMARY KEY (`id`),
  KEY `parent` (`parent`),
  CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `menu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menu
-- ----------------------------

-- ----------------------------
-- Table structure for migration
-- ----------------------------
DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of migration
-- ----------------------------

-- ----------------------------
-- Table structure for sys_license
-- ----------------------------
DROP TABLE IF EXISTS `sys_license`;
CREATE TABLE `sys_license` (
  `id` int(11) NOT NULL,
  `license_code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `contact` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `mobile` varchar(30) DEFAULT NULL,
  `status_id` tinyint(4) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统证书表';

-- ----------------------------
-- Records of sys_license
-- ----------------------------

-- ----------------------------
-- Table structure for team_basic_info
-- ----------------------------
DROP TABLE IF EXISTS `team_basic_info`;
CREATE TABLE `team_basic_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '团队名称',
  `num` int(11) DEFAULT NULL COMMENT '团队人数',
  `responsible_id` int(11) NOT NULL DEFAULT '0' COMMENT '负债人ID',
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`) USING BTREE,
  KEY `fk_team_user_id` (`responsible_id`),
  CONSTRAINT `fk_team_user_id` FOREIGN KEY (`responsible_id`) REFERENCES `zxfa_mgr_dev`.`user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='团队基础信息表';

-- ----------------------------
-- Table structure for team_basic_info_his
-- ----------------------------
DROP TABLE IF EXISTS `team_basic_info_his`;
CREATE TABLE `team_basic_info_his` (
  `his_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '历史主键',
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL COMMENT '团队名称',
  `num` int(11) DEFAULT NULL COMMENT '团队人数',
  `responsible_id` int(11) NOT NULL DEFAULT '0' COMMENT '负债人ID',
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `action` varchar(50) DEFAULT NULL COMMENT '操作类型',
  `his_created_by` int(11) DEFAULT NULL COMMENT '历史记录创建者',
  `his_created_at` timestamp NULL DEFAULT NULL COMMENT '历史记录创建时间',
  PRIMARY KEY (`his_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='团队基础信息历史表';

-- ----------------------------
-- Table structure for team_detail_info
-- ----------------------------
DROP TABLE IF EXISTS `team_detail_info`;
CREATE TABLE `team_detail_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_id` int(11) NOT NULL DEFAULT '0' COMMENT '团队ID',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '成员ID',
  PRIMARY KEY (`id`),
  KEY `id` (`id`) USING BTREE,
  KEY `fk_team_user_id` (`user_id`),
  KEY `fk_team_team_id` (`team_id`),
  CONSTRAINT `fk_team_detail_user_id` FOREIGN KEY (`user_id`) REFERENCES `zxfa_mgr_dev`.`user` (`id`),
  CONSTRAINT `fk_team_team_id` FOREIGN KEY (`team_id`) REFERENCES `team_basic_info` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='团队详细信息表';

-- ----------------------------
-- Table structure for team_detail_info_his
-- ----------------------------
DROP TABLE IF EXISTS `team_detail_info_his`;
CREATE TABLE `team_detail_info_his` (
  `his_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '历史主键',
  `id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL DEFAULT '0' COMMENT '团队ID',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '成员ID',
  `action` varchar(50) DEFAULT NULL COMMENT '操作类型',
  `his_created_by` int(11) DEFAULT NULL COMMENT '历史记录创建者',
  `his_created_at` timestamp NULL DEFAULT NULL COMMENT '历史记录创建时间',
  PRIMARY KEY (`his_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='团队详细信息历史表';

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user
-- ----------------------------
