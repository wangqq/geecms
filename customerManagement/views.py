from customerManagement.models import Customer, User, CustomerDatabase, UserHistory, UserAll
from insuranceManagement.models import InsTypeDs, InsProduct
import hashlib
import time
from customerManagement.forms import UserChangeForm, UserAddForm, CustomerForm, CustomerChangeForm
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction, IntegrityError, connection

from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.response import Response
from rest_framework.authentication import SessionAuthentication
from utils.auth import ExpiringTokenAuthentication
from rest_framework.permissions import IsAuthenticated

from static.common.cousterFunction import list_fetchall, dict_fetchall, gen_password, dictfetchall, get_list
from static.common.HistoryData import *
from static.common.constant import Constant

import os, pymysql
import re
from geeCMS.settings import proDb, proDbName


# Create your views here.

@api_view(['GET'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def index(request):
    items = Customer.objects.order_by('-id').all()

    if request.GET.get('timeStart'):
        items = items.filter(created_at__gte=request.GET.get('timeStart'))
    if request.GET.get('timeEnd'):
        items = items.filter(created_at__lte=request.GET.get('timeEnd'))
    if request.GET.get('name'):
        items = items.filter(name__contains=request.GET.get('name'))
    if request.GET.get('status'):
        items = items.filter(status=request.GET.get('status'))

    result = []
    for item in items:
        nums_valid = User.objects.filter(customer_id=item.id, status=10).count()
        try:
            app_result = {
                'name': item.name,
                'nums_valid': nums_valid,
                'customer_num': item.customer_num,
                'email': item.user.email,
                'created_at': item.created_at,
                'get_status_txt': item.status == 1 and '正常' or '禁用',
                'updated_by': item.updated_by,
                'id': item.id
            }
            result.append(app_result)
        except User.DoesNotExist:
            pass

    count = items.count()

    return Response({'count': count, 'result': result})


@api_view(['POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
@transaction.atomic()
def create(request):
    user_name = request.user.username
    form = CustomerForm(request.data)
    form_user = UserAddForm(request.data)
    if request.method == 'POST':
        if form.is_valid() and form_user.is_valid():
            # DB连接配置
            db = {
                'host': 'localhost',
                'port': 6506,
                'user': 'dashbee',
                'password': 'Devendra@!#12345',
                'charset': 'utf8',
                'cursorclass': pymysql.cursors.Cursor,
            }

            # 创建连接
            cursor = connection.cursor()
            try:
                try:
                    with transaction.atomic():
                        customer_obj = Customer(
                            name=form.cleaned_data.get('name'),
                            email=form.cleaned_data.get('email'),
                            customer_num=form.cleaned_data.get('customer_num'),
                            start_date=str(form.cleaned_data.get('start_date')).replace('/', '-'),
                            end_date=str(form.cleaned_data.get('end_date')).replace('/', '-'),
                            status=form.cleaned_data.get('status'),
                            word_download_status=form.cleaned_data.get('word_download_status'),
                            created_by=user_name,
                            updated_by=user_name,
                            user_id=request.user.id
                        )
                        customer_obj.save()
                        customer_id = customer_obj.id
                        # 查找可使用的账号
                        account_sql = "SELECT tua.unique_id FROM `user` AS tu,user_all AS tua WHERE tu.email = '" + form.cleaned_data.get('email') +\
                                      "' AND tu.status='" + str(Constant.USER_STATUS_ACTIVE) +"'  AND tua.id=tu.user_all_id ORDER BY tua.id limit 1"
                        cursor.execute(account_sql)
                        account_result = cursor.fetchone()
                        if account_result[0]:
                            unique_id = account_result[0]
                        else:
                            unique_id = gen_password(Constant.CAPTCHA_NUM_LONG)
                        # userAll保存
                        user_all_obj = UserAll(
                            unique_id=unique_id,
                            user_origin=1,
                            email=form_user.cleaned_data.get('email')
                        )
                        user_all_obj.save()

                        user_obj = User(
                            user_all_id=user_all_obj.id,
                            email=form_user.cleaned_data.get('email'),
                            firstname=form_user.cleaned_data.get('firstname'),
                            password_hash=hashlib.md5(form_user.cleaned_data.get('password_hash').encode('utf8')).hexdigest(),
                            customer_id=customer_id,
                            start_date=customer_obj.start_date,
                            end_date=customer_obj.end_date,
                            status=Constant.USER_STATUS_ACTIVE
                        )
                        user_obj.save()
                        user_id = user_obj.id
                        customer_obj.user_id = user_id
                        customer_obj.save()

                        db_name = 'zxfa_dev' + str(customer_obj.id)
                        # 连接用户名
                        db_user = db['user']
                        # 连接密码
                        db_pw = db['password']
                        # DB信息表保存
                        database_obj = CustomerDatabase(
                            customer=customer_obj,
                            dsn=db_name,
                            username=db_user,
                            password=db_pw
                        )
                        database_obj.save()
                except IntegrityError:
                    raise IntegrityError

                create_database_sql = """create database if not exists """ + db_name
                # 执行SQL语句
                try:
                    cursor.execute(create_database_sql)
                except IntegrityError:
                    return Response({'code': Constant.FAIL, 'message': "创建新的数据库时出错"})
                # 创建表结构
                filename = os.path.split(os.path.realpath(__file__))[0] + '/zxfa_dev.sql'
                try:
                    os.system("mysql -h%s -P%s -u%s -p%s %s  < %s " % (
                        db['host'], db['port'], db['user'], db['password'], db_name, filename))
                except IntegrityError:
                    return Response({'code': Constant.FAIL, 'message': "创建表结构和初期数据出错"})
                # 添加权限列表
                try:
                    cursor.execute('insert into ' + db_name + '.auth_assignment(`item_name`,`user_id`) VALUE ("%s",%d)' % ('admin', user_id))
                except IntegrityError:
                    return Response({'code': Constant.FAIL, 'message': "插入管理员权限记录出错"})

                # 关闭DB连接
                cursor.close()
                return Response({'code': Constant.SUCCESS, 'message': Constant.SUCCESS_MESSAGE})
            except IntegrityError:
                # 关闭DB连接
                cursor.close()
                return Response({'code': Constant.FAIL, 'message': Constant.FAIL_MESSAGE})
        else:
            return Response({'code': Constant.INVAILD, 'message': form.errors, 'message_user': form_user.errors})


@api_view(['GET', 'POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def update(request, id):
    user_name = request.user.username
    user_id = request.user.id
    try:
        customer_obj = Customer.objects.get(id=id)
        customer_his = Customer.objects.get(id=id)
        user_obj = customer_obj.user
        user_his = customer_his.user
    except ObjectDoesNotExist:
        return Response({'code': Constant.NONE_EXIST, 'message': Constant.NONE_EXIST_MESSAGE})
    if request.method == 'POST':
        form = CustomerChangeForm(request.data, instance=customer_obj)
        form_user = UserChangeForm(request.data, instance=user_obj)
        if form.is_valid() and form_user.is_valid():
            try:
                with transaction.atomic():
                    # 历史数据
                    return_pro_his = fun_customer_his(customer_his, Constant.ACTION_UPDATE, user_id)
                    if return_pro_his == Constant.FAIL:
                        raise IntegrityError

                    # 数据更新
                    customer_obj.customer_num = request.data.get('customer_num')
                    customer_obj.start_date = str(request.data.get('start_date')).replace('/', '-')
                    customer_obj.end_date = str(request.data.get('end_date')).replace('/', '-')
                    customer_obj.updated_by = user_name
                    customer_obj.status = request.data.get('status')
                    customer_obj.word_download_status = request.data.get('word_download_status')
                    customer_obj.save()
                    if request.data.get('password_hash') != '':
                        # 历史数据
                        his_user_obj = UserHistory(
                            fid=user_his.id,
                            mobile=user_his.mobile,
                            email=user_his.email,
                            openid=user_his.openid,
                            unionid=user_his.unionid,
                            password_hash=user_his.password_hash,
                            password_reset_token=user_his.password_reset_token,
                            customer_id=user_his.customer_id,
                            status=user_his.status,
                            created_by=user_his.created_by,
                            updated_by=user_his.updated_by,
                            created_at=user_his.created_at,
                            updated_at=user_his.updated_at,
                            auth_key=user_his.auth_key,
                            firstname=user_his.firstname,
                            agreed=user_his.agreed,
                            password_change=user_his.password_change,
                            register_type=user_his.register_type,
                            img_path=user_his.img_path,
                            start_date=user_his.start_date,
                            end_date=user_his.end_date,
                            work_area=user_his.work_area,
                            work_company=user_his.work_company,
                            work_no=user_his.work_no,
                            work_time_length=user_his.work_time_length,
                            action=Constant.ACTION_UPDATE,
                            his_created_by=int(user_id)
                        )
                        his_user_obj.save()

                        # 数据更新
                        user_obj.password_hash = hashlib.md5(request.data.get('password_hash').encode('utf8')).hexdigest()
                    user_obj.start_date = customer_obj.start_date
                    user_obj.end_date = customer_obj.end_date
                    user_obj.save()
                    return Response({'code': Constant.SUCCESS, 'message': Constant.SUCCESS_MESSAGE})
            except IntegrityError:
                return Response({'code': Constant.FAIL, 'message': Constant.FAIL_MESSAGE})
        else:
            return Response({'code': Constant.INVAILD, 'message': form.errors, 'message_user': form_user.errors})

    else:
        result = {
            'name': customer_obj.name,
            'email': user_obj.email,
            'password_hash': user_obj.password_hash,
            'status': customer_obj.status,
            'word_download_status': customer_obj.word_download_status,
            'customer_num': customer_obj.customer_num,
            'start_date': time.strftime("%Y-%m-%d", time.strptime(customer_obj.start_date.strftime("%Y-%m-%d %H:%S:%M"), "%Y-%m-%d %H:%M:%S")),
            'end_date': time.strftime("%Y-%m-%d", time.strptime(customer_obj.end_date.strftime("%Y-%m-%d %H:%S:%M"), "%Y-%m-%d %H:%M:%S"))
        }

        return Response(result)


@api_view(['GET','POST'])
@authentication_classes((ExpiringTokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def select_product(request, id):
    # DB连接字符获得
    customer_db = CustomerDatabase.objects.get(customer_id=id)
    customer_dsn = customer_db.dsn

    # 连接DB
    cursor = connection.cursor()

    now_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    user_id = request.user.id

    if request.method == 'POST':
        product = request.data.get('product')

        try:
            with transaction.atomic():
                for pro_item in product:
                    if pro_item['id'] and pro_item['check_status']:
                        sel_pro_sql = "SELECT id,product_id,score FROM " + customer_dsn + ".ins_product_selected WHERE id=" + \
                                      str(pro_item['id']) + " AND score = " + str(pro_item['score'])
                        no_change = cursor.execute(sel_pro_sql)

                        if not no_change:
                            his_pro_sql = "SELECT id,product_id,score FROM " + customer_dsn + ".ins_product_selected WHERE id=" + \
                                          str(pro_item['id'])
                            cursor.execute(his_pro_sql)
                            his_pro = cursor.fetchone()

                            his_insert = "INSERT INTO " + customer_dsn + ".ins_product_selected_his (`id`, `product_id`, `score`, `action`, `his_created_by`, `his_created_at`) " \
                                         "VALUES ('" + str(his_pro[0]) + "','" + str(his_pro[1]) + "','" + str(his_pro[2]) \
                                         + "','" + str(Constant.ACTION_UPDATE) + "','" + str(user_id) + "','" \
                                         + now_time + "')"

                            cursor.execute(his_insert)

                            update_sql = "UPDATE " + customer_dsn + ".ins_product_selected SET score = " \
                                         + str(pro_item['score']) + " WHERE id = " + str(pro_item['id'])
                            cursor.execute(update_sql)
                    elif pro_item['id'] and not pro_item['check_status']:
                        his_pro_sql = "SELECT id,product_id,score FROM " + customer_dsn + ".ins_product_selected WHERE id=" + \
                                      str(pro_item['id'])
                        cursor.execute(his_pro_sql)
                        his_pro = cursor.fetchone()

                        his_insert = "INSERT INTO " + customer_dsn + ".ins_product_selected_his (`id`, `product_id`, `score`, `action`, `his_created_by`, `his_created_at`) " \
                                     "VALUES ('" + str(his_pro[0]) + "','" + str(his_pro[1]) + "','" + str(his_pro[2]) \
                                     + "','" + str(Constant.ACTION_DELETE) + "','" + str(user_id) + "','" \
                                     + now_time + "')"

                        cursor.execute(his_insert)

                        delete_sql = "DELETE FROM " + customer_dsn + ".ins_product_selected WHERE id = " \
                                     + str(pro_item['id'])
                        cursor.execute(delete_sql)

                    elif not pro_item['id'] and pro_item['check_status']:
                        insert_sql = "INSERT INTO " + customer_dsn + ".ins_product_selected (`product_id`, `score`) VALUES (" + \
                                     str(pro_item['product_id']) + "," + str(pro_item['score']) + ")"
                        cursor.execute(insert_sql)

                cursor.close()

                return Response({'code': Constant.SUCCESS, 'message': Constant.SUCCESS_MESSAGE})

        except IntegrityError:

            cursor.close()
            return Response({'code': Constant.FAIL, 'message': Constant.FAIL_MESSAGE})

    # 无效产品删除
    productList = get_list(InsProduct.objects.using(proDb).values_list('id').filter(is_active=Constant.STATUS_ACTIVE))
    selList_sql = "SELECT product_id FROM " + customer_dsn + ".ins_product_selected"
    cursor.execute(selList_sql)
    selList = dictfetchall(cursor)
    for delete_item in selList:
        if delete_item['product_id'] not in productList:
            delete_id = delete_item['product_id']
            his_pro_sql = "SELECT id,product_id,score FROM " + customer_dsn \
                          + ".ins_product_selected WHERE product_id=" + str(delete_id)
            cursor.execute(his_pro_sql)
            his_pro = cursor.fetchone()

            his_insert = "INSERT INTO " + customer_dsn + ".ins_product_selected_his (`id`, `product_id`, `score`, `action`, `his_created_by`, `his_created_at`) " \
                                                         "VALUES ('" + str(his_pro[0]) + "','" + \
                         str(his_pro[1]) + "','" + str(his_pro[2]) \
                         + "','" + str(Constant.ACTION_DELETE) + "','" + str(user_id) + "','" \
                         + now_time + "')"

            cursor.execute(his_insert)

            delete_sql = "DELETE FROM " + customer_dsn + ".ins_product_selected WHERE product_id = " \
                         + str(delete_id)
            cursor.execute(delete_sql)

    # 险种取得
    insurance_type = dict_fetchall(InsTypeDs.objects.using(proDb).values_list('id', 'description').filter(is_active=Constant.STATUS_ACTIVE))
    # 产品列表查询项
    sql_select = """SELECT tpr.id,ifnull(tsel.score,tpr.score) AS score,tpr.name AS pro_name,tpr.category_type,
                            tins.name AS ins_name,tsel.id AS selId
    FROM """+proDbName+""".ins_product AS tpr 
    INNER JOIN  """+proDbName+""".insurer AS tins ON tins.id = tpr.insurer_id
    LEFT JOIN """+customer_dsn+""".ins_product_selected AS tsel ON tsel.product_id = tpr.id
    WHERE tpr.is_active = """ + str(Constant.STATUS_ACTIVE)

    # 已选产品个数
    sql_select_num = """SELECT count(tpr.id) AS num
    FROM  """+proDbName+""".ins_product AS tpr 
    INNER JOIN """+proDbName+""".insurer AS tins ON tins.id = tpr.insurer_id
    INNER JOIN """+customer_dsn+""".ins_product_selected AS tsel ON tsel.product_id = tpr.id
    WHERE tpr.is_active = """ + str(Constant.STATUS_ACTIVE)

    # 条件语句
    and_where = ''

    if request.GET.get('search_name'):
        and_where += " AND tpr.name LIKE '%"+request.GET.get('search_name')+"%' "
    if request.GET.get('search_type'):
        and_where += " AND tpr.category_type ="+request.GET.get('search_type')
    if request.GET.get('search_comp'):
        and_where += " AND tins.name LIKE '%"+request.GET.get('search_comp')+"%' "

    # 排序语句
    sql_order_by = " ORDER BY convert(tpr.name using gbk)"

    sql_product = sql_select + and_where + sql_order_by

    # 执行产品查询
    product_nums = cursor.execute(sql_select)
    cursor.execute(sql_product)
    product = cursor.fetchall()
    result = []
    for item in product:

        app_result = {
            'id': item[0],
            'score': item[1],
            'name': item[2],
            'category_type': insurance_type[item[3]],
            'ins_name': item[4],
            'selId': item[5]
        }
        result.append(app_result)

    # 执行已选产品个数查询
    cursor.execute(sql_select_num)
    sel_product_nums = cursor.fetchone()
    sel_nums = sel_product_nums[0]

    cursor.close()

    return Response({'product_nums': product_nums, 'sel_nums': sel_nums, 'result': result, 'insurance_type': insurance_type})
