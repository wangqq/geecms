
import os, datetime, time, requests, string, json

from geeCMS.param import SERVER_HOST, FILE_FOLDER
from pathlib import Path
from django.conf import settings
from django.db import connection
from django.db.models import Min, Max
from django.core.mail import EmailMultiAlternatives
from email.mime.text import MIMEText
from email.header import Header
from email.utils import parseaddr, formataddr

from insuranceManagement.models import InsProductSchedule, InsProSchAtt, InsTypeDs, InsTypeInsurance
from insuranceManagement.models_composite import InsProductBind, InsCompositePro
from insuranceManagement.models_score import InsProductScore, InsProductScoreAtt
from cityManagement.models import BscCity
from uploadArticle.models import WxAccessToken

from collections import namedtuple
from random import choice
from dateutil.relativedelta import relativedelta

from static.common.HistoryData import fun_ins_composite_pro_his
from static.common.log import Logger
from static.common.constant import Constant
from geeCMS.settings import proDb, proDbName, socialDb


def list_fetchall(list):
    "Return all rows from a cursor as a list"
    return_list = []
    for item in list:
        return_list.append([item[0], item[1]])
    return return_list


# 以列表字典的形式返回
def list_fetch_key(list):
    "Return all rows from a cursor as a list"
    return_list = []
    for item in list:
        return_list.append({'id': item[0], 'name': item[1]})
    return return_list


def get_list(list):
    "Return all rows from a cursor as a list"
    return_list = []
    for item in list:
        return_list.append(item[0])
    return return_list


def get_val(list):
    "Return all rows from a cursor as a list"
    if list:
       return list[0][0]
    else:
        return None


def dict_fetchall(list):
    "Return all rows from a cursor as a dict"
    return_dict = {}
    for item in list:
        return_dict[item[0]] = item[1]
    return return_dict


def get_list_str(list):
    "Return all rows from a cursor as a list"
    return_list = []
    for item in list:
        return_list.append(str(item[0]))
    return return_list


# 上传文件
def upload_file(file, filename, file_suffix, path):
    # 存储路径
    mk_path = FILE_FOLDER + 'insurance_source_file' + path
    # 判断目录是否已经存在
    mk_path_exist = Path(mk_path)
    # 存在时，删除目录下的相应文件
    if mk_path_exist.is_dir():
        file_list = os.listdir(mk_path)
        for f in file_list:
            if filename in f:
                os.remove(os.path.join(mk_path, f))
    else:
        # 创建目录
        os.mkdir(mk_path)
    # 文件名全称
    filename_full = filename + file_suffix
    # 文件存储路径
    filepath = mk_path + '/' + filename_full
    # 存储文件
    destination = open(filepath, 'wb+')
    for chunk in file.chunks():
        destination.write(chunk)
    destination.close()
    # 返回文件路径
    file_url = "https://" + SERVER_HOST + "/insurance_source_file" + path + '/' + filename_full
    return file_url


# 发送邮件
def send_mail(customer,pro_name, pro_type, pro_comp):
    sender = settings.DEFAULT_FROM_EMAIL
    # 邮件抬头
    subject = '新增产品'
    # 邮件内容
    mail_msg = "新增" + pro_type +"产品：" + pro_name+"，所属公司" + pro_comp +  " 。<br/>若您已选择自动同步，该产品已同步到您的产品库中。<br/>若未选择自动同步，您可在保险管理页面中手动添加。<br/>" \
               + datetime.datetime.now().strftime('%Y{y}%m{m}%d{d}').format(y='年', m='月', d='日')

    # 三个参数：第一个为文本内容，第二个 plain 设置文本格式，第三个 utf-8 设置编码
    message = MIMEText(mail_msg, 'html', 'utf-8')
    message['From'] = _format_addr(u'寄意财务系统 <%s>' % sender)
    message['To'] = _format_addr(customer.name + ' <%s>' % sender)

    message['Subject'] = Header(subject, 'utf-8')

    try:
        msg = EmailMultiAlternatives(message['Subject'], mail_msg,
                                     _format_addr(u'寄意财务系统 <%s>' % sender),
                                     [_format_addr(customer.name + ' <%s>' % customer.email)])
        msg.content_subtype = "html"
        # 发送
        msg.send()

        return Constant.SUCCESS
    except Exception:
        return Constant.FAIL


# 邮件格式
def _format_addr(s):
    name, addr = parseaddr(s)
    return formataddr((Header(name, 'utf-8').encode(), addr))


# 反转字典里的key和value
def reversed_dict(rdict):
    # 返回的字典
    return_dict = {}
    for k, v in rdict.items():
        return_dict[v] = k
    return return_dict


# 保费评分取得
def get_premium_score(id, pro_type, category_type, guarantee_pattern):
    # model名称
    if int(pro_type) == Constant.MAIN_TYPE:
        query_sck = InsProductSchedule
    else:
        query_sck = InsProSchAtt
    # 保费的评分确定
    premium = avg_premium(id, pro_type, category_type, Constant.SCORE_AMOUNT, guarantee_pattern)
    # 如果保费为0，取最小保额的保费
    if premium == 0:
        min_amount = query_sck.objects.using(proDb).filter(ins_product_id=id, status=Constant.STATUS_ACTIVE).aggregate(
            Min('amount_min'))
        amount = min_amount['amount_min__min']
        if amount:
            min_premium = avg_premium(id, pro_type, category_type, amount, guarantee_pattern)
            if amount > Constant.SCORE_AMOUNT:
                premium = float(min_premium) / (float(amount) / float(Constant.SCORE_AMOUNT))
            else:
                premium = float(min_premium) * (float(Constant.SCORE_AMOUNT) / float(amount))
    # 系数取得
    if category_type == 1:
        ratio_k = Constant.SCORE_RATION_K_LIFE
        ratio_b = Constant.SCORE_RATION_B_LIFE
    elif category_type == 2:
        ratio_k = Constant.SCORE_RATION_K_ACC
        ratio_b = Constant.SCORE_RATION_B_ACC
    elif category_type == 3:
        ratio_k = Constant.SCORE_RATION_K_ILL
        ratio_b = Constant.SCORE_RATION_B_ILL
    else:
        ratio_k = Constant.SCORE_RATION_K_HOSP
        ratio_b = Constant.SCORE_RATION_B_HOSP
    score_pre = round(float(premium) * float(ratio_k) + float(ratio_b), 2)
    if score_pre < -999:
        # 输出错误日志
        log_msg = "产品id【" + str(id) + "】，保费【" + str(premium) + "】，ratio_k【" + str(ratio_k) + "】，ratio_b" + str(ratio_b)
        Logger(Constant.LOG_FILENAME_INS_PRO_SCORE, level='error').logger.error(log_msg)

    return score_pre


# 平均保费取得
def avg_premium(id, pro_type, category_type, amount, guarantee_pattern):
    if int(pro_type) == Constant.MAIN_TYPE:
        table_sch = proDbName + ".ins_product_schedule"
        table_ratio = proDbName + ".ins_product_ratio"
    else:
        table_sch = proDbName + ".ins_pro_sch_att"
        table_ratio = proDbName + ".ins_pro_ratio_att"
    # 年龄列表
    age_list = [0, 10, 20, 30, 40]
    # 性别列表
    gender_list = [1, 2]
    int_category_type = int(category_type)
    int_guarantee_pattern = int(guarantee_pattern)
    if int_category_type == 2:
        add_where = " AND tra.guarantee_period = " + str(Constant.SCORE_GUARANTEE_ACC)
    elif int_guarantee_pattern == Constant.GUARANTEE_PATTERN_REGULAR:
        add_where = " AND tra.guarantee_period = " + str(Constant.SCORE_GUARANTEE)
    else:
        add_where = " AND tra.guarantee_period = " + str(Constant.SCORE_LONG)
    # 保费求得
    premium_sum = 0
    average_num = 0
    premium = 0
    # 缴费年限设定
    where_payment = Constant.SCORE_PAYMENT
    if int_category_type == 2:
        where_payment = Constant.SCORE_PAYMENT_ACC
    # 平均保费取得
    for item_age in age_list:
        for item_gender in gender_list:
            ratio_sql = "SELECT tra.ratio,tsch.amount_type FROM " + table_sch + " AS tsch," + table_ratio + " AS tra" + \
                        " WHERE tra.ins_product_id= " + str(id) + \
                        " AND tra.status = " + str(Constant.STATUS_ACTIVE) + \
                        " AND tra.gender = " + str(item_gender) + \
                        " AND tra.age = " + str(item_age) + \
                        " AND tra.payment_period = " + str(where_payment) + \
                        " AND tra.ratio > 0" + add_where + \
                        " AND tsch.id = tra.ins_product_schedule_id AND tsch.amount_max >= " + str(amount) + \
                        " AND tsch.amount_min <= " + str(amount) + \
                        " limit 1"
            # 查询
            cursor = connection.cursor()
            cursor.execute(ratio_sql)
            ratio_result = cursor.fetchone()
            if ratio_result:
                if ratio_result[1] == Constant.PREMIUM_FIXED:
                    premium_v = ratio_result[0]
                else:
                    premium_v = (ratio_result[0] * amount) / 1000
                premium_sum += premium_v
                average_num += 1
    # 取平均保费
    if average_num:
        premium = premium_sum / average_num

    return premium


# 生成随机码
def gen_password(length=8, chars=string.ascii_letters+string.digits):
    return ''.join([choice(chars) for i in range(length)])


# cursor 返回带索引的结果集
def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


# cursor 返回带索引的结果集
def namedtuplefetchall(cursor):
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]


# 日期转为时间戳
def get_str_time(str):
    time_array = time.strptime(str, "%Y-%m-%d %H:%M:%S")
    time_stamp = int(time.mktime(time_array))
    return time_stamp

# 与当前相差天数
def get_diff_days_2_now(date_str):
    now_time = time.localtime(time.time())
    compare_time = time.strptime(date_str, "%Y-%m-%d")
    # 比较日期
    date1 = datetime.datetime(compare_time[0], compare_time[1], compare_time[2])
    date2 = datetime.datetime(now_time[0], now_time[1], now_time[2])
    diff_days = (date2 - date1).days

    # 上面是正确的获取方法，返回一个int类型天差值，修改时间：2019年8月25日
    # diff_days = str(date2 - date1)
    # # 如果相差0天单纯显示为 00:00:00 不然显示为 [diff_days] : 00:00:00
    # diff_days_arr = diff_days.split(":")
    # if len(diff_days_arr) == 1:
    #     return 0
    # else:
    #     return diff_days_arr[0].split()[0]
    return diff_days


# 获得指定差值的某一天
def get_specified_date(date, diff):
    sdate = datetime.datetime.strptime(date, '%Y-%m-%d')
    delta = sdate + relativedelta(days=diff)
    specified_date = delta.strftime("%Y-%m-%d")
    return specified_date


# 获得指定差值的某月
def get_specified_month(date, diff):
    sdate = datetime.datetime.strptime(date, '%Y-%m-%d')
    delta = sdate + relativedelta(months=diff)
    specified_date = delta.strftime("%Y-%m-%d")
    return specified_date


# 获得指定差值的某月
def get_specified_year(date, diff):
    sdate = datetime.datetime.strptime(date, '%Y-%m-%d')
    delta = sdate + relativedelta(years=diff)
    specified_date = delta.strftime("%Y-%m-%d")
    return specified_date


# 更新时，填写文件
def ins_pro_change_redis(type):
    cache_fresh = InsTypeDs.objects.using(proDb).get(id=type)
    cache_fresh.cache_fresh = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    cache_fresh.save()


# 取到组合产品的相关信息
def get_composite_info(main_id, category_type):
    # 组合组合产品ID
    att_query = InsProductBind.objects.using(proDb).filter(main_id=main_id, status=Constant.STATUS_ACTIVE).exclude(
        bind_type=Constant.BIND_TYPE_NOT_MUST)
    att_ids = get_list_str(att_query.values_list("attach_id"))
    # 取得主险除保费外的评分
    score_main_model = InsProductScore.objects.using(proDb).filter(product_id=main_id).order_by('-score_other').first()
    score_other = 0
    score_other_pre = 0
    if score_main_model:
        score_other = score_main_model.score_other
        score_other_pre = get_premium_score(main_id, Constant.MAIN_TYPE, category_type,
                                            Constant.GUARANTEE_PATTERN_REGULAR)
    # 取得附加险的评分
    for item in att_ids:
        # 其余评分相加
        att_score_model = InsProductScoreAtt.objects.using(proDb).filter(product_id=item).order_by('-score_other').first()
        if att_score_model:
            score_other += att_score_model.score_other
            # 保费的评分相加
            score_other_pre += get_premium_score(item, Constant.ATTACH_TYPE, category_type,
                                                 Constant.GUARANTEE_PATTERN_REGULAR)
    # 返回相关信息
    return {
        "att_ids": att_ids,
        "score_other": score_other,
        "score_other_pre": score_other_pre
    }


# 更新组合产品的相关信息
def set_composite_info(main_id, category_type, user_id):
    # 查询推荐或绑定的产品
    bind_pro_comp = InsProductBind.objects.using(proDb).filter(main_id=main_id, status=Constant.STATUS_ACTIVE).order_by('bind_type').exclude(
        bind_type=Constant.BIND_TYPE_NOT_MUST).all()
    # 有的情况下操作
    if category_type and bind_pro_comp:
        # 取得组合产品相关信息
        comp_info_must = get_composite_info(main_id, category_type)
        # 组合产品分值取得
        score_total = float(comp_info_must['score_other']) + comp_info_must['score_other_pre']
        pro_nums = len(comp_info_must['att_ids'])
        if pro_nums > 1:
            comp_pro_id = ','.join(comp_info_must['att_ids'])
        else:
            comp_pro_id = comp_info_must['att_ids'][0]

        try:
            comp_pro_must = InsCompositePro.objects.using(proDb).get(main_id=main_id)
            # 历史记录
            return_comp_upd_must_his = fun_ins_composite_pro_his(
                comp_pro_must, Constant.ACTION_UPDATE, user_id)
            if return_comp_upd_must_his == Constant.FAIL:
                return Constant.FAIL
            # 更新值设定
            comp_pro_must.comp_pro_id = comp_pro_id
            comp_pro_must.pro_nums = pro_nums
            comp_pro_must.score = score_total
            comp_pro_must.category_type = category_type
            comp_pro_must.status = Constant.STATUS_ACTIVE

            comp_pro_must.save(using=proDb)
        except InsCompositePro.DoesNotExist:
            # 数据保存处理
            comp_pro_ins = InsCompositePro(
                main_id=main_id,
                comp_pro_id=comp_pro_id,
                pro_nums=pro_nums,
                score=score_total,
                category_type=category_type,
                status=Constant.STATUS_ACTIVE,
                created_by=user_id
            )
            comp_pro_ins.save(using=proDb)

        return Constant.SUCCESS
    else:
        # 不是组合产品时，删除组合产品
        try:
            # 如果是必绑或推荐，由组合产品改为非组合时，状态调整为无效
            comp_pro_delete = InsCompositePro.objects.using(proDb).get(main_id=main_id)
            # 历史记录添加
            return_comp_pro_delete_his = fun_ins_composite_pro_his(
                comp_pro_delete, Constant.ACTION_DELETE, user_id)
            if return_comp_pro_delete_his == Constant.FAIL:
                return Constant.FAIL
            # 状态设置为无效
            comp_pro_delete.status = Constant.STATUS_UNACTIVE
            comp_pro_delete.save(using=proDb)
        except InsCompositePro.DoesNotExist:
            pass


# 获取公众号的access_token
def get_access_token(app_id, app_secret):
    try:
        access_token_list = WxAccessToken.objects.get(appid=app_id)
        update_access_token = get_str_time(str(access_token_list.last_time)) - access_token_list.expires_in
        update_token = False
        # access_token过期时，刷新
        if update_access_token > 0:
            update_token = True
        else:
            # 验证access_token是否有效
            check_token = get_server_ip(access_token_list.access_token)
            if not check_token:
                update_token = True

        # 更新token
        if update_token:
            payload_access_token = {
                'grant_type': 'client_credential',
                'appid': app_id,
                'secret': app_secret
            }
            token_url = 'https://api.weixin.qq.com/cgi-bin/token'
            r = requests.get(token_url, params=payload_access_token)
            dict_result = (r.json())
            # 请求成功时，更新
            if 'access_token' in dict_result and dict_result['access_token']:
                access_token_list.access_token = dict_result['access_token']
                access_token_list.save()
            else:
                # 获取失败时，返回错误信息
                Logger('access_token.log', level='error').logger.error(dict_result['errmsg'])
                return {'code': Constant.REQUEST_FAIL_CODE, 'errmsg': dict_result['errmsg']}
        # 返回access_token
        return {'code': Constant.REQUEST_SUCCESS_CODE, 'access_token': access_token_list.access_token}

    except WxAccessToken.DoesNotExist:
        Logger('access_token.log', level='error').logger.error('access_token表公众号记录缺失')
        return {'code': Constant.REQUEST_FAIL_CODE, 'errmsg': 'access_token表公众号记录缺失'}


# 获取微信服务器IP
def get_server_ip(access_token):
    payload_access_token = {
        'access_token': access_token
    }
    ip_url = 'https://api.weixin.qq.com/cgi-bin/get_api_domain_ip'
    r = requests.get(ip_url, params=payload_access_token)
    dict_result = (r.json())
    # access_token失效时
    if 'errcode' in dict_result and dict_result['errcode'] == '40001':
        Logger('access_token.log', level='error').logger.error(dict_result['errmsg'])
        return False
    else:
        # 获取失败时，返回错误信息
        Logger('access_token.log', level='error').logger.error(dict_result['errmsg'])
        return True


# 上传图文消息内的图片获取URL
def upload_img_wechat(path):
    # 获得access_token
    access_token_result = get_access_token(settings.APP_ID, settings.APP_SECRET)
    if access_token_result['code'] == Constant.REQUEST_SUCCESS_CODE:
        access_token = access_token_result['access_token']
        img_url = 'https://api.weixin.qq.com/cgi-bin/media/uploadimg'
        payload_img = {
            'access_token': access_token
        }
        data = {'media': open(path, 'rb')}
        r = requests.post(url=img_url, params=payload_img, files=data)
        result = r.json()
        if 'url' in result:
            return {'code': Constant.REQUEST_SUCCESS_CODE, 'url': result['url']}
        else:
            Logger('upload_img_wechat.log', level='error').logger.error(result)
            return {'code': Constant.REQUEST_FAIL_CODE, 'errmsg': result['errmsg']}
    else:
        return access_token_result


# 上传永久消息素材
def upload_img_text_wechat(article):
    # 获得access_token
    access_token_result = get_access_token(settings.APP_ID, settings.APP_SECRET)
    if access_token_result['code'] == Constant.REQUEST_SUCCESS_CODE:
        access_token = access_token_result['access_token']
        img_url = 'https://api.weixin.qq.com/cgi-bin/material/add_news'
        payload_img_text = {
            'access_token': access_token
        }
        content_source_url = settings.ARTICLE_SHARE_URL + str(article.id)
        articles = [
            {
                "thumb_media_id": article.thumbnail_media_id,
                "author": "正心之作",
                "title": article.title,
                "content_source_url": content_source_url,
                "content": article.content_wechat,
                "digest": article.introduction,
                "show_cover_pic": 0,
                "need_open_comment": 0,
                "only_fans_can_comment": 0
            }
        ]
        data = {'articles': articles}
        r = requests.post(url=img_url, params=payload_img_text, data=json.dumps(data, ensure_ascii=False).encode('utf-8'))
        result = r.json()
        if 'media_id' in result:
            return {'code': Constant.REQUEST_SUCCESS_CODE, 'result': result}
        else:
            Logger('upload_img_text_wechat.log', level='error').logger.error(result)
            return {'code': Constant.REQUEST_FAIL_CODE, 'errmsg': result['errmsg']}
    else:
        return access_token_result


# 上传图文消息素材
def upload_img_text_message_wechat(article):
    # 获得access_token
    access_token_result = get_access_token(settings.APP_ID, settings.APP_SECRET)
    if access_token_result['code'] == Constant.REQUEST_SUCCESS_CODE:
        access_token = access_token_result['access_token']
        img_url = 'https://api.weixin.qq.com/cgi-bin/media/uploadnews'
        payload_img_text = {
            'access_token': access_token
        }
        content_source_url = settings.ARTICLE_SHARE_URL + str(article.id)
        articles = [
            {
                "thumb_media_id": article.thumbnail_media_id,
                "author": "正心之作",
                "title": article.title,
                "content_source_url": content_source_url,
                "content": article.content_wechat,
                "digest": article.introduction,
                "show_cover_pic": 0,
                "need_open_comment": 0,
                "only_fans_can_comment": 0
            }
        ]
        data = {'articles': articles}
        r = requests.post(url=img_url, params=payload_img_text, data=json.dumps(data, ensure_ascii=False).encode('utf-8'))
        result = r.json()
        if 'media_id' in result:
            return {'code': Constant.REQUEST_SUCCESS_CODE, 'result': result}
        else:
            Logger('upload_img_text_message_wechat.log', level='error').logger.error(result)
            return {'code': Constant.REQUEST_FAIL_CODE, 'errmsg': result['errmsg']}
    else:
        return access_token_result


# 修改图文素材
def modify_img_text_wechat(article):
    # 获得access_token
    access_token_result = get_access_token(settings.APP_ID, settings.APP_SECRET)
    if access_token_result['code'] == Constant.REQUEST_SUCCESS_CODE:
        access_token = access_token_result['access_token']
        img_url = 'https://api.weixin.qq.com/cgi-bin/material/update_news'
        payload_img_text = {
            'access_token': access_token
        }
        content_source_url = settings.ARTICLE_SHARE_URL + str(article.id)
        articles = {
                "title": article.title,
                "thumb_media_id": article.thumbnail_media_id,
                "author": "正心之作",
                "digest": article.introduction,
                "show_cover_pic": 0,
                "content": article.content_wechat,
                "content_source_url": content_source_url
            }

        data = {'media_id': article.media_id, 'index': 0, 'articles': articles}
        r = requests.post(url=img_url, params=payload_img_text, data=json.dumps(data, ensure_ascii=False).encode('utf-8'))
        result = r.json()
        if result['errcode'] == 0:
            return {'code': Constant.REQUEST_SUCCESS_CODE, 'result': result}
        else:
            Logger('upload_img_wechat.log', level='error').logger.error(result['errmsg'])
            return {'code': Constant.REQUEST_FAIL_CODE, 'errmsg': result['errmsg']}
    else:
        return access_token_result


# 获取用户列表接口
def get_wechat_user():
    # 获得access_token
    access_token_result = get_access_token(settings.APP_ID, settings.APP_SECRET)
    if access_token_result['code'] == Constant.REQUEST_SUCCESS_CODE:
        access_token = access_token_result['access_token']
        img_url = 'https://api.weixin.qq.com/cgi-bin/user/get'
        payload_img_text = {
            'access_token': access_token
        }

        r = requests.get(url=img_url, params=payload_img_text)
        result = r.json()
        if 'data' in result:
            return {'code': Constant.REQUEST_SUCCESS_CODE, 'result': result}
        else:
            Logger('get_wechat_user.log', level='error').logger.error(result['errmsg'])
            return {'code': Constant.REQUEST_FAIL_CODE, 'errmsg': result['errmsg']}
    else:
        return access_token_result


# 群发接口
def mass_message(openid_list, media_id):
    # 获得access_token
    access_token_result = get_access_token(settings.APP_ID, settings.APP_SECRET)
    if access_token_result['code'] == Constant.REQUEST_SUCCESS_CODE:
        access_token = access_token_result['access_token']
        img_url = 'https://api.weixin.qq.com/cgi-bin/message/mass/send'
        payload_img_text = {
            'access_token': access_token
        }

        data = {
               "touser": openid_list,
               "mpnews": {
                  "media_id": media_id
               },
                "msgtype": "mpnews",
                "send_ignore_reprint": 0
            }
        r = requests.post(url=img_url, params=payload_img_text, data=json.dumps(data))
        result = r.json()
        if result['errcode'] == 0:
            return {'code': Constant.REQUEST_SUCCESS_CODE, 'result': result}
        else:
            Logger('mass_message.log', level='error').logger.error(result['errmsg'])
            return {'code': Constant.REQUEST_FAIL_CODE, 'errmsg': result['errmsg']}
    else:
        return access_token_result


# 根据标签群发接口
def mass_message_tag(media_id):
    # 获得access_token
    access_token_result = get_access_token(settings.APP_ID, settings.APP_SECRET)
    if access_token_result['code'] == Constant.REQUEST_SUCCESS_CODE:
        access_token = access_token_result['access_token']
        img_url = 'https://api.weixin.qq.com/cgi-bin/message/mass/sendall'
        payload_img_text = {
            'access_token': access_token
        }

        data = {
                   "filter": {
                      "is_to_all": True
                   },
                   "mpnews": {
                      "media_id": media_id
                   },
                    "msgtype": "mpnews",
                    "send_ignore_reprint": 0
                }
        Logger('mass_message_tag.log', level='error').logger.error(data)
        r = requests.post(url=img_url, params=payload_img_text, data=json.dumps(data))
        result = r.json()
        if result['errcode'] == 0:
            return {'code': Constant.REQUEST_SUCCESS_CODE, 'result': result}
        else:
            Logger('mass_message_tag.log', level='error').logger.error(result['errmsg'])
            return {'code': Constant.REQUEST_FAIL_CODE, 'errmsg': result['errmsg']}
    else:
        return access_token_result


# 发送验证码
def send_code_wechat(open_id, username, code):
    # 获得access_token
    access_token_result = get_access_token(settings.APP_ID, settings.APP_SECRET)
    if access_token_result['code'] == Constant.REQUEST_SUCCESS_CODE:
        access_token = access_token_result['access_token']
        template_id = "x8YHYtmMpFtpoMlsjoE_VIDZgk3BDKXiJm2B0wuxjEg"
        send_url = 'https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=' + access_token
        data = {
               "touser": open_id,
               "template_id": template_id,
               "data": {
                       "first": {
                           "value": "您好，" + username + '，本次需要进行验证码验证，请勿泄露',
                           "color": "#173177"
                       },
                       "keyword1": {
                           "value": '公众号文章发布',
                           "color": "#173177"
                       },
                       "keyword2": {
                           "value": code,
                           "color": "#173177"
                       },
                       "keyword3": {
                           "value": "5分钟内有效",
                           "color": "#173177"
                       },
                       "remark": {
                           "value": "若非本人操作，可能您的帐号存在安全风险，请及时修改密码。",
                           "color": "#173177"
                       }
               }
           }
        r = requests.post(send_url, data=json.dumps(data))
        result = r.json()
        if result['errcode'] == 0:
            return {'code': Constant.REQUEST_SUCCESS_CODE, 'result': result}
        else:
            Logger('upload_img_wechat.log', level='error').logger.error(result['errmsg'])
            return {'code': Constant.REQUEST_FAIL_CODE, 'errmsg': result['errmsg']}
    else:
        return access_token_result


# 获得图片列表
def get_img_media_list():
    # 获得access_token
    access_token_result = get_access_token(settings.APP_ID, settings.APP_SECRET)
    if access_token_result['code'] == Constant.REQUEST_SUCCESS_CODE:
        access_token = access_token_result['access_token']
        img_url = 'https://api.weixin.qq.com/cgi-bin/material/batchget_material'
        payload_img = {
            'access_token': access_token
        }
        data = {
                    "type": 'image',
                    "offset": 0,
                    "count": 20
                }
        r = requests.post(url=img_url, params=payload_img, data=json.dumps(data))
        result = r.json()
        Logger('get_img_media_list.log', level='error').logger.error(result)
        if 'item' in result:
            return {'code': Constant.REQUEST_SUCCESS_CODE, 'result': result}
        else:
            Logger('get_img_media_list.log', level='error').logger.error(result)
            return {'code': Constant.REQUEST_FAIL_CODE, 'errmsg': result['errmsg']}
    else:
        return access_token_result


# 查询群发状态
def get_mess_status(msg_id):
    # 获得access_token
    access_token_result = get_access_token(settings.APP_ID, settings.APP_SECRET)
    if access_token_result['code'] == Constant.REQUEST_SUCCESS_CODE:
        access_token = access_token_result['access_token']
        img_url = 'https://api.weixin.qq.com/cgi-bin/message/mass/get'
        payload_img_text = {
            'access_token': access_token
        }

        data = {
            "msg_id":  msg_id
        }
        r = requests.post(url=img_url, params=payload_img_text, data=json.dumps(data))
        result = r.json()
        if result['errcode'] == 0:
            return {'code': Constant.REQUEST_SUCCESS_CODE, 'result': result}
        else:
            Logger('get_mess_status.log', level='error').logger.error(result['errmsg'])
            return {'code': Constant.REQUEST_FAIL_CODE, 'errmsg': result['errmsg']}
    else:
        return access_token_result


# 微信网络调查
def wechat_callback_check():
    # 获得access_token
    access_token_result = get_access_token(settings.APP_ID, settings.APP_SECRET)
    if access_token_result['code'] == Constant.REQUEST_SUCCESS_CODE:
        access_token = access_token_result['access_token']
        img_url = 'https://api.weixin.qq.com/cgi-bin/callback/check'
        payload_img_text = {
            'access_token': access_token
        }

        data = {
                    "action": "all",
                    "check_operator": "DEFAULT"
                }
        r = requests.post(url=img_url, params=payload_img_text, data=json.dumps(data))
        result = r.json()
        Logger('get_mess_status.log', level='error').logger.error(result)
        return {'code': Constant.REQUEST_SUCCESS_CODE, 'result': result}
    else:
        return access_token_result


# 取到城市列表
def get_city_dict():
    result = dict_fetchall(BscCity.objects.using(socialDb).values_list('id', 'name'))
    return result


# 每个险种对应的保障利益
def get_ensure_des_all():
    result = {}
    insurance_type = InsTypeDs.objects.using(proDb).filter(is_active=Constant.STATUS_ACTIVE).values('id', 'description')

    for item in insurance_type:
        insurance = InsTypeInsurance.objects.using(proDb).values('id', 'description').filter(category_type=item['id'], is_active=Constant.STATUS_ACTIVE)
        for item_i in insurance:
            return_id = int(str(item['id']) + str(item_i['id']))
            result[return_id] = item_i['description']
    return result


# 保障利益类型
def get_insurance_dict():
    result = []
    insurance_type = InsTypeDs.objects.using(proDb).filter(is_active=Constant.STATUS_ACTIVE).values('id', 'description')

    for item in insurance_type:
        insurance = InsTypeInsurance.objects.using(proDb).values('id', 'description').filter(category_type=item['id'], is_active=Constant.STATUS_ACTIVE)
        v_ins = []
        for item_i in insurance:
            v_ins.append({
                'id': item_i['id'],
                'description': item_i['description']
            })
        result.append({
            'id': item['id'],
            'description': item['description'],
            'ensure': v_ins
        })

    return result


# 保障利益类型
def get_en_dict(category_type):
    result = []
    insurance = InsTypeInsurance.objects.using(proDb).values('id', 'description').filter(category_type=category_type,is_active=Constant.STATUS_ACTIVE)

    for item_i in insurance:
        result.append({
            'id': item_i['id'],
            'description': item_i['description']
        })

    return result



