from insuranceManagement.models import InsProductHistory,InsProductRatioHistory,\
        InsProRatioConAttHistory, InsProSchAttHistory, InsProAttHistory, InsProductScheduleHistory, \
        InsProRatioAttHistory, InsProductRatioConditionHistory
from insuranceManagement.models_dedu_comp import InsProSchCompHis, InsProSchCompAttHis, InsProSchDedHis,\
    InsProSchDedAttHis
from insuranceManagement.models_composite import InsProductBindHistory, InsCompositeProHis, InsScheduleBindHistory, \
    InsAmountBindHis
from insuranceManagement.models_score import InsProductScoreHis, InsProductScoreAttHis
from customerManagement.models import CustomerHistory
from cityManagement.models import CityMedTypeHistory, CityMedPremiumHistory, MedInsuranceTypeHis, CareerSocialMatchHis
from uploadArticle.models import ArticleHis
from static.common.constant import Constant
from django.db import transaction, IntegrityError
import datetime

from geeCMS.settings import socialDb
from geeCMS.settings import proDb


# ins_product_score_his数据插入
def fun_ins_product_score_his(model, pro_type, action, user_id):
    try:
        if int(pro_type) == Constant.MAIN_TYPE:
            his_manage = InsProductScoreHis
        else:
            his_manage = InsProductScoreAttHis
        model_his = his_manage(
            fid=model.id,
            product_id=model.product_id,
            guarantee_pattern=model.guarantee_pattern,
            ensure_profit=model.ensure_profit,
            score=model.score,
            created_time=model.created_time,
            created_by=model.created_by,
            updated_time=model.updated_time,
            updated_by=model.updated_by,
            action=action,
            his_created_by=user_id
        )
        model_his.save(using=proDb)
        return Constant.SUCCESS
    except IntegrityError:
        return Constant.FAIL


# ins_product_his数据插入
def fun_ins_product_his(model,pro_type, action, user_id):
    try:
        if pro_type == Constant.MAIN_TYPE:
            his_manage = InsProductHistory
        else:
            his_manage = InsProAttHistory
        model_his = his_manage(
            fid=model.id,
            name=model.name,
            category=model.category,
            category_type=model.category_type,
            payment_method=model.payment_method,
            created_at=model.created_at,
            updated_at=model.updated_at,
            created_by=model.created_by,
            updated_by=model.updated_by,
            info=model.info,
            insurer=model.insurer,
            has_schedule=model.has_schedule,
            score=model.score,
            policy_pattern=model.policy_pattern,
            guarantee_pattern=model.guarantee_pattern,
            is_active=model.is_active,
            file_url=model.file_url,
            notice_file_url=model.notice_file_url,
            clause_file_url=model.clause_file_url,
            leaflets_file_url=model.leaflets_file_url,
            start_time=model.start_time,
            end_time=model.end_time,
            covered_area=model.covered_area,
            payment_period=model.payment_period,
            age_insured_start=model.age_insured_start,
            age_insured_end=model.age_insured_end,
            no_examination_amount=model.no_examination_amount,
            min_premium=model.min_premium,
            is_composite=model.is_composite,
            category_type_comp=model.category_type_comp,
            category_comp=model.category_comp,
            action=action,
            his_created_by=user_id
        )
        model_his.save(using=proDb)
        return Constant.SUCCESS
    except IntegrityError:
        return Constant.FAIL


# ins_product_schedule_his
def fun_ins_product_schedule_his(model, pro_type, action, user_id):
    try:
        if pro_type == Constant.MAIN_TYPE:
            his_manage = InsProductScheduleHistory
        else:
            his_manage = InsProSchAttHistory

        model_his = his_manage(
                    fid=model.id,
                    ins_product_id=model.ins_product_id,
                    status=model.status,
                    amount_type=model.amount_type,
                    amount_max=model.amount_max,
                    amount_min=model.amount_min,
                    name=model.name,
                    is_default=model.is_default,
                    has_deductible=model.has_deductible,
                    has_compensation_ratio=model.has_compensation_ratio,
                    action=action,
                    his_created_by=int(user_id)
                )
        model_his.save(using=proDb)
        return Constant.SUCCESS
    except IntegrityError:
        return Constant.FAIL


# ins_product_ratio_his数据插入
def fun_ins_product_ratio_his(model, pro_type, action, user_id):
    try:
        if int(pro_type) == Constant.MAIN_TYPE:
            his_manage = InsProductRatioHistory
        else:
            his_manage = InsProRatioAttHistory
        model_his = his_manage(
                            fid=model.id,
                            ins_product_schedule_id=model.ins_product_schedule_id,
                            ins_product_id=model.ins_product_id,
                            ratio=model.ratio,
                            gender=model.gender,
                            has_social_security=model.has_social_security,
                            age=model.age,
                            career_type=model.career_type,
                            guarantee_period=model.guarantee_period,
                            payment_period=model.payment_period,
                            status=model.status,
                            action=action,
                            his_created_by=user_id
                        )
        model_his.save(using=proDb)
        return Constant.SUCCESS
    except IntegrityError:
        return Constant.FAIL


# ins_product_ratio_condition_his数据插入
def fun_ins_product_ratio_condition_his(model, pro_type, action, user_id):
    try:
        if int(pro_type) == Constant.MAIN_TYPE:
            his_manage = InsProductRatioConditionHistory
        else:
            his_manage = InsProRatioConAttHistory
        model_his = his_manage(
            fid=model.id,
            ins_product_id=model.ins_product_id,
            ins_product_schedule_id=model.ins_product_schedule_id,
            gender=model.gender,
            social_guarantee=model.social_guarantee,
            age_min=model.age_min,
            age_max=model.age_max,
            career_type=model.career_type,
            payment_period=model.payment_period,
            guarantee_period=model.guarantee_period,
            status=model.status,
            action=action,
            his_created_by=user_id
        )
        model_his.save(using=proDb)
        return Constant.SUCCESS
    except IntegrityError:
        return Constant.FAIL


# ins_product_schedule_deductible_his数据插入
def fun_ins_pro_sch_deductible_his(model, pro_type, action, user_id):
    try:
        if int(pro_type) == Constant.MAIN_TYPE:
            his_manage = InsProSchDedHis
        else:
            his_manage = InsProSchDedAttHis
        model_his = his_manage(
            fid=model.id,
            ins_product_id=model.ins_product_id,
            ins_product_schedule_id=model.ins_product_schedule_id,
            name=model.name,
            ratio=model.ratio,
            status=model.status,
            action=action,
            his_created_by=user_id
        )
        model_his.save(using=proDb)
        return Constant.SUCCESS
    except IntegrityError:
        return Constant.FAIL


# ins_product_schedule_comp_ratio_his数据插入
def fun_ins_pro_sch_comp_ratio_his(model, pro_type, action, user_id):
    try:
        if int(pro_type) == Constant.MAIN_TYPE:
            his_manage = InsProSchCompHis
        else:
            his_manage = InsProSchCompAttHis
        model_his = his_manage(
            fid=model.id,
            ins_product_id=model.ins_product_id,
            ins_product_schedule_id=model.ins_product_schedule_id,
            name=model.name,
            ratio=model.ratio,
            status=model.status,
            action=action,
            his_created_by=user_id
        )
        model_his.save(using=proDb)
        return Constant.SUCCESS
    except IntegrityError:
        return Constant.FAIL


# customer_his
def fun_customer_his(model, action, user_id):
    try:
        model_his = CustomerHistory(
                    fid=model.id,
                    name=model.name,
                    email=model.email,
                    customer_num=model.customer_num,
                    start_date=model.start_date,
                    end_date=model.end_date,
                    license_id=model.license_id,
                    created_by=model.created_by,
                    updated_by=model.updated_by,
                    created_at=model.created_at,
                    updated_at=model.updated_at,
                    status=model.status,
                    user_id=model.user_id,
                    word_download_status=model.word_download_status,
                    is_syn=model.is_syn,
                    action=action,
                    his_created_by=int(user_id)
                )
        model_his.save()
        return Constant.SUCCESS
    except IntegrityError:
        return Constant.FAIL


# city_med_type_his
def fun_city_med_type_his(model, action, user_id):
    try:
        model_his = CityMedTypeHistory(
                    fid=model.id,
                    bsc_city_id=model.bsc_city_id,
                    med_insurance_type_id=model.med_insurance_type_id,
                    age_start=model.age_start,
                    age_end=model.age_end,
                    menzhenbaoxiao=model.menzhenbaoxiao,
                    zhuyuanbaoxiao=model.zhuyuanbaoxiao,
                    menzhen_pay_standard=model.menzhen_pay_standard,
                    menzhen_standard_unit=model.menzhen_standard_unit,
                    zhuyuan_pay_standard=model.zhuyuan_pay_standard,
                    zhuyuan_standard_unit=model.zhuyuan_standard_unit,
                    yibao_info=model.yibao_info,
                    wx_yibao_info=model.wx_yibao_info,
                    created_by_id=model.created_by_id,
                    updated_by_id=model.updated_by_id,
                    created_at=model.created_at,
                    updated_at=model.updated_at,
                    action=action,
                    his_created_by=int(user_id)
                )
        model_his.save(using=socialDb)
        return Constant.SUCCESS
    except IntegrityError:
        return Constant.FAIL


# city_med_premium_his
def fun_city_med_premium_his(model, action, user_id):
    try:
        model_his = CityMedPremiumHistory(
                        fid=model.id,
                        bsc_city_id=model.bsc_city_id,
                        med_insurance_type_id=model.med_insurance_type_id,
                        age_start=model.age_start,
                        age_end=model.age_end,
                        premium=model.premium,
                        created_by_id=model.created_by_id,
                        updated_by_id=model.updated_by_id,
                        created_at=model.created_at,
                        updated_at=model.updated_at,
                        action=action,
                        his_created_by=int(user_id)
                    )
        model_his.save(using=socialDb)
        return Constant.SUCCESS
    except IntegrityError:
        return Constant.FAIL


# ins_product_bind_his
def fun_ins_product_bind_his(model, action, user_id):
    try:
        model_his = InsProductBindHistory(
                        fid=model.id,
                        main_id=model.main_id,
                        attach_id=model.attach_id,
                        bind_type=model.bind_type,
                        relation_type=model.relation_type,
                        is_relation_payment=model.is_relation_payment,
                        is_relation_gua=model.is_relation_gua,
                        is_relation_social=model.is_relation_social,
                        is_relation_career=model.is_relation_career,
                        status=model.status,
                        create_time=model.create_time,
                        update_time=model.update_time,
                        created_by=model.created_by,
                        updated_by=model.updated_by,
                        action=action,
                        his_created_by=int(user_id)
                    )
        model_his.save(using=proDb)
        return Constant.SUCCESS
    except IntegrityError:
        return Constant.FAIL


# ins_product_bind_his
def fun_ins_schedule_bind_his(model, action, user_id):
    try:
        model_his = InsScheduleBindHistory(
                        fid=model.id,
                        main_sch_id=model.main_sch_id,
                        attach_sch_id=model.attach_sch_id,
                        chain_type=model.chain_type,
                        is_modifiable=model.is_modifiable,
                        is_relation=model.is_relation,
                        ratio=model.ratio,
                        status=model.status,
                        bind_id=model.bind_id,
                        main_id=model.main_id,
                        attach_id=model.attach_id,
                        create_time=model.create_time,
                        update_time=model.update_time,
                        created_by=model.created_by,
                        updated_by=model.updated_by,
                        action=action,
                        his_created_by=int(user_id)
                    )
        model_his.save(using=proDb)
        return Constant.SUCCESS
    except IntegrityError as err:
        return Constant.FAIL


# ins_amount_bind_his
def fun_ins_amount_bind_his(model, action, user_id):
    try:
        model_his = InsAmountBindHis(
            fid=model.id,
            main_id=model.main_id,
            attach_id=model.attach_id,
            bind_id=model.bind_id,
            chain_type=model.chain_type,
            is_modifiable=model.is_modifiable,
            ratio=model.ratio,
            status=model.status,
            create_time=model.create_time,
            update_time=model.update_time,
            created_by=model.created_by,
            updated_by=model.updated_by,
            action=action,
            his_created_by=int(user_id)
        )
        model_his.save(using=proDb)
        return Constant.SUCCESS
    except IntegrityError:
        return Constant.FAIL


# ins_composite pro_his
def fun_ins_composite_pro_his(model, action, user_id):
    try:
        model_his = InsCompositeProHis(
                        fid=model.id,
                        main_id=model.main_id,
                        comp_pro_id=model.comp_pro_id,
                        pro_nums=model.pro_nums,
                        score=model.score,
                        category_type=model.category_type,
                        status=model.status,
                        create_time=model.create_time,
                        update_time=model.update_time,
                        created_by=model.created_by,
                        updated_by=model.updated_by,
                        action=action,
                        his_created_by=int(user_id)
                    )
        model_his.save(using=proDb)
        return Constant.SUCCESS
    except IntegrityError as err:
        return Constant.FAIL


# career_social_match_his
def fun_career_social_match_his(model, action, user_id):
    try:
        model_his = CareerSocialMatchHis(
                        fid=model.id,
                        bsc_city_id=model.bsc_city_id,
                        career_id=model.career_id,
                        med_id=model.med_id,
                        created_at=model.created_at,
                        updated_at=model.updated_at,
                        action=action,
                        his_created_by=int(user_id)
                    )
        model_his.save(using=socialDb)
        return Constant.SUCCESS
    except IntegrityError as err:
        return Constant.FAIL


# med_insurance_type_his
def fun_med_insurance_type_his(model, action, user_id):
    try:
        try:
            updated_at = model.updated_at
            datetime.datetime(updated_at)
        except ValueError:
            updated_at = None
        except TypeError:
            updated_at = None

        model_his = MedInsuranceTypeHis(
                        fid=model.id,
                        bsc_city_id=model.bsc_city_id,
                        description=model.description,
                        is_active=model.is_active,
                        created_by=model.created_by,
                        updated_by=model.updated_by,
                        created_at=model.created_at,
                        updated_at=updated_at,
                        action=action,
                        his_created_by=int(user_id)
                    )
        model_his.save(using=socialDb)
        return Constant.SUCCESS
    except IntegrityError as err:
        return Constant.FAIL


# z_article_his
def fun_z_article_his(model, action, user_id):
    try:
        model_his = ArticleHis(
                        fid=model.id,
                        media_id=model.media_id,
                        type_id=model.type_id,
                        content=model.content,
                        content_wechat=model.content_wechat,
                        introduction=model.introduction,
                        thumbnail=model.thumbnail,
                        thumbnail_media_id=model.thumbnail_media_id,
                        title=model.title,
                        browse_num=model.browse_num,
                        share_num=model.share_num,
                        message_num=model.message_num,
                        creator_id=model.creator_id,
                        status=model.status,
                        release_time=model.release_time,
                        upload_wechat_time=model.upload_wechat_time,
                        reason=model.reason,
                        createtime=model.createtime,
                        updatetime=model.updatetime,
                        updated_by=model.updated_by,
                        action=action,
                        his_created_by=int(user_id)
                    )
        model_his.save()
        return Constant.SUCCESS
    except IntegrityError as err:
        return Constant.FAIL

