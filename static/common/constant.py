# coding:utf-8


class Constant():
    # 数据查询失败的错误code
    SQL_DATA_NOT_EXIST_CODE = "err100100"
    SQL_DATA_NOT_EXIST_MSG = "数据不存在"
    # 数据执行失败的错误code
    SQL_EXEC_ERR_CODE = "err100101"
    SQL_EXEC_ERR_MSG = "SQL执行失败"
    # 验证失败的错误code
    AUTH_ERR_CODE = "err100102"
    AUTH_ERR_MSG = "没有权限"
    # 数据不存在
    ERR_NONE_EXIST_CODE = 'err100104'
    ERR_NONE_EXIST_MESSAGE = '不存在'

    # 参数错误
    ERR_PARAM_CODE = 'err100106'
    ERR_PARAM_CODE_MESSAGE = '参数错误'

    # 已存在
    ERR_EXIST_CODE = 'err100105'
    ERR_EXIST_MES = '已存在'
    # 成功
    REQUEST_SUCCESS_CODE = "s100200"
    REQUEST_SUCCESS_MSG = "成功"
    # 失败
    REQUEST_FAIL_CODE = 'err100200'
    REQUEST_FAIL_MSG = '失败'
    # 数据无效
    DATA_INVAILD_CODE = 'err100300'

    # 导入数据错误code
    KEY_ERROR_CODE = "err100301"
    VALUE_ERROR_CODE = "err100302"

    # 年龄段无效
    VALUE_AGE_ERROR_CODE = "err100301"
    VALUE_AGE_ERROR_MSG = "年龄填写重复"

    # 拼接两个值的字符
    SPLIT_JOINT_CHAR = ','

    # 数据操作成功的返回值
    SUCCESS = '200'
    SUCCESS_MESSAGE = '成功'
    # 数据操作失败的返回值
    FAIL = '400'
    FAIL_MESSAGE = '失败'
    # 数据不存在
    NONE_EXIST = '500'
    NONE_EXIST_MESSAGE = '不存在'
    #无效参数
    INVAILD = '501'
    INVAILD_MESSAGE = '无效的参数'
    #已存在
    EXIST = '502'
    EXIST_MESSAGE = '已存在'
    # 邮件发送失败
    EMAIL_SEND_FAIL = '503'
    EMAIL_SEND_FAIL_MESSAGE = '发送失败'
    #险种类型列表
    CATEGORY_TYPE = ((0, '人寿险'), (1, '意外险'), (2, '医疗险'), (3, '重疾险'))
    #险种
    CATEGORY = ((17,'住院'),(18,'手术'),(19,'生育'),(20,'重疾'),(21,'门急诊'),(22,'体检'),(23,'疫苗'),(24,'齿科'),(25,'眼科'),(26,'海外重疾'),(27,'孕中险'),(28,'意外医疗'),(29,'恶性肿瘤'))
    #消费型或储蓄型
    POLICY_PATTERN = ((1, '消费型'), (2, '储蓄型'))
    #定期型或终身型
    GUARANTEE_PATTERN = ((1, '定期型'), (2, '终身型'), (3, '终身型或定期型'))
    # 定期型或终身型--字典类型
    GUARANTEE_PATTERN_DICT = {"1": "定期型", "2": "终身型", "3": "终身型或定期型"}
    # 定期型或终身型--字典类型
    GUARANTEE_PATTERN_SCORE = {"1": "定期型", "2": "终身型"}
    # 定期型值
    GUARANTEE_PATTERN_REGULAR = 1
    GUARANTEE_PATTERN_LONG = 2
    GUARANTEE_PATTERN_ALL = 3
    #员工状态
    STATUS_CHOICE = ['禁用', '正常']
    PAYMENT_METHOD = ((1, '给付'), (2, '补偿'))
    AMOUNT_TYPE = ((1, '固定'), (2, '不固定'))
    #个人规划
    PER_PLANNING = '1'
    #家庭规划
    FAM_PLANNING = '2'
    #城市题号
    FINANCIAL_CITY = 2
    #个人收入题号
    PER_INCOME_MONTH = [18]
    PER_INCOME_YEAR = [19]
    PER_INCOME_TOTAL = [13]
    #个人支出题号
    PER_EXPENSE_MONTH = [6, 7, 8, 9, 10]
    PER_EXPENSE_YEAR = [11, 12]
    PER_EXPENSE_FOUR = [5]
    #家庭收入题号
    FAM_INCOME_MONTH = [21, 22]
    FAM_INCOME_YEAR = [23, 24]
    FAM_INCOME_TOTAL = [16, 17]
    #家庭支出题号
    FAM_EXPENSE_MONTH = [7, 8, 9, 10, 11, 12, 13]
    FAM_EXPENSE_YEAR = [14, 15]
    FAM_EXPENSE_FOUR = [7]
    #有几个孩子题目序号
    QUE_CHILD_NUM = 5
    #个人保险题号
    PER_POLICY_ANSER = [18]
    #家庭保险题号
    FAM_POLICY_ANSER = [24, 25]
    #个人医保题号
    PER_MED_INSURANCE = [14]
    #家庭医保题号
    FAM_MED_INSURANCE = [18, 19]
    #有效
    STATUS_ACTIVE = 1
    #无效
    STATUS_UNACTIVE = 0
    #停售
    STATUS_STOP_SALE = 2
    #个人险
    INS_TRADE = 1
    #保单咨询已回答
    POLICY_ANSWERED = 1
    #未回答
    NO_ANSWER = '700'
    NO_ANSWER_MESSAGE = '暂无经纪人回答'
    #已修改
    USER_INFO_MODIFIED = 1

    #数据库操作
    ACTION_DELETE = 'delete'
    ACTION_UPDATE = 'update'
    ACTION_INSERT = 'insert'
    #有效用户
    USER_STATUS_ACTIVE = 10
    USER_STATUS_INACTIVE = 0
    USER_STATUS_DELETE = 20

    # 评分计算常量值
    SCORE_RATION_K_ILL = -10/2131
    SCORE_RATION_B_ILL = 110125/2131
    SCORE_RATION_K_ACC = -5/138
    SCORE_RATION_B_ACC = 625/23
    SCORE_RATION_K_LIFE = -1/200
    SCORE_RATION_B_LIFE = 121/4
    SCORE_RATION_K_HOSP = -30/316
    SCORE_RATION_B_HOSP = 17250/316
    SCORE_GUARANTEE = 970
    SCORE_PAYMENT = 20
    SCORE_PAYMENT_ACC = 1
    SCORE_LONG = 9999
    SCORE_GUARANTEE_ACC = 1
    SCORE_AMOUNT = 500000
    # 保费计算类型
    PREMIUM_FIXED = 1
    PREMIUM_UNFIXED = 2

    # 保障区域列表
    CONVERED_AREA_LIST = ((1, '全球 '), (2, '全球除美'), (3, '亚太'), (4, '大中华'), (5, '国内'))
    # 不限
    EXPORT_VALUE_UNLIMITED = '不限'
    # 社保的行号
    SOCIAL_ROW_NO = 0
    # 职业等级的行号
    CAREER_ROW_NO = 1
    # 性别的行号
    SEX_ROW_NO = 2
    # 保障期限的行号
    GUARANTEE_ROW_NO = 3
    # 缴费期限的行号
    PAYMENT_ROW_NO = 4
    # 年龄的列号
    AGE_COLUMN_NO = 0
    # 社保列表
    SOCIAL_DICT = {'1': '有', '0': '无'}
    # 职业等级列表
    CAREER_DICT = {'1': '1类', '2': '2类', '3': '3类', '4': '4类', '5': '5类', '6': '6类'}
    # 性别列表
    SEX_DICT = {"1": "男性", "2": "女性"}
    # 保障期限列表
    GUARANTEE_PERIOD_DICT = {'1': '1年', '10': '10年', '20': '20年', '25': '25年', '30': '30年', '960': '保至60岁', '966': '保至66岁', '965': '保至65岁', '970': '保至70岁','977': '保至77岁', '980': '保至80岁', '988': '保至88岁', '9999': '终身'}
    # 缴费年限列表
    PAYMENT_PERIOD_DICT = {'1': '1年', '3': '3年', '5': '5年', '10': '10年', '15': '15年', '20': '20年', '30': '30年', '960': '至60岁', '965': '至65岁', '970': '至70岁'}
    # condition 表格
    CON_DATA_SET = {"1": {'social': set(), 'age': set(), 'career': set(), 'payment': set(), 'guarantee': set()},
                    "2": {'social': set(), 'age': set(), 'career': set(), 'payment': set(), 'guarantee': set()}}

    # 有效长
    EFFECTIVE_TIME_LENGTH = 1*24*60*60

    # 绑定的状态
    BIND_DICT = {"1": "必绑", "2": "推荐绑定", "3": "可绑定"}
    BIND_TYPE_MUST = 1
    BIND_TYPE_RECOMMEND = 2
    BIND_TYPE_NOT_MUST = 3

    # 主险产品type
    MAIN_TYPE = 1
    # 附加险type
    ATTACH_TYPE = 2

    CAPTCHA_NUM_LONG = 30

    # 绑定关联类型
    RELATION_TYPE_NONE = 3
    RELATION_TYPE_SCH = 1
    RELATION_TYPE_AMOUNT = 2

    # 保额关联类型
    INSURED_AMOUNT_TYPE_NONE = 3
    INSURED_AMOUNT_TYPE_VAL = 1
    INSURED_AMOUNT_TYPE_UPPER_LIMIT = 2

    # 和主险的保额是否有关系
    RELATION_YES = 1
    RELATION_NONE = 2

    # 保额是否可以修改
    AMOUNT_MODIFY_YES = 1
    AMOUNT_MODIFY_NONE = 2

    # 日志文件名称
    LOG_FILENAME_INS_PRO = "ins_pro_error.log"
    LOG_FILENAME_INS_PRO_SCORE = "ins_pro_score_error.log"

    # 文章状态，创作中
    ARTICLE_STATUS_CREATING = 2
    # 审核中
    ARTICLE_STATUS_IN_REVIEW = 3
    # 发布
    ARTICLE_STATUS_RELEASE = 1

    # 文章验证码长度
    ARTICLE_CODE_LENGTH = 8
    # 文章验证码有效期
    ARTICLE_CODE_VALID_TIME = 300
